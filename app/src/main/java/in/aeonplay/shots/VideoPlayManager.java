package in.aeonplay.shots;

import android.content.Context;

import androidx.annotation.OptIn;
import androidx.media3.common.C;
import androidx.media3.common.MediaItem;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.Player;
import androidx.media3.common.util.UnstableApi;
import androidx.media3.common.util.Util;
import androidx.media3.datasource.DataSource;
import androidx.media3.datasource.DefaultDataSourceFactory;
import androidx.media3.exoplayer.DefaultRenderersFactory;
import androidx.media3.exoplayer.ExoPlayer;
import androidx.media3.exoplayer.RenderersFactory;
import androidx.media3.exoplayer.drm.DefaultDrmSessionManagerProvider;
import androidx.media3.exoplayer.ima.ImaServerSideAdInsertionMediaSource;
import androidx.media3.exoplayer.source.DefaultMediaSourceFactory;
import androidx.media3.exoplayer.source.MediaSource;
import androidx.media3.exoplayer.trackselection.DefaultTrackSelector;
import androidx.media3.exoplayer.trackselection.TrackSelector;

import java.io.PrintWriter;
import java.io.StringWriter;

import in.aeonplay.BuildConfig;
import in.aeonplay.R;

@OptIn(markerClass = UnstableApi.class)
public class VideoPlayManager {
    private volatile static VideoPlayManager mInstance = null;
    private Context mContext;
    private ExoPlayer mSimpleExoPlayer;
    private VideoPlayTask mCurVideoPlayTask;
    private ImaServerSideAdInsertionMediaSource.AdsLoader serverSideAdsLoader;
    private ImaServerSideAdInsertionMediaSource.AdsLoader.State serverSideAdsLoaderState;

    public static VideoPlayManager getInstance(Context context) {
        if (mInstance == null) {
            synchronized (VideoPlayManager.class) {
                if (mInstance == null) {
                    mInstance = new VideoPlayManager(context);
                }
            }
        }
        return mInstance;
    }

    public VideoPlayManager(Context context) {
        this.mContext = context;
    }

    public static boolean useExtensionRenderers() {
        return BuildConfig.DEBUG;
    }

    public static RenderersFactory buildRenderersFactory(
            Context context, boolean preferExtensionRenderer) {
        @DefaultRenderersFactory.ExtensionRendererMode
        int extensionRendererMode =
                useExtensionRenderers()
                        ? (preferExtensionRenderer
                        ? DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_ON)
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF;
        return new DefaultRenderersFactory(context.getApplicationContext())
                .setExtensionRendererMode(extensionRendererMode);
    }

    private MediaSource.Factory createMediaSourceFactory() {
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(
                mContext, Util.getUserAgent(mContext, mContext.getString(R.string.app_name)));

        DefaultDrmSessionManagerProvider drmSessionManagerProvider =
                new DefaultDrmSessionManagerProvider();
        drmSessionManagerProvider.setDrmHttpDataSourceFactory(
                DemoUtil.getHttpDataSourceFactory(/* context= */ mContext));
        ImaServerSideAdInsertionMediaSource.AdsLoader.Builder serverSideAdLoaderBuilder =
                new ImaServerSideAdInsertionMediaSource.AdsLoader.Builder(/* context= */ mContext, mCurVideoPlayTask.getSimpleExoPlayerView());
        if (serverSideAdsLoaderState != null) {
            serverSideAdLoaderBuilder.setAdsLoaderState(serverSideAdsLoaderState);
        }
        serverSideAdsLoader = serverSideAdLoaderBuilder.build();
        ImaServerSideAdInsertionMediaSource.Factory imaServerSideAdInsertionMediaSourceFactory =
                new ImaServerSideAdInsertionMediaSource.Factory(
                        serverSideAdsLoader,
                        new DefaultMediaSourceFactory(/* context= */ mContext)
                                .setDataSourceFactory(dataSourceFactory));
        return new DefaultMediaSourceFactory(/* context= */ mContext)
                .setDataSourceFactory(dataSourceFactory);
    }

    public void startPlay() {
        releasePlayer();
        if (mCurVideoPlayTask == null) {
            return;
        }

        TrackSelector mTrackSelector = new DefaultTrackSelector(mContext);
        mTrackSelector.setParameters(new DefaultTrackSelector.ParametersBuilder()
                .setRendererDisabled(C.TRACK_TYPE_VIDEO, false)
                .build());

        boolean preferExtensionDecoders = false;
        RenderersFactory renderersFactory = buildRenderersFactory(/* context= */ mContext, preferExtensionDecoders);

        mSimpleExoPlayer = new ExoPlayer.Builder(mContext, renderersFactory)
                .setMediaSourceFactory(createMediaSourceFactory())
                .setTrackSelector(mTrackSelector)
                .setReleaseTimeoutMs(2000)
                .build();

        mSimpleExoPlayer.setRepeatMode(Player.REPEAT_MODE_ALL);
        mCurVideoPlayTask.getSimpleExoPlayerView().setPlayer(mSimpleExoPlayer);
        mCurVideoPlayTask.getSimpleExoPlayerView().setKeepScreenOn(true);

        mSimpleExoPlayer.addListener(new Player.Listener() {
            @Override
            public void onPlayerError(PlaybackException error) {
                Player.Listener.super.onPlayerError(error);

                StringWriter errors = new StringWriter();
                error.printStackTrace(new PrintWriter(errors));
                String errorLog = errors.toString();

            }
        });

        MediaItem media = MediaItem.fromUri(mCurVideoPlayTask.getVideoUrl());
        mSimpleExoPlayer.setMediaItem(media);

        mSimpleExoPlayer.setPlayWhenReady(true);
        mSimpleExoPlayer.getPlaybackState();
        mSimpleExoPlayer.prepare();
        mSimpleExoPlayer.play();
    }

    public boolean isPlaying() {
        if (mSimpleExoPlayer != null) {
            return mSimpleExoPlayer.isPlaying();
        }

        return false;
    }

    public void resumePlay() {
        if (mSimpleExoPlayer != null) {
            mSimpleExoPlayer.setPlayWhenReady(true);
            mSimpleExoPlayer.prepare();
            mSimpleExoPlayer.play();
        } else {
            startPlay();
        }
    }

    public void pausePlay() {
        if (mSimpleExoPlayer != null) {
            mSimpleExoPlayer.setPlayWhenReady(false);
            mSimpleExoPlayer.getPlaybackState();
        }
    }

    public void releasePlayer() {
        if (mSimpleExoPlayer != null) {
            mSimpleExoPlayer.release();
            mSimpleExoPlayer = null;
        }
    }

    /********************************************* VideoCache start ***************************************/
//    private HttpProxyCacheServer mHttpProxyCacheServer;
//
//    public HttpProxyCacheServer getProxy() {
//        if (mHttpProxyCacheServer == null) {
//            mHttpProxyCacheServer = newProxy();
//        }
//        return mHttpProxyCacheServer;
//    }
//
//    private HttpProxyCacheServer newProxy() {
//        return new HttpProxyCacheServer.Builder(mContext.getApplicationContext())
//                .maxCacheSize(512 * 1024 * 1024)
//                .maxCacheFilesCount(20)
//                .fileNameGenerator(new VideoFileNameGenerator())
//                .cacheDirectory(new File(mContext.getFilesDir() + "/videoCache/"))
//                .build();
//    }

    /********************************************* VideoCache end ***************************************/
    public VideoPlayTask getCurVideoPlayTask() {
        return mCurVideoPlayTask;
    }

    public void setCurVideoPlayTask(VideoPlayTask mCurVideoPlayTask) {
        this.mCurVideoPlayTask = mCurVideoPlayTask;
    }
}
