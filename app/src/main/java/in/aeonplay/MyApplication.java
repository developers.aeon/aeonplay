package in.aeonplay;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;

import com.google.firebase.FirebaseApp;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import in.aeonplay.comman.ExceptionHandler;
import in.aeonplay.model.TokenModel.CCTokenModel;
import in.aeonplay.model.TokenModel.ErrorModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class MyApplication extends Application {

    private static MyApplication instance;

    public static MyApplication getInstance() {
        return instance;
    }

    private String TAG = MyApplication.class.getSimpleName();

    public static boolean hasNetwork() {
        return instance.isNetworkConnected();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (instance == null) {
            instance = this;
        }

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(getApplicationContext()));
        FirebaseApp.initializeApp(getInstance().getApplicationContext());
        getOAuthTokenRequest();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    public String getOAuthTokenRequest() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        String getToken = "";
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> oauthTokenCall = apiInterface.postOAuthToken(BuildConfig.CLIENT_ID,
                BuildConfig.CLIENT_SECRET,
                getString(R.string.grant_type_cc),
                getString(R.string.scope));

        try {
            Response<ResponseBody> response = oauthTokenCall.execute();

            if (response.isSuccessful()) {
                Gson gson = new Gson();
                Reader reader = new StringReader(response.body().string());
                CCTokenModel ccTokenModel = gson.fromJson(reader, CCTokenModel.class);
                getToken = ccTokenModel.getTokenType() + " " + ccTokenModel.getAccessToken();

            } else {

                try {
                    JSONObject jsonObject = new JSONObject(response.errorBody().string());

                    Gson gson = new Gson();
                    Reader reader = new StringReader(jsonObject.toString());
                    ErrorModel errorModel = gson.fromJson(reader, ErrorModel.class);
                    getToken = errorModel.getMessage();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return getToken;
    }
}
