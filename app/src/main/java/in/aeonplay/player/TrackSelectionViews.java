package in.aeonplay.player;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Handler;
import android.text.Html;
import android.text.Spanned;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.AttrRes;
import androidx.annotation.Nullable;
import androidx.media3.common.C;
import androidx.media3.common.Format;
import androidx.media3.common.TrackGroup;
import androidx.media3.common.util.Assertions;
import androidx.media3.common.util.UnstableApi;
import androidx.media3.exoplayer.RendererCapabilities;
import androidx.media3.exoplayer.source.TrackGroupArray;
import androidx.media3.exoplayer.trackselection.DefaultTrackSelector;
import androidx.media3.exoplayer.trackselection.MappingTrackSelector;
import androidx.media3.ui.DefaultTrackNameProvider;
import androidx.media3.ui.TrackNameProvider;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.Arrays;

import in.aeonplay.R;

@UnstableApi
public class TrackSelectionViews extends LinearLayout {

    private static final String TAG = TrackSelectionViews.class.getSimpleName();
    public static Format mFormat;
    private static TrackSelectionViews selectionView;
    private static BottomSheetDialog bottomSheetDialog;
    //    private final int selectableItemBackgroundResourceId;
    private final LayoutInflater inflater;
    private final CheckedTextView disableView;
    private final CheckedTextView defaultView;
    private final ComponentListener componentListener;
    private CheckedTextView trackView;
    private boolean allowAdaptiveSelections;
    private TrackUpdateData TrackUpdateData = null;
    private TrackNameProvider trackNameProvider;
    private CheckedTextView[][] trackViews;
    private DefaultTrackSelector trackSelector;
    private int rendererIndex;
    private TrackGroupArray trackGroups;
    private boolean isDisabled;
    private @Nullable
    DefaultTrackSelector.SelectionOverride override;
    private int trackType;
    private int trackViewLayoutId;

    public TrackSelectionViews(Context context) {
        this(context, null);
    }

    public TrackSelectionViews(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    @SuppressWarnings("nullness")
    public TrackSelectionViews(
            Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
//        TypedArray attributeArray =
//                context
//                        .getTheme()
//                        .obtainStyledAttributes(new int[]{android.R.attr.selectableItemBackground});
////        selectableItemBackgroundResourceId = attributeArray.getResourceId(0, 0);
//        attributeArray.recycle();

        inflater = LayoutInflater.from(context);
        componentListener = new ComponentListener();
        trackNameProvider = new DefaultTrackNameProvider(getResources());

        // View for disabling the renderer.
        disableView =
                (CheckedTextView)
                        inflater.inflate(android.R.layout.simple_list_item_single_choice, this, false);
        disableView.setCheckMarkDrawable(R.drawable.activated_checkbox);
        disableView.setBackgroundResource(R.drawable.player_settings_selector);
        disableView.setText(getStringFormat("No quality found", context.getString(R.string.exo_track_selection_none)));
        disableView.setEnabled(false);
//        disableView.setFocusable(true);
//        disableView.setFocusableInTouchMode(true);

        disableView.setOnClickListener(componentListener);
        disableView.setVisibility(View.GONE);
        addView(disableView);
        addView(inflater.inflate(R.layout.exo_list_divider, this, false));

        defaultView =
                (CheckedTextView)
                        inflater.inflate(android.R.layout.simple_list_item_single_choice, this, false);
        defaultView.setCheckMarkDrawable(R.drawable.activated_checkbox);
        defaultView.setBackgroundResource(R.drawable.player_settings_selector);
        defaultView.setText(getStringFormat("Recommended for best quality", context.getString(R.string.exo_track_selection_auto)));
        defaultView.setEnabled(false);
//        defaultView.setFocusable(true);
//        defaultView.setFocusableInTouchMode(true);

        defaultView.setOnClickListener(componentListener);
        addView(defaultView);
    }

    public static boolean willHaveContent(DefaultTrackSelector trackSelector) {
        MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
        return mappedTrackInfo != null && willHaveContent(mappedTrackInfo);
    }

    /**
     * Returns whether a track selection dialog will have content to display if initialized with the
     * specified {@link MappingTrackSelector.MappedTrackInfo}.
     */
    public static boolean willHaveContent(MappingTrackSelector.MappedTrackInfo mappedTrackInfo) {
        for (int i = 0; i < mappedTrackInfo.getRendererCount(); i++) {
            if (showTabForRenderer(mappedTrackInfo, i)) {
                return true;
            }
        }
        return false;
    }

    private static boolean showTabForRenderer(MappingTrackSelector.MappedTrackInfo mappedTrackInfo, int rendererIndex) {
        TrackGroupArray trackGroupArray = mappedTrackInfo.getTrackGroups(rendererIndex);
        if (trackGroupArray.length == 0) {
            return false;
        }
        int trackType = mappedTrackInfo.getRendererType(rendererIndex);
        return isSupportedTrackType(trackType);
    }

    private static boolean isSupportedTrackType(int trackType) {
        switch (trackType) {
            case C.TRACK_TYPE_VIDEO:
            case C.TRACK_TYPE_AUDIO:
            case C.TRACK_TYPE_TEXT:
                return true;
            default:
                return false;
        }
    }

    public static Pair<BottomSheetDialog, TrackSelectionViews> getDialog(
            Activity activity,
            CharSequence title,
            DefaultTrackSelector trackSelector,
            int rendererIndex,
            Format format, TrackUpdateData TrackUpdateData) {

        mFormat = format;

        bottomSheetDialog = new BottomSheetDialog(activity, R.style.AppBottomSheetDialogTheme);
        LayoutInflater dialogInflater = LayoutInflater.from(activity);
        View dialogView = dialogInflater.inflate(R.layout.track_selection, null);
        bottomSheetDialog.setContentView(dialogView);

        bottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog dialogc = (BottomSheetDialog) dialog;
                dialogc.getWindow().setLayout((int) activity.getResources().getDimension(com.intuit.sdp.R.dimen._300sdp),
                        ViewGroup.LayoutParams.MATCH_PARENT);
                FrameLayout bottomSheet = dialogc.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        selectionView = dialogView.findViewById(R.id.exo_track_selection_view);
        selectionView.init(trackSelector, rendererIndex, TrackUpdateData);

//        selectionView.setTrackNameProvider(new TrackNameProvider() {
//            @Override
//            public String getTrackName(Format format) {
//
//                return format.height + "p";
//            }
//        });


        TextView filterTitle = dialogView.findViewById(R.id.filterTitle);
        filterTitle.setText(title);

        return Pair.create(bottomSheetDialog, selectionView);
    }

    private static int[] getTracksAdding(int[] tracks, int addedTrack) {
        tracks = Arrays.copyOf(tracks, tracks.length + 1);
        tracks[tracks.length - 1] = addedTrack;
        return tracks;
    }

    private static int[] getTracksRemoving(int[] tracks, int removedTrack) {
        int[] newTracks = new int[tracks.length - 1];
        int trackCount = 0;
        for (int track : tracks) {
            if (track != removedTrack) {
                newTracks[trackCount++] = track;
            }
        }
        return newTracks;
    }

    /**
     * Sets whether adaptive selections (consisting of more than one track) can be made using this
     * selection view.
     *
     * <p>For the view to enable adaptive selection it is necessary both for this feature to be
     * enabled, and for the target renderer to support adaptation between the available tracks.
     *
     * @param allowAdaptiveSelections Whether adaptive selection is enabled.
     */
    public void setAllowAdaptiveSelections(boolean allowAdaptiveSelections) {
        if (this.allowAdaptiveSelections != allowAdaptiveSelections) {
            this.allowAdaptiveSelections = allowAdaptiveSelections;
            updateViews();
        }
    }

    /**
     * Sets whether an option is available for disabling the renderer.
     *
     * @param showDisableOption Whether the disable option is shown.
     */
    public void setShowDisableOption(boolean showDisableOption) {
        disableView.setVisibility(showDisableOption ? View.VISIBLE : View.GONE);
    }

    /**
     * Sets the {@link TrackNameProvider} used to generate the user visible name of each track and
     * updates the view with track names queried from the specified provider.
     *
     * @param trackNameProvider The {@link TrackNameProvider} to use.
     */
    public void setTrackNameProvider(TrackNameProvider trackNameProvider) {
        this.trackNameProvider = Assertions.checkNotNull(trackNameProvider);
        updateViews();
    }

    /**
     * Initialize the view to select tracks for a specified renderer using a {@link
     * DefaultTrackSelector}.
     *
     * @param trackSelector The {@link DefaultTrackSelector}.
     * @param rendererIndex The index of the renderer.
     */
    public void init(DefaultTrackSelector trackSelector, int rendererIndex, TrackUpdateData TrackUpdateData) {
        this.trackSelector = trackSelector;
        this.rendererIndex = rendererIndex;
        this.TrackUpdateData = TrackUpdateData;
        updateViews();
    }

    private void updateViews() {
        // Remove previous per-track views.
        for (int i = getChildCount() - 1; i >= 3; i--) {
            removeViewAt(i);
        }

        MappingTrackSelector.MappedTrackInfo trackInfo =
                trackSelector == null ? null : trackSelector.getCurrentMappedTrackInfo();
        if (trackSelector == null || trackInfo == null) {
            // The view is not initialized.
            disableView.setEnabled(false);
            defaultView.setEnabled(false);
            return;
        }
        disableView.setEnabled(true);
        defaultView.setEnabled(true);

        trackGroups = trackInfo.getTrackGroups(rendererIndex);

        DefaultTrackSelector.Parameters parameters = trackSelector.getParameters();
        isDisabled = parameters.getRendererDisabled(rendererIndex);
        override = parameters.getSelectionOverride(rendererIndex, trackGroups);

        // Add per-track views.
        trackType = trackInfo.getRendererType(rendererIndex);
        Boolean isRendererDisabled = parameters.getRendererDisabled(rendererIndex);
        DefaultTrackSelector.SelectionOverride selectionOverride = parameters.getSelectionOverride(rendererIndex, trackGroups);

        trackViews = new CheckedTextView[trackGroups.length][];
        for (int groupIndex = 0; groupIndex < trackGroups.length; groupIndex++) {
            TrackGroup group = trackGroups.get(groupIndex);

            boolean enableAdaptiveSelections =
                    allowAdaptiveSelections
                            && trackGroups.get(groupIndex).length > 1
                            && trackInfo.getAdaptiveSupport(rendererIndex, groupIndex, false)
                            != RendererCapabilities.ADAPTIVE_NOT_SUPPORTED;
            trackViews[groupIndex] = new CheckedTextView[group.length];
            for (int trackIndex = 0; trackIndex < group.length; trackIndex++) {
                if (trackIndex == 0) {
                    addView(inflater.inflate(R.layout.exo_list_divider, this, false));
                }

                trackViewLayoutId =
                        enableAdaptiveSelections
                                ? android.R.layout.simple_list_item_multiple_choice
                                : android.R.layout.simple_list_item_single_choice;
                trackView =
                        (CheckedTextView) inflater.inflate(trackViewLayoutId, this, false);
                trackView.setCheckMarkDrawable(R.drawable.activated_checkbox);
                trackView.setBackgroundResource(R.drawable.player_settings_selector);

                String trackName = new DefaultTrackNameProvider(getResources()).getTrackName(trackGroups.get(groupIndex).getFormat(trackIndex));
                Boolean isTrackSupported = trackInfo.getTrackSupport(rendererIndex, groupIndex, trackIndex) == RendererCapabilities.FORMAT_HANDLED;

                //144p, 240p, 360p, 480p, 720p, 1080p, 1440p, 2160p, 2880p
                String value = trackNameProvider.getTrackName(group.getFormat(trackIndex));
                if (value.equalsIgnoreCase("720p")) {
                    trackView.setText(getStringFormat("Up to " + value, "HD"));

                } else if (value.equalsIgnoreCase("1080p")) {
                    trackView.setText(getStringFormat("Up to " + value, "Full HD"));

                } else if (value.equalsIgnoreCase("1440p")) {
                    trackView.setText(getStringFormat("Up to " + value, "Quad HD"));

                } else if (value.equalsIgnoreCase("2160p")) {
                    trackView.setText(getStringFormat("Up to " + value, "Ultra HD"));

                } else if (value.equalsIgnoreCase("2880p")) {
                    trackView.setText(getStringFormat("Up to " + value, "5K"));

                } else {
                    trackView.setText(getStringFormat("(Data saver) " + value , "Low"));
                }

                if (trackInfo.getTrackSupport(rendererIndex, groupIndex, trackIndex)
                        == RendererCapabilities.FORMAT_HANDLED) {
//                    trackView.setFocusableInTouchMode(true);
//                    trackView.setFocusable(true);

                    trackView.setTag(Pair.create(groupIndex, trackIndex));
                    trackView.setOnClickListener(componentListener);

                } else {
//                    trackView.setFocusableInTouchMode(true);
//                    trackView.setFocusable(true);
                    trackView.setEnabled(false);
                }
                trackViews[groupIndex][trackIndex] = trackView;
                addView(trackView);
            }

        }

        updateViewStates();
    }

    private Spanned getStringFormat(String string, String string1) {
        return Html.fromHtml("<b><font color=" + Color.WHITE + ">" + string1 + "</font></b> " +
                "<b><font color=" + Color.GRAY  + ">" + string + "</font></b>");
    }

    public int getTrackCountSubtitle() {
        if (trackSelector != null) {
            final MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
            if (mappedTrackInfo != null) {
                for (int rendererIndex = 0; rendererIndex < mappedTrackInfo.getRendererCount(); rendererIndex++) {
                    if (mappedTrackInfo.getRendererType(rendererIndex) == C.TRACK_TYPE_TEXT) {
                        final TrackGroupArray trackGroups = mappedTrackInfo.getTrackGroups(rendererIndex);
                        return trackGroups.length;
                    }
                }
            }
        }
        return 0;
    }

    private void updateViewStates() {
        disableView.setChecked(isDisabled);
        defaultView.setChecked(!isDisabled && override == null);

//        if (!isDisabled && override == null) {
//
//            MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
//            if (mappedTrackInfo == null) {
//                return;
//            }
//
//            for (int i = 0; i < mappedTrackInfo.getRendererCount(); i++) {
//                TrackGroupArray trackGroups = mappedTrackInfo.getTrackGroups(i);
//
//                if (trackGroups.length != 0) {
//                    for (int groupIndex = 0; groupIndex < trackGroups.length; groupIndex++) {
//                        TrackGroup group = trackGroups.get(groupIndex);
//                        TrackNameProvider trackNameProvider = new DefaultTrackNameProvider(getResources());
//
//                        for (int trackIndex = 0; trackIndex < group.length; trackIndex++) {
//
//                            switch (trackType) {
//                                case C.TRACK_TYPE_AUDIO:
//                                    defaultView.setText("Auto " + trackNameProvider.getTrackName(group.getFormat(trackIndex)));
//
//                                    break;
//                                case C.TRACK_TYPE_VIDEO:
//                                    defaultView.setText("Auto " + trackNameProvider.getTrackName(group.getFormat(trackIndex)));
//
//                                    break;
//                                case C.TRACK_TYPE_TEXT:
//                                    defaultView.setText("Auto " + trackNameProvider.getTrackName(group.getFormat(trackIndex)));
//
//                                    break;
//                                default:
//                                    continue;
//                            }
//                        }
//                    }
//                }
//            }
//        }

        for (int i = 0; i < trackViews.length; i++) {
            for (int j = 0; j < trackViews[i].length; j++) {
                trackViews[i][j].setChecked(
                        override != null && override.groupIndex == i && override.containsTrack(j));
            }
        }
    }

    private void applySelection() {
        DefaultTrackSelector.Parameters.Builder parametersBuilder = trackSelector.buildUponParameters();
        parametersBuilder.setRendererDisabled(rendererIndex, isDisabled);
        if (override != null) {
            parametersBuilder.setSelectionOverride(rendererIndex, trackGroups, override);
        } else {
            parametersBuilder.clearSelectionOverrides(rendererIndex);
        }
        trackSelector.setParameters(parametersBuilder);
    }

    private void onDisableViewClicked() {
        isDisabled = true;
        override = null;
    }

    private void onDefaultViewClicked() {
        isDisabled = false;
        override = null;
    }

    public void onTrackViewClicked(View view) {
        isDisabled = false;
        @SuppressWarnings("unchecked")
        Pair<Integer, Integer> tag = (Pair<Integer, Integer>) view.getTag();
        int groupIndex = tag.first;
        int trackIndex = tag.second;
        if (override == null || override.groupIndex != groupIndex || !allowAdaptiveSelections) {
            // A new override is being started.
            override = new DefaultTrackSelector.SelectionOverride(groupIndex, trackIndex);
        } else {
            // An existing override is being modified.
            int overrideLength = override.length;
            int[] overrideTracks = override.tracks;
            if (((CheckedTextView) view).isChecked()) {
                // Remove the track from the override.
                if (overrideLength == 1) {
                    // The last track is being removed, so the override becomes empty.
                    override = null;
                    isDisabled = true;
                } else {
                    int[] tracks = getTracksRemoving(overrideTracks, trackIndex);
                    override = new DefaultTrackSelector.SelectionOverride(groupIndex, tracks);
                }
            } else {
                int[] tracks = getTracksAdding(overrideTracks, trackIndex);
                override = new DefaultTrackSelector.SelectionOverride(groupIndex, tracks);
            }
        }

        CheckedTextView checkedTextView = (CheckedTextView) view;
        if (TrackUpdateData != null) {
            TrackUpdateData.updateTextView(checkedTextView.getText().toString());
        }
    }

    // Internal classes.

    private String trackTypeToName(int trackType) {
        switch (trackType) {
            case C.TRACK_TYPE_VIDEO:
                return "TRACK_TYPE_VIDEO";
            case C.TRACK_TYPE_AUDIO:
                return "TRACK_TYPE_AUDIO";
            case C.TRACK_TYPE_TEXT:
                return "TRACK_TYPE_TEXT";
            default:
                return "Invalid track type";
        }
    }

    private class ComponentListener implements OnClickListener {

        @Override
        public void onClick(View view) {

            if (view == disableView) {
                onDisableViewClicked();
            } else if (view == defaultView) {
                onDefaultViewClicked();
            } else {
                onTrackViewClicked(view);
            }

            updateViewStates();
            selectionView.applySelection();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    bottomSheetDialog.dismiss();
                }
            }, 1000);

        }
    }

//    public String getSelectedTrackName(int trackType) {
//        final MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
//        if (mappedTrackInfo != null) {
//            for (int rendererIndex = 0; rendererIndex < mappedTrackInfo.getRendererCount(); rendererIndex++) {
//                if (mappedTrackInfo.getRendererType(rendererIndex) == trackType) {
//                    return mappedTrackInfo.getRendererName(rendererIndex);
//                }
//            }
//        }
//
//        return null;
//    }
}