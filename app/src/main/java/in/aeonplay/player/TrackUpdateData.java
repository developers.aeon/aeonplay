package in.aeonplay.player;

public interface TrackUpdateData {
    void updateTextView(String data);
}
