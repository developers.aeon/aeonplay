/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package in.aeonplay.player;

import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.core.content.ContextCompat;
import androidx.media3.common.C;
import androidx.media3.common.Format;
import androidx.media3.common.MediaItem;
import androidx.media3.common.MediaMetadata;
import androidx.media3.common.MimeTypes;
import androidx.media3.common.PlaybackParameters;
import androidx.media3.common.Player;
import androidx.media3.common.util.UnstableApi;
import androidx.media3.exoplayer.ExoPlayer;
import androidx.media3.exoplayer.trackselection.MappingTrackSelector;
import androidx.media3.exoplayer.util.DebugTextViewHelper;
import androidx.media3.ui.AspectRatioFrameLayout;
import androidx.media3.ui.PlayerView;
import androidx.media3.ui.TrackNameProvider;
import androidx.mediarouter.app.MediaRouteButton;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAd;
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAdLoadCallback;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;

import in.aeonplay.R;
import in.aeonplay.activity.MasterActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.model.Comman.CommanDataList;
import in.aeonplay.model.PlaybackModel.PlaybackModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

@UnstableApi
public class PlayerActivity extends MasterActivity
        implements PlayerManager.Listener, OnClickListener {

    private String TAG = PlayerActivity.class.getSimpleName();
    private BroadcastReceiver mPhoneStateReceiver;
    private PlayerView mPlayerView;
    private PlayerManager mPlayerManager;
    private int mDefaultMode = AspectRatioFrameLayout.RESIZE_MODE_FIT;
    boolean isLock = false;
    private boolean isShowingTrackSelectionDialog;
    private DebugTextViewHelper debugViewHelper;
    public static int lastSpeedPosition = 0;
    // Activity lifecycle methods.
    private ImageButton mImgLock;
    private ImageView mArtworkImageView, mCompanyLogo, mImgCircle;
    private SeekBar mVolumeSeekbar;
    private AppCompatSeekBar mSeekbar;
    private LinearLayout mLinearBottom, mLinearMid, mLinearCast, mLinearVolume, mLinearTimer, mLinearLive;
    private Bundle bundle;
    private CommanDataList mCommanDataList;
    private TextView mTxtMediaTitle, mTxtLockTitle, mTxtVolume, mTxtPosition, mTxtDuration;
    public static TextView mCastInfo;
    private String assetType = "", trailerUrl;
    private ProgressBar mProgressBar;
    private Handler handler;
    private Runnable runnable;
    private int index = 0;
    private ArrayList<Long> list = new ArrayList<>();
    private RewardedInterstitialAd rewardedInterstitialAd = null;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            mVolumeSeekbar.setProgress((mVolumeSeekbar.getProgress() + 1 > mVolumeSeekbar.getMax()) ? mVolumeSeekbar.getMax() : mVolumeSeekbar.getProgress() + 1);
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            mVolumeSeekbar.setProgress((mVolumeSeekbar.getProgress() - 1 < 0) ? 0 : mVolumeSeekbar.getProgress() - 1);
        }

        return super.onKeyDown(keyCode, event);
    }

    private void setVolumeIcon(int position) {
        mTxtVolume.setText("Volume " + position + "%");
//        int volumeIndex = position <= 0 ? R.drawable.ic_action_volume_off : R.drawable.ic_action_volume_on;
//        mImgVolume.setImageResource(volumeIndex);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        Init();
        setAudioConfig();
        setData();
    }

    public static String getMimeType(String mVideoUrl) {
        if (mVideoUrl.endsWith(".m3u8"))
            return MimeTypes.APPLICATION_M3U8;
        else if (mVideoUrl.endsWith(".mpd"))
            return MimeTypes.APPLICATION_MPD;
        else
            return MimeTypes.APPLICATION_MP4;
    }

    private void setAudioConfig() {
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mVolumeSeekbar.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        mVolumeSeekbar.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));

        setVolumeIcon(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
        mVolumeSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(final SeekBar seekBar, final int progress, final boolean fromUser) {
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
                setVolumeIcon(progress);
            }

            @Override
            public void onStartTrackingTouch(final SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(final SeekBar seekBar) {
            }
        });
    }

    private void setData() {
        bundle = getIntent().getExtras();

        if (bundle != null) {
            mCommanDataList = bundle.getParcelable(Constants.DATA);
            trailerUrl = bundle.getString(Constants.TRAILER);
            mTxtMediaTitle.setText(mCommanDataList.getTitle());

            if (!TextUtils.isEmpty(mCommanDataList.getThumbnail().getBackground())) {
                Glide.with(getApplicationContext())
                        .load(mCommanDataList.getThumbnail().getBackground())
                        .into(mArtworkImageView);
            }

            mCompanyLogo.setVisibility(View.VISIBLE);
            if (mCommanDataList.getContentType().equalsIgnoreCase("live_channel")) {
                mCompanyLogo.setVisibility(View.GONE);
            }

            assetType = mCommanDataList.getContentType().equalsIgnoreCase("episode") ? "original" : "movie";

            if (mCommanDataList.getProvider().equalsIgnoreCase("erosnow")) {
                requestPlayback(assetType, mCommanDataList.getContentId());

            } else {
                configurePlayer(mCommanDataList.getHostUrl());
            }
        }

        mPlayerManager.localPlayer.addListener(new Player.Listener() {
        });

        mSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    long currentPosition = (long) (progress * 1000);
                    mPlayerManager.localPlayer.seekTo(currentPosition);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

//        mDefaultTimeBar.addListener(new TimeBar.OnScrubListener() {
//            @Override
//            public void onScrubStart(TimeBar timeBar, long position) {
//
//            }
//
//            @Override
//            public void onScrubMove(TimeBar timeBar, long position) {
//                mPlayerManager.localPlayer.seekTo(position);
//                if (list.size() > 0) {
//                    if (position == list.get(index)) {
//                        initAd();
//                        index++;
//                    }
//                }
//            }
//
//            @Override
//            public void onScrubStop(TimeBar timeBar, long position, boolean canceled) {
//
//            }
//        });
    }

    private void Init() {
        mPlayerView = findViewById(R.id.player_view);
        mPlayerView.requestFocus();

        if (mCastContext != null)
            mPlayerManager = new PlayerManager(/* listener= */ this, this, mPlayerView, /* context= */ mCastContext);

        mProgressBar = findViewById(R.id.player_loader);
        mArtworkImageView = mPlayerView.findViewById(R.id.exo_thumbnail);
        mArtworkImageView.animate().alpha(0.0f).setDuration(1000); // Default

        mCompanyLogo = findViewById(R.id.imgCompanyLogo);
        mVolumeSeekbar = mPlayerView.findViewById(R.id.volume_seek);
        mTxtVolume = mPlayerView.findViewById(R.id.txtVolume);

        mPlayerView.findViewById(R.id.linearBack).setOnClickListener(this);
        mPlayerView.findViewById(R.id.linearSettings).setOnClickListener(this);
        mPlayerView.findViewById(R.id.linearLock).setOnClickListener(this);
        mPlayerView.findViewById(R.id.linearAspect).setOnClickListener(this);
        mPlayerView.findViewById(R.id.linearSpeed).setOnClickListener(this);

        mLinearMid = mPlayerView.findViewById(R.id.midContainer);
        mLinearBottom = mPlayerView.findViewById(R.id.bottomContainer);
        mSeekbar = mPlayerView.findViewById(R.id.exo_seekbar);
        mTxtMediaTitle = mPlayerView.findViewById(R.id.txtMediaTitle);
        mImgLock = mPlayerView.findViewById(R.id.imgLock);
        mTxtLockTitle = mPlayerView.findViewById(R.id.txtLockTitle);
        mLinearCast = mPlayerView.findViewById(R.id.linearCast);
        mCastInfo = mPlayerView.findViewById(R.id.exo_cast_info_tv);

        mLinearVolume = mPlayerView.findViewById(R.id.linearVolume);
        mLinearTimer = mPlayerView.findViewById(R.id.linearTimer);
        mLinearLive = mPlayerView.findViewById(R.id.linearLive);
        mTxtPosition = mPlayerView.findViewById(R.id.exo_position);
        mTxtDuration = mPlayerView.findViewById(R.id.exo_duration);

        mImgCircle = mPlayerView.findViewById(R.id.imgLive);
        Animation a = new AlphaAnimation(0.0f, 1.0f);
        a.setDuration(1000);
        a.setRepeatCount(Animation.INFINITE);
        a.setRepeatMode(Animation.REVERSE);
        mImgCircle.setAnimation(a);

        MediaRouteButton mediaRouteButton = mPlayerView.findViewById(R.id.imgCast);
        mediaRouteButton.setRemoteIndicatorDrawable(ContextCompat.getDrawable(this, R.drawable.custom_cast_button));
        CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), mediaRouteButton);

        mPlayerManager.localPlayer.addListener(new Player.Listener() {
            @Override
            public void onPlaybackStateChanged(int playbackState) {
                Player.Listener.super.onPlaybackStateChanged(playbackState);

                if (playbackState == ExoPlayer.STATE_READY) {
                    mArtworkImageView.animate().alpha(0.0f).setDuration(1000);
                    mProgressBar.setVisibility(View.INVISIBLE);

                    if (mCommanDataList.getContentType().equalsIgnoreCase("live_channel") ||
                            mCommanDataList.getContentType().equalsIgnoreCase("live_stream")) {
                        mSeekbar.setProgress((int) 100);
                        mSeekbar.setEnabled(false);

                        mLinearTimer.setVisibility(View.GONE);
                        mLinearLive.setVisibility(View.VISIBLE);

                    } else {
                        mLinearTimer.setVisibility(View.VISIBLE);
                        setProgress();
                    }

                    showAdvertisementInitial();
//                    showAdvertisementEvery5Min(exoPlayer.getDuration());
//                    showAdvertisementTotalDurationDividedBy10(mPlayerManager.localPlayer.getDuration());

                } else if (playbackState == ExoPlayer.STATE_BUFFERING) {
                    mProgressBar.setVisibility(View.VISIBLE);

                } else if (playbackState == ExoPlayer.STATE_IDLE) {
                    mArtworkImageView.animate().alpha(1.0f).setDuration(1000);
                    mProgressBar.setVisibility(View.INVISIBLE);

                } else if (playbackState == ExoPlayer.STATE_ENDED) {
                    mArtworkImageView.animate().alpha(1.0f).setDuration(1000);
                }
            }
        });
    }

    private void requestPlayback(String assetType, String assetID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getPlaybackInfo(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), assetType, assetID);

        APIClient.callAPI(this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                mProgressBar.setVisibility(View.GONE);

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final PlaybackModel playbackModel = gson.fromJson(reader, PlaybackModel.class);

                    if (playbackModel.getData() != null) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            configurePlayer(playbackModel.getData().getPlayback().getUrl());
                        }

                    } else {
                        showMessageToUser("Data not found!");
                        showErrorDialog(PlayerActivity.this,
                                loginCall.request().url().toString(),
                                Constants.EROS_PARTNER_PLAYBACK + "assetId=" + assetID + "&assetType=" + assetType);
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                Log.d(TAG, error);
                showMessageToUser(error);
            }
        });
    }

    private void configurePlayer(String path) {
        MediaItem mediaItem = new MediaItem.Builder()
                .setUri(Uri.parse(path))
                .setMimeType(getMimeType(path))
                .setMediaMetadata(new MediaMetadata.Builder()
                        .setTitle(mCommanDataList.getTitle())
                        .setArtworkUri(Uri.parse(mCommanDataList.getThumbnail().getLandscape()))
                        .build())
                .build();

        mPlayerManager.addItem(mediaItem);
        setPlayPause(true);
    }

    private void showAdvertisementTotalDurationDividedBy10(long totalDuration) {
        if (totalDuration != 0) {
            long adsLong[] = new long[10];
            boolean adsBoolean[] = new boolean[10];

            for (int i = 1; i <= 10; ++i) {
                list.add((totalDuration / 10) * i);
                adsLong[i - 1] = ((totalDuration / 10) * i);
                adsBoolean[i - 1] = false;
            }

            mPlayerView.setExtraAdGroupMarkers(
                    adsLong, adsBoolean);

            handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        if (index < list.size()) {
                            if ((int) mPlayerManager.localPlayer.getCurrentPosition() >= list.get(index)) {
                                initAd();
                                index++;
                            }
                        }

                        handler.postDelayed(runnable, 1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            handler.post(runnable);
        }
    }

    private void showAdvertisementEvery5Min(long totalDuration) {
        if (totalDuration != 0) {
            long totalCalculatedDuration = 0;
            long delay = 10 * 60 * 1000;

            long adsLong[] = new long[(int) Math.ceil(totalDuration / (delay * 1.0f))];
            boolean adsBoolean[] = new boolean[adsLong.length];

            int i = 0;
            while (totalCalculatedDuration < totalDuration) {
                list.add(delay);
                adsLong[i] = delay;
                adsBoolean[i] = false;
                i++;
                totalCalculatedDuration += delay;
            }

            mPlayerView.setExtraAdGroupMarkers(
                    adsLong, adsBoolean);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    initAd();
                }
            });
        }
    }

    private void showAdvertisementInitial() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                initAd();
            }
        });
    }

    private void initAd() {
        if (rewardedInterstitialAd != null) return;
        MobileAds.initialize(this);
        RewardedInterstitialAd.load(this, getString(R.string.rewarded_ads),
                new AdRequest.Builder().build(), new RewardedInterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull RewardedInterstitialAd p0) {
                        rewardedInterstitialAd = p0;
                        rewardedInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {

                            @Override
                            public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                            }

                            @Override
                            public void onAdShowedFullScreenContent() {
                                setPlayPause(false);
                            }

                            @Override
                            public void onAdDismissedFullScreenContent() {
                                rewardedInterstitialAd = null;
                            }
                        });

                        rewardedInterstitialAd.show(PlayerActivity.this, rewardItem -> {
                        });
                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        rewardedInterstitialAd = null;
                    }
                });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.linearSettings) {
            MappingTrackSelector.MappedTrackInfo mappedTrackInfo = mPlayerManager.mTrackSelector.getCurrentMappedTrackInfo();
            if (mappedTrackInfo != null) {
                ExoPlayer exoPlayer = (ExoPlayer) mPlayerView.getPlayer();
                int rendererType = mappedTrackInfo.getRendererType(0);
                boolean allowAdaptiveSelections =
                        rendererType == C.TRACK_TYPE_VIDEO
                                || (rendererType == C.TRACK_TYPE_AUDIO
                                && mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_VIDEO)
                                == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_NO_TRACKS);
                Pair<BottomSheetDialog, TrackSelectionViews> dialogPair =
                        TrackSelectionViews.getDialog(PlayerActivity.this, "Quality", mPlayerManager.mTrackSelector,
                                0, exoPlayer.getVideoFormat(), new TrackUpdateData() {
                                    @Override
                                    public void updateTextView(String data) {
                                        SharePreferenceManager.saveVideoTrack(getApplicationContext(), Constants.KEY_VIDEO, data);
                                    }
                                });
                dialogPair.second.setShowDisableOption(false);

                dialogPair.second.setTrackNameProvider(new TrackNameProvider() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public String getTrackName(Format format) {
                        return format.height + "p";
                    }
                });

                dialogPair.first.setCanceledOnTouchOutside(true);
                dialogPair.first.show();
            }
        }

        if (view.getId() == R.id.linearAspect) {
            if (mDefaultMode == AspectRatioFrameLayout.RESIZE_MODE_FIT) {
                mPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                mDefaultMode = AspectRatioFrameLayout.RESIZE_MODE_FILL; // Fill mode

            } else if (mDefaultMode == AspectRatioFrameLayout.RESIZE_MODE_FILL) {
                mPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_ZOOM);
                mDefaultMode = AspectRatioFrameLayout.RESIZE_MODE_ZOOM; // Zoom mode

            } else if (mDefaultMode == AspectRatioFrameLayout.RESIZE_MODE_ZOOM) {
                mPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_HEIGHT);
                mDefaultMode = AspectRatioFrameLayout.RESIZE_MODE_FIXED_HEIGHT; // Fixed height mode

            } else if (mDefaultMode == AspectRatioFrameLayout.RESIZE_MODE_FIXED_HEIGHT) {
                mPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_WIDTH);
                mDefaultMode = AspectRatioFrameLayout.RESIZE_MODE_FIXED_WIDTH; // Fixed width mode

            } else if (mDefaultMode == AspectRatioFrameLayout.RESIZE_MODE_FIXED_WIDTH) {
                mPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
                mDefaultMode = AspectRatioFrameLayout.RESIZE_MODE_FIT; // Fit mode
            }

        } else if (view.getId() == R.id.linearLock) {
            if (!isLock) {
                mImgLock.setImageResource(R.drawable.ic_action_lock);
            } else {
                mImgLock.setImageResource(R.drawable.ic_action_unlock);
            }
            isLock = !isLock;
            lockScreen(isLock);

        } else if (view.getId() == R.id.linearSpeed) {
            View popupView = getLayoutInflater().inflate(R.layout.pop_layout, null);
            PopupWindow popupWindow = new PopupWindow(popupView,
                    WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);

            RecyclerView mNavigationList = (RecyclerView) popupView.findViewById(R.id.nav_list);
            mNavigationList.setLayoutManager(new LinearLayoutManager(PlayerActivity.this, LinearLayoutManager.VERTICAL, false));
            mNavigationList.setItemAnimator(new DefaultItemAnimator());

            List<String> sampleList = new ArrayList<>();
            sampleList.add("0.5x");
            sampleList.add("Default");
            sampleList.add("1.5x");
            sampleList.add("2.0x");
            sampleList.add("2.5x");

            PlayerSpeedAdapter mPlayerSpeedAdapter = new PlayerSpeedAdapter(PlayerActivity.this, sampleList);
            mNavigationList.setAdapter(mPlayerSpeedAdapter);
            mPlayerSpeedAdapter.notifyDataSetChanged();

            mPlayerSpeedAdapter.setOnItemClickListener(new PlayerSpeedAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    updateNavigation(mPlayerSpeedAdapter, position);
                    popupWindow.dismiss();

                    if (position == 0) {
                        mPlayerManager.localPlayer.setPlaybackParameters(new PlaybackParameters(0.5f));

                    } else if (position == 1) {
                        mPlayerManager.localPlayer.setPlaybackParameters(new PlaybackParameters(1f));

                    } else if (position == 2) {
                        mPlayerManager.localPlayer.setPlaybackParameters(new PlaybackParameters(1.5f));

                    } else if (position == 3) {
                        mPlayerManager.localPlayer.setPlaybackParameters(new PlaybackParameters(2f));

                    } else if (position == 4) {
                        mPlayerManager.localPlayer.setPlaybackParameters(new PlaybackParameters(2.5f));
                    }
                }
            });

            ImageView mUpImageView = (ImageView) popupView.findViewById(R.id.arrow_up);
            ImageView mDownImageView = (ImageView) popupView.findViewById(R.id.arrow_down);

            popupWindow.setOutsideTouchable(true);
            popupWindow.setFocusable(true);
            popupWindow.setBackgroundDrawable(new ColorDrawable());
            int[] location = new int[2];
            view.getLocationOnScreen(location);

            Rect anchorRect = new Rect(location[0], location[1], location[0]
                    + view.getWidth(), location[1] + view.getHeight());

            popupView.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            int rootHeight = popupView.getMeasuredHeight();
            int rootWidth = popupView.getMeasuredWidth();
            final int screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
            final int screenHeight = Resources.getSystem().getDisplayMetrics().heightPixels;

            int yPos = anchorRect.top - rootHeight;

            boolean onTop = true;

            if (anchorRect.top < screenHeight / 2) {
                yPos = anchorRect.bottom;
                onTop = false;
            }

            int whichArrow, requestedX;

            whichArrow = ((onTop) ? R.id.arrow_down : R.id.arrow_up);
            requestedX = anchorRect.centerX();

            View arrow = whichArrow == R.id.arrow_up ? mUpImageView
                    : mDownImageView;
            View hideArrow = whichArrow == R.id.arrow_up ? mDownImageView
                    : mUpImageView;

            final int arrowWidth = arrow.getMeasuredWidth();

            arrow.setVisibility(View.VISIBLE);

            ViewGroup.MarginLayoutParams param = (ViewGroup.MarginLayoutParams) arrow
                    .getLayoutParams();

            hideArrow.setVisibility(View.INVISIBLE);

            int xPos = 0;

            // ETXTREME RIGHT CLICKED
            if (anchorRect.left + rootWidth > screenWidth) {
                xPos = (screenWidth - rootWidth);
            }
            // ETXTREME LEFT CLICKED
            else if (anchorRect.left - (rootWidth / 2) < 0) {
                xPos = anchorRect.left;
            }
            // IN BETWEEN
            else {
                xPos = (anchorRect.centerX() - (rootWidth / 2));
            }

            param.leftMargin = (requestedX - xPos) - (arrowWidth / 2);
            popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, xPos, yPos);

        } else if (view.getId() == R.id.linearBack) {
            finish();
        }
    }

    public void updateNavigation(PlayerSpeedAdapter adapter, int position) {
        adapter.setSelectedPosition(position);
        adapter.notifyDataSetChanged();
    }

    public void lockScreen(boolean lock) {
        mLinearMid.setVisibility(lock == true ? View.INVISIBLE : View.VISIBLE);
        mLinearBottom.setVisibility(lock == true ? View.INVISIBLE : View.VISIBLE);
        mTxtLockTitle.setText(lock == true ? "Unlock" : "Lock");
        mSeekbar.setVisibility(lock == true ? View.INVISIBLE : View.VISIBLE);
        mLinearVolume.setVisibility(lock == true ? View.INVISIBLE : View.VISIBLE);
        mLinearTimer.setVisibility(lock == true ? View.INVISIBLE : View.VISIBLE);
        mLinearLive.setVisibility(lock == true ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter();
        filter.addAction(android.telephony.TelephonyManager.ACTION_PHONE_STATE_CHANGED);
        filter.addAction("android.filter.action.PHONE_STATE");
        filter.addAction("android.filter.action.NEW_OUTGOING_CALL");

        mPhoneStateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle extras = intent.getExtras();

                if (extras != null) {

                    String state = extras.getString(TelephonyManager.EXTRA_STATE);
                    if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                        if (mPlayerManager.localPlayer != null)
                            setPlayPause(false);

                    } else if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                        if (mPlayerManager.localPlayer != null)
                            setPlayPause(false);

                    } else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                        if (mPlayerManager.localPlayer != null)
                            setPlayPause(true);
                    }
                }
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            registerReceiver(mPhoneStateReceiver, filter, Context.RECEIVER_NOT_EXPORTED);
        } else {
            registerReceiver(mPhoneStateReceiver, filter);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPlayerManager != null) {
            mPlayerManager.release();
            mPlayerManager = null;
        }
    }

    private void setPlayPause(boolean play) {
        mPlayerManager.localPlayer.setPlayWhenReady(play);
        mPlayerManager.localPlayer.getPlaybackState();
        mPlayerManager.localPlayer.prepare();
        mPlayerManager.localPlayer.play();
    }

    // Activity input.

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        // If the event was not handled then see if the player view can handle it.
        return super.dispatchKeyEvent(event) || mPlayerManager.dispatchKeyEvent(event);
    }


    // PlayerManager.Listener implementation.

    @Override
    public void onQueuePositionChanged(int previousIndex, int newIndex) {

    }

    @Override
    public void onUnsupportedTrack(int trackType) {
        if (trackType == C.TRACK_TYPE_AUDIO) {
            showMessageToUser(getString(R.string.error_unsupported_audio));
        } else if (trackType == C.TRACK_TYPE_VIDEO) {
            showMessageToUser(getString(R.string.error_unsupported_video));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        hideToolbarStatus();
    }

    @Override
    public void onBackPressed() {
//        if (isLock)
//            return;
        finish();
    }

    public void hideToolbarStatus() {
        getWindow()
                .getDecorView()
                .setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LOW_PROFILE |
                                View.SYSTEM_UI_FLAG_FULLSCREEN |
                                SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                                View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                                SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);

        // View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        getWindow().setFlags(FLAG_FULLSCREEN, FLAG_FULLSCREEN);
        getWindow().addFlags(FLAG_FULLSCREEN | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    private void setProgress() {
        mSeekbar.setMax((int) mPlayerManager.localPlayer.getDuration() / 1000);
        mTxtPosition.setText(stringForTime((int) mPlayerManager.localPlayer.getCurrentPosition()));
        mTxtDuration.setText(stringForTime((int) mPlayerManager.localPlayer.getDuration()));

        if (handler == null) handler = new Handler();
        //Make sure you update Seekbar on UI thread
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (mPlayerManager != null) {
                    if (mPlayerManager.localPlayer != null) {
                        if (mPlayerManager.localPlayer.isPlaying()) {
                            mSeekbar.setMax((int) mPlayerManager.localPlayer.getDuration() / 1000);
                            int mCurrentPosition = (int) mPlayerManager.localPlayer.getCurrentPosition() / 1000;
                            int mBufferPosition = (int) mPlayerManager.localPlayer.getBufferedPosition() / 1000;
                            mSeekbar.setProgress(mCurrentPosition);
                            mSeekbar.setSecondaryProgress(mBufferPosition);
                            mTxtPosition.setText(stringForTime((int) mPlayerManager.localPlayer.getCurrentPosition()));
                            mTxtDuration.setText(stringForTime((int) mPlayerManager.localPlayer.getDuration()));

                            handler.postDelayed(this, 1000);
                        }
                    }
                }


            }
        });
    }

    private String stringForTime(int timeMs) {
        StringBuilder mFormatBuilder;
        Formatter mFormatter;
        mFormatBuilder = new StringBuilder();
        mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());
        int totalSeconds = timeMs / 1000;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;

        mFormatBuilder.setLength(0);
        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }
}

