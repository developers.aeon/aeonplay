package in.aeonplay.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ErrorsDataModel implements Serializable {

    @SerializedName("otp")
    @Expose
    private List<String> otp;
    @SerializedName("email")
    @Expose
    private List<String> email;
    private final static long serialVersionUID = 740245799788812379L;

    public List<String> getOtp() {
        return otp;
    }

    public void setOtp(List<String> otp) {
        this.otp = otp;
    }

    public List<String> getEmail() {
        return email;
    }

    public void setEmail(List<String> email) {
        this.email = email;
    }
}