package in.aeonplay.model.NewsModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class NewsModel implements Parcelable {

    @SerializedName("data")
    @Expose
    private List<NewsData> data = new ArrayList<>();
    public final static Creator<NewsModel> CREATOR = new Creator<NewsModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public NewsModel createFromParcel(android.os.Parcel in) {
            return new NewsModel(in);
        }

        public NewsModel[] newArray(int size) {
            return (new NewsModel[size]);
        }

    };

    protected NewsModel(android.os.Parcel in) {
        in.readList(this.data, (NewsData.class.getClassLoader()));
    }

    public NewsModel() {
    }

    public List<NewsData> getData() {
        return data;
    }

    public void setData(List<NewsData> data) {
        this.data = data;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeList(data);
    }

    public int describeContents() {
        return 0;
    }
}
