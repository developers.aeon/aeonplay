package in.aeonplay.model.NewsModel;

import java.util.List;

import in.aeonplay.model.Comman.RecyclerViewItem;

public class NewsHeader extends RecyclerViewItem {

    private String title;
    private List<NewsData> newsDataLst = null;
    boolean isAdvertisement = false;

    public NewsHeader(String title, List<NewsData> newsList) {
        this.title = title;
        this.newsDataLst = newsList;
    }

    public NewsHeader() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<NewsData> getDatum() {
        return newsDataLst;
    }

    public void setDatum(List<NewsData> newsDataLst) {
        this.newsDataLst = newsDataLst;
    }
}