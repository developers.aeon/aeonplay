package in.aeonplay.model.Package;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PackageGroupModel implements Parcelable {

    @SerializedName("data")
    @Expose
    private List<PackageDataModel> data = null;
    public final static Creator<PackageGroupModel> CREATOR = new Creator<PackageGroupModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PackageGroupModel createFromParcel(android.os.Parcel in) {
            return new PackageGroupModel(in);
        }

        public PackageGroupModel[] newArray(int size) {
            return (new PackageGroupModel[size]);
        }

    };

    protected PackageGroupModel(android.os.Parcel in) {
        in.readList(this.data, (PackageDataModel.class.getClassLoader()));
    }

    public PackageGroupModel() {
    }

    public List<PackageDataModel> getData() {
        return data;
    }

    public void setData(List<PackageDataModel> data) {
        this.data = data;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeList(data);
    }

    public int describeContents() {
        return 0;
    }

}