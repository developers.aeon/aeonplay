package in.aeonplay.model.Package;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PackageContentAccess implements Parcelable {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("pivot")
    @Expose
    private PackagePivotModel packagePivotModel;
    public final static Creator<PackageContentAccess> CREATOR = new Creator<PackageContentAccess>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PackageContentAccess createFromParcel(android.os.Parcel in) {
            return new PackageContentAccess(in);
        }

        public PackageContentAccess[] newArray(int size) {
            return (new PackageContentAccess[size]);
        }

    };

    protected PackageContentAccess(android.os.Parcel in) {
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.packagePivotModel = ((PackagePivotModel) in.readValue((PackagePivotModel.class.getClassLoader())));
    }

    public PackageContentAccess() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public PackagePivotModel getPivot() {
        return packagePivotModel;
    }

    public void setPivot(PackagePivotModel packagePivotModel) {
        this.packagePivotModel = packagePivotModel;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(title);
        dest.writeValue(packagePivotModel);
    }

    public int describeContents() {
        return 0;
    }

}