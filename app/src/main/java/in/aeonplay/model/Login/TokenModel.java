package in.aeonplay.model.Login;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TokenModel implements Parcelable {

    @SerializedName("token_type")
    @Expose
    private String tokenType;
    @SerializedName("expires_in")
    @Expose
    private Integer expiresIn;
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("refresh_token")
    @Expose
    private String refreshToken;
    public final static Creator<TokenModel> CREATOR = new Creator<TokenModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public TokenModel createFromParcel(Parcel in) {
            return new TokenModel(in);
        }

        public TokenModel[] newArray(int size) {
            return (new TokenModel[size]);
        }

    };

    protected TokenModel(Parcel in) {
        this.tokenType = ((String) in.readValue((String.class.getClassLoader())));
        this.expiresIn = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.accessToken = ((String) in.readValue((String.class.getClassLoader())));
        this.refreshToken = ((String) in.readValue((String.class.getClassLoader())));
    }

    public TokenModel() {
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(tokenType);
        dest.writeValue(expiresIn);
        dest.writeValue(accessToken);
        dest.writeValue(refreshToken);
    }

    public int describeContents() {
        return 0;
    }

}