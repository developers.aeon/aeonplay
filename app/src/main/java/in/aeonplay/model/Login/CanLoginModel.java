package in.aeonplay.model.Login;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CanLoginModel implements Parcelable {

    @SerializedName("data")
    @Expose
    private CanLoginDataModel canLoginDataModel;
    public final static Creator<CanLoginModel> CREATOR = new Creator<CanLoginModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CanLoginModel createFromParcel(android.os.Parcel in) {
            return new CanLoginModel(in);
        }

        public CanLoginModel[] newArray(int size) {
            return (new CanLoginModel[size]);
        }

    };

    protected CanLoginModel(android.os.Parcel in) {
        this.canLoginDataModel = ((CanLoginDataModel) in.readValue((CanLoginDataModel.class.getClassLoader())));
    }

    public CanLoginModel() {
    }

    public CanLoginDataModel getData() {
        return canLoginDataModel;
    }

    public void setData(CanLoginDataModel canLoginDataModel) {
        this.canLoginDataModel = canLoginDataModel;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(canLoginDataModel);
    }

    public int describeContents() {
        return 0;
    }

}