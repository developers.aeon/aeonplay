package in.aeonplay.model.Shots;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShotDetailModel implements Parcelable {

    @SerializedName("data")
    @Expose
    private ShotSubDataModel shotSubDataModel;
    public final static Creator<ShotDetailModel> CREATOR = new Creator<ShotDetailModel>() {

        public ShotDetailModel createFromParcel(android.os.Parcel in) {
            return new ShotDetailModel(in);
        }

        public ShotDetailModel[] newArray(int size) {
            return (new ShotDetailModel[size]);
        }

    };

    @SuppressWarnings({
            "unchecked"
    })
    protected ShotDetailModel(android.os.Parcel in) {
        this.shotSubDataModel = ((ShotSubDataModel) in.readValue((ShotSubDataModel.class.getClassLoader())));
    }

    public ShotDetailModel() {
    }

    public ShotSubDataModel getData() {
        return shotSubDataModel;
    }

    public void setData(ShotSubDataModel shotSubDataModel) {
        this.shotSubDataModel = shotSubDataModel;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(shotSubDataModel);
    }

    public int describeContents() {
        return 0;
    }

}