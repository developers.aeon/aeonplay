package in.aeonplay.model.Shots.Campaign;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.aeonplay.model.Profile.ProfileDataModel;

public class CampaignResultDataModel implements Parcelable {
    public final static Creator<CampaignResultDataModel> CREATOR = new Creator<CampaignResultDataModel>() {

        @Override
        public CampaignResultDataModel createFromParcel(Parcel parcel) {
            return null;
        }

        public CampaignResultDataModel[] newArray(int size) {
            return (new CampaignResultDataModel[size]);
        }

    };
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("campaign_id")
    @Expose
    private Integer campaignId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("user")
    @Expose
    private ProfileDataModel user;

    @SuppressWarnings({
            "unchecked"
    })
    protected CampaignResultDataModel(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.campaignId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.userId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.user = ((ProfileDataModel) in.readValue((ProfileDataModel.class.getClassLoader())));
    }

    public CampaignResultDataModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ProfileDataModel getUser() {
        return user;
    }

    public void setUser(ProfileDataModel user) {
        this.user = user;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(campaignId);
        dest.writeValue(userId);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(user);
    }

    public int describeContents() {
        return 0;
    }
}
