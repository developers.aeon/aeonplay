package in.aeonplay.model.Shots.Follow;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FollowModel implements Parcelable {

    public final static Creator<FollowModel> CREATOR = new Creator<FollowModel>() {


        public FollowModel createFromParcel(android.os.Parcel in) {
            return new FollowModel(in);
        }

        public FollowModel[] newArray(int size) {
            return (new FollowModel[size]);
        }

    };
    @SerializedName("data")
    @Expose
    private FollowDataModel data;

    @SuppressWarnings({
            "unchecked"
    })
    protected FollowModel(android.os.Parcel in) {
        this.data = ((FollowDataModel) in.readValue((FollowDataModel.class.getClassLoader())));
    }

    public FollowModel() {
    }

    public FollowDataModel getData() {
        return data;
    }

    public void setData(FollowDataModel data) {
        this.data = data;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }

}