package in.aeonplay.model.Shots.Campaign;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CampaignResultModel implements Parcelable {

    public final static Creator<CampaignResultModel> CREATOR = new Creator<CampaignResultModel>() {


        public CampaignResultModel createFromParcel(android.os.Parcel in) {
            return new CampaignResultModel(in);
        }

        public CampaignResultModel[] newArray(int size) {
            return (new CampaignResultModel[size]);
        }

    };
    @SerializedName("data")
    @Expose
    private List<CampaignResultDataModel> data;

    @SuppressWarnings({
            "unchecked"
    })
    protected CampaignResultModel(android.os.Parcel in) {
        in.readList(this.data, (CampaignResultDataModel.class.getClassLoader()));
    }

    public CampaignResultModel() {
    }

    public List<CampaignResultDataModel> getData() {
        return data;
    }

    public void setData(List<CampaignResultDataModel> data) {
        this.data = data;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeList(data);
    }

    public int describeContents() {
        return 0;
    }

}