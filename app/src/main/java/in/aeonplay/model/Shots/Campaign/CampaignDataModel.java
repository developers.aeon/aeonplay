package in.aeonplay.model.Shots.Campaign;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import in.aeonplay.activity.MasterActivity;
import in.aeonplay.model.Shots.ShotLastVotedModel;

public class CampaignDataModel implements Parcelable {

    public final static Creator<CampaignDataModel> CREATOR = new Creator<CampaignDataModel>() {


        public CampaignDataModel createFromParcel(android.os.Parcel in) {
            return new CampaignDataModel(in);
        }

        public CampaignDataModel[] newArray(int size) {
            return (new CampaignDataModel[size]);
        }

    };
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("starting")
    @Expose
    private String starting;
    @SerializedName("closing")
    @Expose
    private String closing;
    @SerializedName("registration_opening")
    @Expose
    private String registrationOpening;
    @SerializedName("registration_closing")
    @Expose
    private String registrationClosing;
    @SerializedName("vote_opening")
    @Expose
    private String voteOpening;
    @SerializedName("vote_closing")
    @Expose
    private String voteClosing;
    @SerializedName("award_announcement")
    @Expose
    private String awardAnnouncement;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("image_file")
    @Expose
    private String image_file;
    @SerializedName("votes_count")
    @Expose
    private int votes_count;
    @SerializedName("is_participated")
    @Expose
    private boolean is_participated;
    @SerializedName("last_voted")
    @Expose
    private ShotLastVotedModel last_voted = null;
    @SerializedName("participants")
    @Expose
    private List<CampaignParticipantModel> participants = new ArrayList<>();

    @SuppressWarnings({
            "unchecked"
    })
    protected CampaignDataModel(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.starting = ((String) in.readValue((String.class.getClassLoader())));
        this.closing = ((String) in.readValue((String.class.getClassLoader())));
        this.registrationOpening = ((String) in.readValue((String.class.getClassLoader())));
        this.registrationClosing = ((String) in.readValue((String.class.getClassLoader())));
        this.voteOpening = ((String) in.readValue((String.class.getClassLoader())));
        this.voteClosing = ((String) in.readValue((String.class.getClassLoader())));
        this.awardAnnouncement = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.image_file = ((String) in.readValue((String.class.getClassLoader())));
        this.votes_count = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.is_participated = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.last_voted = ((ShotLastVotedModel) in.readValue((ShotLastVotedModel.class.getClassLoader())));
        in.readList(this.participants, (CampaignParticipantModel.class.getClassLoader()));
    }

    public CampaignDataModel() {
    }

    public CampaignDataModel(int defaultId, String defaultTitle) {
        this.id = defaultId;
        this.title = defaultTitle;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return MasterActivity.checkStringIsNull(title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return MasterActivity.checkStringIsNull(description);
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStarting() {
        return MasterActivity.checkStringIsNull(starting);
    }

    public void setStarting(String starting) {
        this.starting = starting;
    }

    public String getClosing() {
        return MasterActivity.checkStringIsNull(closing);
    }

    public void setClosing(String closing) {
        this.closing = closing;
    }

    public String getRegistrationOpening() {
        return MasterActivity.checkStringIsNull(registrationOpening);
    }

    public void setRegistrationOpening(String registrationOpening) {
        this.registrationOpening = registrationOpening;
    }

    public String getRegistrationClosing() {
        return MasterActivity.checkStringIsNull(registrationClosing);
    }

    public void setRegistrationClosing(String registrationClosing) {
        this.registrationClosing = registrationClosing;
    }

    public String getVoteOpening() {
        return MasterActivity.checkStringIsNull(voteOpening);
    }

    public void setVoteOpening(String voteOpening) {
        this.voteOpening = voteOpening;
    }

    public String getVoteClosing() {
        return MasterActivity.checkStringIsNull(voteClosing);
    }

    public void setVoteClosing(String voteClosing) {
        this.voteClosing = voteClosing;
    }

    public String getAwardAnnouncement() {
        return MasterActivity.checkStringIsNull(awardAnnouncement);
    }

    public void setAwardAnnouncement(String awardAnnouncement) {
        this.awardAnnouncement = awardAnnouncement;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return MasterActivity.checkStringIsNull(createdAt);
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return MasterActivity.checkStringIsNull(updatedAt);
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getImage_file() {
        return image_file;
    }

    public void setImage_file(String image_file) {
        this.image_file = image_file;
    }

    public int getVotes_count() {
        return votes_count;
    }

    public void setVotes_count(int votes_count) {
        this.votes_count = votes_count;
    }

    public boolean isIs_participated() {
        return is_participated;
    }

    public void setIs_participated(boolean is_participated) {
        this.is_participated = is_participated;
    }

    public ShotLastVotedModel isLast_voted() {
        return last_voted;
    }

    public void setLast_voted(ShotLastVotedModel last_voted) {
        this.last_voted = last_voted;
    }

    public List<CampaignParticipantModel> getParticipants() {
        return participants;
    }

    public void setParticipants(List<CampaignParticipantModel> participants) {
        this.participants = participants;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(title);
        dest.writeValue(description);
        dest.writeValue(starting);
        dest.writeValue(closing);
        dest.writeValue(registrationOpening);
        dest.writeValue(registrationClosing);
        dest.writeValue(voteOpening);
        dest.writeValue(voteClosing);
        dest.writeValue(awardAnnouncement);
        dest.writeValue(status);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(image_file);
        dest.writeValue(votes_count);
        dest.writeValue(is_participated);
        dest.writeValue(last_voted);
        dest.writeList(participants);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "CampaignDataModel{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", starting='" + starting + '\'' +
                ", closing='" + closing + '\'' +
                ", registrationOpening='" + registrationOpening + '\'' +
                ", registrationClosing='" + registrationClosing + '\'' +
                ", voteOpening='" + voteOpening + '\'' +
                ", voteClosing='" + voteClosing + '\'' +
                ", awardAnnouncement='" + awardAnnouncement + '\'' +
                ", status=" + status +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", image_file='" + image_file + '\'' +
                ", votes_count=" + votes_count +
                ", is_participated=" + is_participated +
                ", last_voted=" + last_voted +
                ", participants=" + participants +
                '}';
    }
}