package in.aeonplay.model.Shots;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShotModel implements Parcelable {

    @SerializedName("data")
    @Expose
    private ShotDataModel shotDataModel;
    public final static Creator<ShotModel> CREATOR = new Creator<ShotModel>() {

        public ShotModel createFromParcel(android.os.Parcel in) {
            return new ShotModel(in);
        }

        public ShotModel[] newArray(int size) {
            return (new ShotModel[size]);
        }

    };

    @SuppressWarnings({
            "unchecked"
    })
    protected ShotModel(android.os.Parcel in) {
        this.shotDataModel = ((ShotDataModel) in.readValue((ShotDataModel.class.getClassLoader())));
    }

    public ShotModel() {
    }

    public ShotDataModel getData() {
        return shotDataModel;
    }

    public void setData(ShotDataModel shotDataModel) {
        this.shotDataModel = shotDataModel;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(shotDataModel);
    }

    public int describeContents() {
        return 0;
    }

}