package in.aeonplay.model.Shots;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShotCampaignModel implements Parcelable {

    public final static Creator<ShotCampaignModel> CREATOR = new Creator<ShotCampaignModel>() {


        public ShotCampaignModel createFromParcel(android.os.Parcel in) {
            return new ShotCampaignModel(in);
        }

        public ShotCampaignModel[] newArray(int size) {
            return (new ShotCampaignModel[size]);
        }

    };
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("starting")
    @Expose
    private String starting;
    @SerializedName("closing")
    @Expose
    private String closing;
    @SerializedName("registration_opening")
    @Expose
    private String registrationOpening;
    @SerializedName("registration_closing")
    @Expose
    private String registrationClosing;
    @SerializedName("vote_opening")
    @Expose
    private String voteOpening;
    @SerializedName("vote_closing")
    @Expose
    private String voteClosing;
    @SerializedName("award_announcement")
    @Expose
    private String awardAnnouncement;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("image_file")
    @Expose
    private String imageFile;
    @SerializedName("votes_count")
    @Expose
    private Integer votesCount;
    @SerializedName("is_participated")
    @Expose
    private Boolean isParticipated;
    @SerializedName("last_voted")
    @Expose
    private ShotLastVotedModel lastVoted;
    @SerializedName("participants")
    @Expose
    private List<ShotParticipantModel> participants;

    @SuppressWarnings({
            "unchecked"
    })
    protected ShotCampaignModel(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.starting = ((String) in.readValue((String.class.getClassLoader())));
        this.closing = ((String) in.readValue((String.class.getClassLoader())));
        this.registrationOpening = ((String) in.readValue((String.class.getClassLoader())));
        this.registrationClosing = ((String) in.readValue((String.class.getClassLoader())));
        this.voteOpening = ((String) in.readValue((String.class.getClassLoader())));
        this.voteClosing = ((String) in.readValue((String.class.getClassLoader())));
        this.awardAnnouncement = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.imageFile = ((String) in.readValue((String.class.getClassLoader())));
        this.votesCount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.isParticipated = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.lastVoted = ((ShotLastVotedModel) in.readValue((ShotLastVotedModel.class.getClassLoader())));
        in.readList(this.participants, (ShotParticipantModel.class.getClassLoader()));
    }

    public ShotCampaignModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStarting() {
        return starting;
    }

    public void setStarting(String starting) {
        this.starting = starting;
    }

    public String getClosing() {
        return closing;
    }

    public void setClosing(String closing) {
        this.closing = closing;
    }

    public String getRegistrationOpening() {
        return registrationOpening;
    }

    public void setRegistrationOpening(String registrationOpening) {
        this.registrationOpening = registrationOpening;
    }

    public String getRegistrationClosing() {
        return registrationClosing;
    }

    public void setRegistrationClosing(String registrationClosing) {
        this.registrationClosing = registrationClosing;
    }

    public String getVoteOpening() {
        return voteOpening;
    }

    public void setVoteOpening(String voteOpening) {
        this.voteOpening = voteOpening;
    }

    public String getVoteClosing() {
        return voteClosing;
    }

    public void setVoteClosing(String voteClosing) {
        this.voteClosing = voteClosing;
    }

    public String getAwardAnnouncement() {
        return awardAnnouncement;
    }

    public void setAwardAnnouncement(String awardAnnouncement) {
        this.awardAnnouncement = awardAnnouncement;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getImageFile() {
        return imageFile;
    }

    public void setImageFile(String imageFile) {
        this.imageFile = imageFile;
    }

    public Integer getVotesCount() {
        return votesCount;
    }

    public void setVotesCount(Integer votesCount) {
        this.votesCount = votesCount;
    }

    public Boolean getIsParticipated() {
        return isParticipated;
    }

    public void setIsParticipated(Boolean isParticipated) {
        this.isParticipated = isParticipated;
    }

    public ShotLastVotedModel getLastVoted() {
        return lastVoted;
    }

    public void setLastVoted(ShotLastVotedModel lastVoted) {
        this.lastVoted = lastVoted;
    }

    public List<ShotParticipantModel> getParticipants() {
        return participants;
    }

    public void setParticipants(List<ShotParticipantModel> participants) {
        this.participants = participants;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(title);
        dest.writeValue(description);
        dest.writeValue(starting);
        dest.writeValue(closing);
        dest.writeValue(registrationOpening);
        dest.writeValue(registrationClosing);
        dest.writeValue(voteOpening);
        dest.writeValue(voteClosing);
        dest.writeValue(awardAnnouncement);
        dest.writeValue(status);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(imageFile);
        dest.writeValue(votesCount);
        dest.writeValue(isParticipated);
        dest.writeValue(lastVoted);
        dest.writeList(participants);
    }

    public int describeContents() {
        return 0;
    }

}