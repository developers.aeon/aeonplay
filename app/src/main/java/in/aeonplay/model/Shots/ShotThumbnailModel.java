package in.aeonplay.model.Shots;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShotThumbnailModel implements Parcelable {

    public final static Creator<ShotThumbnailModel> CREATOR = new Creator<ShotThumbnailModel>() {


        public ShotThumbnailModel createFromParcel(android.os.Parcel in) {
            return new ShotThumbnailModel(in);
        }

        public ShotThumbnailModel[] newArray(int size) {
            return (new ShotThumbnailModel[size]);
        }

    };
    @SerializedName("portrait")
    @Expose
    private ShotPortraitModel portrait;

    @SuppressWarnings({
            "unchecked"
    })
    protected ShotThumbnailModel(android.os.Parcel in) {
        this.portrait = ((ShotPortraitModel) in.readValue((ShotPortraitModel.class.getClassLoader())));
    }

    public ShotThumbnailModel() {
    }

    public ShotPortraitModel getPortrait() {
        return portrait;
    }

    public void setPortrait(ShotPortraitModel portrait) {
        this.portrait = portrait;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(portrait);
    }

    public int describeContents() {
        return 0;
    }

}
