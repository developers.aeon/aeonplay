package in.aeonplay.model.Shots;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShotSubDataModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("published_at")
    @Expose
    private Object publishedAt;
    @SerializedName("unpublished_at")
    @Expose
    private Object unpublishedAt;
    @SerializedName("approved_at")
    @Expose
    private Object approvedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("playback_url")
    @Expose
    private String playbackUrl;
    public final static Creator<ShotSubDataModel> CREATOR = new Creator<ShotSubDataModel>() {


        public ShotSubDataModel createFromParcel(android.os.Parcel in) {
            return new ShotSubDataModel(in);
        }

        public ShotSubDataModel[] newArray(int size) {
            return (new ShotSubDataModel[size]);
        }

    };
    @SerializedName("download_url")
    @Expose
    private String downloadUrl;
    @SerializedName("campaign_id")
    @Expose
    private String campaignId;
    @SerializedName("likes_count")
    @Expose
    private int likes_count;
    @SerializedName("dislikes_count")
    @Expose
    private int dislikes_count;
    @SerializedName("is_liked")
    @Expose
    private boolean is_liked;
    @SerializedName("is_disliked")
    @Expose
    private boolean is_disliked;
    @SerializedName("is_following")
    @Expose
    private boolean is_following;
    @SerializedName("campaign")
    @Expose
    private ShotCampaignModel campaign;
    @SerializedName("thumbnail")
    @Expose
    private ShotThumbnailModel thumbnail;

    @SuppressWarnings({
            "unchecked"
    })
    protected ShotSubDataModel(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.userId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.publishedAt = ((Object) in.readValue((Object.class.getClassLoader())));
        this.unpublishedAt = ((Object) in.readValue((Object.class.getClassLoader())));
        this.approvedAt = ((Object) in.readValue((Object.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.deletedAt = ((Object) in.readValue((Object.class.getClassLoader())));
        this.playbackUrl = ((String) in.readValue((String.class.getClassLoader())));
        this.downloadUrl = ((String) in.readValue((String.class.getClassLoader())));
        this.campaignId = ((String) in.readValue((String.class.getClassLoader())));
        this.likes_count = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.dislikes_count = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.is_liked = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.is_disliked = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.is_following = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.campaign = ((ShotCampaignModel) in.readValue((ShotCampaignModel.class.getClassLoader())));
        this.thumbnail = ((ShotThumbnailModel) in.readValue((ShotThumbnailModel.class.getClassLoader())));
    }

    public ShotSubDataModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(Object publishedAt) {
        this.publishedAt = publishedAt;
    }

    public Object getUnpublishedAt() {
        return unpublishedAt;
    }

    public void setUnpublishedAt(Object unpublishedAt) {
        this.unpublishedAt = unpublishedAt;
    }

    public Object getApprovedAt() {
        return approvedAt;
    }

    public void setApprovedAt(Object approvedAt) {
        this.approvedAt = approvedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getPlaybackUrl() {
        return playbackUrl;
    }

    public void setPlaybackUrl(String playbackUrl) {
        this.playbackUrl = playbackUrl;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public int getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(int likes_count) {
        this.likes_count = likes_count;
    }

    public int getDislikes_count() {
        return dislikes_count;
    }

    public void setDislikes_count(int dislikes_count) {
        this.dislikes_count = dislikes_count;
    }

    public boolean isIs_liked() {
        return is_liked;
    }

    public void setIs_liked(boolean is_liked) {
        this.is_liked = is_liked;
    }

    public boolean isIs_disliked() {
        return is_disliked;
    }

    public void setIs_disliked(boolean is_disliked) {
        this.is_disliked = is_disliked;
    }

    public boolean isIs_following() {
        return is_following;
    }

    public void setIs_following(boolean is_following) {
        this.is_following = is_following;
    }

    public ShotCampaignModel getCampaign() {
        return campaign;
    }

    public void setCampaign(ShotCampaignModel campaign) {
        this.campaign = campaign;
    }

    public ShotThumbnailModel getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(ShotThumbnailModel thumbnail) {
        this.thumbnail = thumbnail;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(userId);
        dest.writeValue(title);
        dest.writeValue(description);
        dest.writeValue(publishedAt);
        dest.writeValue(unpublishedAt);
        dest.writeValue(approvedAt);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(deletedAt);
        dest.writeValue(playbackUrl);
        dest.writeValue(downloadUrl);
        dest.writeValue(campaignId);
        dest.writeValue(likes_count);
        dest.writeValue(dislikes_count);
        dest.writeValue(is_liked);
        dest.writeValue(is_disliked);
        dest.writeValue(is_following);
        dest.writeValue(campaign);
        dest.writeValue(thumbnail);
    }

    public int describeContents() {
        return 0;
    }

}