package in.aeonplay.model.Shots;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShotPortraitModel implements Parcelable {

    public final static Creator<ShotPortraitModel> CREATOR = new Creator<ShotPortraitModel>() {


        public ShotPortraitModel createFromParcel(android.os.Parcel in) {
            return new ShotPortraitModel(in);
        }

        public ShotPortraitModel[] newArray(int size) {
            return (new ShotPortraitModel[size]);
        }

    };
    @SerializedName("original")
    @Expose
    private String original;
    @SerializedName("converted")
    @Expose
    private String converted;

    @SuppressWarnings({
            "unchecked"
    })
    protected ShotPortraitModel(android.os.Parcel in) {
        this.original = ((String) in.readValue((String.class.getClassLoader())));
        this.converted = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ShotPortraitModel() {
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getConverted() {
        return converted;
    }

    public void setConverted(String converted) {
        this.converted = converted;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(original);
        dest.writeValue(converted);
    }

    public int describeContents() {
        return 0;
    }

}
