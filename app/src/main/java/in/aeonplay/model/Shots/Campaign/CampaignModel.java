package in.aeonplay.model.Shots.Campaign;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CampaignModel implements Parcelable {

    public final static Creator<CampaignModel> CREATOR = new Creator<CampaignModel>() {


        public CampaignModel createFromParcel(android.os.Parcel in) {
            return new CampaignModel(in);
        }

        public CampaignModel[] newArray(int size) {
            return (new CampaignModel[size]);
        }

    };
    @SerializedName("data")
    @Expose
    private CampaignDataModel data;

    @SuppressWarnings({
            "unchecked"
    })
    protected CampaignModel(android.os.Parcel in) {
        this.data = ((CampaignDataModel) in.readValue((CampaignDataModel.class.getClassLoader())));
    }

    public CampaignModel() {
    }

    public CampaignDataModel getData() {
        return data;
    }

    public void setData(CampaignDataModel data) {
        this.data = data;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }

}