package in.aeonplay.model.Shots.Campaign;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CampaignListModel implements Parcelable {

    public final static Creator<CampaignListModel> CREATOR = new Creator<CampaignListModel>() {


        public CampaignListModel createFromParcel(android.os.Parcel in) {
            return new CampaignListModel(in);
        }

        public CampaignListModel[] newArray(int size) {
            return (new CampaignListModel[size]);
        }

    };
    @SerializedName("data")
    @Expose
    private List<CampaignDataModel> data;

    @SuppressWarnings({
            "unchecked"
    })
    protected CampaignListModel(android.os.Parcel in) {
        in.readList(this.data, (CampaignDataModel.class.getClassLoader()));
    }

    public CampaignListModel() {
    }

    public List<CampaignDataModel> getData() {
        return data;
    }

    public void setData(List<CampaignDataModel> data) {
        this.data = data;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeList(data);
    }

    public int describeContents() {
        return 0;
    }

}