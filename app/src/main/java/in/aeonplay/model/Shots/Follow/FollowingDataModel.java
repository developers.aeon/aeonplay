package in.aeonplay.model.Shots.Follow;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.aeonplay.model.SubscriptionModel.Pivot;

public class FollowingDataModel implements Parcelable {

    public final static Creator<FollowingDataModel> CREATOR = new Creator<FollowingDataModel>() {


        public FollowingDataModel createFromParcel(android.os.Parcel in) {
            return new FollowingDataModel(in);
        }

        public FollowingDataModel[] newArray(int size) {
            return (new FollowingDataModel[size]);
        }

    };
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("middle_name")
    @Expose
    private Object middleName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;
    @SerializedName("email")
    @Expose
    private Object email;
    @SerializedName("avatar")
    @Expose
    private Object avatar;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("gender")
    @Expose
    private Object gender;
    @SerializedName("provider")
    @Expose
    private Object provider;
    @SerializedName("provider_id")
    @Expose
    private Object providerId;
    @SerializedName("provider_access_token")
    @Expose
    private Object providerAccessToken;
    @SerializedName("email_verified_at")
    @Expose
    private Object emailVerifiedAt;
    @SerializedName("mobile_verified_at")
    @Expose
    private Object mobileVerifiedAt;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("mobile_network_operator")
    @Expose
    private String mobileNetworkOperator;
    @SerializedName("mnc")
    @Expose
    private String mnc;
    @SerializedName("mcc")
    @Expose
    private String mcc;
    @SerializedName("mobile_number_registered_location")
    @Expose
    private String mobileNumberRegisteredLocation;
    @SerializedName("pivot")
    @Expose
    private Pivot pivot;

    @SuppressWarnings({
            "unchecked"
    })
    protected FollowingDataModel(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.firstName = ((String) in.readValue((String.class.getClassLoader())));
        this.middleName = ((Object) in.readValue((Object.class.getClassLoader())));
        this.lastName = ((String) in.readValue((String.class.getClassLoader())));
        this.mobileNo = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((Object) in.readValue((Object.class.getClassLoader())));
        this.avatar = ((Object) in.readValue((Object.class.getClassLoader())));
        this.dob = ((String) in.readValue((String.class.getClassLoader())));
        this.gender = ((Object) in.readValue((Object.class.getClassLoader())));
        this.provider = ((Object) in.readValue((Object.class.getClassLoader())));
        this.providerId = ((Object) in.readValue((Object.class.getClassLoader())));
        this.providerAccessToken = ((Object) in.readValue((Object.class.getClassLoader())));
        this.emailVerifiedAt = ((Object) in.readValue((Object.class.getClassLoader())));
        this.mobileVerifiedAt = ((Object) in.readValue((Object.class.getClassLoader())));
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.mobileNetworkOperator = ((String) in.readValue((String.class.getClassLoader())));
        this.mnc = ((String) in.readValue((String.class.getClassLoader())));
        this.mcc = ((String) in.readValue((String.class.getClassLoader())));
        this.mobileNumberRegisteredLocation = ((String) in.readValue((String.class.getClassLoader())));
        this.pivot = ((Pivot) in.readValue((Pivot.class.getClassLoader())));
    }

    public FollowingDataModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Object getMiddleName() {
        return middleName;
    }

    public void setMiddleName(Object middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

    public Object getAvatar() {
        return avatar;
    }

    public void setAvatar(Object avatar) {
        this.avatar = avatar;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Object getGender() {
        return gender;
    }

    public void setGender(Object gender) {
        this.gender = gender;
    }

    public Object getProvider() {
        return provider;
    }

    public void setProvider(Object provider) {
        this.provider = provider;
    }

    public Object getProviderId() {
        return providerId;
    }

    public void setProviderId(Object providerId) {
        this.providerId = providerId;
    }

    public Object getProviderAccessToken() {
        return providerAccessToken;
    }

    public void setProviderAccessToken(Object providerAccessToken) {
        this.providerAccessToken = providerAccessToken;
    }

    public Object getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setEmailVerifiedAt(Object emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public Object getMobileVerifiedAt() {
        return mobileVerifiedAt;
    }

    public void setMobileVerifiedAt(Object mobileVerifiedAt) {
        this.mobileVerifiedAt = mobileVerifiedAt;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getMobileNetworkOperator() {
        return mobileNetworkOperator;
    }

    public void setMobileNetworkOperator(String mobileNetworkOperator) {
        this.mobileNetworkOperator = mobileNetworkOperator;
    }

    public String getMnc() {
        return mnc;
    }

    public void setMnc(String mnc) {
        this.mnc = mnc;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getMobileNumberRegisteredLocation() {
        return mobileNumberRegisteredLocation;
    }

    public void setMobileNumberRegisteredLocation(String mobileNumberRegisteredLocation) {
        this.mobileNumberRegisteredLocation = mobileNumberRegisteredLocation;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(firstName);
        dest.writeValue(middleName);
        dest.writeValue(lastName);
        dest.writeValue(mobileNo);
        dest.writeValue(email);
        dest.writeValue(avatar);
        dest.writeValue(dob);
        dest.writeValue(gender);
        dest.writeValue(provider);
        dest.writeValue(providerId);
        dest.writeValue(providerAccessToken);
        dest.writeValue(emailVerifiedAt);
        dest.writeValue(mobileVerifiedAt);
        dest.writeValue(status);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(mobileNetworkOperator);
        dest.writeValue(mnc);
        dest.writeValue(mcc);
        dest.writeValue(mobileNumberRegisteredLocation);
    }

    public int describeContents() {
        return 0;
    }

}
