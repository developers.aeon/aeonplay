package in.aeonplay.model.Shots;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShotLastVotedModel implements Parcelable {

    public final static Creator<ShotLastVotedModel> CREATOR = new Creator<ShotLastVotedModel>() {


        public ShotLastVotedModel createFromParcel(android.os.Parcel in) {
            return new ShotLastVotedModel(in);
        }

        public ShotLastVotedModel[] newArray(int size) {
            return (new ShotLastVotedModel[size]);
        }

    };
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("short_id")
    @Expose
    private Integer shortId;
    @SerializedName("campaign_id")
    @Expose
    private Integer campaignId;
    @SerializedName("voted_by")
    @Expose
    private Integer votedBy;
    @SerializedName("voted_to")
    @Expose
    private Integer votedTo;
    @SerializedName("voted_at")
    @Expose
    private String votedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("next_vote")
    @Expose
    private String nextVote;

    @SuppressWarnings({
            "unchecked"
    })
    protected ShotLastVotedModel(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.shortId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.campaignId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.votedBy = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.votedTo = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.votedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.nextVote = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ShotLastVotedModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getShortId() {
        return shortId;
    }

    public void setShortId(Integer shortId) {
        this.shortId = shortId;
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    public Integer getVotedBy() {
        return votedBy;
    }

    public void setVotedBy(Integer votedBy) {
        this.votedBy = votedBy;
    }

    public Integer getVotedTo() {
        return votedTo;
    }

    public void setVotedTo(Integer votedTo) {
        this.votedTo = votedTo;
    }

    public String getVotedAt() {
        return votedAt;
    }

    public void setVotedAt(String votedAt) {
        this.votedAt = votedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getNextVote() {
        return nextVote;
    }

    public void setNextVote(String nextVote) {
        this.nextVote = nextVote;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(shortId);
        dest.writeValue(campaignId);
        dest.writeValue(votedBy);
        dest.writeValue(votedTo);
        dest.writeValue(votedAt);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(nextVote);
    }

    public int describeContents() {
        return 0;
    }

}
