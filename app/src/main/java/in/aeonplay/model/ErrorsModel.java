package in.aeonplay.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ErrorsModel implements Serializable {

    @SerializedName("errors")
    @Expose
    private ErrorsDataModel errors;
    private final static long serialVersionUID = -8423124292238004784L;

    public ErrorsDataModel getErrors() {
        return errors;
    }

    public void setErrors(ErrorsDataModel errors) {
        this.errors = errors;
    }

}
