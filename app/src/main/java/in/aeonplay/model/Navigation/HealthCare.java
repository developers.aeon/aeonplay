package in.aeonplay.model.Navigation;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HealthCare implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    public final static Creator<HealthCare> CREATOR = new Creator<HealthCare>() {


        @SuppressWarnings({
                "unchecked"
        })
        public HealthCare createFromParcel(android.os.Parcel in) {
            return new HealthCare(in);
        }

        public HealthCare[] newArray(int size) {
            return (new HealthCare[size]);
        }

    };

    protected HealthCare(android.os.Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
    }

    public HealthCare() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
    }

    public int describeContents() {
        return 0;
    }

}