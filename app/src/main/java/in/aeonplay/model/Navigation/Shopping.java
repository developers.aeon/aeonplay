package in.aeonplay.model.Navigation;


import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shopping implements Parcelable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    public final static Creator<Shopping> CREATOR = new Creator<Shopping>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Shopping createFromParcel(android.os.Parcel in) {
            return new Shopping(in);
        }

        public Shopping[] newArray(int size) {
            return (new Shopping[size]);
        }

    }
            ;

    protected Shopping(android.os.Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Shopping() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
    }

    public int describeContents() {
        return 0;
    }

}