package in.aeonplay.model.Navigation;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Datum implements Parcelable
{

    @SerializedName("entertainment")
    @Expose
    private List<Entertainment> entertainment = null;
    @SerializedName("education")
    @Expose
    private List<Education> education = null;
    @SerializedName("health care")
    @Expose
    private List<HealthCare> healthCare = null;
    @SerializedName("shopping")
    @Expose
    private List<Shopping> shopping = null;
    @SerializedName("utility")
    @Expose
    private List<Utility> utility = null;
    public final static Creator<Datum> CREATOR = new Creator<Datum>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Datum createFromParcel(android.os.Parcel in) {
            return new Datum(in);
        }

        public Datum[] newArray(int size) {
            return (new Datum[size]);
        }

    }
            ;

    protected Datum(android.os.Parcel in) {
        in.readList(this.entertainment, (Entertainment.class.getClassLoader()));
        in.readList(this.education, (Education.class.getClassLoader()));
        in.readList(this.healthCare, (HealthCare.class.getClassLoader()));
        in.readList(this.shopping, (Shopping.class.getClassLoader()));
        in.readList(this.utility, (Utility.class.getClassLoader()));
    }

    public Datum() {
    }

    public List<Entertainment> getEntertainment() {
        return entertainment;
    }

    public void setEntertainment(List<Entertainment> entertainment) {
        this.entertainment = entertainment;
    }

    public List<Education> getEducation() {
        return education;
    }

    public void setEducation(List<Education> education) {
        this.education = education;
    }

    public List<HealthCare> getHealthCare() {
        return healthCare;
    }

    public void setHealthCare(List<HealthCare> healthCare) {
        this.healthCare = healthCare;
    }

    public List<Shopping> getShopping() {
        return shopping;
    }

    public void setShopping(List<Shopping> shopping) {
        this.shopping = shopping;
    }

    public List<Utility> getUtility() {
        return utility;
    }

    public void setUtility(List<Utility> utility) {
        this.utility = utility;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeList(entertainment);
        dest.writeList(education);
        dest.writeList(healthCare);
        dest.writeList(shopping);
        dest.writeList(utility);
    }

    public int describeContents() {
        return 0;
    }

}