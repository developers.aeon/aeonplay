package in.aeonplay.model.PlaybackModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlaybackDataModel implements Parcelable {

    @SerializedName("playback")
    @Expose
    private Playback playback;
    public final static Creator<PlaybackDataModel> CREATOR = new Creator<PlaybackDataModel>() {

        @SuppressWarnings({
                "unchecked"
        })
        public PlaybackDataModel createFromParcel(android.os.Parcel in) {
            return new PlaybackDataModel(in);
        }

        public PlaybackDataModel[] newArray(int size) {
            return (new PlaybackDataModel[size]);
        }

    };

    protected PlaybackDataModel(android.os.Parcel in) {
        this.playback = ((Playback) in.readValue((Playback.class.getClassLoader())));
    }

    public PlaybackDataModel() {
    }

    public Playback getPlayback() {
        return playback;
    }

    public void setPlayback(Playback playback) {
        this.playback = playback;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(playback);
    }

    public int describeContents() {
        return 0;
    }

}