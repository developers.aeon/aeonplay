package in.aeonplay.model.PlaybackModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Playback implements Parcelable {

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("url")
    @Expose
    private String url;
    public final static Creator<Playback> CREATOR = new Creator<Playback>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Playback createFromParcel(android.os.Parcel in) {
            return new Playback(in);
        }

        public Playback[] newArray(int size) {
            return (new Playback[size]);
        }

    };

    protected Playback(android.os.Parcel in) {
        this.token = ((String) in.readValue((String.class.getClassLoader())));
        this.url = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Playback() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(token);
        dest.writeValue(url);
    }

    public int describeContents() {
        return 0;
    }

}