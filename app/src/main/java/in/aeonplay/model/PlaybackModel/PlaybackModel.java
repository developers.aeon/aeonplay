package in.aeonplay.model.PlaybackModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlaybackModel implements Parcelable {

    @SerializedName("data")
    @Expose
    private PlaybackDataModel playbackDataModel;
    public final static Creator<PlaybackModel> CREATOR = new Creator<PlaybackModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PlaybackModel createFromParcel(android.os.Parcel in) {
            return new PlaybackModel(in);
        }

        public PlaybackModel[] newArray(int size) {
            return (new PlaybackModel[size]);
        }

    };

    protected PlaybackModel(android.os.Parcel in) {
        this.playbackDataModel = ((PlaybackDataModel) in.readValue((PlaybackDataModel.class.getClassLoader())));
    }

    public PlaybackModel() {
    }

    public PlaybackDataModel getData() {
        return playbackDataModel;
    }

    public void setData(PlaybackDataModel playbackDataModel) {
        this.playbackDataModel = playbackDataModel;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(playbackDataModel);
    }

    public int describeContents() {
        return 0;
    }

}
