package in.aeonplay.model.SubscriptionModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payment implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("txnid")
    @Expose
    private String txnid;
    @SerializedName("paymentid")
    @Expose
    private String paymentid;
    @SerializedName("paid_amount")
    @Expose
    private Integer paidAmount;
    @SerializedName("refunded")
    @Expose
    private Integer refunded;
    @SerializedName("refunded_amount")
    @Expose
    private Object refundedAmount;
    @SerializedName("rawPaymentResponse")
    @Expose
    private RawPaymentResponse rawPaymentResponse;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    public final static Creator<Payment> CREATOR = new Creator<Payment>() {


        public Payment createFromParcel(android.os.Parcel in) {
            return new Payment(in);
        }

        public Payment[] newArray(int size) {
            return (new Payment[size]);
        }

    };

    @SuppressWarnings({
            "unchecked"
    })
    protected Payment(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.userId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.txnid = ((String) in.readValue((String.class.getClassLoader())));
        this.paymentid = ((String) in.readValue((String.class.getClassLoader())));
        this.paidAmount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.refunded = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.refundedAmount = ((Object) in.readValue((Object.class.getClassLoader())));
        this.rawPaymentResponse = ((RawPaymentResponse) in.readValue((RawPaymentResponse.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Payment() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTxnid() {
        return txnid;
    }

    public void setTxnid(String txnid) {
        this.txnid = txnid;
    }

    public String getPaymentid() {
        return paymentid;
    }

    public void setPaymentid(String paymentid) {
        this.paymentid = paymentid;
    }

    public Integer getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(Integer paidAmount) {
        this.paidAmount = paidAmount;
    }

    public Integer getRefunded() {
        return refunded;
    }

    public void setRefunded(Integer refunded) {
        this.refunded = refunded;
    }

    public Object getRefundedAmount() {
        return refundedAmount;
    }

    public void setRefundedAmount(Object refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    public RawPaymentResponse getRawPaymentResponse() {
        return rawPaymentResponse;
    }

    public void setRawPaymentResponse(RawPaymentResponse rawPaymentResponse) {
        this.rawPaymentResponse = rawPaymentResponse;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(userId);
        dest.writeValue(txnid);
        dest.writeValue(paymentid);
        dest.writeValue(paidAmount);
        dest.writeValue(refunded);
        dest.writeValue(refundedAmount);
        dest.writeValue(rawPaymentResponse);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
    }

    public int describeContents() {
        return 0;
    }

}