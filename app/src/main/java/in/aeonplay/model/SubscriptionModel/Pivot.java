package in.aeonplay.model.SubscriptionModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pivot implements Parcelable
{

    @SerializedName("package_id")
    @Expose
    private Integer packageId;
    @SerializedName("content_provider_id")
    @Expose
    private Integer contentProviderId;
    @SerializedName("validity")
    @Expose
    private Integer validity;
    public final static Creator<Pivot> CREATOR = new Creator<Pivot>() {


        public Pivot createFromParcel(android.os.Parcel in) {
            return new Pivot(in);
        }

        public Pivot[] newArray(int size) {
            return (new Pivot[size]);
        }

    }
            ;

    @SuppressWarnings({
            "unchecked"
    })
    protected Pivot(android.os.Parcel in) {
        this.packageId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.contentProviderId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.validity = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public Pivot() {
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public Integer getContentProviderId() {
        return contentProviderId;
    }

    public void setContentProviderId(Integer contentProviderId) {
        this.contentProviderId = contentProviderId;
    }

    public Integer getValidity() {
        return validity;
    }

    public void setValidity(Integer validity) {
        this.validity = validity;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(packageId);
        dest.writeValue(contentProviderId);
        dest.writeValue(validity);
    }

    public int describeContents() {
        return 0;
    }

}