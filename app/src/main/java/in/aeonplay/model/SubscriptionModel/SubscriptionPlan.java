package in.aeonplay.model.SubscriptionModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubscriptionPlan implements Parcelable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("description")
    @Expose
    private List<String> description;
    @SerializedName("validity")
    @Expose
    private Integer validity;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("package_group_id")
    @Expose
    private Object packageGroupId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("offer_max_count")
    @Expose
    private Integer offerMaxCount;
    @SerializedName("offer_status")
    @Expose
    private Integer offerStatus;
    @SerializedName("mobile_carrier")
    @Expose
    private String mobileCarrier;
    @SerializedName("content_access")
    @Expose
    private List<ContentAccess> contentAccess;
    @SerializedName("packagegroup")
    @Expose
    private SubscriptionPackageGroup packagegroup;
    public final static Creator<SubscriptionPlan> CREATOR = new Creator<SubscriptionPlan>() {


        public SubscriptionPlan createFromParcel(android.os.Parcel in) {
            return new SubscriptionPlan(in);
        }

        public SubscriptionPlan[] newArray(int size) {
            return (new SubscriptionPlan[size]);
        }

    }
            ;

    @SuppressWarnings({
            "unchecked"
    })
    protected SubscriptionPlan(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.amount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.description, (java.lang.String.class.getClassLoader()));
        this.validity = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.packageGroupId = ((Object) in.readValue((Object.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((String) in.readValue((String.class.getClassLoader())));
        this.offerMaxCount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.offerStatus = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.mobileCarrier = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.contentAccess, (ContentAccess.class.getClassLoader()));
        this.packagegroup = ((SubscriptionPackageGroup) in.readValue((SubscriptionPackageGroup.class.getClassLoader())));
    }

    public SubscriptionPlan() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    public Integer getValidity() {
        return validity;
    }

    public void setValidity(Integer validity) {
        this.validity = validity;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getPackageGroupId() {
        return packageGroupId;
    }

    public void setPackageGroupId(Object packageGroupId) {
        this.packageGroupId = packageGroupId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getOfferMaxCount() {
        return offerMaxCount;
    }

    public void setOfferMaxCount(Integer offerMaxCount) {
        this.offerMaxCount = offerMaxCount;
    }

    public Integer getOfferStatus() {
        return offerStatus;
    }

    public void setOfferStatus(Integer offerStatus) {
        this.offerStatus = offerStatus;
    }

    public String getMobileCarrier() {
        return mobileCarrier;
    }

    public void setMobileCarrier(String mobileCarrier) {
        this.mobileCarrier = mobileCarrier;
    }

    public List<ContentAccess> getContentAccess() {
        return contentAccess;
    }

    public void setContentAccess(List<ContentAccess> contentAccess) {
        this.contentAccess = contentAccess;
    }

    public SubscriptionPackageGroup getPackagegroup() {
        return packagegroup;
    }

    public void setPackagegroup(SubscriptionPackageGroup packagegroup) {
        this.packagegroup = packagegroup;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(title);
        dest.writeValue(amount);
        dest.writeList(description);
        dest.writeValue(validity);
        dest.writeValue(status);
        dest.writeValue(packageGroupId);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(type);
        dest.writeValue(offerMaxCount);
        dest.writeValue(offerStatus);
        dest.writeValue(mobileCarrier);
        dest.writeList(contentAccess);
        dest.writeValue(packagegroup);
    }

    public int describeContents() {
        return 0;
    }

}