package in.aeonplay.model.SubscriptionModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubscriptionModel implements Parcelable
{

    @SerializedName("data")
    @Expose
    private Data data;
    public final static Creator<SubscriptionModel> CREATOR = new Creator<SubscriptionModel>() {


        public SubscriptionModel createFromParcel(android.os.Parcel in) {
            return new SubscriptionModel(in);
        }

        public SubscriptionModel[] newArray(int size) {
            return (new SubscriptionModel[size]);
        }

    }
            ;

    @SuppressWarnings({
            "unchecked"
    })
    protected SubscriptionModel(android.os.Parcel in) {
        this.data = ((Data) in.readValue((Data.class.getClassLoader())));
    }

    public SubscriptionModel() {
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }

}