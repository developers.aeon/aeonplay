package in.aeonplay.model.SubscriptionModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RawPaymentResponse implements Parcelable {

    @SerializedName("mihpayid")
    @Expose
    private String mihpayid;
    @SerializedName("mode")
    @Expose
    private String mode;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("unmappedstatus")
    @Expose
    private String unmappedstatus;
    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("txnid")
    @Expose
    private String txnid;
    @SerializedName("amount")
    @Expose
    private Object amount;
    @SerializedName("cardCategory")
    @Expose
    private String cardCategory;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("net_amount_debit")
    @Expose
    private Object netAmountDebit;
    @SerializedName("addedon")
    @Expose
    private String addedon;
    @SerializedName("productinfo")
    @Expose
    private String productinfo;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private Object lastname;
    @SerializedName("address1")
    @Expose
    private Object address1;
    @SerializedName("address2")
    @Expose
    private Object address2;
    @SerializedName("city")
    @Expose
    private Object city;
    @SerializedName("state")
    @Expose
    private Object state;
    @SerializedName("country")
    @Expose
    private Object country;
    @SerializedName("zipcode")
    @Expose
    private Object zipcode;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("udf1")
    @Expose
    private Object udf1;
    @SerializedName("udf2")
    @Expose
    private Object udf2;
    @SerializedName("udf3")
    @Expose
    private String udf3;
    @SerializedName("udf4")
    @Expose
    private String udf4;
    @SerializedName("udf5")
    @Expose
    private String udf5;
    @SerializedName("udf6")
    @Expose
    private Object udf6;
    @SerializedName("udf7")
    @Expose
    private Object udf7;
    @SerializedName("udf8")
    @Expose
    private Object udf8;
    @SerializedName("udf9")
    @Expose
    private Object udf9;
    @SerializedName("udf10")
    @Expose
    private Object udf10;
    @SerializedName("hash")
    @Expose
    private String hash;
    @SerializedName("field1")
    @Expose
    private String field1;
    @SerializedName("field2")
    @Expose
    private String field2;
    @SerializedName("field3")
    @Expose
    private String field3;
    @SerializedName("field4")
    @Expose
    private Object field4;
    @SerializedName("field5")
    @Expose
    private String field5;
    @SerializedName("field6")
    @Expose
    private String field6;
    @SerializedName("field7")
    @Expose
    private String field7;
    @SerializedName("field8")
    @Expose
    private Object field8;
    @SerializedName("field9")
    @Expose
    private String field9;
    @SerializedName("payment_source")
    @Expose
    private String paymentSource;
    @SerializedName("PG_TYPE")
    @Expose
    private String pgType;
    @SerializedName("bank_ref_num")
    @Expose
    private Object bankRefNum;
    @SerializedName("bankcode")
    @Expose
    private String bankcode;
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("error_Message")
    @Expose
    private String errorMessage;
    @SerializedName("cardnum")
    @Expose
    private String cardnum;
    @SerializedName("cardhash")
    @Expose
    private String cardhash;
    @SerializedName("device_type")
    @Expose
    private String deviceType;
    public final static Creator<RawPaymentResponse> CREATOR = new Creator<RawPaymentResponse>() {


        public RawPaymentResponse createFromParcel(android.os.Parcel in) {
            return new RawPaymentResponse(in);
        }

        public RawPaymentResponse[] newArray(int size) {
            return (new RawPaymentResponse[size]);
        }

    };

    @SuppressWarnings({
            "unchecked"
    })
    protected RawPaymentResponse(android.os.Parcel in) {
        this.mihpayid = ((String) in.readValue((String.class.getClassLoader())));
        this.mode = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.unmappedstatus = ((String) in.readValue((String.class.getClassLoader())));
        this.key = ((String) in.readValue((String.class.getClassLoader())));
        this.txnid = ((String) in.readValue((String.class.getClassLoader())));
        this.amount = ((Object) in.readValue((Object.class.getClassLoader())));
        this.cardCategory = ((String) in.readValue((String.class.getClassLoader())));
        this.discount = ((String) in.readValue((String.class.getClassLoader())));
        this.netAmountDebit = ((Object) in.readValue((Object.class.getClassLoader())));
        this.addedon = ((String) in.readValue((String.class.getClassLoader())));
        this.productinfo = ((String) in.readValue((String.class.getClassLoader())));
        this.firstname = ((String) in.readValue((String.class.getClassLoader())));
        this.lastname = ((Object) in.readValue((Object.class.getClassLoader())));
        this.address1 = ((Object) in.readValue((Object.class.getClassLoader())));
        this.address2 = ((Object) in.readValue((Object.class.getClassLoader())));
        this.city = ((Object) in.readValue((Object.class.getClassLoader())));
        this.state = ((Object) in.readValue((Object.class.getClassLoader())));
        this.country = ((Object) in.readValue((Object.class.getClassLoader())));
        this.zipcode = ((Object) in.readValue((Object.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.phone = ((String) in.readValue((String.class.getClassLoader())));
        this.udf1 = ((Object) in.readValue((Object.class.getClassLoader())));
        this.udf2 = ((Object) in.readValue((Object.class.getClassLoader())));
        this.udf3 = ((String) in.readValue((String.class.getClassLoader())));
        this.udf4 = ((String) in.readValue((String.class.getClassLoader())));
        this.udf5 = ((String) in.readValue((String.class.getClassLoader())));
        this.udf6 = ((Object) in.readValue((Object.class.getClassLoader())));
        this.udf7 = ((Object) in.readValue((Object.class.getClassLoader())));
        this.udf8 = ((Object) in.readValue((Object.class.getClassLoader())));
        this.udf9 = ((Object) in.readValue((Object.class.getClassLoader())));
        this.udf10 = ((Object) in.readValue((Object.class.getClassLoader())));
        this.hash = ((String) in.readValue((String.class.getClassLoader())));
        this.field1 = ((String) in.readValue((String.class.getClassLoader())));
        this.field2 = ((String) in.readValue((String.class.getClassLoader())));
        this.field3 = ((String) in.readValue((String.class.getClassLoader())));
        this.field4 = ((Object) in.readValue((Object.class.getClassLoader())));
        this.field5 = ((String) in.readValue((String.class.getClassLoader())));
        this.field6 = ((String) in.readValue((String.class.getClassLoader())));
        this.field7 = ((String) in.readValue((String.class.getClassLoader())));
        this.field8 = ((Object) in.readValue((Object.class.getClassLoader())));
        this.field9 = ((String) in.readValue((String.class.getClassLoader())));
        this.paymentSource = ((String) in.readValue((String.class.getClassLoader())));
        this.pgType = ((String) in.readValue((String.class.getClassLoader())));
        this.bankRefNum = ((Object) in.readValue((Object.class.getClassLoader())));
        this.bankcode = ((String) in.readValue((String.class.getClassLoader())));
        this.error = ((String) in.readValue((String.class.getClassLoader())));
        this.errorMessage = ((String) in.readValue((String.class.getClassLoader())));
        this.cardnum = ((String) in.readValue((String.class.getClassLoader())));
        this.cardhash = ((String) in.readValue((String.class.getClassLoader())));
        this.deviceType = ((String) in.readValue((String.class.getClassLoader())));
    }

    public RawPaymentResponse() {
    }

    public String getMihpayid() {
        return mihpayid;
    }

    public void setMihpayid(String mihpayid) {
        this.mihpayid = mihpayid;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUnmappedstatus() {
        return unmappedstatus;
    }

    public void setUnmappedstatus(String unmappedstatus) {
        this.unmappedstatus = unmappedstatus;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTxnid() {
        return txnid;
    }

    public void setTxnid(String txnid) {
        this.txnid = txnid;
    }

    public Object getAmount() {
        return amount;
    }

    public void setAmount(Object amount) {
        this.amount = amount;
    }

    public String getCardCategory() {
        return cardCategory;
    }

    public void setCardCategory(String cardCategory) {
        this.cardCategory = cardCategory;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public Object getNetAmountDebit() {
        return netAmountDebit;
    }

    public void setNetAmountDebit(Object netAmountDebit) {
        this.netAmountDebit = netAmountDebit;
    }

    public String getAddedon() {
        return addedon;
    }

    public void setAddedon(String addedon) {
        this.addedon = addedon;
    }

    public String getProductinfo() {
        return productinfo;
    }

    public void setProductinfo(String productinfo) {
        this.productinfo = productinfo;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public Object getLastname() {
        return lastname;
    }

    public void setLastname(Object lastname) {
        this.lastname = lastname;
    }

    public Object getAddress1() {
        return address1;
    }

    public void setAddress1(Object address1) {
        this.address1 = address1;
    }

    public Object getAddress2() {
        return address2;
    }

    public void setAddress2(Object address2) {
        this.address2 = address2;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public Object getState() {
        return state;
    }

    public void setState(Object state) {
        this.state = state;
    }

    public Object getCountry() {
        return country;
    }

    public void setCountry(Object country) {
        this.country = country;
    }

    public Object getZipcode() {
        return zipcode;
    }

    public void setZipcode(Object zipcode) {
        this.zipcode = zipcode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Object getUdf1() {
        return udf1;
    }

    public void setUdf1(Object udf1) {
        this.udf1 = udf1;
    }

    public Object getUdf2() {
        return udf2;
    }

    public void setUdf2(Object udf2) {
        this.udf2 = udf2;
    }

    public String getUdf3() {
        return udf3;
    }

    public void setUdf3(String udf3) {
        this.udf3 = udf3;
    }

    public String getUdf4() {
        return udf4;
    }

    public void setUdf4(String udf4) {
        this.udf4 = udf4;
    }

    public String getUdf5() {
        return udf5;
    }

    public void setUdf5(String udf5) {
        this.udf5 = udf5;
    }

    public Object getUdf6() {
        return udf6;
    }

    public void setUdf6(Object udf6) {
        this.udf6 = udf6;
    }

    public Object getUdf7() {
        return udf7;
    }

    public void setUdf7(Object udf7) {
        this.udf7 = udf7;
    }

    public Object getUdf8() {
        return udf8;
    }

    public void setUdf8(Object udf8) {
        this.udf8 = udf8;
    }

    public Object getUdf9() {
        return udf9;
    }

    public void setUdf9(Object udf9) {
        this.udf9 = udf9;
    }

    public Object getUdf10() {
        return udf10;
    }

    public void setUdf10(Object udf10) {
        this.udf10 = udf10;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public String getField2() {
        return field2;
    }

    public void setField2(String field2) {
        this.field2 = field2;
    }

    public String getField3() {
        return field3;
    }

    public void setField3(String field3) {
        this.field3 = field3;
    }

    public Object getField4() {
        return field4;
    }

    public void setField4(Object field4) {
        this.field4 = field4;
    }

    public String getField5() {
        return field5;
    }

    public void setField5(String field5) {
        this.field5 = field5;
    }

    public String getField6() {
        return field6;
    }

    public void setField6(String field6) {
        this.field6 = field6;
    }

    public String getField7() {
        return field7;
    }

    public void setField7(String field7) {
        this.field7 = field7;
    }

    public Object getField8() {
        return field8;
    }

    public void setField8(Object field8) {
        this.field8 = field8;
    }

    public String getField9() {
        return field9;
    }

    public void setField9(String field9) {
        this.field9 = field9;
    }

    public String getPaymentSource() {
        return paymentSource;
    }

    public void setPaymentSource(String paymentSource) {
        this.paymentSource = paymentSource;
    }

    public String getPgType() {
        return pgType;
    }

    public void setPgType(String pgType) {
        this.pgType = pgType;
    }

    public Object getBankRefNum() {
        return bankRefNum;
    }

    public void setBankRefNum(Object bankRefNum) {
        this.bankRefNum = bankRefNum;
    }

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getCardnum() {
        return cardnum;
    }

    public void setCardnum(String cardnum) {
        this.cardnum = cardnum;
    }

    public String getCardhash() {
        return cardhash;
    }

    public void setCardhash(String cardhash) {
        this.cardhash = cardhash;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(mihpayid);
        dest.writeValue(mode);
        dest.writeValue(status);
        dest.writeValue(unmappedstatus);
        dest.writeValue(key);
        dest.writeValue(txnid);
        dest.writeValue(amount);
        dest.writeValue(cardCategory);
        dest.writeValue(discount);
        dest.writeValue(netAmountDebit);
        dest.writeValue(addedon);
        dest.writeValue(productinfo);
        dest.writeValue(firstname);
        dest.writeValue(lastname);
        dest.writeValue(address1);
        dest.writeValue(address2);
        dest.writeValue(city);
        dest.writeValue(state);
        dest.writeValue(country);
        dest.writeValue(zipcode);
        dest.writeValue(email);
        dest.writeValue(phone);
        dest.writeValue(udf1);
        dest.writeValue(udf2);
        dest.writeValue(udf3);
        dest.writeValue(udf4);
        dest.writeValue(udf5);
        dest.writeValue(udf6);
        dest.writeValue(udf7);
        dest.writeValue(udf8);
        dest.writeValue(udf9);
        dest.writeValue(udf10);
        dest.writeValue(hash);
        dest.writeValue(field1);
        dest.writeValue(field2);
        dest.writeValue(field3);
        dest.writeValue(field4);
        dest.writeValue(field5);
        dest.writeValue(field6);
        dest.writeValue(field7);
        dest.writeValue(field8);
        dest.writeValue(field9);
        dest.writeValue(paymentSource);
        dest.writeValue(pgType);
        dest.writeValue(bankRefNum);
        dest.writeValue(bankcode);
        dest.writeValue(error);
        dest.writeValue(errorMessage);
        dest.writeValue(cardnum);
        dest.writeValue(cardhash);
        dest.writeValue(deviceType);
    }

    public int describeContents() {
        return 0;
    }

}