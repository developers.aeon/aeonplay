package in.aeonplay.model.SubscriptionModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data implements Parcelable {

    @SerializedName("active_subscription")
    @Expose
    private List<ActiveSubscription> activeSubscription;
    @SerializedName("subscription_history")
    @Expose
    private List<SubscriptionHistory> subscriptionHistory;
    public final static Creator<Data> CREATOR = new Creator<Data>() {


        public Data createFromParcel(android.os.Parcel in) {
            return new Data(in);
        }

        public Data[] newArray(int size) {
            return (new Data[size]);
        }

    };

    @SuppressWarnings({
            "unchecked"
    })
    protected Data(android.os.Parcel in) {
        in.readList(this.activeSubscription, (ActiveSubscription.class.getClassLoader()));
        in.readList(this.subscriptionHistory, (SubscriptionHistory.class.getClassLoader()));
    }

    public Data() {
    }

    public List<ActiveSubscription> getActiveSubscription() {
        return activeSubscription;
    }

    public void setActiveSubscription(List<ActiveSubscription> activeSubscription) {
        this.activeSubscription = activeSubscription;
    }

    public List<SubscriptionHistory> getSubscriptionHistory() {
        return subscriptionHistory;
    }

    public void setSubscriptionHistory(List<SubscriptionHistory> subscriptionHistory) {
        this.subscriptionHistory = subscriptionHistory;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeList(activeSubscription);
        dest.writeList(subscriptionHistory);
    }

    public int describeContents() {
        return 0;
    }

}