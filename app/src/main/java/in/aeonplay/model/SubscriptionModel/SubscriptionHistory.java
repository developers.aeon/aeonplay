package in.aeonplay.model.SubscriptionModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubscriptionHistory implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("package_id")
    @Expose
    private Integer packageId;
    @SerializedName("payment_id")
    @Expose
    private Integer paymentId;
    @SerializedName("subscribed_at")
    @Expose
    private String subscribedAt;
    @SerializedName("expiry_at")
    @Expose
    private String expiryAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("payment")
    @Expose
    private Payment payment;
    @SerializedName("subscription_plan")
    @Expose
    private SubscriptionPlan subscriptionPlan;
    @SerializedName("subscribed_content_providers")
    @Expose
    private List<SubscribedContentProvider> subscribedContentProviders;
    public final static Creator<SubscriptionHistory> CREATOR = new Creator<SubscriptionHistory>() {


        public SubscriptionHistory createFromParcel(android.os.Parcel in) {
            return new SubscriptionHistory(in);
        }

        public SubscriptionHistory[] newArray(int size) {
            return (new SubscriptionHistory[size]);
        }

    };

    @SuppressWarnings({
            "unchecked"
    })
    protected SubscriptionHistory(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.userId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.packageId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.paymentId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.subscribedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.expiryAt = ((String) in.readValue((String.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.payment = ((Payment) in.readValue((Payment.class.getClassLoader())));
        this.subscriptionPlan = ((SubscriptionPlan) in.readValue((SubscriptionPlan.class.getClassLoader())));
        in.readList(this.subscribedContentProviders, (SubscribedContentProvider.class.getClassLoader()));
    }

    public SubscriptionHistory() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public String getSubscribedAt() {
        return subscribedAt;
    }

    public void setSubscribedAt(String subscribedAt) {
        this.subscribedAt = subscribedAt;
    }

    public String getExpiryAt() {
        return expiryAt;
    }

    public void setExpiryAt(String expiryAt) {
        this.expiryAt = expiryAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public SubscriptionPlan getSubscriptionPlan() {
        return subscriptionPlan;
    }

    public void setSubscriptionPlan(SubscriptionPlan subscriptionPlan) {
        this.subscriptionPlan = subscriptionPlan;
    }

    public List<SubscribedContentProvider> getSubscribedContentProviders() {
        return subscribedContentProviders;
    }

    public void setSubscribedContentProviders(List<SubscribedContentProvider> subscribedContentProviders) {
        this.subscribedContentProviders = subscribedContentProviders;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(userId);
        dest.writeValue(packageId);
        dest.writeValue(paymentId);
        dest.writeValue(subscribedAt);
        dest.writeValue(expiryAt);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(payment);
        dest.writeValue(subscriptionPlan);
        dest.writeList(subscribedContentProviders);
    }

    public int describeContents() {
        return 0;
    }

}