package in.aeonplay.model.SubscriptionModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubscribedContentProvider implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("user_subscribed_package_id")
    @Expose
    private Integer userSubscribedPackageId;
    @SerializedName("content_provider_id")
    @Expose
    private Integer contentProviderId;
    @SerializedName("provider_subscribed_at")
    @Expose
    private String providerSubscribedAt;
    @SerializedName("provider_expiry_at")
    @Expose
    private String providerExpiryAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    public final static Creator<SubscribedContentProvider> CREATOR = new Creator<SubscribedContentProvider>() {


        public SubscribedContentProvider createFromParcel(android.os.Parcel in) {
            return new SubscribedContentProvider(in);
        }

        public SubscribedContentProvider[] newArray(int size) {
            return (new SubscribedContentProvider[size]);
        }

    };

    @SuppressWarnings({
            "unchecked"
    })
    protected SubscribedContentProvider(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.userId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.userSubscribedPackageId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.contentProviderId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.providerSubscribedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.providerExpiryAt = ((String) in.readValue((String.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
    }

    public SubscribedContentProvider() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserSubscribedPackageId() {
        return userSubscribedPackageId;
    }

    public void setUserSubscribedPackageId(Integer userSubscribedPackageId) {
        this.userSubscribedPackageId = userSubscribedPackageId;
    }

    public Integer getContentProviderId() {
        return contentProviderId;
    }

    public void setContentProviderId(Integer contentProviderId) {
        this.contentProviderId = contentProviderId;
    }

    public String getProviderSubscribedAt() {
        return providerSubscribedAt;
    }

    public void setProviderSubscribedAt(String providerSubscribedAt) {
        this.providerSubscribedAt = providerSubscribedAt;
    }

    public String getProviderExpiryAt() {
        return providerExpiryAt;
    }

    public void setProviderExpiryAt(String providerExpiryAt) {
        this.providerExpiryAt = providerExpiryAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(userId);
        dest.writeValue(userSubscribedPackageId);
        dest.writeValue(contentProviderId);
        dest.writeValue(providerSubscribedAt);
        dest.writeValue(providerExpiryAt);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
    }

    public int describeContents() {
        return 0;
    }

}