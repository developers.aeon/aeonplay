package in.aeonplay.model.SubscriptionModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContentAccess implements Parcelable {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("pivot")
    @Expose
    private Pivot pivot;
    public final static Creator<ContentAccess> CREATOR = new Creator<ContentAccess>() {


        public ContentAccess createFromParcel(android.os.Parcel in) {
            return new ContentAccess(in);
        }

        public ContentAccess[] newArray(int size) {
            return (new ContentAccess[size]);
        }

    };

    @SuppressWarnings({
            "unchecked"
    })
    protected ContentAccess(android.os.Parcel in) {
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.pivot = ((Pivot) in.readValue((Pivot.class.getClassLoader())));
    }

    public ContentAccess() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Pivot getPivot() {
        return pivot;
    }

    public void setPivot(Pivot pivot) {
        this.pivot = pivot;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(title);
        dest.writeValue(pivot);
    }

    public int describeContents() {
        return 0;
    }

}