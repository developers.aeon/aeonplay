package in.aeonplay.model.Comman;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import in.aeonplay.activity.MasterActivity;

public class CommanDataList implements Parcelable {

    @Override
    public String toString() {
        return "CommanDataList{" +
                "id=" + id +
                ", contentProviderId=" + contentProviderId +
                ", contentType='" + contentType + '\'' +
                ", contentId='" + contentId + '\'' +
                ", provider='" + provider + '\'' +
                ", source='" + source + '\'' +
                ", title='" + title + '\'' +
                ", discription='" + discription + '\'' +
                ", duration='" + duration + '\'' +
                ", releaseYear='" + releaseYear + '\'' +
                ", longDescription='" + longDescription + '\'' +
                ", castAndCrew='" + castAndCrew + '\'' +
                ", deeplinkUrl='" + deeplinkUrl + '\'' +
                ", deeplink='" + deeplink + '\'' +
                ", hostUrl=" + hostUrl +
                ", showId='" + showId + '\'' +
                ", seasonNumber=" + seasonNumber +
//                ", seriesTitle=" + seriesTitle +
//                ", seriesDescription=" + seriesDescription +
//                ", seriesLongDescription=" + seriesLongDescription +
                ", episodeNumber=" + episodeNumber +
//                ", episodeTitle=" + episodeTitle +
//                ", episodeDescription=" + episodeDescription +
//                ", episodeLongDescription=" + episodeLongDescription +
                ", originalAirDate='" + originalAirDate + '\'' +
                ", genre=" + genre +
                ", language='" + language + '\'' +
                ", latest=" + latest +
                ", comingSoon=" + comingSoon +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", extra=" + extra +
                ", thumbnail=" + thumbnail +
                ", searchKeyword=" + searchKeyword +
                ", watchListID=" + watchListID +
                ", currentPosition=" + currentPosition +
                ", totalDuration=" + totalDuration +
                '}';
    }

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("content_provider_id")
    @Expose
    private Integer contentProviderId;
    @SerializedName("content_type")
    @Expose
    private String contentType;
    @SerializedName("content_id")
    @Expose
    private String contentId;
    @SerializedName("provider")
    @Expose
    private String provider;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("discription")
    @Expose
    private String discription;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("release_year")
    @Expose
    private String releaseYear;
    @SerializedName("long_description")
    @Expose
    private String longDescription;
    @SerializedName("cast_and_crew")
    @Expose
    private String castAndCrew;
    @SerializedName("deeplink_url")
    @Expose
    private String deeplinkUrl;
    @SerializedName("deeplink")
    @Expose
    private String deeplink;
    @SerializedName("host_url")
    @Expose
    private String hostUrl;
    @SerializedName("trailer_host_url")
    @Expose
    private String traileHostUrl;
    @SerializedName("showId")
    @Expose
    private String showId;
    @SerializedName("season_number")
    @Expose
    private Integer seasonNumber;
//    @SerializedName("series_title")
//    @Expose
//    private Object seriesTitle;
//    @SerializedName("series_description")
//    @Expose
//    private Object seriesDescription;
//    @SerializedName("series_long_description")
//    @Expose
//    private Object seriesLongDescription;
    @SerializedName("episode_number")
    @Expose
    private int episodeNumber;
//    @SerializedName("episode_title")
//    @Expose
//    private Object episodeTitle;
//    @SerializedName("episode_description")
//    @Expose
//    private Object episodeDescription;
//    @SerializedName("episode_long_description")
//    @Expose
//    private Object episodeLongDescription;
    @SerializedName("originalAirDate")
    @Expose
    private String originalAirDate;
    @SerializedName("genre")
    @Expose
    private List<String> genre = new ArrayList<>();
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("latest")
    @Expose
    private Integer latest;
    @SerializedName("coming_soon")
    @Expose
    private Integer comingSoon;
    @SerializedName("top")
    @Expose
    private Integer top;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("extra")
    @Expose
    private CommanExtra extra;
    @SerializedName("thumbnail")
    @Expose
    private CommanThumbnail thumbnail;
    @SerializedName("search_keyword")
    @Expose
    private List<CommanSearchKeyword> searchKeyword = new ArrayList<>();
    private int watchListID;
    private int currentPosition = 0;
    private int totalDuration = 0;

    public int getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    public int getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(int totalDuration) {
        this.totalDuration = totalDuration;
    }

    public int getWatchListID() {
        return watchListID;
    }

    public void setWatchListID(int watchListID) {
        this.watchListID = watchListID;
    }

    public final static Creator<CommanDataList> CREATOR = new Creator<CommanDataList>() {

        @SuppressWarnings({
                "unchecked"
        })
        public CommanDataList createFromParcel(android.os.Parcel in) {
            return new CommanDataList(in);
        }

        public CommanDataList[] newArray(int size) {
            return (new CommanDataList[size]);
        }

    };

    protected CommanDataList(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.contentProviderId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.contentType = ((String) in.readValue((String.class.getClassLoader())));
        this.contentId = ((String) in.readValue((String.class.getClassLoader())));
        this.provider = ((String) in.readValue((String.class.getClassLoader())));
        this.source = ((String) in.readValue((String.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.discription = ((String) in.readValue((String.class.getClassLoader())));
        this.duration = ((String) in.readValue((String.class.getClassLoader())));
        this.releaseYear = ((String) in.readValue((String.class.getClassLoader())));
        this.longDescription = ((String) in.readValue((String.class.getClassLoader())));
        this.castAndCrew = ((String) in.readValue((String.class.getClassLoader())));
        this.deeplinkUrl = ((String) in.readValue((String.class.getClassLoader())));
        this.deeplink = ((String) in.readValue((String.class.getClassLoader())));
        this.hostUrl = ((String) in.readValue((String.class.getClassLoader())));
        this.traileHostUrl = ((String) in.readValue((String.class.getClassLoader())));
        this.showId = ((String) in.readValue((String.class.getClassLoader())));
        this.seasonNumber = ((Integer) in.readValue((Object.class.getClassLoader())));
//        this.seriesTitle = ((Object) in.readValue((Object.class.getClassLoader())));
//        this.seriesDescription = ((Object) in.readValue((Object.class.getClassLoader())));
//        this.seriesLongDescription = ((Object) in.readValue((Object.class.getClassLoader())));
        this.episodeNumber = ((Integer) in.readValue((Object.class.getClassLoader())));
//        this.episodeTitle = ((Object) in.readValue((Object.class.getClassLoader())));
//        this.episodeDescription = ((Object) in.readValue((Object.class.getClassLoader())));
//        this.episodeLongDescription = ((Object) in.readValue((Object.class.getClassLoader())));
        this.originalAirDate = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.genre, (String.class.getClassLoader()));
        this.language = ((String) in.readValue((String.class.getClassLoader())));
        this.latest = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.comingSoon = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.top = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.extra = ((CommanExtra) in.readValue((CommanExtra.class.getClassLoader())));
        this.thumbnail = ((CommanThumbnail) in.readValue((CommanThumbnail.class.getClassLoader())));
        in.readList(this.searchKeyword, (CommanSearchKeyword.class.getClassLoader()));
    }

    public CommanDataList() {
    }

    public CommanDataList(String title, String youtubeUrl, String contentTyp, String prov) {
        this.title = title;
        this.hostUrl = youtubeUrl;
        this.contentType = contentTyp;
        this.provider = prov;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getContentProviderId() {
        return contentProviderId;
    }

    public void setContentProviderId(Integer contentProviderId) {
        this.contentProviderId = contentProviderId;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getReleaseYear() {
        return MasterActivity.checkStringIsNull(releaseYear);
    }

    public void setReleaseYear(String releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public String getCastAndCrew() {
        return castAndCrew;
    }

    public void setCastAndCrew(String castAndCrew) {
        this.castAndCrew = castAndCrew;
    }

    public String getDeeplinkUrl() {
        return deeplinkUrl;
    }

    public void setDeeplinkUrl(String deeplinkUrl) {
        this.deeplinkUrl = deeplinkUrl;
    }

    public String getDeeplink() {
        return deeplink;
    }

    public void setDeeplink(String deeplink) {
        this.deeplink = deeplink;
    }

    public String getHostUrl() {
        return hostUrl;
    }

    public void setHostUrl(String hostUrl) {
        this.hostUrl = hostUrl;
    }

    public String getTraileHostUrl() {
        return traileHostUrl;
    }

    public void setTraileHostUrl(String traileHostUrl) {
        this.traileHostUrl = traileHostUrl;
    }

    public String getShowId() {
        return showId;
    }

    public void setShowId(String showId) {
        this.showId = showId;
    }

    public int getSeasonNumber() {
        return seasonNumber;
    }

    public void setSeasonNumber(int seasonNumber) {
        this.seasonNumber = seasonNumber;
    }

//    public Object getSeriesTitle() {
//        return seriesTitle;
//    }
//
//    public void setSeriesTitle(Object seriesTitle) {
//        this.seriesTitle = seriesTitle;
//    }

//    public Object getSeriesDescription() {
//        return seriesDescription;
//    }
//
//    public void setSeriesDescription(Object seriesDescription) {
//        this.seriesDescription = seriesDescription;
//    }

//    public Object getSeriesLongDescription() {
//        return seriesLongDescription;
//    }
//
//    public void setSeriesLongDescription(Object seriesLongDescription) {
//        this.seriesLongDescription = seriesLongDescription;
//    }

    public int getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(int episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

//    public Object getEpisodeTitle() {
//        return episodeTitle;
//    }
//
//    public void setEpisodeTitle(Object episodeTitle) {
//        this.episodeTitle = episodeTitle;
//    }

//    public Object getEpisodeDescription() {
//        return episodeDescription;
//    }
//
//    public void setEpisodeDescription(Object episodeDescription) {
//        this.episodeDescription = episodeDescription;
//    }

//    public Object getEpisodeLongDescription() {
//        return episodeLongDescription;
//    }
//
//    public void setEpisodeLongDescription(Object episodeLongDescription) {
//        this.episodeLongDescription = episodeLongDescription;
//    }

    public String getOriginalAirDate() {
        return originalAirDate;
    }

    public void setOriginalAirDate(String originalAirDate) {
        this.originalAirDate = originalAirDate;
    }

    public List<String> getGenre() {
        return genre;
    }

    public void setGenre(List<String> genre) {
        this.genre = genre;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getLatest() {
        return latest;
    }

    public void setLatest(Integer latest) {
        this.latest = latest;
    }

    public Integer getComingSoon() {
        return comingSoon;
    }

    public void setComingSoon(Integer comingSoon) {
        this.comingSoon = comingSoon;
    }

    public Integer getTop() {
        return top;
    }

    public void setTop(Integer top) {
        this.top = top;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public CommanExtra getExtra() {
        return extra;
    }

    public void setExtra(CommanExtra extra) {
        this.extra = extra;
    }

    public CommanThumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(CommanThumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public List<CommanSearchKeyword> getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(List<CommanSearchKeyword> searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(contentProviderId);
        dest.writeValue(contentType);
        dest.writeValue(contentId);
        dest.writeValue(provider);
        dest.writeValue(source);
        dest.writeValue(title);
        dest.writeValue(discription);
        dest.writeValue(duration);
        dest.writeValue(releaseYear);
        dest.writeValue(longDescription);
        dest.writeValue(castAndCrew);
        dest.writeValue(deeplinkUrl);
        dest.writeValue(deeplink);
        dest.writeValue(hostUrl);
        dest.writeValue(traileHostUrl);
        dest.writeValue(showId);
        dest.writeValue(seasonNumber);
//        dest.writeValue(seriesTitle);
//        dest.writeValue(seriesDescription);
//        dest.writeValue(seriesLongDescription);
        dest.writeValue(episodeNumber);
//        dest.writeValue(episodeTitle);
//        dest.writeValue(episodeDescription);
//        dest.writeValue(episodeLongDescription);
        dest.writeValue(originalAirDate);
        dest.writeList(genre);
        dest.writeValue(language);
        dest.writeValue(latest);
        dest.writeValue(comingSoon);
        dest.writeValue(top);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(extra);
        dest.writeValue(thumbnail);
        dest.writeList(searchKeyword);
    }

    public int describeContents() {
        return 0;
    }
}