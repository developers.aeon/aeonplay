package in.aeonplay.model.Comman;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommanDeeplink implements Parcelable {

    @SerializedName("data")
    @Expose
    private CommanDataList data;
    public final static Creator<CommanDeeplink> CREATOR = new Creator<CommanDeeplink>() {


        public CommanDeeplink createFromParcel(android.os.Parcel in) {
            return new CommanDeeplink(in);
        }

        public CommanDeeplink[] newArray(int size) {
            return (new CommanDeeplink[size]);
        }

    };

    @SuppressWarnings({
            "unchecked"
    })
    protected CommanDeeplink(android.os.Parcel in) {
        this.data = ((CommanDataList) in.readValue((CommanDataList.class.getClassLoader())));
    }

    public CommanDeeplink() {
    }

    public CommanDataList getData() {
        return data;
    }

    public void setData(CommanDataList data) {
        this.data = data;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }

}