package in.aeonplay.model.Comman;

import java.util.ArrayList;

public class CommanHeader extends RecyclerViewItem {

    private String title;
    private ArrayList<CommanDataList> commanDataLists = new ArrayList<>();
    boolean isAdvertisement = false;
    boolean isHeader = false;
    boolean isNews = false;
    boolean isWatchlist = false;
    boolean isTop10 = false;
    boolean isComingSoon = false;
    boolean isCampaign = false;
    boolean isShot = false;
    boolean isVideoSong = false;

    public CommanHeader(String title, ArrayList<CommanDataList> commanDataLists) {
        this.title = title;
        this.commanDataLists = commanDataLists;
    }

    public boolean isNews() {
        return isNews;
    }

    public void setNews(boolean news) {
        isNews = news;
    }

    public boolean isWatchlist() {
        return isWatchlist;
    }

    public void setWatchlist(boolean isWatchlist) {
        isWatchlist = isWatchlist;
    }

    public CommanHeader(boolean advertisement, boolean isHeader, boolean isNews, boolean isWatchlist, boolean isTop10,
                        boolean isCampaign, boolean isComingSoon, boolean isShot, boolean isVideoSong) {
        this.isAdvertisement = advertisement;
        this.isHeader = isHeader;
        this.isNews = isNews;
        this.isWatchlist = isWatchlist;
        this.isTop10 = isTop10;
        this.isCampaign = isCampaign;
        this.isComingSoon = isComingSoon;
        this.isShot = isShot;
        this.isVideoSong = isVideoSong;
    }

    public boolean isTop10() {
        return isTop10;
    }

    public void setTop10(boolean isTop10) {
        isTop10 = isTop10;
    }

    public boolean isComingSoon() {
        return isComingSoon;
    }

    public void setComingSoon(boolean isComingSoon) {
        isComingSoon = isComingSoon;
    }

    public boolean isCampaign() {
        return isCampaign;
    }

    public void setCampaign(boolean campaign) {
        isCampaign = campaign;
    }

    public boolean isShot() {
        return isShot;
    }

    public void setShot(boolean shot) {
        isShot = shot;
    }

    public boolean isVideoSong() {
        return isVideoSong;
    }

    public void setVideoSong(boolean videoSong) {
        isVideoSong = videoSong;
    }

    public CommanHeader() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<CommanDataList> getDatum() {
        return commanDataLists;
    }

    public void setDatum(ArrayList<CommanDataList> commanDataLists) {
        this.commanDataLists = commanDataLists;
    }

    public boolean isAdvertisement() {
        return isAdvertisement;
    }

    public void setAdvertisement(boolean advertisement) {
        isAdvertisement = advertisement;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }
}