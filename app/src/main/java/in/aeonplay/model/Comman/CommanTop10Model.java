package in.aeonplay.model.Comman;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommanTop10Model implements Parcelable {

    @SerializedName("data")
    @Expose
    private List<CommanDataList> data;

    public final static Creator<CommanTop10Model> CREATOR = new Creator<CommanTop10Model>() {

        @SuppressWarnings({
                "unchecked"
        })
        public CommanTop10Model createFromParcel(android.os.Parcel in) {
            return new CommanTop10Model(in);
        }

        public CommanTop10Model[] newArray(int size) {
            return (new CommanTop10Model[size]);
        }

    };

    protected CommanTop10Model(android.os.Parcel in) {
        in.readList(this.data, (CommanDataList.class.getClassLoader()));
    }

    public CommanTop10Model() {
    }

    public List<CommanDataList> getData() {
        return data;
    }

    public void setData(List<CommanDataList>  data) {
        this.data = data;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }

}