package in.aeonplay.model.Comman;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommanExtra implements Parcelable {

    @SerializedName("apk_package_id")
    @Expose
    private String apkPackageId;
    @SerializedName("ipa_package_id")
    @Expose
    private String ipaPackageId;
    @SerializedName("ott_service")
    @Expose
    private CommanOttService commanOttService;
    public final static Creator<CommanExtra> CREATOR = new Creator<CommanExtra>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CommanExtra createFromParcel(android.os.Parcel in) {
            return new CommanExtra(in);
        }

        public CommanExtra[] newArray(int size) {
            return (new CommanExtra[size]);
        }

    };

    protected CommanExtra(android.os.Parcel in) {
        this.apkPackageId = ((String) in.readValue((String.class.getClassLoader())));
        this.ipaPackageId = ((String) in.readValue((String.class.getClassLoader())));
        this.commanOttService = ((CommanOttService) in.readValue((CommanOttService.class.getClassLoader())));
    }

    public CommanExtra() {
    }

    public String getApkPackageId() {
        return apkPackageId;
    }

    public void setApkPackageId(String apkPackageId) {
        this.apkPackageId = apkPackageId;
    }

    public String getIpaPackageId() {
        return ipaPackageId;
    }

    public void setIpaPackageId(String ipaPackageId) {
        this.ipaPackageId = ipaPackageId;
    }

    public CommanOttService getOttService() {
        return commanOttService;
    }

    public void setOttService(CommanOttService commanOttService) {
        this.commanOttService = commanOttService;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(apkPackageId);
        dest.writeValue(ipaPackageId);
        dest.writeValue(commanOttService);
    }

    public int describeContents() {
        return 0;
    }

}