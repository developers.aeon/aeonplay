package in.aeonplay.model.Comman;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommanOttService implements Parcelable
{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("logo")
    @Expose
    private String logo;
    public final static Creator<CommanOttService> CREATOR = new Creator<CommanOttService>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CommanOttService createFromParcel(android.os.Parcel in) {
            return new CommanOttService(in);
        }

        public CommanOttService[] newArray(int size) {
            return (new CommanOttService[size]);
        }

    }
            ;

    protected CommanOttService(android.os.Parcel in) {
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.logo = ((String) in.readValue((String.class.getClassLoader())));
    }

    public CommanOttService() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(name);
        dest.writeValue(logo);
    }

    public int describeContents() {
        return 0;
    }

}