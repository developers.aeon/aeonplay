package in.aeonplay.model.Comman;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CommanSearchKeyword implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("content_id")
    @Expose
    private Integer contentId;
    @SerializedName("keyword")
    @Expose
    private List<String> keyword = new ArrayList<>();
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    public final static Creator<CommanSearchKeyword> CREATOR = new Creator<CommanSearchKeyword>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CommanSearchKeyword createFromParcel(android.os.Parcel in) {
            return new CommanSearchKeyword(in);
        }

        public CommanSearchKeyword[] newArray(int size) {
            return (new CommanSearchKeyword[size]);
        }

    };

    protected CommanSearchKeyword(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.contentId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.keyword, (String.class.getClassLoader()));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
    }

    public CommanSearchKeyword() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getContentId() {
        return contentId;
    }

    public void setContentId(Integer contentId) {
        this.contentId = contentId;
    }

    public List<String> getKeyword() {
        return keyword;
    }

    public void setKeyword(List<String> keyword) {
        this.keyword = keyword;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(contentId);
        dest.writeList(keyword);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
    }

    public int describeContents() {
        return 0;
    }

}