package in.aeonplay.model.OTPModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendReceiveDataModel implements Parcelable {

    @SerializedName("otp")
    @Expose
    private SendReceiveOtpModel sendReceiveOtpModel;
    public final static Creator<SendReceiveDataModel> CREATOR = new Creator<SendReceiveDataModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SendReceiveDataModel createFromParcel(android.os.Parcel in) {
            return new SendReceiveDataModel(in);
        }

        public SendReceiveDataModel[] newArray(int size) {
            return (new SendReceiveDataModel[size]);
        }

    };

    protected SendReceiveDataModel(android.os.Parcel in) {
        this.sendReceiveOtpModel = ((SendReceiveOtpModel) in.readValue((SendReceiveOtpModel.class.getClassLoader())));
    }

    public SendReceiveDataModel() {
    }

    public SendReceiveOtpModel getOtp() {
        return sendReceiveOtpModel;
    }

    public void setOtp(SendReceiveOtpModel sendReceiveOtpModel) {
        this.sendReceiveOtpModel = sendReceiveOtpModel;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(sendReceiveOtpModel);
    }

    public int describeContents() {
        return 0;
    }

}