package in.aeonplay.model.OTPModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendReceiveOtpModel implements Parcelable {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    public final static Creator<SendReceiveOtpModel> CREATOR = new Creator<SendReceiveOtpModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SendReceiveOtpModel createFromParcel(android.os.Parcel in) {
            return new SendReceiveOtpModel(in);
        }

        public SendReceiveOtpModel[] newArray(int size) {
            return (new SendReceiveOtpModel[size]);
        }

    };

    protected SendReceiveOtpModel(android.os.Parcel in) {
        this.status = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
    }

    public SendReceiveOtpModel() {
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
    }

    public int describeContents() {
        return 0;
    }

}