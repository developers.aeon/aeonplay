package in.aeonplay.model.WatchlistModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.aeonplay.model.Comman.CommanDataList;

public class WatchlistModelDataModel implements Parcelable {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("watchable_id")
    @Expose
    private Integer watchableId;
    @SerializedName("watchable_type")
    @Expose
    private String watchableType;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("watchable")
    @Expose
    private CommanDataList watchable;
    @SerializedName("error")
    @Expose
    private String error;

    public final static Creator<WatchlistModelDataModel> CREATOR = new Creator<WatchlistModelDataModel>() {


        public WatchlistModelDataModel createFromParcel(android.os.Parcel in) {
            return new WatchlistModelDataModel(in);
        }

        public WatchlistModelDataModel[] newArray(int size) {
            return (new WatchlistModelDataModel[size]);
        }

    };

    @SuppressWarnings({"unchecked"})
    protected WatchlistModelDataModel(android.os.Parcel in) {
        this.userId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.watchableId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.watchableType = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.watchable = ((CommanDataList) in.readValue((CommanDataList.class.getClassLoader())));
        this.error = ((String) in.readValue((String.class.getClassLoader())));
    }

    public WatchlistModelDataModel() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getWatchableId() {
        return watchableId;
    }

    public void setWatchableId(Integer watchableId) {
        this.watchableId = watchableId;
    }

    public String getWatchableType() {
        return watchableType;
    }

    public void setWatchableType(String watchableType) {
        this.watchableType = watchableType;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CommanDataList getWatchable() {
        return watchable;
    }

    public void setWatchable(CommanDataList watchable) {
        this.watchable = watchable;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }


    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(userId);
        dest.writeValue(watchableId);
        dest.writeValue(watchableType);
        dest.writeValue(updatedAt);
        dest.writeValue(createdAt);
        dest.writeValue(id);
        dest.writeValue(watchable);
        dest.writeValue(error);
    }

    public int describeContents() {
        return 0;
    }

    public WatchlistModelListDatumModel toWatchlistModelListDatumModel(){
        WatchlistModelListDatumModel watchlistModelListDatumModel = new WatchlistModelListDatumModel();
        watchlistModelListDatumModel.setUserId(getUserId());
        watchlistModelListDatumModel.setWatchableId(getWatchableId());
        watchlistModelListDatumModel.setWatchableType(getWatchableType());
        watchlistModelListDatumModel.setUpdatedAt(getUpdatedAt());
        watchlistModelListDatumModel.setCreatedAt(getCreatedAt());
        watchlistModelListDatumModel.setId(getId());
        watchlistModelListDatumModel.setWatchable(getWatchable());
        return watchlistModelListDatumModel;
    }

}