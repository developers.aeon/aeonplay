package in.aeonplay.model.WatchlistModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WatchlistModelList implements Parcelable {

    @SerializedName("data")
    @Expose
    private List<WatchlistModelListDatumModel> data;
    public final static Creator<WatchlistModelList> CREATOR = new Creator<WatchlistModelList>() {


        public WatchlistModelList createFromParcel(android.os.Parcel in) {
            return new WatchlistModelList(in);
        }

        public WatchlistModelList[] newArray(int size) {
            return (new WatchlistModelList[size]);
        }

    };

    @SuppressWarnings({
            "unchecked"
    })
    protected WatchlistModelList(android.os.Parcel in) {
        in.readList(this.data, (WatchlistModelListDatumModel.class.getClassLoader()));
    }

    public WatchlistModelList() {
    }

    public List<WatchlistModelListDatumModel> getData() {
        return data;
    }

    public void setData(List<WatchlistModelListDatumModel> data) {
        this.data = data;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeList(data);
    }

    public int describeContents() {
        return 0;
    }

}