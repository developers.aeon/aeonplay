package in.aeonplay.model.WatchlistModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.aeonplay.model.Comman.CommanDataList;

public class WatchlistModelListDatumModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("watchable_type")
    @Expose
    private String watchableType;
    @SerializedName("watchable_id")
    @Expose
    private Integer watchableId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("watchable")
    @Expose
    private CommanDataList watchable;
    public final static Creator<WatchlistModelListDatumModel> CREATOR = new Creator<WatchlistModelListDatumModel>() {


        public WatchlistModelListDatumModel createFromParcel(android.os.Parcel in) {
            return new WatchlistModelListDatumModel(in);
        }

        public WatchlistModelListDatumModel[] newArray(int size) {
            return (new WatchlistModelListDatumModel[size]);
        }

    };

    @SuppressWarnings({
            "unchecked"
    })
    protected WatchlistModelListDatumModel(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.userId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.watchableType = ((String) in.readValue((String.class.getClassLoader())));
        this.watchableId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.watchable = ((CommanDataList) in.readValue((CommanDataList.class.getClassLoader())));
    }

    public WatchlistModelListDatumModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getWatchableType() {
        return watchableType;
    }

    public void setWatchableType(String watchableType) {
        this.watchableType = watchableType;
    }

    public Integer getWatchableId() {
        return watchableId;
    }

    public void setWatchableId(Integer watchableId) {
        this.watchableId = watchableId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public CommanDataList getWatchable() {
        return watchable;
    }

    public void setWatchable(CommanDataList watchable) {
        this.watchable = watchable;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(userId);
        dest.writeValue(watchableType);
        dest.writeValue(watchableId);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(watchable);
    }

    public int describeContents() {
        return 0;
    }

    public WatchlistModelListDatumModel toWatchlistModelListDatumModel(){
        WatchlistModelListDatumModel watchlistModelListDatumModel = new WatchlistModelListDatumModel();
        watchlistModelListDatumModel.setId(getId());
        watchlistModelListDatumModel.setWatchableId(getWatchableId());
        watchlistModelListDatumModel.setWatchable(getWatchable());
        watchlistModelListDatumModel.setCreatedAt(getCreatedAt());
        watchlistModelListDatumModel.setUpdatedAt(getUpdatedAt());
        watchlistModelListDatumModel.setUserId(getUserId());
        watchlistModelListDatumModel.setWatchableType(getWatchableType());
        return watchlistModelListDatumModel;
    }

    @Override
    public String toString() {
        return "WatchlistModelListDatumModel{" +
                "id=" + id +
                ", userId=" + userId +
                ", watchableType='" + watchableType + '\'' +
                ", watchableId=" + watchableId +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", watchable=" + watchable +
                '}';
    }
}