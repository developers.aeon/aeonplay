package in.aeonplay.model.WatchlistModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WatchlistModel implements Parcelable {

    @SerializedName("data")
    @Expose
    private WatchlistModelDataModel watchlistModelDataModel;

    public final static Creator<WatchlistModel> CREATOR = new Creator<WatchlistModel>() {


        public WatchlistModel createFromParcel(android.os.Parcel in) {
            return new WatchlistModel(in);
        }

        public WatchlistModel[] newArray(int size) {
            return (new WatchlistModel[size]);
        }

    };

    @SuppressWarnings({
            "unchecked"
    })
    protected WatchlistModel(android.os.Parcel in) {
        this.watchlistModelDataModel = ((WatchlistModelDataModel) in.readValue((WatchlistModelDataModel.class.getClassLoader())));
   }

    public WatchlistModel() {
    }

    public WatchlistModelDataModel getData() {
        return watchlistModelDataModel;
    }

    public void setData(WatchlistModelDataModel data) {
        this.watchlistModelDataModel = data;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(watchlistModelDataModel);
    }

    public int describeContents() {
        return 0;
    }

}