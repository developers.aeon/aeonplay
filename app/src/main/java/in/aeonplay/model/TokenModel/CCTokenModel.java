package in.aeonplay.model.TokenModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CCTokenModel implements Parcelable {

    @SerializedName("token_type")
    @Expose
    private String tokenType;
    @SerializedName("expires_in")
    @Expose
    private Integer expiresIn;
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    public final static Creator<CCTokenModel> CREATOR = new Creator<CCTokenModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CCTokenModel createFromParcel(android.os.Parcel in) {
            return new CCTokenModel(in);
        }

        public CCTokenModel[] newArray(int size) {
            return (new CCTokenModel[size]);
        }

    };

    protected CCTokenModel(android.os.Parcel in) {
        this.tokenType = ((String) in.readValue((String.class.getClassLoader())));
        this.expiresIn = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.accessToken = ((String) in.readValue((String.class.getClassLoader())));
    }

    public CCTokenModel() {
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(tokenType);
        dest.writeValue(expiresIn);
        dest.writeValue(accessToken);
    }

    public int describeContents() {
        return 0;
    }

}