package in.aeonplay.model.Profile;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileDataModel implements Parcelable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("middle_name")
    @Expose
    private String middleName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("provider")
    @Expose
    private String provider;
    @SerializedName("provider_id")
    @Expose
    private String providerId;
    @SerializedName("provider_access_token")
    @Expose
    private String providerAccessToken;
    @SerializedName("email_verified_at")
    @Expose
    private String emailVerifiedAt;
    @SerializedName("mobile_verified_at")
    @Expose
    private String mobileVerifiedAt;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("mobile_network_operator")
    @Expose
    private String mobileNetworkOperator;
    @SerializedName("mnc")
    @Expose
    private String mnc;
    @SerializedName("mcc")
    @Expose
    private String mcc;
    @SerializedName("mobile_number_registered_location")
    @Expose
    private String mobileNumberRegisteredLocation;
    @SerializedName("followers_count")
    @Expose
    private int followers_count;
    @SerializedName("followings_count")
    @Expose
    private int followings_count;
    @SerializedName("shorts_count")
    @Expose
    private int shorts_count;
    @SerializedName("voted_by_count")
    @Expose
    private int voted_by_count;

    public final static Creator<ProfileDataModel> CREATOR = new Creator<ProfileDataModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ProfileDataModel createFromParcel(android.os.Parcel in) {
            return new ProfileDataModel(in);
        }

        public ProfileDataModel[] newArray(int size) {
            return (new ProfileDataModel[size]);
        }

    }
            ;

    protected ProfileDataModel(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.firstName = ((String) in.readValue((String.class.getClassLoader())));
        this.middleName = ((String) in.readValue((String.class.getClassLoader())));
        this.lastName = ((String) in.readValue((String.class.getClassLoader())));
        this.mobileNo = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.avatar = ((String) in.readValue((String.class.getClassLoader())));
        this.dob = ((String) in.readValue((String.class.getClassLoader())));
        this.gender = ((String) in.readValue((String.class.getClassLoader())));
        this.provider = ((String) in.readValue((String.class.getClassLoader())));
        this.providerId = ((String) in.readValue((String.class.getClassLoader())));
        this.providerAccessToken = ((String) in.readValue((String.class.getClassLoader())));
        this.emailVerifiedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.mobileVerifiedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.mobileNetworkOperator = ((String) in.readValue((String.class.getClassLoader())));
        this.mnc = ((String) in.readValue((String.class.getClassLoader())));
        this.mcc = ((String) in.readValue((String.class.getClassLoader())));
        this.mobileNumberRegisteredLocation = ((String) in.readValue((String.class.getClassLoader())));
        this.followers_count = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.followings_count = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.shorts_count = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.voted_by_count = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public ProfileDataModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getProviderAccessToken() {
        return providerAccessToken;
    }

    public void setProviderAccessToken(String providerAccessToken) {
        this.providerAccessToken = providerAccessToken;
    }

    public String getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setEmailVerifiedAt(String emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public String getMobileVerifiedAt() {
        return mobileVerifiedAt;
    }

    public void setMobileVerifiedAt(String mobileVerifiedAt) {
        this.mobileVerifiedAt = mobileVerifiedAt;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getMobileNetworkOperator() {
        return mobileNetworkOperator;
    }

    public void setMobileNetworkOperator(String mobileNetworkOperator) {
        this.mobileNetworkOperator = mobileNetworkOperator;
    }

    public String getMnc() {
        return mnc;
    }

    public void setMnc(String mnc) {
        this.mnc = mnc;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getMobileNumberRegisteredLocation() {
        return mobileNumberRegisteredLocation;
    }

    public void setMobileNumberRegisteredLocation(String mobileNumberRegisteredLocation) {
        this.mobileNumberRegisteredLocation = mobileNumberRegisteredLocation;
    }

    public int getFollowers_count() {
        return followers_count;
    }

    public void setFollowers_count(int followers_count) {
        this.followers_count = followers_count;
    }

    public int getFollowings_count() {
        return followings_count;
    }

    public void setFollowings_count(int followings_count) {
        this.followings_count = followings_count;
    }

    public int getShorts_count() {
        return shorts_count;
    }

    public void setShorts_count(int shorts_count) {
        this.shorts_count = shorts_count;
    }

    public int getVoted_by_count() {
        return voted_by_count;
    }

    public void setVoted_by_count(int voted_by_count) {
        this.voted_by_count = voted_by_count;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(firstName);
        dest.writeValue(middleName);
        dest.writeValue(lastName);
        dest.writeValue(mobileNo);
        dest.writeValue(email);
        dest.writeValue(avatar);
        dest.writeValue(dob);
        dest.writeValue(gender);
        dest.writeValue(provider);
        dest.writeValue(providerId);
        dest.writeValue(providerAccessToken);
        dest.writeValue(emailVerifiedAt);
        dest.writeValue(mobileVerifiedAt);
        dest.writeValue(status);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(mobileNetworkOperator);
        dest.writeValue(mnc);
        dest.writeValue(mcc);
        dest.writeValue(mobileNumberRegisteredLocation);
        dest.writeValue(followers_count);
        dest.writeValue(followings_count);
        dest.writeValue(shorts_count);
        dest.writeValue(voted_by_count);
    }

    public int describeContents() {
        return 0;
    }

}