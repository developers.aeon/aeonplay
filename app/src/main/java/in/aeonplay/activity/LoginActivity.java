package in.aeonplay.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import in.aeonplay.BuildConfig;
import in.aeonplay.MyApplication;
import in.aeonplay.R;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.OneShotClickListener;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class LoginActivity extends MasterActivity implements GoogleApiClient.OnConnectionFailedListener {

    private String TAG = LoginActivity.class.getSimpleName();
    private Button btnLogin;
    private EditText edtMobileNumber;
    private LinearLayout parentContainer;
    private ProgressBar progressBar;

    // For Facebook Login ...
//    private LinearLayout mFacebookLogin;
//    public CallbackManager mFacebookCallbackManager;
//    private String accessToken;
//    private LoginManager mLoginManager;

    // For Google Login ...
    private GoogleSignInOptions gso;
    private GoogleApiClient mGoogleApiClient;
    private LinearLayout mGoogleLogin;
    private int RC_SIGN_IN = 100;

    // For Phone Number Picker ...
//    private static final int CREDENTIAL_PICKER_REQUEST = 1;
//    private ActivityResultLauncher<IntentSenderRequest> phoneNumberHintIntentResultLauncher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorTransparent));
        getWindow().setNavigationBarColor(getResources().getColor(R.color.colorTransparent));
        setContentView(R.layout.activity_login);

        Init();
//        configureHint();
//        configureGoogleInfo();
        //configureFaceboookUserInfo();
        setData();
    }

//    private void configureHint() {
//        phoneNumberHintIntentResultLauncher =
//                registerForActivityResult(
//                        new ActivityResultContracts.StartIntentSenderForResult(), new ActivityResultCallback<ActivityResult>() {
//                            @Override
//                            public void onActivityResult(ActivityResult result) {
//                                if (result != null && result.getData() != null) {
//                                    try {
//                                        String phoneNumber = Identity.getSignInClient(getApplicationContext()).getPhoneNumberFromIntent(result.getData());
//                                        if (phoneNumber.contains("+91")) {
//                                            edtMobileNumber.setText(phoneNumber.substring(3));
//                                            edtMobileNumber.setSelection(edtMobileNumber.getText().length());
//
//                                        } else if (phoneNumber.contains("91")) {
//                                            edtMobileNumber.setText(phoneNumber.substring(2));
//                                            edtMobileNumber.setSelection(edtMobileNumber.getText().length());
//
//                                        } else if (phoneNumber.contains("0") || phoneNumber.contains("+")) {
//                                            edtMobileNumber.setText(phoneNumber.substring(1));
//                                            edtMobileNumber.setSelection(edtMobileNumber.getText().length());
//
//                                        } else {
//                                            edtMobileNumber.setText(phoneNumber);
//                                            edtMobileNumber.setSelection(edtMobileNumber.getText().length());
//                                        }
//
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//                            }
//                        });
//    }

//    private void configureGoogleInfo() {
//        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestEmail()
//                .build();
//
//        //Initializing google api client......
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(LoginActivity.this, this)
//                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                .build();
//    }

//    private void configureFaceboookUserInfo() {
//        mLoginManager = LoginManager.getInstance();
//        mFacebookCallbackManager = CallbackManager.Factory.create();
//
//        LoginManager.getInstance().registerCallback(mFacebookCallbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                Log.d("mFacebook Status: ", "Success");
//
//                accessToken = loginResult.getAccessToken()
//                        .getToken();
//                Log.d("mFacebook AccessToken: ", accessToken);
//
//                GraphRequest request = GraphRequest.newMeRequest(
//                        loginResult.getAccessToken(),
//                        new GraphRequest.GraphJSONObjectCallback() {
//                            @Override
//                            public void onCompleted(JSONObject object,
//                                                    GraphResponse response) {
//                                Log.d("mFacebook Object: ", object.toString());
//                                Log.d("mFacebook Response: ", response.toString());
//
//                                try {
//                                    try {
//                                        URL profile_pic = new URL("http://graph.facebook.com/" + object.getString("id") + "/picture?type=large");
//                                        Log.d("mFacebook ProfilePic: ", profile_pic + "");
//
//                                    } catch (MalformedURLException e) {
//                                        e.printStackTrace();
//                                    }
//
//                                    Log.d("mFacebook UserInfo: ", String.valueOf(object));
//                                    JSONObject jsonObject = new JSONObject(String.valueOf(object));
//
//                                    Intent intent = new Intent(LoginActivity.this,
//                                            RegisterActivity.class);
//                                    intent.putExtra(Constants.NAME, jsonObject.getString("name"));
//                                    intent.putExtra(Constants.EMAIL, jsonObject.getString("email"));
//                                    startActivity(intent);
//
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        });
//                Bundle parameters = new Bundle();
//                parameters.putString("fields", "id,name,link,gender,birthday,email");
//                request.setParameters(parameters);
//                request.executeAsync();
//            }
//
//            @Override
//            public void onCancel() {
//                Log.d("mFacebook Status: ", "Cancel");
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//                Log.d("mFacebook Status: ", "Failed");
//            }
//        });
//    }

    //This function will option signing intent
//    private void signIn() {
//        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
//        startActivityForResult(signInIntent, RC_SIGN_IN);
//    }

    //After the signing we are calling this function
//    @RequiresApi(api = Build.VERSION_CODES.M)
//    private void handleSignInResult(final GoogleSignInResult result) {
//        if (result.isSuccess()) {
//            GoogleSignInAccount acct = result.getSignInAccount();
//
//            Intent intent = new Intent(LoginActivity.this,
//                    RegisterActivity.class);
//            intent.putExtra(Constants.NAME, acct.getDisplayName());
//            intent.putExtra(Constants.EMAIL, acct.getEmail());
//            startActivity(intent);
//
//        } else {
//            Toast.makeText(this, "Google Login Failed.", Toast.LENGTH_SHORT).show();
//        }
//    }

    private void setData() {
        Log.d(TAG, "OAuthToken: " + MyApplication.getInstance().getOAuthTokenRequest());
        if (BuildConfig.DEBUG){
            edtMobileNumber.setText("9000000099");
        }

        btnLogin.setOnClickListener(new OneShotClickListener() {
            @Override
            public void onClicked(View v) {
                if (edtMobileNumber.getText().toString().equalsIgnoreCase("")) {
                    showMessageToUser("Field should not be black!");

                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    btnLogin.setEnabled(false);

                    checkLogin(edtMobileNumber.getText().toString());
                    showHideKeyboard(edtMobileNumber, false);
                }
            }
        });
//        edtMobileNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
//                if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    showHideKeyboard(edtMobileNumber, false);
////                    btnLogin.performClick();
//                    return true;
//                }
//
//                return false;
//            }
//        });

//        edtMobileNumber.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showHideKeyboard(edtMobileNumber, true);
//            }
//        });

//        edtMobileNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean hasFocus) {
//                if (view.isInTouchMode() && hasFocus) {
//                    showHideKeyboard(edtMobileNumber, false);
//                    GetPhoneNumberHintIntentRequest request = GetPhoneNumberHintIntentRequest.builder().build();
//                    Identity.getSignInClient(LoginActivity.this)
//                            .getPhoneNumberHintIntent(request)
//                            .addOnSuccessListener(result -> {
//                                try {
//                                    IntentSender intentSender = result.getIntentSender();
//                                    phoneNumberHintIntentResultLauncher.launch(new IntentSenderRequest.Builder(intentSender).build());
//                                } catch (Exception e) {
//                                    Log.i("Error launching", "error occurred in launching Activity result");
//                                }
//                            })
//                            .addOnFailureListener(new OnFailureListener() {
//                                @Override
//                                public void onFailure(@NonNull Exception e) {
//                                    Log.i("Failure occurred", "Failure getting phone number");
//                                }
//                            });
//
//////                    PendingIntent intent = Credentials.getClient(LoginActivity.this)
//////                            .getHintPickerIntent(new HintRequest.Builder()
//////                                    .setPhoneNumberIdentifierSupported(true)
//////                                    .build());
//////
//////                    try {
//////                        startIntentSenderForResult(intent.getIntentSender(), CREDENTIAL_PICKER_REQUEST, null, 0, 0, FLAG_MUTABLE, new Bundle());
//////                    } catch (IntentSender.SendIntentException e) {
//////                        e.printStackTrace();
//////                    }
//                }
//            }
//        });

//        mFacebookLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (appInstalledOrNot("com.facebook.katana")) {
//                    AccessToken accessToken = AccessToken.getCurrentAccessToken();
//                    boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
//
////                if (isLoggedIn) {
////                    mLoginManager.logOut();
////                } else {
//                    mLoginManager.logInWithReadPermissions(LoginActivity.this, Arrays.asList("email"));
////                }
//
//                } else {
//                    showMessageToUser("Facebook app is not installed in this device!");
//                }
//            }
//        });

//        mGoogleLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                signIn();
//            }
//        });
    }

    private void showHideKeyboard(EditText editText, boolean state) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            editText.setShowSoftInputOnFocus(state);
        } else {
            editText.setTextIsSelectable(!state);
        }
    }

    private void checkLogin(String username) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.postUserCanLogin(MyApplication.getInstance().getOAuthTokenRequest(), username);
        APIClient.callAPI(this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                btnLogin.setEnabled(true);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jData = jsonObject.getJSONObject("data");
                    if (jData.has("login")) {
                        Intent intent = new Intent(LoginActivity.this, OTPActivity.class);
                        intent.putExtra(Constants.MODE, 0);
                        intent.putExtra(Constants.USERNAME, username);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();

                    } else if (jData.has("registration")) {
                        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                        intent.putExtra(Constants.USERNAME, username);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                progressBar.setVisibility(View.GONE);
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                progressBar.setVisibility(View.GONE);
                showMessageToUser(error);
            }
        });
    }

//    private void facebookHashKey(Context pContext) {
//        try {
//            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(),
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                String hashKey = new String(Base64.encode(md.digest(), 0));
////                Log.i(TAG, "mFacebook Hash Key: " + hashKey);
//            }
//        } catch (NoSuchAlgorithmException e) {
////            Log.e(TAG, "mFacebook Hash Key: ", e);
//        } catch (Exception e) {
////            Log.e(TAG, "mFacebook Hash Key: ", e);
//        }
//    }

    private void Init() {
        btnLogin = findViewById(R.id.btnLogin);
        edtMobileNumber = findViewById(R.id.edtMobileNumber);
        progressBar = findViewById(R.id.loader);

        parentContainer = findViewById(R.id.parentContainer);
        mGoogleLogin = findViewById(R.id.mGoogleLogin);
        //mFacebookLogin = findViewById(R.id.mFacebookLogin);
//        facebookHashKey(LoginActivity.this);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

//    @RequiresApi(api = Build.VERSION_CODES.M)
//    @Override
//    protected void onActivityResult(int requestCode, int responseCode,
//                                    Intent data) {
//        if (requestCode == RC_SIGN_IN) {
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            handleSignInResult(result);
//        } else {
//            //mFacebookCallbackManager.onActivityResult(requestCode, responseCode, data);
//        }
//
////        if (requestCode == CREDENTIAL_PICKER_REQUEST && responseCode == RESULT_OK) {
////            Credential credentials = data.getParcelableExtra(Credential.EXTRA_KEY);
////            edtMobileNumber.setText(credentials.getId().substring(3));
////            edtMobileNumber.setSelection(edtMobileNumber.getText().length());
////        } else if (requestCode == CREDENTIAL_PICKER_REQUEST && responseCode ==
////                CredentialsApi.ACTIVITY_RESULT_NO_HINTS_AVAILABLE) {
////            //Toast.makeText(LoginActivity.this, "No phone numbers found", Toast.LENGTH_LONG).show();
////        }
//
//        super.onActivityResult(requestCode, responseCode, data);
//    }

    @Override
    public void onBackPressed() {
        finish();
    }
}