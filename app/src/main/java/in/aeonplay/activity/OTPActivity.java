package in.aeonplay.activity;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;

import java.io.Reader;
import java.io.StringReader;
import java.util.concurrent.TimeUnit;

import in.aeonplay.BuildConfig;
import in.aeonplay.MyApplication;
import in.aeonplay.R;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.OneShotClickListener;
import in.aeonplay.model.ErrorsModel;
import in.aeonplay.model.Login.CanBundleData;
import in.aeonplay.model.Login.TokenModel;
import in.aeonplay.model.OTPModel.SendReceiveModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.otp.PinEntryEditText;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class OTPActivity extends MasterActivity {

    private String TAG = OTPActivity.class.getSimpleName();
    private Button btnContinue;
    private Bundle bundle;
    private CanBundleData canBundleData;
    private int comeFrom;
    private PinEntryEditText otpEditText;
    private Call<ResponseBody> otpCallBack;
    private ProgressBar progressBar;
    private TextView txtResend, txtMobileNumber, txtNotes, txtTimer;
    private ImageView imgEdit;
    private int resentCounter = 0;
    private CountDownTimer mCountDownTimer;
    private String username;
    private static final int SMS_CONSENT_REQUEST = 1002;
    private int beginIndex = 0, endIndex = 6;
    private String mFCMInitialToken;

    private final BroadcastReceiver smsVerificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
                Bundle extras = intent.getExtras();
                Status smsRetrieverStatus = (Status) extras.get(SmsRetriever.EXTRA_STATUS);

                switch (smsRetrieverStatus.getStatusCode()) {
                    case CommonStatusCodes.SUCCESS:
                        // Get consent intent
                        Intent consentIntent = extras.getParcelable(SmsRetriever.EXTRA_CONSENT_INTENT);
                        try {
                            startActivityForResult(consentIntent, SMS_CONSENT_REQUEST);
                        } catch (ActivityNotFoundException e) {
                            e.printStackTrace();
                        }
                        break;
                    case CommonStatusCodes.TIMEOUT:
                        // Time out occurred, handle the error.
                        break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorTransparent));
        getWindow().setNavigationBarColor(getResources().getColor(R.color.colorTransparent));
        setContentView(R.layout.activity_otp);

        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }

                        // Get new FCM registration token
                        mFCMInitialToken = task.getResult();
                        Log.d("FirebaseApp Token: ", mFCMInitialToken);
                    }
                });

        Init();
        setBundle();
        setSMSReceiver();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SMS_CONSENT_REQUEST) {
            if (resultCode == RESULT_OK) {
                // Get SMS message content
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                // Extract one-time code from the message and complete verification
                // `sms` contains the entire text of the SMS message, so you will need
                // to parse the string.
                if (message != null) {
                    otpEditText.setText(parseOneTimeCode(message));
                }
            }
        }
    }

    private String parseOneTimeCode(String otp) {
        return otp.substring(beginIndex, endIndex);
    }

    private void setSMSReceiver() {
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            registerReceiver(smsVerificationReceiver, intentFilter, Context.RECEIVER_EXPORTED);
        } else {
            registerReceiver(smsVerificationReceiver, intentFilter);
        }

        SmsRetriever.getClient(this).startSmsUserConsent(null);
    }

    private void Init() {
        btnContinue = findViewById(R.id.btnContinue);
        imgEdit = findViewById(R.id.imgEdit);
        txtResend = findViewById(R.id.txtResend);
        txtNotes = findViewById(R.id.txtNotes);
        txtTimer = findViewById(R.id.txtTimer);
        txtMobileNumber = findViewById(R.id.txtMobileNumber);
        otpEditText = findViewById(R.id.otpEditText);
        progressBar = findViewById(R.id.progressBar);
    }

    private void setBundle() {
        bundle = getIntent().getExtras();
        if (bundle != null) {
            comeFrom = bundle.getInt(Constants.MODE);
            if (comeFrom == 0) {
                username = bundle.getString(Constants.USERNAME);

            } else {
                canBundleData = bundle.getParcelable(Constants.DATA);
            }

            btnContinue.setEnabled(false);
            btnContinue.setBackgroundResource(R.drawable.button_enable);

            otpEditText.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @Override
                public void onPinEntered(CharSequence str) {
                    btnContinue.setEnabled(str.length() == 6);
                    btnContinue.setBackgroundResource(R.drawable.button_focus);
                }
            });

            sendOTP(comeFrom);
            btnContinue.setOnClickListener(new OneShotClickListener() {
                @Override
                public void onClicked(View v) {
                    progressBar.setVisibility(View.VISIBLE);
                    btnContinue.setEnabled(false);

                    if (comeFrom == 0) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getLoginRequest(username, otpEditText.getText().toString());
                            }
                        }, 1000);

                    } else {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getRegisterRequest(canBundleData, otpEditText.getText().toString());
                            }
                        }, 1000);
                    }
                }
            });

//            otpEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//                @Override
//                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                    if (actionId == EditorInfo.IME_ACTION_DONE) {
//                        //btnContinue.performClick();
//                        return true;
//                    }
//                    return false;
//                }
//            });

            txtResend.setOnClickListener(new OneShotClickListener() {
                @Override
                public void onClicked(View v) {
                    resentCounter = resentCounter + 1;
                    if (resentCounter > 3) {
                        showMessageToUser("You have reached max limit!");
                    } else {
                        sendOTP(comeFrom);
                    }
                }
            });

            imgEdit.setOnClickListener(new OneShotClickListener() {

                @Override
                public void onClicked(View v) {
                    if (comeFrom == 0) {
                        Intent intent = new Intent(OTPActivity.this, LoginActivity.class);
                        intent.putExtra(Constants.MODE, 0);
                        intent.putExtra(Constants.USERNAME, username);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        cancelTimer();

                    } else {
                        Intent intent = new Intent(OTPActivity.this, RegisterActivity.class);
                        intent.putExtra(Constants.NAME, canBundleData.getFirstname() + " " + canBundleData.getLastname());
                        intent.putExtra(Constants.EMAIL, canBundleData.getEmail());
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        cancelTimer();
                    }
                }
            });
        }
    }

    private void sendOTP(int comeFrom) {
        String isAction = comeFrom == 0 ? "login" : "registration";
        String isMobileNumber = comeFrom == 0 ? username : canBundleData.getMobile();
        txtNotes.setText("Please enter the 6 digit code from the SMS sent to your phone.");
        txtMobileNumber.setText("+91 " + "XXXXXXX" + isMobileNumber.substring(isMobileNumber.length() - 3));

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> otpCallBack = apiInterface.postUserOTP(
                MyApplication.getInstance().getOAuthTokenRequest(),
                isMobileNumber, isAction);

        APIClient.callAPI(this, otpCallBack, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                Reader reader = new StringReader(response);
                SendReceiveModel sendReceiveModel = gson.fromJson(reader, SendReceiveModel.class);

                if (sendReceiveModel.getData() != null) {
                    if (sendReceiveModel.getData().getOtp().getStatus() == true) {
                        showCounter();

                    } else {
                        showMessageToUser(sendReceiveModel.getData().getOtp().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                showMessageToUser(error);
            }
        });
    }

    private void getLoginRequest(String username, String userOTP) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> otpCallBack = apiInterface.postUserLogin(
                MyApplication.getInstance().getOAuthTokenRequest(),
                BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET, getString(R.string.grant_type_otp),
                userOTP, username, username, mFCMInitialToken, getDeviceName(), getDeviceID());

        APIClient.callAPI(this, otpCallBack, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                btnContinue.setEnabled(true);

                Gson gson = new Gson();
                Reader reader = new StringReader(response);
                TokenModel loginModel = gson.fromJson(reader, TokenModel.class);

                if (loginModel != null) {
                    SharePreferenceManager.save(Constants.TOKEN_TYPE, loginModel.getTokenType());
                    SharePreferenceManager.save(Constants.TOKEN_ACCESS, loginModel.getAccessToken());
                    SharePreferenceManager.save(Constants.TOKEN_REFRESH, loginModel.getRefreshToken());
                    SharePreferenceManager.save(Constants.TOKEN_EXPIRES_IN, loginModel.getExpiresIn());

                    captureFirebaseEvent(FirebaseAnalytics.Event.LOGIN);
                    Intent intent = new Intent(OTPActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                progressBar.setVisibility(View.GONE);
                btnContinue.setEnabled(true);

                try {
                    try {
                        Gson gson = new Gson();
                        Reader reader = new StringReader(error);
                        ErrorsModel errorsModel = gson.fromJson(reader, ErrorsModel.class);

                        if (errorsModel.getErrors() != null) {
                            if (!errorsModel.getErrors().getOtp().isEmpty()) {
                                showMessageToUser(errorsModel.getErrors().getOtp().get(0));
                            }

                            if (!errorsModel.getErrors().getEmail().isEmpty()) {
                                showMessageToUser(errorsModel.getErrors().getEmail().get(0));
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    otpEditText.setError(true);
                    otpEditText.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            otpEditText.setText(null);
                        }
                    }, 1000);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                progressBar.setVisibility(View.GONE);
                btnContinue.setEnabled(true);
                showMessageToUser(error);

                otpEditText.setError(true);
                otpEditText.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        otpEditText.setText(null);
                    }
                }, 1000);
            }
        });
    }

    private void getRegisterRequest(CanBundleData canBundleData, String userOTP) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> otpCallBack = apiInterface.postUserRegister(
                MyApplication.getInstance().getOAuthTokenRequest(),
                BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET, getString(R.string.grant_type_otp),
                userOTP, canBundleData.getFirstname(), canBundleData.getLastname(), canBundleData.getMobile(),
                canBundleData.getMobile(), mFCMInitialToken, getDeviceName(), getDeviceID(), canBundleData.getEmail());

        APIClient.callAPI(this, otpCallBack, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                btnContinue.setEnabled(true);

                Gson gson = new Gson();
                Reader reader = new StringReader(response);
                TokenModel loginModel = gson.fromJson(reader, TokenModel.class);

                if (loginModel != null) {
                    SharePreferenceManager.save(Constants.TOKEN_TYPE, loginModel.getTokenType());
                    SharePreferenceManager.save(Constants.TOKEN_ACCESS, loginModel.getAccessToken());
                    SharePreferenceManager.save(Constants.TOKEN_REFRESH, loginModel.getRefreshToken());
                    SharePreferenceManager.save(Constants.TOKEN_EXPIRES_IN, loginModel.getExpiresIn());

                    AlertDialog.Builder builder = new AlertDialog.Builder(OTPActivity.this, R.style.AlertTheme);
                    builder.setMessage("Thanks! Your account has been successfully created. Please login using mobile number.")
                            .setTitle(R.string.app_name);

                    builder.setCancelable(false)
                            .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    captureFirebaseEvent(FirebaseAnalytics.Event.SIGN_UP);
                                    Intent intent = new Intent(OTPActivity.this, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                progressBar.setVisibility(View.GONE);
                btnContinue.setEnabled(true);

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(error);
                    ErrorsModel errorsModel = gson.fromJson(reader, ErrorsModel.class);

                    if (errorsModel.getErrors() != null) {
                        if (!errorsModel.getErrors().getOtp().isEmpty()) {
                            showMessageToUser(errorsModel.getErrors().getOtp().get(0));
                        }

                        if (!errorsModel.getErrors().getEmail().isEmpty()) {
                            showMessageToUser(errorsModel.getErrors().getEmail().get(0));
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                progressBar.setVisibility(View.GONE);
                btnContinue.setEnabled(true);
                showMessageToUser(error);
            }
        });
    }

    public Spanned textStyle(String string, String str){
        return Html.fromHtml("<big><font color='" + Color.WHITE + "'>" + string + "</font></big>" +
                "<small><font color='" + Color.GRAY + "'> " + str + "</font></small>");
    }

    private void showCounter() {
        txtTimer.setVisibility(View.VISIBLE);
        txtResend.setVisibility(View.GONE);
        mCountDownTimer = new CountDownTimer(120000, 1000) {
            public void onTick(long millisUntilFinished) {
                txtTimer.setText(textStyle(String.valueOf(TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)), "second left"));
            }

            public void onFinish() {
                txtTimer.setText(textStyle("00:00", "second left"));
                txtTimer.setVisibility(View.INVISIBLE);
                txtResend.setVisibility(View.VISIBLE);
            }
        };
        mCountDownTimer.start();
    }

    void cancelTimer() {
        if (mCountDownTimer != null)
            mCountDownTimer.cancel();
    }

    public String formatTime(long millis) {
        String output;
        long seconds = millis / 1000;
        long minutes = seconds / 60;

        seconds = seconds % 60;
        minutes = minutes % 60;

        String sec = String.valueOf(seconds);
        String min = String.valueOf(minutes);

        if (seconds < 10)
            sec = "0" + seconds;
        if (minutes < 10)
            min = "0" + minutes;

        output = sec;
        return output;
    }

    @Override
    public void onBackPressed() {
        if (comeFrom == 0) {
            Intent intent = new Intent(OTPActivity.this, LoginActivity.class);
            intent.putExtra(Constants.MODE, 0);
            intent.putExtra(Constants.USERNAME, username);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            cancelTimer();

        } else {
            Intent intent = new Intent(OTPActivity.this, RegisterActivity.class);
            intent.putExtra(Constants.NAME, canBundleData.getFirstname() + " " + canBundleData.getLastname());
            intent.putExtra(Constants.EMAIL, canBundleData.getEmail());
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            cancelTimer();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (smsVerificationReceiver != null)
            unregisterReceiver(smsVerificationReceiver);
    }
}