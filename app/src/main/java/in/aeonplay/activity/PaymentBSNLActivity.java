package in.aeonplay.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.json.JSONObject;

import in.aeonplay.R;
import in.aeonplay.comman.Constants;
import in.aeonplay.model.Package.PackageChildModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class PaymentBSNLActivity extends MasterActivity implements PaymentResultWithDataListener {

    private String TAG = PaymentBSNLActivity.class.getSimpleName();
    private PackageChildModel packageChildModel;
    private String mPackageChildIdentifier;
    private Bundle bundle;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Checkout.preload(getApplicationContext());

        bundle = getIntent().getExtras();
        if (bundle != null) {
            packageChildModel = bundle.getParcelable(Constants.DATA);
            mPackageChildIdentifier = String.valueOf(packageChildModel.getId());
            Log.d(TAG, "onCreate: " + mPackageChildIdentifier);
        }

        setData();
    }

    private void setData() {
        startPayment(SharePreferenceManager.getInt(Constants.RPAYMENT_AMOUNT),
                SharePreferenceManager.getString(Constants.RPAYMENT_ORDER_ID));
    }

    private void startPayment(int amount, String orderID) {
        final Checkout co = new Checkout();
        co.setImage(R.drawable.ic_action_logo);

        try {
            JSONObject options = new JSONObject();
            options.put("name", getString(R.string.app_name));
            options.put("description", "Aeon Subscription Package");
            options.put("amount", amount);
            options.put("send_sms_hash", true);
            options.put("payment_capture", true);
            options.put("currency", "INR");
            options.put("order_id", orderID);

            JSONObject notes = new JSONObject();
            notes.put("user_id", String.valueOf(SharePreferenceManager.getUserData().getId()));
            notes.put("package_id", mPackageChildIdentifier);

            options.put("email", SharePreferenceManager.getUserData().getEmail());
            options.put("contact", SharePreferenceManager.getUserData().getMobileNo());
            options.put("notes", notes);

            co.open(PaymentBSNLActivity.this, options);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String razorpayPaymentID, PaymentData paymentData) {
        try {
           confirmOrder(razorpayPaymentID);
            //showAlertDialog("success", razorpayPaymentID);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentError(int code, String response, PaymentData paymentData) {
        showMessageToUser("Transaction cancelled by user");
        SharePreferenceManager.save(Constants.RPAYMENT_ORDER_ID, "");
        SharePreferenceManager.save(Constants.RPAYMENT_AMOUNT, 0);
        SharePreferenceManager.save(Constants.RPAYMENT_STATUS, "");
        finish();
    }

    public void confirmOrder(String paymentID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.confirmOrder(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), paymentID);

        APIClient.callAPI(PaymentBSNLActivity.this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                Log.d(TAG, "confirmOrder onSuccess: " + response);
                AlertDialog.Builder builder = new AlertDialog.Builder(PaymentBSNLActivity.this, R.style.AlertTheme);
                builder.setMessage("Thank you! Your package subscription request has been received successfully")
                        .setTitle(R.string.app_name);

                builder.setCancelable(false)
                        .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(PaymentBSNLActivity.this, MainActivity.class);
                                intent.putExtra(Constants.MODE, "0");
                                intent.putExtra(Constants.PAYMENT, "PAYMENT");
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }

            @Override
            public void onFailure(String error, int responseCode) {
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                showMessageToUser(error);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}

