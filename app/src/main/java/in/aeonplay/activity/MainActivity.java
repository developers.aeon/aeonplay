package in.aeonplay.activity;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.OptIn;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.media3.common.util.UnstableApi;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;

import in.aeonplay.R;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.OneShotClickListener;
import in.aeonplay.fragment.BaseFragment;
import in.aeonplay.fragment.DetailFragment;
import in.aeonplay.fragment.EpisodeFragment;
import in.aeonplay.fragment.GameFragment;
import in.aeonplay.fragment.HomeFragment;
import in.aeonplay.fragment.LiveFragment;
import in.aeonplay.fragment.MoreFragment;
import in.aeonplay.fragment.MovieTabFragment;
import in.aeonplay.fragment.MoviesFragment;
import in.aeonplay.fragment.MusicVideoFragment;
import in.aeonplay.fragment.MyPlanFragment;
import in.aeonplay.fragment.NewsFragment;
import in.aeonplay.fragment.NotificationFragment;
import in.aeonplay.fragment.PlanFragment;
import in.aeonplay.fragment.SearchFragment;
import in.aeonplay.fragment.SettingsFragment;
import in.aeonplay.fragment.ShotsDetailsFragment;
import in.aeonplay.fragment.ShotsFragment;
import in.aeonplay.fragment.TVShowsFragment;
import in.aeonplay.fragment.WebFragment;
import in.aeonplay.model.Comman.CommanDataList;
import in.aeonplay.model.Profile.ProfileModel;
import in.aeonplay.model.SubscriptionModel.ActiveSubscription;
import in.aeonplay.model.SubscriptionModel.ContentAccess;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.player.PlayerActivity;
import in.aeonplay.preferences.SharePreferenceManager;
import in.aeonplay.shots.VideoPlayManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class MainActivity extends MasterActivity implements NavigationBarView.OnItemSelectedListener {

    public static Toolbar mToolbar;
    public static DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private BottomNavigationView mBottomNavigationView;
    private NavigationView mLeftNavigationView;
    private ImageView mImgLogo;
    private TextView mProfileName, mProfileMobile, mSubscribe;
    private OnBackListener mBackListener;
    private Bundle bundle;

    public void setOnBackListener(OnBackListener mBackListener) {
        this.mBackListener = mBackListener;
    }

    public interface OnBackListener {
        void onBackPress();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bundle = getIntent().getExtras();
        if (bundle != null) {
            String mode = bundle.getString(Constants.MODE);
            if (mode.equalsIgnoreCase("0"))
                addFragment(new MyPlanFragment());

            else if (mode.equalsIgnoreCase("1")) {
                CommanDataList commanDataList = bundle.getParcelable(Constants.DATA);
                Bundle bundle = new Bundle();
                bundle.putParcelable("DATA", commanDataList);

                DetailFragment detailFragment = new DetailFragment();
                detailFragment.setArguments(bundle);
                addFragment(detailFragment);

            } else if (mode.equalsIgnoreCase("2")) {
                int userID = bundle.getInt(Constants.USER_ID);
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.USER_ID, userID);

                ShotsDetailsFragment shotsDetailsFragment = new ShotsDetailsFragment();
                shotsDetailsFragment.setArguments(bundle);
                addFragment(shotsDetailsFragment);

            } else if (mode.equalsIgnoreCase("3")) {
                NewsFragment newsFragment = new NewsFragment();
                addFragment(newsFragment);
            }

        } else {
            addFragment(new HomeFragment());
        }

        Init();

        setUpNavigationDrawer();
        setUpBottomNavigation();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getProfile();
            }
        }, Constants.INTERVAL);

        getSupportFragmentManager().addOnBackStackChangedListener(
                new FragmentManager.OnBackStackChangedListener() {

                    @Override
                    public void onBackStackChanged() {
                        Fragment f = getSupportFragmentManager()
                                .findFragmentById(R.id.fragment_container);

                        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                            finish();
                        } else {
                            if (f != null) {
                                updateToolbarTitle(f);
                            }
                        }

                    }
                });

        checkForAppUpdate();
    }

    private void updateToolbarTitle(Fragment fragment) {
        String fragClassName = fragment.getClass().getName();
        if (fragClassName.equals(HomeFragment.class.getName())) {

            if (isDarkTheme()) {
                mImgLogo.setImageResource(R.drawable.ic_action_logo_light);

            } else {
                mImgLogo.setImageResource(R.drawable.ic_action_logo_dark);
            }

            mImgLogo.setVisibility(View.VISIBLE);
            mToolbar.setTitle("");
            mToolbar.setSubtitle("");

            ((HomeFragment) fragment).getHomeAdapter().notifyItemChanged(1);
            updateBottomNavigation(0);
            showToolbar();

        } else if (fragClassName.equals(MovieTabFragment.class.getName())) {
            updateToolbarTitle(getString(R.string.menu_item2));
            updateBottomNavigation(1);
            showToolbar();

        } else if (fragClassName.equals(GameFragment.class.getName())) {
            updateToolbarTitle(getString(R.string.menu_item3));
            updateBottomNavigation(2);
            showToolbar();

        } else if (fragClassName.equals(TVShowsFragment.class.getName())) {
            updateToolbarTitle(getString(R.string.menu_item4));
            updateBottomNavigation(3);
            showToolbar();

        } else if (fragClassName.equals(LiveFragment.class.getName())) {
            updateToolbarTitle(getString(R.string.menu_item5));
            updateBottomNavigation(4);
            showToolbar();

        } else if (fragClassName.equals(MyPlanFragment.class.getName())) {
            updateToolbarTitle(getString(R.string.menu_item6));
            showToolbar();

        } else if (fragClassName.equals(NotificationFragment.class.getName())) {
            updateToolbarTitle(getString(R.string.menu_item7));
            showToolbar();

        } else if (fragClassName.equals(PlanFragment.class.getName())) {
            updateToolbarTitle(getString(R.string.menu_item10));
            showToolbar();

        } else if (fragClassName.equals(SearchFragment.class.getName())) {
            hideToolbar();

        } else if (fragClassName.equals(NewsFragment.class.getName())) {
            updateToolbarTitle(getString(R.string.menu_item_upcoming0));
            showToolbar();

        } else if (fragClassName.equals(DetailFragment.class.getName())) {
            updateToolbarImage();
            hideToolbar();

        } else if (fragClassName.equals(EpisodeFragment.class.getName())) {
            updateToolbarImage();
            hideToolbar();

        } else if (fragClassName.equals(MoreFragment.class.getName())) {
            updateToolbarImage();
            showToolbar();

        } else if (fragClassName.equals(WebFragment.class.getName())) {
            updateToolbarImage();
            showToolbar();

        } else if (fragClassName.equals(SettingsFragment.class.getName())) {
            updateToolbarTitle(getString(R.string.menu_item_upcoming6));
            showToolbar();

        } else if (fragClassName.equals(ShotsFragment.class.getName())) {
            updateToolbarTitle(getString(R.string.menu_item_upcoming5));
            showToolbar();

        } else if (fragClassName.equals(MusicVideoFragment.class.getName())) {
            updateToolbarTitle(getString(R.string.menu_item_upcoming7));
            showToolbar();

        }
    }

    private void setUpNavigationDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                mToolbar, 0, 0) {

            /**
             * Called when a drawer has settled in a completely open state.
             */
            public void onDrawerOpened(View drawerView) {

            }

            /**
             * Called when a drawer has settled in a completely closed state.
             */
            public void onDrawerClosed(View view) {

            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        mDrawerToggle.setDrawerIndicatorEnabled(true);

        mDrawerToggle
                .setToolbarNavigationClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MainActivity.this.onSupportNavigateUp();
                    }
                });

        mLeftNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                mLeftNavigationView.setCheckedItem(item);
                mDrawerLayout.closeDrawers();

                Fragment fragment = null;
                switch (item.getItemId()) {
                    case R.id.nav_item_home:
                        if (isExists(HomeFragment.class))
                            fragment = new HomeFragment();
                        else
                            return false;
                        break;

                    case R.id.nav_item_myspace:
                        if (isExists(MyPlanFragment.class))
                            fragment = new MyPlanFragment();
                        else
                            return false;
                        break;

                    case R.id.nav_item_notification:
                        if (isExists(NotificationFragment.class))
                            fragment = new NotificationFragment();
                        else
                            return false;
                        break;

                    case R.id.nav_item_search:
                        if (isExists(SearchFragment.class))
                            fragment = new SearchFragment();
                        else
                            return false;
                        break;

                    case R.id.nav_item_news:
                        if (isExists(NewsFragment.class))
                            fragment = new NewsFragment();
                        else
                            return false;
                        break;

                    case R.id.nav_item_shorts:
                        if (isExists(ShotsFragment.class))
                            fragment = new ShotsFragment();
                        else
                            return false;
                        break;

                    case R.id.nav_item_music_video:
                        if (isExists(MusicVideoFragment.class))
                            fragment = new MusicVideoFragment();
                        else
                            return false;
                        break;

                    case R.id.nav_item_education:
                        showMessageToUser("Coming soon");
                        break;

                    case R.id.nav_item_healthcare:
                        showMessageToUser("Coming soon");
                        break;

                    case R.id.nav_item_shopping:
                        showMessageToUser("Coming soon");
                        break;

                    case R.id.nav_item_utility:
                        showMessageToUser("Coming soon");
                        break;

                    case R.id.nav_item_settings:
                        if (isExists(SettingsFragment.class))
                            fragment = new SettingsFragment();
                        else
                            return false;
                        break;

                }

                return addFragment(fragment);
            }
        });
    }

    private void Init() {
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mImgLogo = findViewById(R.id.imgLogo);
        mSubscribe = findViewById(R.id.txtSubscribe);
        mBottomNavigationView = findViewById(R.id.bottomNavigationView);
        mDrawerLayout = findViewById(R.id.drawerLayout);
        mLeftNavigationView = findViewById(R.id.leftNavigationView);

        mProfileName = mLeftNavigationView.getHeaderView(0).findViewById(R.id.profile_name);
        mProfileMobile = mLeftNavigationView.getHeaderView(0).findViewById(R.id.profile_mobile);

        mSubscribe.setOnClickListener(new OneShotClickListener() {
            @Override
            public void onClicked(View v) {
                addFragment(new PlanFragment());
            }
        });
    }

    public boolean addFragment(final Fragment fragment) {
        if (fragment != null) {

            if (VideoPlayManager.getInstance(MainActivity.this).isPlaying()) {
                VideoPlayManager.getInstance(MainActivity.this).pausePlay();
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    FragmentTransaction transaction = getSupportFragmentManager()
                            .beginTransaction();
                    transaction.setCustomAnimations(
                            R.anim.slide_in,  // enter
                            R.anim.slide_out  // exit
                    );
                    transaction.add(R.id.fragment_container, fragment);
                    transaction.addToBackStack(fragment.getClass().getName());
                    transaction.commitAllowingStateLoss();
                }
            }, 200);

            return true;
        }
        return false;
    }

    public void removeFragment(final Fragment fragments) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                FragmentTransaction transaction = getSupportFragmentManager()
                        .beginTransaction();
                transaction.setCustomAnimations(
                        R.anim.slide_in,  // enter
                        R.anim.slide_out  // exit
                );
                transaction.detach(fragments);
                transaction.remove(fragments);
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
            }
        }, 200);
    }


    private void setUpBottomNavigation() {
        mBottomNavigationView.setOnItemSelectedListener(this);
    }

    public void updateBottomNavigation(int position) {
        mBottomNavigationView.getMenu().getItem(position).setChecked(true);
    }

    public void showToolbar() {
        mToolbar.setVisibility(View.VISIBLE);
    }

    public void hideToolbar() {
        mToolbar.setVisibility(View.GONE);
    }

    public void updateToolbarTitle(String title) {
        mImgLogo.setVisibility(View.GONE);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setSubtitle(title);
    }

    public void updateToolbarImage() {
        mImgLogo.setVisibility(View.GONE);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.nav_item_1:
                if (isExists(HomeFragment.class))
                    fragment = new HomeFragment();
                else
                    return false;
                break;

            case R.id.nav_item_2:
                if (isExists(MovieTabFragment.class))
                    fragment = new MovieTabFragment();
                else
                    return false;
                break;

            case R.id.nav_item_3:
                if (isExists(GameFragment.class))
                    fragment = new GameFragment();
                else
                    return false;
                break;

            case R.id.nav_item_4:
                if (isExists(TVShowsFragment.class))
                    fragment = new TVShowsFragment();
                else
                    return false;
                break;

            case R.id.nav_item_5:
                if (isExists(LiveFragment.class))
                    fragment = new LiveFragment();
                else
                    return false;
                break;
        }

        return addFragment(fragment);
    }

    private boolean isExists(Class aClass) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        return !aClass.isInstance(fragment);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            if (fragment instanceof SettingsFragment) {
                if (mBackListener != null) {
                    mBackListener.onBackPress();
                }
            }

            return onBack();
        }

        return super.onKeyDown(keyCode, event);
    }

    public boolean onBack() {
        try {
            FragmentManager fm = getSupportFragmentManager();
            BaseFragment fragment = (BaseFragment) fm.findFragmentById(R.id.fragment_container);
            fragment = (BaseFragment) fm.findFragmentById(R.id.fragment_container);
            fragment.onBack();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }


    public void showPrompt(CommanDataList commanDataModel) {
        final Dialog dialog = new Dialog(MainActivity.this, R.style.DialogTheme);
        dialog.setContentView(R.layout.dialog_prompt);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorTransparent)));
        dialog.show();

        ImageView imgPromptLogo = dialog.findViewById(R.id.imgPromptLogo);
        TextView imgPromptTitle = dialog.findViewById(R.id.imgPromptTitle);
        TextView imgPromptDetail = dialog.findViewById(R.id.imgPromptDetail);
        Button btnProceed = dialog.findViewById(R.id.btnProceed);

        if (commanDataModel.getProvider().equalsIgnoreCase("sonyliv")) {
            imgPromptLogo.setImageResource(R.mipmap.ic_action_sonyliv);
            imgPromptTitle.setText("You will be redirected to SonyLiv");
        }

        imgPromptDetail.setText("Please ensure you are logged in with your aeonplay registered mobile number "
                + "'XXXXXX" + SharePreferenceManager.getUserData().getMobileNo().substring(
                SharePreferenceManager.getUserData().getMobileNo().length() - 4) + "'");
        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    launchAppUsingSSO(commanDataModel.getDeeplink(), commanDataModel.getExtra().getApkPackageId());
                }
                dialog.dismiss();
            }
        });
    }

    @OptIn(markerClass = UnstableApi.class)
    public void requestDeepLink(CommanDataList commanDataModel) {
        if (commanDataModel.getProvider().equalsIgnoreCase("sonyliv")) {

            if (activeSubscriptionList.size() > 0) {
                for (ActiveSubscription activeSubscription : activeSubscriptionList) {
                    for (ContentAccess contentAccess : activeSubscription.getSubscriptionPlan().getContentAccess()) {
                        if (commanDataModel.getProvider().equalsIgnoreCase(contentAccess.getTitle())) {

                            try {
                                showPrompt(commanDataModel);

                            } catch (ActivityNotFoundException | NullPointerException |
                                     SecurityException e) {
                                openStore(commanDataModel.getExtra().getApkPackageId());
                            }

                        } else {
                            addFragment(new PlanFragment());
                        }
                    }
                }

            } else {
                addFragment(new PlanFragment());
            }

        } else if (commanDataModel.getProvider().equalsIgnoreCase("aeonplay")) {
            if (commanDataModel.getHostUrl() == null)
                showMessageToUser("Playback not found!");
            else {
                Intent intent = new Intent(MainActivity.this, PlayerActivity.class);
                intent.putExtra(Constants.DATA, commanDataModel);
                intent.putExtra(Constants.TRAILER, commanDataModel.getHostUrl());
                startActivity(intent);
            }

        } else if (commanDataModel.getProvider().equalsIgnoreCase("youtube")) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" +
                    commanDataModel.getTraileHostUrl()));
            intent.putExtra("force_fullscreen", true);
            intent.putExtra("finish_on_ended", true);
            startActivity(intent);

        } else {
            Intent intent = new Intent(MainActivity.this, PlayerActivity.class);
            intent.putExtra(Constants.DATA, commanDataModel);
            startActivity(intent);
        }
    }

    private void getProfile() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getUserProfile(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS));

        APIClient.callAPI(this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final ProfileModel profileModel = gson.fromJson(reader, ProfileModel.class);

                    if (profileModel.getData() != null) {
                        SharePreferenceManager.setUserData(profileModel.getData());
                        if (profileModel.getData() != null) {
                            String text = "<big>" + profileModel.getData().getFirstName() + "</big><br>" +
                                    "<small>" + profileModel.getData().getLastName() + "</font></small>";
                            mProfileName.setText(Html.fromHtml(text));
                            mProfileMobile.setText(profileModel.getData().getMobileNo() + "");
                        }

                        if (profileModel.getData().getMobileNetworkOperator() == null &&
                                profileModel.getData().getMnc() == null &&
                                profileModel.getData().getMcc() == null &&
                                profileModel.getData().getMobileNumberRegisteredLocation() == null) {
                            getUpdateOperator();
                        }

                        if (profileModel.getData().getStatus() == 0) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    showAccountStatusDialog(Constants.ACTIVATE);
                                }
                            }, 700);
                        }
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                showMessageToUser(error);
            }
        });
    }

    private void getUpdateOperator() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getUpdtOperator(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS));

        APIClient.callAPI(this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final ProfileModel profileModel = gson.fromJson(reader, ProfileModel.class);

                    if (profileModel.getData().getStatus() == 1) {
                        SharePreferenceManager.setUserData(profileModel.getData());

                        if (profileModel.getData() != null) {
                            String text = "<big><font color=" + getResources().getColor(R.color.colorPureWhite) + "> " + profileModel.getData().getFirstName() + "</font></big><br>" +
                                    "<small><font color=" + getResources().getColor(R.color.colorGray) + "> " + profileModel.getData().getLastName() + "</font></small>";
                            mProfileName.setText(Html.fromHtml(text));
                            mProfileMobile.setText(profileModel.getData().getMobileNo() + "");
                        }
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                showMessageToUser(error);
            }
        });
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.home_main, menu);
//        return true;
//    }
//
//    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
////        switch (id) {
////            case R.id.action_notification:
////                loadFragment(new NotificationFragment());
////                return true;
////        }
//        return super.onOptionsItemSelected(item);
//    }

}