package in.aeonplay.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.UiModeManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.app.ShareCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentActivity;

import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.InstallReferrerStateListener;
import com.android.installreferrer.api.ReferrerDetails;
import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.CastStateListener;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import in.aeonplay.BuildConfig;
import in.aeonplay.R;
import in.aeonplay.comman.ConnectivityReceiver;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.EncrptionDecrption;
import in.aeonplay.comman.ExceptionHandler;
import in.aeonplay.fragment.PlanFragment;
import in.aeonplay.model.Comman.CommanDataList;
import in.aeonplay.model.Comman.CommanModel;
import in.aeonplay.model.NewsModel.NewsData;
import in.aeonplay.model.NewsModel.NewsModel;
import in.aeonplay.model.Profile.ProfileDataModel;
import in.aeonplay.model.Profile.ProfileModel;
import in.aeonplay.model.Shots.Campaign.CampaignDataModel;
import in.aeonplay.model.Shots.Campaign.CampaignListModel;
import in.aeonplay.model.Shots.ShotDataModel;
import in.aeonplay.model.Shots.ShotModel;
import in.aeonplay.model.Shots.ShotSubDataModel;
import in.aeonplay.model.SubscriptionModel.ActiveSubscription;
import in.aeonplay.model.SubscriptionModel.SubscriptionHistory;
import in.aeonplay.model.SubscriptionModel.SubscriptionModel;
import in.aeonplay.model.WatchlistModel.WatchlistModelList;
import in.aeonplay.model.WatchlistModel.WatchlistModelListDatumModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class MasterActivity extends AppCompatActivity {

    private final int RC_APP_UPDATE = 99;
    public MasterActivity masterActivity;
    public static ArrayList<CommanDataList> mLatestList = new ArrayList<>();
    public static ArrayList<CommanDataList> mComingSoonList = new ArrayList<>();
    public static ArrayList<CommanDataList> mTop10List = new ArrayList<>();
    public static ArrayList<ShotSubDataModel> mShotList = new ArrayList<>();
    public static ArrayList<CommanDataList> mMusicVideo = new ArrayList<>();
    public static ShotDataModel mShotDataModel = new ShotDataModel();
    public ArrayList<NewsData> mNewsList = new ArrayList<>();
    public ArrayList<WatchlistModelListDatumModel> mWatchDataList = new ArrayList<>();
    public ArrayList<CampaignDataModel> mCampaignList = new ArrayList<>();
    public ArrayList<ActiveSubscription> activeSubscriptionList = new ArrayList<>();
    public ArrayList<SubscriptionHistory> subscriptionHistoryList = new ArrayList<>();

    // For App Update
    public AppUpdateManager appUpdateManager;
    private final String TAG = MasterActivity.class.getSimpleName();
    private AppUpdateInfo appUpdateInfo = null;
    public Dialog mInternetDialog = null;
    public static int lastSpeedPosition = 0;
    public boolean showOfferDialog = true;
    public static SessionManagerListener<CastSession> mSessionManagerListener =
            new MySessionManagerListener();
    public static CastContext mCastContext;
    public static CastSession mCastSession;
    public CastStateListener mCastStateListener;
    // variable for install referrer client.
    private InstallReferrerClient referrerClient;
    public FirebaseAnalytics mFirebaseAnalytics;

    public static String getFormatedVote(int amount) {
        return NumberFormat.getNumberInstance(Locale.US).format(amount);
    }

    public static String checkStringIsNull(String value) {
        if (value == null)
            return "";
        else if (value.equals("null"))
            return "";
        else
            return value;
    }

    public static String changeDateFormat(String time, String inputPtrn, String outputPtrn) {
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPtrn);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPtrn);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String changeDateFormat(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd MMM, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public boolean isTVDevice() {
        UiModeManager uiModeManager = (UiModeManager) getSystemService(UI_MODE_SERVICE);
        return uiModeManager.getCurrentModeType() == Configuration.UI_MODE_TYPE_TELEVISION;
    }

    public void captureFirebaseEvent(String event) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.METHOD, "Aeonplay App");
        mFirebaseAnalytics.logEvent(event, bundle);
    }

    public void loadImage(Object object, String orientation, ImageView imageView) {
        if (object instanceof CommanDataList) {
            CommanDataList commanDataList = (CommanDataList) object;
            if (commanDataList.getThumbnail() != null) {
                if (orientation.equalsIgnoreCase(Constants.PORTRAIT)) {
                    if (commanDataList.getThumbnail().getPortrait() != null) {
                        Glide.with(masterActivity)
                                .load(commanDataList.getThumbnail().getPortrait())
                                .placeholder(R.drawable.ic_action_noimage)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(imageView);
                    } else {
                        Glide.with(masterActivity)
                                .asDrawable()
                                .load(R.drawable.ic_action_noimage)
                                .placeholder(R.drawable.ic_action_noimage)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(imageView);
                    }

                } else if (orientation.equalsIgnoreCase(Constants.LANDSCAPE)) {
                    if (commanDataList.getThumbnail().getLandscape() != null) {
                        Glide.with(masterActivity)
                                .load(commanDataList.getThumbnail().getLandscape())
                                .placeholder(R.drawable.ic_action_noimage_land)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(imageView);
                    } else {
                        Glide.with(masterActivity)
                                .asDrawable()
                                .load(R.drawable.ic_action_noimage_land)
                                .placeholder(R.drawable.ic_action_noimage_land)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(imageView);
                    }

                } else {
                    if (commanDataList.getThumbnail().getBackground() != null) {
                        Glide.with(masterActivity)
                                .load(commanDataList.getThumbnail().getBackground())
                                .placeholder(R.drawable.ic_action_noimage_land)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(imageView);
                    } else {
                        Glide.with(masterActivity)
                                .asDrawable()
                                .load(R.drawable.ic_action_noimage_land)
                                .placeholder(R.drawable.ic_action_noimage_land)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(imageView);
                    }
                }
            } else {
                if (orientation.equalsIgnoreCase(Constants.PORTRAIT)) {
                    Glide.with(masterActivity)
                            .asDrawable()
                            .load(R.drawable.ic_action_noimage)
                            .transition(GenericTransitionOptions.with(R.anim.fade_in))
                            .placeholder(R.drawable.ic_action_noimage)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imageView);
                } else if (orientation.equalsIgnoreCase(Constants.LANDSCAPE) ||
                        orientation.equalsIgnoreCase(Constants.BACKGROUND)) {
                    Glide.with(masterActivity)
                            .asDrawable()
                            .load(R.drawable.ic_action_noimage_land)
                            .transition(GenericTransitionOptions.with(R.anim.fade_in))
                            .placeholder(R.drawable.ic_action_noimage_land)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imageView);
                }
            }

        } else if (object instanceof CampaignDataModel) {
            CampaignDataModel campaignDataModel = (CampaignDataModel) object;
            Glide.with(masterActivity)
                    .asDrawable()
                    .load(campaignDataModel.getImage_file())
                    .placeholder(R.drawable.ic_action_noimage_land)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);
        }
    }

    public void shareLink(String title, String id, ImageView imageView) {
        try {
            Uri bitmapUri = saveImageExternal(getBitmapFromView(imageView));
            Intent shareIntent = ShareCompat.IntentBuilder.from(this)
                    .setType("image/*")
                    .setStream(bitmapUri)
                    .setText("Check out " + title + " on Aeonplay! " + BuildConfig.AUTHORIZE_URL + id)
                    .getIntent();

            if (shareIntent.resolveActivity(getPackageManager()) != null) {
                startActivity(shareIntent);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showExitAppDialog(String title, String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MasterActivity.this, R.style.AlertTheme);
        alertDialog.setTitle(title)
                .setMessage(message)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        Dialog dialog = alertDialog.show();
        dialog.show();

    }

    public boolean isDarkTheme() {
        SharePreferenceManager sharePreferenceManager = new SharePreferenceManager();
        return sharePreferenceManager.getBoolean("SelectedTheme");
    }

    public Uri saveImageExternal(Bitmap image) throws Exception {
        File file = new File(getCacheDir(), "aeonplay.jpg");
        FileOutputStream stream = new FileOutputStream(file);
        image.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        stream.close();
        Uri uri = FileProvider.getUriForFile(
                MasterActivity.this,
                getPackageName() + ".provider", //(use your app signature + ".provider" )
                file);
        ;

        return uri;
    }

    private void getInstallReferrer() {
        // on below line we are building our install referrer client and building it.
        referrerClient = InstallReferrerClient.newBuilder(this).build();

        // on below line we are starting its connection.
        referrerClient.startConnection(new InstallReferrerStateListener() {
            @Override
            public void onInstallReferrerSetupFinished(int responseCode) {
                // this method is called when install referrer setup is finished.
                switch (responseCode) {
                    // we are using switch case to check the response.
                    case InstallReferrerClient.InstallReferrerResponse.OK:
                        // this case is called when the status is OK and
                        ReferrerDetails response = null;
                        try {
                            // on below line we are getting referrer details
                            // by calling get install referrer.
                            response = referrerClient.getInstallReferrer();

                            // on below line we are getting referrer url.
                            String referrerUrl = response.getInstallReferrer();

                            // on below line we are getting referrer click time.
                            long referrerClickTime = response.getReferrerClickTimestampSeconds();

                            // on below line we are getting app install time
                            long appInstallTime = response.getInstallBeginTimestampSeconds();

                            // on below line we are getting our time when
                            // user has used our apps instant experience.
                            boolean instantExperienceLaunched = response.getGooglePlayInstantParam();

                            // on below line we are getting our
                            // apps install referrer.
                            String referrer = response.getInstallReferrer();

                            // on below line we are setting all detail to our text view.
//                            Log.e("Referrer is: ", referrerUrl + "\n" +
//                                    "Referrer Click Time is: " + referrerClickTime + "\n" +
//                                    "App Install Time: " + appInstallTime + "\n" +
//                                    "Install ExperienceLaunched: " + instantExperienceLaunched + "\n" +
//                                    "Install: " + referrer);

                        } catch (RemoteException e) {
                            // handling error case.
                            e.printStackTrace();
                        }
                        break;
                    case InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED:
                        // API not available on the current Play Store app.
                        //showMessageToUser("Feature not supported..");
                        break;
                    case InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE:
                        // Connection couldn't be established.
                        //showMessageToUser("Fail to establish connection..");
                        break;
                }
            }

            @Override
            public void onInstallReferrerServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                // showMessageToUser("Service disconnected..");
            }
        });
    }

    public void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showMessageToUser(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public int getToolbarHeight() {
        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }

        return actionBarHeight;
    }

    public void getDeactivateRequest(String status) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getUserProfileDeactivate(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), status);

        APIClient.callAPI(this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                // status = 0 deactivate
                // status = 1;

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final ProfileModel profileModel = gson.fromJson(reader, ProfileModel.class);

                    if (profileModel.getData() != null) {
                        SharePreferenceManager.setUserData(profileModel.getData());

                        if (profileModel.getData().getStatus() == 0) {
                            selfLogout();
                        }
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                showMessageToUser(error);
            }
        });
    }

    public void showAccountStatusDialog(String status) {
        String accountStatus = status.equalsIgnoreCase("activate") ? "ACTIVATE" : "DEACTIVATE";
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MasterActivity.this, R.style.AlertTheme);
        alertDialog.setTitle("Dear " + SharePreferenceManager.getUserData().getFirstName() + " " +
                        SharePreferenceManager.getUserData().getLastName() + ",")
                .setMessage("Are you sure you want to " + status + " your account?")
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                        if (status.equalsIgnoreCase("deactivate"))
                            finish();
                    }
                }).setPositiveButton(accountStatus, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getDeactivateRequest(status);
                            }
                        }, 700);
                    }
                });
        Dialog dialog = alertDialog.show();
        dialog.setCancelable(false);
        dialog.show();

    }

    public void selfLogout() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getSelfLogout(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS));

        APIClient.callAPI(this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject data = jsonObject.getJSONObject("data");
                    String logout = data.optString("logout");

                    if (logout.equalsIgnoreCase("true")) {
                        SharePreferenceManager.clearDB();

                        finish();
                        Intent intent = new Intent(MasterActivity.this, LoginActivity.class);
                        Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(MasterActivity.this).toBundle();
                        startActivity(intent, bundle);
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    showMessageToUser(exception.getMessage());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                showMessageToUser(error);
            }
        });
    }

    private void getSubscriptionList() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getSubscriptionHistory(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS));

        APIClient.callAPI(this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final SubscriptionModel subscriptionModel = gson.fromJson(reader, SubscriptionModel.class);

                    if (subscriptionModel.getData() != null) {

                        // Check for active subscription ....
                        activeSubscriptionList = new ArrayList<>();
                        if (subscriptionModel.getData().getActiveSubscription() != null) {
                            activeSubscriptionList.addAll(subscriptionModel.getData().getActiveSubscription());
                        }

                        // Check subscription history ...
                        subscriptionHistoryList = new ArrayList<>();
                        if (subscriptionModel.getData().getSubscriptionHistory() != null) {
                            subscriptionHistoryList.addAll(subscriptionModel.getData().getSubscriptionHistory());
                        }
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                showMessageToUser(error);
            }
        });
    }

    private void getNewsList() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getNews(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS));

        APIClient.callAPI(this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final NewsModel newsModel = gson.fromJson(reader, NewsModel.class);

                    mNewsList = new ArrayList<>();
                    if (!newsModel.getData().isEmpty()) {
                        for (NewsData newsData : newsModel.getData()) {
                            mNewsList.add(newsData);
                        }
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                showMessageToUser(error);
            }
        });
    }

    public void getWatchlist() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getWishList(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS));

        APIClient.callAPI(this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final WatchlistModelList watchlistModelList = gson.fromJson(reader, WatchlistModelList.class);

                    mWatchDataList = new ArrayList<>();
                    if (!watchlistModelList.getData().isEmpty()) {
                        for (WatchlistModelListDatumModel watchlistModelListDatumModel : watchlistModelList.getData()) {
                            mWatchDataList.add(watchlistModelListDatumModel);
                        }
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                showMessageToUser(error);
            }
        });
    }

    public Bitmap getBitmapFromView(View view) {
        if (view instanceof ImageView) {
            ImageView imageView = (ImageView) view;
            Bitmap bitmap = imageView.getDrawingCache();
            if (bitmap == null) {
                BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
                bitmap = drawable.getBitmap();
            }
            if (bitmap != null)
                return bitmap;
        }
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap((int) getResources().getDimension(com.intuit.sdp.R.dimen._500sdp), (int) getResources().getDimension(com.intuit.sdp.R.dimen._300sdp), Bitmap.Config.ARGB_8888);
        //Bind a canvas to itview.getLayoutParams().width
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

    public boolean checkWatchListExist(String contentID) {
        if (!mWatchDataList.isEmpty()) {
            for (WatchlistModelListDatumModel watchlistModelListDatumModel : mWatchDataList) {
                if (watchlistModelListDatumModel.getWatchable().getContentId().equalsIgnoreCase(contentID)) {
                    watchlistModelListDatumModel.getWatchable().setWatchListID(watchlistModelListDatumModel.getId());
                    return true;
                }
            }
        }
        return false;
    }

    public String getDeviceID() {
        return Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public String getDeviceName() {
        return Build.BRAND + " - " + Build.MODEL;
    }

    public void showNoInternetDialog() {
        if (mInternetDialog != null)
            return;
        if (masterActivity.isFinishing() || masterActivity.isDestroyed())
            return;
        try {
            mInternetDialog = new Dialog(masterActivity, R.style.DialogTheme);
            mInternetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mInternetDialog.setContentView(R.layout.dialog_nointernet);
            mInternetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
            mInternetDialog.setCancelable(false);

            Button btnTryAgain = mInternetDialog.findViewById(R.id.btnTryAgain);
            btnTryAgain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ConnectivityReceiver.isConnected()) {
                        if (mInternetDialog != null)
                            mInternetDialog.dismiss();
                    }
                }
            });
            mInternetDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void noActiveSubscription(MainActivity context, String upgrade) {
        final Dialog dialog = new Dialog(MasterActivity.this, R.style.DialogTheme);
        dialog.setContentView(R.layout.dialog_comman);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorTransparent)));
        dialog.show();

        TextView dialogTitle = dialog.findViewById(R.id.dialogTitle);
        TextView dialogInfo = dialog.findViewById(R.id.dialogInfo);
        Button dialogCancel = dialog.findViewById(R.id.dialogCancel);
        Button dialogOk = dialog.findViewById(R.id.dialogOk);


        if (upgrade.equalsIgnoreCase("UPGRADE")) {
            dialogTitle.setText("Already subscribed!");
            dialogInfo.setText("You have already subscribed premium plan");
            dialogOk.setText("Okay");

        } else {
            dialogTitle.setText("No Active Subscription!");
            dialogInfo.setText("You don't have an active premium subscription. " +
                    "Visit the 'Offers' section to subscribe to one of our packs");
            dialogOk.setText("Yes");
        }

        dialogCancel.setText("Later");
        dialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialogOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                if (!upgrade.equalsIgnoreCase("UPGRADE"))
                    context.addFragment(new PlanFragment());
            }
        });
    }

    public void showErrorDialog(FragmentActivity context, String requestURL, String requestPURL) {
        final Dialog dialog = new Dialog(context, R.style.DialogTheme);
        dialog.setContentView(R.layout.dialog_error);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorTransparent)));
        dialog.show();

        TextView dialogInfo = dialog.findViewById(R.id.dialogInfo);
        TextView dialogPInfo = dialog.findViewById(R.id.dialogPInfo);
        Button dialogShare = dialog.findViewById(R.id.dialogShare);
        Button dialogCancel = dialog.findViewById(R.id.dialogCancel);

        dialogInfo.setText("");
        dialogPInfo.setText(requestPURL);
        dialogShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, requestURL + "\n\n" + requestPURL);
                sendIntent.setType("text/*");

                Intent shareIntent = Intent.createChooser(sendIntent, null);
                startActivity(shareIntent);
            }
        });

        dialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void openStore(String apkPackageId) {
        startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/apps/details?id=" + apkPackageId)));
    }

    public static String titleCase(String stringToConvert) {
        if (TextUtils.isEmpty(stringToConvert)) {
            return "";
        }
        return Character.toUpperCase(stringToConvert.charAt(0)) +
                stringToConvert.substring(1).toLowerCase();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        masterActivity = this;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        if (isDarkTheme()) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);

        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Adding this line will prevent taking screenshot in your app
//        window.setFlags(WindowManager.LayoutParams.FLAG_SECURE,
//                WindowManager.LayoutParams.FLAG_SECURE);

        ProfileDataModel profileDataModel = SharePreferenceManager.getUserData();
        if (profileDataModel != null) {
            FirebaseCrashlytics.getInstance().setCustomKey("USER_NAME", profileDataModel.getFirstName() + " " + profileDataModel.getLastName());
            FirebaseCrashlytics.getInstance().setCustomKey("USER_EMAIL", profileDataModel.getEmail());
        }

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        getInstallReferrer();
        if (!SharePreferenceManager.getString(Constants.TOKEN_ACCESS).equalsIgnoreCase("")) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getSubscriptionList();
                    getNewsList();
                    getWatchlist();
                    getCampaignList();
                    getShotList();
                    getMusicVideo();
                }
            }, Constants.INTERVAL);
        }

        if (!isTVDevice()) {
            mCastStateListener = newState -> {
            };

            try {
                mCastContext = CastContext.getSharedInstance(this);

            } catch (RuntimeException e) {
                Throwable cause = e.getCause();
                while (cause != null) {
                    if (cause instanceof DynamiteModule.LoadingException) {
                        setContentView(R.layout.cast_context_error);
                        return;
                    }
                    cause = cause.getCause();
                }
                // Unknown error. We propagate it.
                throw e;
            }
        }

//        Log.e("TOKEN: ", SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
//                SharePreferenceManager.getString(Constants.TOKEN_ACCESS));
    }

    public void getCampaignList() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getCampaignList(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS));

        APIClient.callAPI(this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final CampaignListModel campaignModel = gson.fromJson(reader, CampaignListModel.class);

                    if (!campaignModel.getData().isEmpty()) {
                        mCampaignList = new ArrayList<>(campaignModel.getData());
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                showMessageToUser(error);
            }
        });
    }

    public void getShotList() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getShots(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), "1");

        APIClient.callAPI(this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final ShotModel shotModel = gson.fromJson(reader, ShotModel.class);

                    mShotList = new ArrayList<>();
                    mShotList.add(0, new ShotSubDataModel());
                    if (shotModel.getData() != null){
                        mShotDataModel = shotModel.getData();
                        if (!shotModel.getData().getData().isEmpty()) {
                            for (ShotSubDataModel shotSubDataModel : shotModel.getData().getData()) {
                                mShotList.add(shotSubDataModel);
                            }
                        }
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                showMessageToUser(error);
            }
        });
    }

    public void getMusicVideo() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getMusicVideByLanguage(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), Constants.VIDEO_SONG,
                "hindi", "1");

        APIClient.callAPI(this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final CommanModel commanModel = gson.fromJson(reader, CommanModel.class);
                    if (!commanModel.getData().getData().isEmpty()) {
                        mMusicVideo = new ArrayList<>(commanModel.getData().getData());
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                showMessageToUser(error);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void launchAppUsingSSO(String deeplink, String packageID) {
        try {
            EncrptionDecrption encrptionDecrption = new EncrptionDecrption();
            String paramValue = SharePreferenceManager.getUserData().getMobileNo() + "|" +
                    Instant.now().toEpochMilli() + 5 * 60 * 1000;
            String token = EncrptionDecrption.encrypt(paramValue, EncrptionDecrption.aesKey, EncrptionDecrption.iv);
            String decryptValue = EncrptionDecrption.decrypt(token, EncrptionDecrption.aesKey, EncrptionDecrption.iv);
            String rootURL = deeplink + "?source=aeoncom&loginType=sso&token=" + token;

            try {
                Intent sonyLiv = new Intent();
                sonyLiv.setAction(Intent.ACTION_VIEW);
                sonyLiv.setData(Uri.parse(rootURL));
                startActivity(sonyLiv);

            } catch (ActivityNotFoundException | NullPointerException | SecurityException e) {
                openStore(packageID);
            }

        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
    }

    // In App Update ...
    public void checkForAppUpdate() {
        appUpdateManager = AppUpdateManagerFactory.create(masterActivity);
        appUpdateManager.registerListener(installStateUpdatedListener);

        // Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        // Checks whether the platform allows the specified type of update,
        // and checks the update priority.
        appUpdateInfoTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
            }
        });
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            this.appUpdateInfo = appUpdateInfo;
            if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED)
                popupSnackbarForCompleteUpdate();
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                // Request an immediate update.
                if (appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
                    popupSnackbarUpdateAvailable();
                }
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void popupSnackbarForCompleteUpdate() {

        try {
            LinearLayout.LayoutParams objLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            Snackbar snackbar = Snackbar.make(this.findViewById(android.R.id.content), "", Snackbar.LENGTH_INDEFINITE);

            // Get the Snackbar layout view
            Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
            layout.setBackgroundResource(R.color.colorUpdateDialog);

            // Set snackbar layout params
            FrameLayout.LayoutParams parentParams = (FrameLayout.LayoutParams) layout.getLayoutParams();
            parentParams.setMargins(0, 0, 0, 0);
            layout.setLayoutParams(parentParams);
            layout.setPadding(0, 0, 0, 0);
            layout.setLayoutParams(parentParams);

            // Inflate our custom view
            View snackView = getLayoutInflater().inflate(R.layout.snack_for_update_complete, null);
            snackView.findViewById(R.id.actionInstall).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    snackbar.dismiss();
                    if (appUpdateManager != null) {
                        appUpdateManager.completeUpdate();
                    }

                }
            });

            // Add our custom view to the Snackbar's layout
            layout.addView(snackView, objLayoutParams);
            View view = snackbar.getView();

            // Position snackbar at top
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
            params.width = FrameLayout.LayoutParams.MATCH_PARENT;
            view.setLayoutParams(params);
            snackbar.setAnchorView(R.id.bottomNavigationView);
            // Show the Snackbar
            snackbar.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    InstallStateUpdatedListener installStateUpdatedListener = new
            InstallStateUpdatedListener() {
                @Override
                public void onStateUpdate(InstallState state) {
                    if (state.installStatus() == InstallStatus.DOWNLOADED) {
                        popupSnackbarForCompleteUpdate();
                    } else if (state.installStatus() == InstallStatus.INSTALLED) {
                        if (appUpdateManager != null) {
                            appUpdateManager.unregisterListener(installStateUpdatedListener);
                        }

                        // Remove Old Pref and set it into new pref.
                        SharePreferenceManager.copyPref();
                    } else {
                    }
                }
            };

    @SuppressLint("RestrictedApi")
    private void popupSnackbarUpdateAvailable() {

        try {
            LinearLayout.LayoutParams objLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            Snackbar snackbar = Snackbar.make(this.findViewById(android.R.id.content), "", Snackbar.LENGTH_INDEFINITE);

            // Get the Snackbar layout view
            Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
            layout.setBackgroundResource(R.color.colorUpdateDialog);

            // Set snackbar layout params
            FrameLayout.LayoutParams parentParams = (FrameLayout.LayoutParams) layout.getLayoutParams();
            parentParams.setMargins(0, 0, 0, 0);
            layout.setLayoutParams(parentParams);
            layout.setPadding(0, 0, 0, 0);
            layout.setLayoutParams(parentParams);

            // Inflate our custom view
            View snackView = getLayoutInflater().inflate(R.layout.snack_for_update_available, null);
            snackView.findViewById(R.id.actionUpdate).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    snackbar.dismiss();
                    if (appUpdateManager != null && appUpdateInfo != null) {
//                        MyApplication.mAppPause = true;
                        try {
                            appUpdateManager.startUpdateFlowForResult(
                                    // Pass the intent that is returned by 'getAppUpdateInfo()'.
                                    appUpdateInfo,
                                    // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                                    AppUpdateType.FLEXIBLE,
                                    // The current activity making the update request.
                                    MasterActivity.this,
                                    // Include a request code to later monitor this update request.
                                    RC_APP_UPDATE);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            snackView.findViewById(R.id.actionClose).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    snackbar.dismiss();
                }
            });
            // Add our custom view to the Snackbar's layout
            layout.addView(snackView, objLayoutParams);

            View view = snackbar.getView();

            // Position snackbar at top
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
            params.width = FrameLayout.LayoutParams.MATCH_PARENT;
            view.setLayoutParams(params);
            snackbar.setAnchorView(R.id.bottomNavigationView);


            // Show the Snackbar
            snackbar.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static class MySessionManagerListener implements SessionManagerListener<CastSession> {

        @Override
        public void onSessionEnded(CastSession session, int error) {
            if (session == mCastSession) {
                mCastSession = null;
            }
        }

        @Override
        public void onSessionResumed(CastSession session, boolean wasSuspended) {
            mCastSession = session;
        }

        @Override
        public void onSessionStarted(CastSession session, String sessionId) {
            mCastSession = session;
        }

        @Override
        public void onSessionStarting(CastSession session) {
        }

        @Override
        public void onSessionStartFailed(CastSession session, int error) {
        }

        @Override
        public void onSessionEnding(CastSession session) {
        }

        @Override
        public void onSessionResuming(CastSession session, String sessionId) {
        }

        @Override
        public void onSessionResumeFailed(CastSession session, int error) {
        }

        @Override
        public void onSessionSuspended(CastSession session, int reason) {
        }
    }

    @Override
    protected void onResume() {
        if (!isTVDevice()) {
            mCastContext.addCastStateListener(mCastStateListener);
            mCastContext.getSessionManager().addSessionManagerListener(
                    mSessionManagerListener, CastSession.class);

            if (mCastSession == null) {
                mCastSession = CastContext.getSharedInstance(getApplicationContext()).getSessionManager()
                        .getCurrentCastSession();
            }
        }

        super.onResume();
    }

    @Override
    public void onPause() {
        if (!isTVDevice()) {
            mCastContext.removeCastStateListener(mCastStateListener);
            mCastContext.getSessionManager().removeSessionManagerListener(
                    mSessionManagerListener, CastSession.class);
        }
        super.onPause();
    }

    @Override
    public boolean dispatchKeyEvent(@NonNull KeyEvent event) {
        return mCastContext.onDispatchVolumeKeyEventBeforeJellyBean(event)
                || super.dispatchKeyEvent(event);
    }
}
