package in.aeonplay.activity;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import in.aeonplay.R;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.OneShotClickListener;
import in.aeonplay.model.Login.CanBundleData;

public class RegisterActivity extends MasterActivity {

    private Button btnRegister;
    private Bundle bundle;
    private EditText edtFirstName, edtLastName, edtEmail, edtMobile;
    private TextView scanQRCode;
    private CheckBox registerNotes;

    // For Phone Number Picker ...
    private static final int CREDENTIAL_PICKER_REQUEST = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorTransparent));
        getWindow().setNavigationBarColor(getResources().getColor(R.color.colorTransparent));
        setContentView(R.layout.activity_register);

        Init();
        setData();
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//
////        if (mQRCodeDeviceID != null){
////            scanQRCode.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_ok, 0);
////            scanQRCode.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
////            scanQRCode.setClickable(false);
////        }
//    }

    private void Init() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        btnRegister = findViewById(R.id.btnRegister);
        scanQRCode = findViewById(R.id.scanQRCode);

        edtFirstName = findViewById(R.id.edtFirstname);
        edtLastName = findViewById(R.id.edtLastname);
        edtEmail = findViewById(R.id.edtEmail);
        edtMobile = findViewById(R.id.edtMobile);

        registerNotes = findViewById(R.id.registerNotes);
    }

    private void setData() {
        bundle = getIntent().getExtras();
        if (bundle != null) {

            if (bundle.getString(Constants.NAME) != null &&
                    bundle.getString(Constants.EMAIL) != null) {

                String[] fullName = bundle.getString(Constants.NAME).split(" ");
                edtFirstName.setText(fullName[0]);
                edtLastName.setText(fullName[1]);

                edtEmail.setText(bundle.getString(Constants.EMAIL));

            } else if (bundle.getString(Constants.USERNAME) != null) {
                edtMobile.setText(bundle.getString(Constants.USERNAME));
            }
        }

        String text = "<b><font color=" + getResources().getColor(R.color.colorWhite) + ">SCAN QR CODE</font></b><br>" +
                "<font color=" + getResources().getColor(R.color.colorGray) + "> (Only for AEON Box Users)</font> ";
        scanQRCode.setText(Html.fromHtml(text));

        scanQRCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (bundle != null) {
//
////                    Intent intent = new Intent(RegisterActivity.this,
////                            QRCodeActivity.class);
////                    intent.putExtra("NAME", bundle.getString("NAME"));
////                    intent.putExtra("EMAIL", bundle.getString("EMAIL"));
////                    startActivity(intent);
//
//                } else {
                startActivity(new Intent(RegisterActivity.this,
                        QRCodeActivity.class));
//                }
            }
        });

        btnRegister.setEnabled(false);
        btnRegister.setBackgroundResource(R.drawable.button_enable);
        registerNotes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                btnRegister.setEnabled(false);
                btnRegister.setBackgroundResource(R.drawable.button_enable);
                if (isChecked) {
                    btnRegister.setEnabled(isChecked);
                    btnRegister.setBackgroundResource(R.drawable.button_focus);
                }
            }
        });

        btnRegister.setOnClickListener(new OneShotClickListener() {
            @Override
            public void onClicked(View v) {
                if (edtFirstName.getText().toString().equalsIgnoreCase("")) {
                    showMessageToUser("Enter your firstname");

                } else if (edtLastName.getText().toString().equalsIgnoreCase("")) {
                    showMessageToUser("Enter your lastname");

                } else if (edtEmail.getText().toString().equalsIgnoreCase("")) {
                    showMessageToUser("Enter your email");

                } else if (edtMobile.getText().toString().equalsIgnoreCase("")) {
                    showMessageToUser("Enter your mobile number");

                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            CanBundleData canBundleData = new CanBundleData(edtFirstName.getText().toString(),
                                    edtLastName.getText().toString(),
                                    edtEmail.getText().toString(),
                                    edtMobile.getText().toString());

                            Bundle bundle = new Bundle();
                            bundle.putInt(Constants.MODE, 1);
                            bundle.putParcelable(Constants.DATA, canBundleData);

                            Intent intent = new Intent(RegisterActivity.this, OTPActivity.class);
                            intent.putExtras(bundle);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    }, 700);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
        finish();
    }
}
