package in.aeonplay.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.payu.base.models.ErrorResponse;
import com.payu.base.models.PayUPaymentParams;
import com.payu.checkoutpro.PayUCheckoutPro;
import com.payu.checkoutpro.models.PayUCheckoutProConfig;
import com.payu.checkoutpro.utils.PayUCheckoutProConstants;
import com.payu.ui.model.listeners.PayUCheckoutProListener;
import com.payu.ui.model.listeners.PayUHashGenerationListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import in.aeonplay.R;
import in.aeonplay.comman.Constants;
import in.aeonplay.model.Package.PackageChildModel;
import in.aeonplay.model.Profile.ProfileDataModel;
import in.aeonplay.payment.AppEnvironment;
import in.aeonplay.preferences.SharePreferenceManager;

public class PaymentActivity extends MasterActivity {

    private String TAG = PaymentActivity.class.getSimpleName();
    private int TIMEOUT = 3000;
    private Bundle bundle;
    private PackageChildModel packageChildModel;

    private PayUCheckoutProConfig getCheckoutProConfig() {
        PayUCheckoutProConfig checkoutProConfig = new PayUCheckoutProConfig();
        checkoutProConfig.setShowCbToolbar(true);
        checkoutProConfig.setAutoSelectOtp(true);
        checkoutProConfig.setAutoApprove(true);
        checkoutProConfig.setShowExitConfirmationOnPaymentScreen(true);
        checkoutProConfig.setShowExitConfirmationOnCheckoutScreen(true);
        checkoutProConfig.setMerchantName(getString(R.string.app_name));
        checkoutProConfig.setMerchantLogo(R.mipmap.ic_launcher_round);
        checkoutProConfig.setWaitingTime(TIMEOUT);
        checkoutProConfig.setMerchantResponseTimeout(TIMEOUT);
        return checkoutProConfig;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bundle = getIntent().getExtras();
        if (bundle != null) {
            packageChildModel = bundle.getParcelable(Constants.DATA);
            paymentInitialize(packageChildModel);
        }
    }

    public static String getMd5Hash(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String md5 = number.toString(16);

            while (md5.length() < 32)
                md5 = "0" + md5;

            return md5;
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    private void paymentInitialize(PackageChildModel packageChildModel) {
        ProfileDataModel profileDataModel = SharePreferenceManager.getUserData();
        String transactionID = getMd5Hash(profileDataModel.getMobileNo() + profileDataModel.getId() + System.currentTimeMillis() +
                packageChildModel.getId());

        HashMap<String, Object> additionalParams = new HashMap<>();
        additionalParams.put(PayUCheckoutProConstants.CP_UDF1, "package_" + packageChildModel.getId()); /*package_subscription_id*/
        additionalParams.put(PayUCheckoutProConstants.CP_UDF2, String.valueOf(profileDataModel.getId())); /* userid */
        additionalParams.put(PayUCheckoutProConstants.CP_UDF3, "udf3");
        additionalParams.put(PayUCheckoutProConstants.CP_UDF4, "udf4");
        additionalParams.put(PayUCheckoutProConstants.CP_UDF5, "udf5");

        // to show saved sodexo card
        // additionalParams.put(PayUCheckoutProConstants.SODEXO_SOURCE_ID, "srcid123");

        PayUPaymentParams.Builder builder = new PayUPaymentParams.Builder();
        builder.setAmount(packageChildModel.getAmount() + "")
                .setIsProduction(true)
                .setProductInfo("Aeon Subscription Package")
                .setKey(AppEnvironment.PRODUCTION.merchant_Key())
                .setPhone(profileDataModel.getMobileNo() + "")
                .setTransactionId(transactionID)
                .setFirstName(profileDataModel.getFirstName() + " " + profileDataModel.getLastName())
                .setEmail(profileDataModel.getEmail())
                .setSurl(AppEnvironment.PRODUCTION.surl())
                .setFurl(AppEnvironment.PRODUCTION.furl())
                .setUserCredential(AppEnvironment.PRODUCTION.merchant_Key() + ":" + profileDataModel.getEmail())
                .setAdditionalParams(additionalParams)
                .build();

        PayUPaymentParams payUPaymentParams = builder.build();

        PayUCheckoutPro.open(
                this, payUPaymentParams, getCheckoutProConfig(),
                new PayUCheckoutProListener() {

                    @Override
                    public void onPaymentSuccess(Object response) {
                        showAlertDialog(response);
                    }

                    @Override
                    public void onPaymentFailure(Object response) {
                        showAlertDialog(response);
                    }

                    @Override
                    public void onPaymentCancel(boolean isTxnInitiated) {
                        showMessageToUser("Transaction cancelled by user");
                        if (!isTxnInitiated){
                            finish();
                        }
                    }

                    @Override
                    public void onError(ErrorResponse errorResponse) {
                        showMessageToUser(errorResponse.getErrorCode() + " : " + errorResponse.getErrorMessage());
                    }

                    @Override
                    public void setWebViewProperties(@Nullable WebView webView, @Nullable Object o) {
                        //For setting webview properties, if any. Check Customized Integration section for more details on this
                    }

                    @Override
                    public void generateHash(HashMap<String, String> valueMap, PayUHashGenerationListener hashGenerationListener) {
                        String hashName = valueMap.get(PayUCheckoutProConstants.CP_HASH_NAME);
                        String hashData = valueMap.get(PayUCheckoutProConstants.CP_HASH_STRING);

                        if (!TextUtils.isEmpty(hashName) && !TextUtils.isEmpty(hashData)) {
                            //Do not generate hash from local, it needs to be calculated from server side only.
                            //Here, hashString contains hash created from your server side.
                            String salt = AppEnvironment.PRODUCTION.salt();
                            if (valueMap.containsKey(PayUCheckoutProConstants.CP_POST_SALT))
                                salt = salt + "" + (valueMap.get(PayUCheckoutProConstants.CP_POST_SALT));

                            String hash = calculateHash(hashData + salt);
                            HashMap<String, String> dataMap = new HashMap<>();
                            dataMap.put(hashName, hash);
                            hashGenerationListener.onHashGenerated(dataMap);
                        }
                    }
                }
        );
    }

    private void showAlertDialog(Object response) {
        HashMap<String, Object> result = (HashMap<String, Object>) response;
        String payuResponse = (String) result.get(PayUCheckoutProConstants.CP_PAYU_RESPONSE);
        String merchantResponse = (String) result.get(PayUCheckoutProConstants.CP_MERCHANT_RESPONSE);
        Log.d("payUData: ", payuResponse + "\n\n" + "MerchantData: " + merchantResponse);

        try {
            JSONObject jsonObject = new JSONObject(payuResponse);

            String payuStatus = "";
            if (jsonObject.getString("status").equalsIgnoreCase("failure")) {
                payuStatus = "Your package subscription request has been a failure";

            } else if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                payuStatus = "Your package subscription request has been received successfully";

            } else {
                payuStatus = "Your package subscription request has been a cancelled";
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(PaymentActivity.this, R.style.AlertTheme);
            builder.setMessage("Thank you! " + payuStatus)
                    .setTitle(R.string.app_name);

            builder.setCancelable(false)
                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(PaymentActivity.this, MainActivity.class);
                            intent.putExtra(Constants.MODE, "0");
                            intent.putExtra(Constants.PAYMENT, "PAYMENT");
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();

        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    private String calculateHash(String hashString) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
            messageDigest.update(hashString.getBytes());
            byte[] mdbytes = messageDigest.digest();
            return getHexString(mdbytes);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String getHexString(byte[] array) {
        StringBuilder hash = new StringBuilder();
        for (byte hashByte : array) {
            hash.append(Integer.toString((hashByte & 0xff) + 0x100, 16).substring(1));
        }
        return hash.toString();
    }
}
