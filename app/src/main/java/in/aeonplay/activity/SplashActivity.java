package in.aeonplay.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import in.aeonplay.R;
import in.aeonplay.comman.Constants;
import in.aeonplay.preferences.SharePreferenceManager;
//import io.branch.indexing.BranchUniversalObject;
//import io.branch.referral.Branch;
//import io.branch.referral.BranchError;
//import io.branch.referral.util.LinkProperties;

public class SplashActivity extends MasterActivity {
    protected int splashTime = 2000;
    private Thread splashTread;
    private TextView appTitle;
    private ImageView appLogo;

//    @Override
//    protected void onStart() {
//        super.onStart();
//        startService(new Intent(SplashActivity.this, StartupService.class));
//    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setData();
        if (isTVDevice()) {
            showExitAppDialog("Mobile App not supported on this device.",
                    "Please install the TV version of this app.");
        } else {
            splashCall();
        }
    }

    private void setData() {
        appLogo = findViewById(R.id.logo);
        appTitle = findViewById(R.id.title);

        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scale_splash);
        appLogo.setAnimation(animation);

        Animation animationFade = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_splash);
        appTitle.setAnimation(animationFade);
        appTitle.setText(getString(R.string.app_name));
    }

    private void splashCall() {
        final SplashActivity sPlashScreen = this;

        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(splashTime);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if (!SharePreferenceManager.getString(Constants.TOKEN_ACCESS).equalsIgnoreCase("")) {
                        Intent i = new Intent();
                        i.setClass(sPlashScreen, MainActivity.class);
                        startActivity(i);
                        finish();

                    } else {
                        Intent i = new Intent();
                        i.setClass(sPlashScreen, LoginActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
            }
        };
        splashTread.start();
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        Branch.sessionBuilder(this).withCallback(new Branch.BranchUniversalReferralInitListener() {
//            @Override
//            public void onInitFinished(BranchUniversalObject branchUniversalObject, LinkProperties linkProperties, BranchError error) {
//                if (error != null) {
////                    Log.e("BranchSDK_Tester", "branch init failed. Caused by -" + error.getMessage());
//                } else {
////                    Log.i("BranchSDK_Tester", "branch init complete!");
//                    if (branchUniversalObject != null) {
////                        Log.i("BranchSDK_Tester", "title " + branchUniversalObject.getTitle());
////                        Log.i("BranchSDK_Tester", "CanonicalIdentifier " + branchUniversalObject.getCanonicalIdentifier());
////                        Log.i("BranchSDK_Tester", "metadata " + branchUniversalObject.getContentMetadata().convertToJson());
//                    }
//
//                    if (linkProperties != null) {
////                        Log.i("BranchSDK_Tester", "Channel " + linkProperties.getChannel());
////                        Log.i("BranchSDK_Tester", "control params " + linkProperties.getControlParams());
//                    }
//                }
//            }
//        }).withData(this.getIntent().getData()).init();
//    }

//    @Override
//    public void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//        this.setIntent(intent);
//        if (intent != null && intent.hasExtra("branch_force_new_session") && intent.getBooleanExtra("branch_force_new_session",false)) {
//            Branch.sessionBuilder(this).withCallback(new Branch.BranchReferralInitListener() {
//                @Override
//                public void onInitFinished(JSONObject referringParams, BranchError error) {
//                    if (error != null) {
////                        Log.e("BranchSDK_Tester", error.getMessage());
//                    } else if (referringParams != null) {
////                        Log.i("BranchSDK_Tester", referringParams.toString());
//                    }
//                }
//            }).reInit();
//        }
//    }
}