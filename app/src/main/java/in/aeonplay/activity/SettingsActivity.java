package in.aeonplay.activity;

import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import in.aeonplay.R;
import in.aeonplay.fragment.SettingsFragment;

public class SettingsActivity extends MasterActivity {

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        mToolbar.setSubtitle(getString(R.string.menu_item_upcoming6));

        if (savedInstanceState == null) {
            addFragment(new SettingsFragment());
        }
    }

    public boolean addFragment(final Fragment fragment) {
        if (fragment != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    FragmentTransaction transaction = getSupportFragmentManager()
                            .beginTransaction();
                    transaction.setCustomAnimations(
                            R.anim.slide_in,  // enter
                            R.anim.slide_out  // exit
                    );
                    transaction.add(R.id.idFrameLayout, fragment);
                    transaction.addToBackStack(fragment.getClass().getName());
                    transaction.commitAllowingStateLoss();
                }
            }, 200);

            return true;
        }
        return false;
    }

    public void removeFragment(final Fragment fragments) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                FragmentTransaction transaction = getSupportFragmentManager()
                        .beginTransaction();
                transaction.setCustomAnimations(
                        R.anim.slide_in,  // enter
                        R.anim.slide_out  // exit
                );
                transaction.detach(fragments);
                transaction.remove(fragments);
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
            }
        }, 200);
    }


    @Override
    public void onBackPressed() {
        finish();
    }
}