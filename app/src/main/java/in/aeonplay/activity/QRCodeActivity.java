package in.aeonplay.activity;

import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.android.gms.vision.barcode.Barcode;

import java.util.List;

import in.aeonplay.R;
import in.aeonplay.qrcode.BarcodeReaderFragment;


public class QRCodeActivity extends MasterActivity implements BarcodeReaderFragment.BarcodeReaderListener {

    private static final String TAG = QRCodeActivity.class.getSimpleName();
    private BarcodeReaderFragment barcodeReader;
    private ImageView imgBorder;
    public static String mQRCodeDeviceID;
    private TextView introTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);

        Init();
    }

    private void Init() {
        barcodeReader = (BarcodeReaderFragment) getSupportFragmentManager().findFragmentById(R.id.barcode_fragment);
        barcodeReader.setListener(this);

        imgBorder = (ImageView) findViewById(R.id.border);
        imgBorder.startAnimation(AnimationUtils.loadAnimation(QRCodeActivity.this, R.anim.fade_qrcode));

        introTitle = findViewById(R.id.introTitle);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) introTitle.getLayoutParams();
        layoutParams.topMargin = getStatusBarHeight();
        introTitle.setLayoutParams(layoutParams);
    }

    @Override
    public void onScanned(final Barcode barcode) {
        barcodeReader.playBeep();

        mQRCodeDeviceID = barcode.displayValue;
        finish();
    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {
//        String codes = "";
//        for (Barcode barcode : barcodes) {
//            codes += barcode.displayValue + ", ";
//        }
//
//        final String finalCodes = codes;
//        Toast.makeText(getActivity(), "Barcodes: " + finalCodes, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {
    }

    @Override
    public void onCameraPermissionDenied() {
        showMessageToUser("Camera permission denied!");
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}