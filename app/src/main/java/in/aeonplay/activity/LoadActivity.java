package in.aeonplay.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;

import in.aeonplay.R;
import in.aeonplay.comman.Constants;
import in.aeonplay.model.Comman.CommanDeeplink;
import in.aeonplay.model.Shots.ShotDetailModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class LoadActivity extends MasterActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        handleIntent();
    }

    private void handleIntent() {
        Intent appLinkIntent = getIntent();
//        String appLinkAction = appLinkIntent.getAction();
//        Uri appLinkData = appLinkIntent.getData();
//        if (appLinkData != null) {
//            String path = appLinkData.getLastPathSegment();
//            // Received only - 1700000899
//        }

        if (appLinkIntent.getDataString() != null) {
            // Received Complete Path - https://production.aeongroup.in/1700000899
            if (appLinkIntent.getDataString().contains("shorts")) {
                Uri uri = Uri.parse(appLinkIntent.getDataString());
                getShotDetailsByID(uri.getLastPathSegment());

            } else if (appLinkIntent.getDataString().contains("newspaper")) {
                Intent intent = new Intent(LoadActivity.this, MainActivity.class);
                intent.putExtra(Constants.MODE, "3");

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else {
                Uri uri = Uri.parse(appLinkIntent.getDataString());
                getContentDetailsByID(uri.getLastPathSegment());
            }
        }
    }

    private void getShotDetailsByID(String id) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> loginCall = apiInterface.getShotsFromID(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), id);

        APIClient.callAPI(this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final ShotDetailModel shotsDetailModel = gson.fromJson(reader, ShotDetailModel.class);

                    if (shotsDetailModel.getData() != null) {
                        Intent intent = new Intent(LoadActivity.this, MainActivity.class);
                        intent.putExtra(Constants.MODE, "2");
                        intent.putExtra(Constants.USER_ID, shotsDetailModel.getData().getUserId());

                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                showMessageToUser(error);
            }
        });
    }

    private void getContentDetailsByID(String id) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> loginCall = apiInterface.getContentsFromID(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), id);

        APIClient.callAPI(this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final CommanDeeplink commanDeeplink = gson.fromJson(reader, CommanDeeplink.class);

                    if (commanDeeplink.getData() != null) {
                        Intent intent = new Intent(LoadActivity.this, MainActivity.class);
                        intent.putExtra(Constants.MODE, "1");
                        intent.putExtra(Constants.DATA, commanDeeplink.getData());

                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                showMessageToUser(error);
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleIntent();
    }
}
