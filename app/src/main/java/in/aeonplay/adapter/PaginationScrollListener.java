package in.aeonplay.adapter;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

public abstract class PaginationScrollListener extends RecyclerView.OnScrollListener {

    private GridLayoutManager gridLayoutManager;
    private LinearLayoutManager linearLayoutManager;
    private StaggeredGridLayoutManager staggeredGridLayoutManager;
    private int mode;

    protected PaginationScrollListener(int mode, LinearLayoutManager layoutManager) {
        this.linearLayoutManager = layoutManager;
        this.mode = mode;
    }

    protected PaginationScrollListener(int mode, GridLayoutManager layoutManager) {
        this.gridLayoutManager = layoutManager;
        this.mode = mode;
    }

    protected PaginationScrollListener(int mode, StaggeredGridLayoutManager layoutManager) {
        this.staggeredGridLayoutManager = layoutManager;
        this.mode = mode;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (mode == 0) {
            int visibleItemCount = linearLayoutManager.getChildCount();
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItemPosition = findFirstVisiblePosition(linearLayoutManager);

            if (!isLoading() && !isLastPage()) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0) {
                    loadMoreItems();
                }
            }
        }

        if (mode == 1) {
//            scrollY += dy;
//
//            int headerTranslation = (int) (-scrollY * .5f);
//            mView.setTranslationY(headerTranslation);

            int visibleItemCount = gridLayoutManager.getChildCount();
            int totalItemCount = gridLayoutManager.getItemCount();
            int firstVisibleItemPosition = findFirstVisiblePosition(gridLayoutManager);

            if (!isLoading() && !isLastPage()) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0) {
                    loadMoreItems();
                }
            }
        }

        if (mode == 2) {
//            scrollY += dy;
//
//            int headerTranslation = (int) (-scrollY * .5f);
//            mView.setTranslationY(headerTranslation);

            int visibleItemCount = staggeredGridLayoutManager.getChildCount();
            int totalItemCount = staggeredGridLayoutManager.getItemCount();
            int firstVisibleItemPosition = findFirstVisiblePosition(staggeredGridLayoutManager);

            if (!isLoading() && !isLastPage()) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0) {
                    loadMoreItems();
                }
            }
        }
    }

    private int findFirstVisiblePosition(RecyclerView.LayoutManager layoutManager) {
        int firstVisiblePosition = 0;
        if (layoutManager instanceof GridLayoutManager) {
            firstVisiblePosition = ((GridLayoutManager) layoutManager).findFirstVisibleItemPosition();
        } else if (layoutManager instanceof LinearLayoutManager) {
            firstVisiblePosition = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
        } else if (layoutManager instanceof StaggeredGridLayoutManager) {
            int[] into = new int[((StaggeredGridLayoutManager) layoutManager).getSpanCount()];
            ((StaggeredGridLayoutManager) layoutManager).findFirstVisibleItemPositions(into);
            firstVisiblePosition = Integer.MAX_VALUE;
            for (int pos : into) {
                firstVisiblePosition = Math.min(pos, firstVisiblePosition);
            }
        }
        return firstVisiblePosition;
    }

    protected abstract void loadMoreItems();

    public abstract int getTotalPageCount();

    public abstract boolean isLastPage();

    public abstract boolean isLoading();

}
