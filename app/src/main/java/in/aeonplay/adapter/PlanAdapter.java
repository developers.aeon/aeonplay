package in.aeonplay.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.util.List;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.activity.PaymentActivity;
import in.aeonplay.activity.PaymentBSNLActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.fragment.WebFragment;
import in.aeonplay.model.Package.PackageChildModel;
import in.aeonplay.model.Package.PackageContentAccess;
import in.aeonplay.model.Payment.RazorpayModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class PlanAdapter extends RecyclerView.Adapter<PlanAdapter.CustomViewHolder> {

    private MainActivity context;
    private List<PackageChildModel> mItems;
    private int checkedPosition = 0;

    public PlanAdapter(MainActivity context, List<PackageChildModel> item) {
        this.context = context;
        this.mItems = item;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, final int selectedItem) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_plan_list_item, viewGroup, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder viewHolder, @SuppressLint("RecyclerView") final int position) {
        PackageChildModel item = mItems.get(position);

        if (checkedPosition == position) {
            viewHolder.bottomContainer.setVisibility(View.VISIBLE);
            viewHolder.topContainer.setBackgroundResource(R.drawable.ic_action_box_offer_selected);

        } else {
            viewHolder.bottomContainer.setVisibility(View.GONE);
            viewHolder.topContainer.setBackgroundResource(R.drawable.ic_action_box_offer);
        }

        int dimens = (int) context.getResources().getDimension(com.intuit.sdp.R.dimen._20sdp);
        LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(dimens, dimens);
        viewHolder.linearIcons.setHasTransientState(false);
        for (PackageContentAccess packageContentAccess : item.getContentAccess()) {
            ImageView imageView = new ImageView(context);
            layoutParams.setMargins(10, 0, 0, 0);
            imageView.setLayoutParams(layoutParams);

            if (packageContentAccess.getTitle().equalsIgnoreCase("erosnow"))
                imageView.setImageResource(R.mipmap.ic_action_erosnow);
            else if (packageContentAccess.getTitle().equalsIgnoreCase("sonyliv"))
                imageView.setImageResource(R.mipmap.ic_action_sonyliv);
//            else
//                imageView.setImageResource(R.mipmap.ic_action_ap);

            viewHolder.linearIcons.setHasTransientState(true);
            viewHolder.linearIcons.addView(imageView, layoutParams);
        }

        viewHolder.txtPlanTitle.setText(item.getHeaderTitle());
        viewHolder.txtPlanCost.setText(Html.fromHtml("<sup><small>" + "₹ " + "</small></sup>" +
                "<big>" + item.getAmount() + "</big> "));
        viewHolder.txtPlanDuration.setText(item.getTitle() + "");
        viewHolder.txtPlanDescription.setText(item.getDescription().toString().replace("[", "").replace("]", ""));

        viewHolder.itemView.setHasTransientState(false);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkedPosition != position) {
                    notifyItemChanged(checkedPosition);
                    checkedPosition = position;
                    notifyItemChanged(checkedPosition);
                }
            }
        });

        viewHolder.btnSelectPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String currentOperator = SharePreferenceManager.getUserData().getMobileNetworkOperator();

                if (currentOperator.equalsIgnoreCase("BSNL")) {
                    context.addFragment(WebFragment.newInstance("https://portal.bsnl.in/myportal/quickrecharge.do"));

//                    Log.d("RAZORPAY_ORDERID: ", SharePreferenceManager.getString(Constants.RPAYMENT_ORDER_ID));
//                    Log.d("RAZORPAY_AMOUNT: ", SharePreferenceManager.getInt(Constants.RPAYMENT_AMOUNT) + "");
//                    Log.d("RAZORPAY_STATUS: ", SharePreferenceManager.getString(Constants.RPAYMENT_STATUS));

                    /*Reset Preferences*/
//                    SharePreferenceManager.save(Constants.RPAYMENT_ORDER_ID, "");
//                    SharePreferenceManager.save(Constants.RPAYMENT_AMOUNT, 0);
//                    SharePreferenceManager.save(Constants.RPAYMENT_STATUS, "");

//                    if (SharePreferenceManager.getString(Constants.RPAYMENT_STATUS).equalsIgnoreCase("") ||
//                            SharePreferenceManager.getString(Constants.RPAYMENT_STATUS).equalsIgnoreCase("paid")) {
//                        // If order does not exists in preference. Create a new order...
//                        createNewOrder(item);
//
//                    } else {
////                        If check status other then paid. Initialize payment.
//                        Bundle bundle = new Bundle();
//                        bundle.putParcelable(Constants.DATA, item);
//
//                        Intent intent = new Intent(context, PaymentBSNLActivity.class);
//                        intent.putExtras(bundle);
//                        context.startActivity(intent);
//                    }

                } else {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Constants.DATA, item);

                    Intent intent = new Intent(context, PaymentActivity.class);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        private TextView txtPlanTitle, txtPlanCost, txtPlanDuration, txtPlanDescription;
        private LinearLayout linearIcons, topContainer, bottomContainer;
        private Button btnSelectPlan;

        public CustomViewHolder(View itemView) {
            super(itemView);
            txtPlanTitle = itemView.findViewById(R.id.txtPlanTitle);
            txtPlanCost = itemView.findViewById(R.id.txtPlanCost);
            txtPlanDuration = itemView.findViewById(R.id.txtPlanDuration);
            txtPlanDescription = itemView.findViewById(R.id.txtPlanDescription);

            linearIcons = itemView.findViewById(R.id.linearIcons);
            topContainer = itemView.findViewById(R.id.topContainer);
            bottomContainer = itemView.findViewById(R.id.bottomContainer);

            btnSelectPlan = itemView.findViewById(R.id.btnSelectPlan);
        }
    }

    public void createNewOrder(PackageChildModel packageChildModel) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.createOrder(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), String.valueOf(packageChildModel.getId()));

        APIClient.callAPI(context, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                Log.d("BSNL_ORDER_REQ:", "onSuccess: " + response);
                // {"id":"order_LAruHJptW3AsiE","entity":"order","amount":1100,"amount_paid":0,
                // "amount_due":1100,"currency":"INR","receipt":null,"offer_id":null,"status":"created",
                // "attempts":0,"notes":[],"created_at":1675186460}

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    RazorpayModel razorpayModel = gson.fromJson(reader, RazorpayModel.class);
                    SharePreferenceManager.save(Constants.RPAYMENT_ORDER_ID, razorpayModel.getId());
                    SharePreferenceManager.save(Constants.RPAYMENT_AMOUNT, razorpayModel.getAmount());
                    SharePreferenceManager.save(Constants.RPAYMENT_STATUS, razorpayModel.getStatus());

                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Constants.DATA, packageChildModel);

                    Intent intent = new Intent(context, PaymentBSNLActivity.class);
                    intent.putExtras(bundle);
                    context.startActivity(intent);

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    context.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                context.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                context.showMessageToUser(error);
            }
        });
    }
}