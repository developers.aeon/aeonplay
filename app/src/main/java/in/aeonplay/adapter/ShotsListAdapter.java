package in.aeonplay.adapter;

import static android.provider.MediaStore.Video.Thumbnails.MINI_KIND;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.ByteArrayOutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.VideoThumbnailTask;
import in.aeonplay.fragment.ShotsDetailsFragment;
import in.aeonplay.fragment.ShotsFragment;
import in.aeonplay.fragment.VideoUploadFragment;
import in.aeonplay.model.Shots.ShotDetailModel;
import in.aeonplay.model.Shots.ShotSubDataModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class ShotsListAdapter extends RecyclerView.Adapter {

    private MainActivity context;
    private static List<ShotSubDataModel> mItem;

    public ShotsListAdapter(MainActivity context, List<ShotSubDataModel> items) {
        this.context = context;
        this.mItem = items;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int position) {
        if (!mItem.isEmpty() && mItem != null) {
            final ViewHolder viewHolders = (ViewHolder) viewHolder;
            ShotSubDataModel shotSubDataModel = mItem.get(position);

            if (position == 0) {
                viewHolders.appImage.setBackgroundColor(Color.DKGRAY);
                Glide.with(context)
                        .asDrawable()
                        .load(R.drawable.ic_action_create_shots)
                        .transition(GenericTransitionOptions.with(R.anim.fade_in))
                        .placeholder(R.drawable.ic_action_create_shots)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(viewHolders.appImage);
            }

            if (shotSubDataModel.getDownloadUrl() != null) {
                Glide.with(context)
                        .load(shotSubDataModel.getThumbnail().getPortrait().getConverted())
                        .transition(GenericTransitionOptions.with(R.anim.fade_in))
                        .placeholder(R.drawable.ic_action_noimage)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(viewHolders.appImage);

//                String videoUrl = shotSubDataModel.getDownloadUrl();
//                VideoThumbnailTask task = new VideoThumbnailTask(context, viewHolders.appImage);
//                task.execute(videoUrl);
            }

            viewHolders.appMore.setVisibility(View.GONE);
            if (shotSubDataModel.getUserId() != null) {
                if ((int) shotSubDataModel.getUserId() == SharePreferenceManager.getUserData().getId()) {
                    viewHolders.appMore.setVisibility(View.VISIBLE);
                }
            }

            viewHolders.appMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context, R.style.AlertTheme);
                    alertDialog.setTitle("Remove shot!")
                            .setMessage("Are you sure you want to remove this shot?")
                            .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    deleteShotByID(shotSubDataModel.getId(), position);
                                    dialog.dismiss();
                                }
                            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    Dialog dialog = alertDialog.show();
                    dialog.show();
                }
            });

            viewHolders.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (position == 0) {
                        VideoUploadFragment videoUploadFragment = new VideoUploadFragment();
                        context.addFragment(videoUploadFragment);

                    } else {
//                        Bundle bundle = new Bundle();
//                        bundle.putInt(Constants.USER_ID, shotSubDataModel.getUserId());
//
//                        ShotsDetailsFragment shotsDetailsFragment = new ShotsDetailsFragment();
//                        shotsDetailsFragment.setArguments(bundle);
//                        context.addFragment(shotsDetailsFragment);
                        Bundle bundle = new Bundle();
                        bundle.putInt(Constants.INDEX, position);

                        ShotsFragment shotsDetailsFragment = new ShotsFragment();
                        shotsDetailsFragment.setArguments(bundle);
                        context.addFragment(shotsDetailsFragment);
                    }

                }
            });
        }
    }

    private void deleteShotByID(int ShotID, int position) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> loginCall = apiInterface.deleteShotsByID(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), String.valueOf(ShotID));

        APIClient.callAPI(context, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final ShotDetailModel shotsDetailModel = gson.fromJson(reader, ShotDetailModel.class);

                    if (shotsDetailModel.getData() != null) {
                        mItem.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, mItem.size());
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    context.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                context.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                context.showMessageToUser(error);
            }
        });
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.card_shot_item, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public int getItemCount() {
        return mItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected ImageView appImage, appMore;

        public ViewHolder(View itemView) {
            super(itemView);
            this.appImage = itemView.findViewById(R.id.itemImage);
            this.appMore = itemView.findViewById(R.id.appMore);
        }
    }
}
