package in.aeonplay.adapter;

import android.annotation.SuppressLint;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.model.SubscriptionModel.ActiveSubscription;

public class MyPlanAdapter extends RecyclerView.Adapter<MyPlanAdapter.CustomViewHolder> {

    private MainActivity context;
    private List<ActiveSubscription> mItems;
    private static int currentPosition = 0;
    private RecyclerView recyclerView;

    public MyPlanAdapter(MainActivity context, List<ActiveSubscription> item, RecyclerView recyclerView) {
        this.context = context;
        this.mItems = item;
        this.recyclerView = recyclerView;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, final int selectedItem) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_myplan_item, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, @SuppressLint("RecyclerView") final int position) {
        ActiveSubscription activeSubscription = mItems.get(position);

        String planValidity = "";
        if (activeSubscription.getSubscriptionPlan().getValidity() == 1) {
            planValidity = "Monthly";
        } else if (activeSubscription.getSubscriptionPlan().getValidity() == 6) {
            planValidity = "6 Month";
        } else if (activeSubscription.getSubscriptionPlan().getValidity() == 12) {
            planValidity = "Yearly";
        }

        customViewHolder.txtPlanValidity.setText(planValidity);

        String planAmount = activeSubscription.getSubscriptionPlan().getAmount() == 0 ? "FREE" : "INR " + activeSubscription.getSubscriptionPlan().getAmount();
        customViewHolder.txtPlanAmount.setText(planAmount);

        customViewHolder.txtPlanExpiry.setText(context.changeDateFormat(
                activeSubscription.getExpiryAt(), "yyyy-MM-dd HH:mm:ss", "dd MMM, yy"));

        if (activeSubscription.getSubscriptionPlan().getPackagegroup() != null) {
            customViewHolder.txtPlanTitle.setText(Html.fromHtml("<big>" +
                    activeSubscription.getSubscriptionPlan().getPackagegroup().getTitle() + "</big>"));

        } else {
            customViewHolder.txtPlanTitle.setText(Html.fromHtml("<big>" +
                    activeSubscription.getSubscriptionPlan().getTitle() + "</big>"));
        }

        subscriptionExpiredOrNot(customViewHolder, activeSubscription);
        customViewHolder.btnPlanUpgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.noActiveSubscription(context, "");
            }
        });
    }

    private void subscriptionExpiredOrNot(CustomViewHolder customViewHolder, ActiveSubscription activeSubscription) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date futureDate = sdf.parse(activeSubscription.getExpiryAt());

            if (Calendar.getInstance().getTime().getTime() > futureDate.getTime()) {
                customViewHolder.btnPlanUpgrade.setVisibility(View.VISIBLE);
                //context.noActiveSubscription(context, "");

            } else {
                //context.noActiveSubscription(context, "UPGRADE");
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        private Button btnPlanUpgrade;
        private TextView txtPlanTitle, txtPlanExpiry, txtPlanAmount, txtPlanValidity;

        public CustomViewHolder(View view) {
            super(view);
            btnPlanUpgrade = view.findViewById(R.id.btnPlanUpgrade);
            txtPlanTitle = view.findViewById(R.id.txtPlanTitle);
            txtPlanExpiry = view.findViewById(R.id.txtPlanExpiry);
            txtPlanAmount = view.findViewById(R.id.txtPlanAmount);
            txtPlanValidity = view.findViewById(R.id.txtPlanValidity);
        }
    }
}