package in.aeonplay.adapter;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.List;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.NativeAds;
import in.aeonplay.comman.SpaceItemDecorationHome;
import in.aeonplay.comman.VPIndicator;
import in.aeonplay.comman.ZoomOutTransformation;
import in.aeonplay.fragment.MoreFragment;
import in.aeonplay.fragment.MusicVideoFragment;
import in.aeonplay.fragment.NewsFragment;
import in.aeonplay.fragment.ShotsFragment;
import in.aeonplay.model.Comman.CommanDataList;
import in.aeonplay.model.Comman.CommanHeader;
import in.aeonplay.preferences.SharePreferenceManager;

public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_HEADER = 0; // Latest
    private static final int ITEM_WATCHLIST = 1; // Watchlist
    private static final int ITEM_AD = 2; // Advertisement (ISKCON)
    private static final int ITEM_CAMPAIGN = 3; // Campaign
    private static final int ITEM_TOP10 = 4; // Top 10
    private static final int ITEM_COMING_SOON = 5; // Coming soon
    private static final int ITEM_NEWS = 6; // Newspaper
    private static final int ITEM_VIEW = 7; // All Items
    private static final int ITEM_LOADING = 8; // Loader
    private static final int ITEM_SHOT = 9; // Shot
    private static final int ITEM_VIDEO_SONG = 10; // Video Song

    private final MainActivity mContext;
    private final List<CommanHeader> commanHeaders;
    private CountDownTimer countDownTimer;
    private int advertisementIndex = 1;
    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;
    public static WatchlistAdapter watchlistAdapter;

    public HomeAdapter(MainActivity activity) {
        this.mContext = activity;
        this.commanHeaders = new ArrayList<>();
    }

    public List<CommanHeader> getMovies() {
        return commanHeaders;
    }

    public void add(CommanHeader r) {
        commanHeaders.add(r);
        //notifyItemInserted(movieResults.size() - 1);
    }

    public void addAll(List<CommanHeader> moveResults) {
        for (CommanHeader result : moveResults) {
            add(result);
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new CommanHeader());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = commanHeaders.size() - 1;
        CommanHeader result = getItem(position);

        if (result != null) {
            commanHeaders.remove(position);
            notifyItemRemoved(position);
        }
    }

    public CommanHeader getItem(int position) {
        return commanHeaders.get(position);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case ITEM_AD:
                View viewAd = inflater.inflate(R.layout.card_adview_item, viewGroup, false);
                viewHolder = new AdRowHolder(viewAd);
                break;
            case ITEM_VIEW:
                View viewModel = inflater.inflate(R.layout.card_home_item, viewGroup, false);
                viewHolder = new ItemRowHolder(viewModel);
                break;
            case ITEM_LOADING:
                View viewLoading = inflater.inflate(R.layout.card_progress_item, viewGroup, false);
                viewHolder = new LoadingHolder(viewLoading);
                break;
            case ITEM_HEADER:
                View viewHeader = inflater.inflate(R.layout.card_header, viewGroup, false);
                viewHolder = new HeaderRowHolder(viewHeader);
                break;
            case ITEM_NEWS:
                View viewHeaderNews = inflater.inflate(R.layout.card_home_item, viewGroup, false);
                viewHolder = new ItemRowHolder(viewHeaderNews);
                break;
            case ITEM_WATCHLIST:
                View viewHeaderCV = inflater.inflate(R.layout.card_home_item, viewGroup, false);
                viewHolder = new ItemRowHolder(viewHeaderCV);
                break;
            case ITEM_TOP10:
                View viewModelMov = inflater.inflate(R.layout.card_home_item, viewGroup, false);
                viewHolder = new ItemRowHolder(viewModelMov);
                break;
            case ITEM_COMING_SOON:
                View viewModelCS = inflater.inflate(R.layout.card_home_item, viewGroup, false);
                viewHolder = new ItemRowHolder(viewModelCS);
                break;
            case ITEM_CAMPAIGN:
                View viewModelCampaign = inflater.inflate(R.layout.card_campaign, viewGroup, false);
                viewHolder = new CampaignRowHolder(viewModelCampaign);
                break;
            case ITEM_SHOT:
                View viewModelShot = inflater.inflate(R.layout.card_home_item, viewGroup, false);
                viewHolder = new ItemRowHolder(viewModelShot);
                break;
            case ITEM_VIDEO_SONG:
                View viewHeaderVS = inflater.inflate(R.layout.card_home_item, viewGroup, false);
                viewHolder = new ItemRowHolder(viewHeaderVS);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {
            case ITEM_AD:
                AdRowHolder adRowHolder = (AdRowHolder) holder;
                NativeAds.ShowIskconAds(mContext, adRowHolder.adsContainer);
                //NativeAds.ShowNativeAdsIndex(mContext, adRowHolder.adsContainer, position);

                break;

            case ITEM_VIEW:
                CommanHeader commanHeader = getMovies().get(position);
                ItemRowHolder itemRowHolder = (ItemRowHolder) holder;

                itemRowHolder.itemTitle.setText(commanHeader.getTitle().split("_")[0]);
                HomeChildAdapter itemListDataAdapter = new HomeChildAdapter(mContext, commanHeader.getDatum());

                itemRowHolder.mRecyclerChildItem.setLayoutManager(
                        new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
                itemRowHolder.mRecyclerChildItem.setAdapter(itemListDataAdapter);

                itemRowHolder.itemMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.TITLE, commanHeader.getTitle().split("_")[0]);
                        bundle.putParcelableArrayList(Constants.DATA, commanHeader.getDatum());
                        bundle.putInt(Constants.CATEGORY_ID, Integer.parseInt(commanHeader.getTitle().split("_")[1]));

                        MoreFragment moreFragment = new MoreFragment();
                        moreFragment.setArguments(bundle);
                        mContext.addFragment(moreFragment);
                    }
                });
                break;

            case ITEM_LOADING:
                LoadingHolder loadingVH = (LoadingHolder) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

//                    loadingVH.mErrorTxt.setText(
//                            errorMsg != null ?
//                                    errorMsg :
//                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;

            case ITEM_HEADER:
                HeaderRowHolder headerRowHolder = (HeaderRowHolder) holder;
                getDashboardVPList(headerRowHolder);
                break;

            case ITEM_NEWS:
                ItemRowHolder itemRowHolderNews = (ItemRowHolder) holder;
                getNewsList(itemRowHolderNews);
                break;

            case ITEM_WATCHLIST:
                ItemRowHolder itemRowHolderCW = (ItemRowHolder) holder;
                getWatchList(itemRowHolderCW);
                break;

            case ITEM_TOP10:
                ItemRowHolder itemRowHolderTop = (ItemRowHolder) holder;
                itemRowHolderTop.itemTitle.setVisibility(View.GONE);
                itemRowHolderTop.itemMore.setVisibility(View.GONE);

                HomeChildTop10Adapter homeChildTop10Adapter = new HomeChildTop10Adapter(mContext, mContext.mTop10List);
                itemRowHolderTop.mRecyclerChildItem.setLayoutManager(
                        new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
                itemRowHolderTop.mRecyclerChildItem.setItemAnimator(new DefaultItemAnimator());
                itemRowHolderTop.mRecyclerChildItem.setAdapter(homeChildTop10Adapter);

                if (!mContext.mTop10List.isEmpty()) {
                    itemRowHolderTop.itemTitle.setVisibility(View.VISIBLE);
                    itemRowHolderTop.itemTitle.setText("Today's Top10");
                    homeChildTop10Adapter.notifyDataSetChanged();
                }

                break;

            case ITEM_COMING_SOON:
                ItemRowHolder itemRowHolderCS = (ItemRowHolder) holder;
                itemRowHolderCS.itemTitle.setVisibility(View.GONE);
                itemRowHolderCS.itemMore.setVisibility(View.GONE);

                HomeChildAdapter homeChildAdapterCS = new HomeChildAdapter(mContext, mContext.mComingSoonList);
                itemRowHolderCS.mRecyclerChildItem.setLayoutManager(
                        new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
                itemRowHolderCS.mRecyclerChildItem.setItemAnimator(new DefaultItemAnimator());
                itemRowHolderCS.mRecyclerChildItem.setAdapter(homeChildAdapterCS);

                if (!mContext.mComingSoonList.isEmpty()) {
                    itemRowHolderCS.itemTitle.setVisibility(View.VISIBLE);
                    itemRowHolderCS.itemTitle.setText("Coming soon");
                    homeChildAdapterCS.notifyDataSetChanged();
                }

                break;

            case ITEM_CAMPAIGN:
                CampaignRowHolder campaignRowHolder = (CampaignRowHolder) holder;
                getCampaignList(campaignRowHolder);
                break;

            case ITEM_SHOT:
                ItemRowHolder itemRowHolderShot = (ItemRowHolder) holder;
                itemRowHolderShot.itemTitle.setVisibility(View.GONE);
                itemRowHolderShot.itemMore.setVisibility(View.GONE);

                ShotsListAdapter shotsListAdapter = new ShotsListAdapter(mContext, mContext.mShotList);
                itemRowHolderShot.mRecyclerChildItem.setLayoutManager(
                        new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
                itemRowHolderShot.mRecyclerChildItem.setItemAnimator(new DefaultItemAnimator());
                itemRowHolderShot.mRecyclerChildItem.setAdapter(shotsListAdapter);

                if (!mContext.mShotList.isEmpty()) {
                    itemRowHolderShot.itemTitle.setVisibility(View.VISIBLE);
                    itemRowHolderShot.itemMore.setVisibility(View.VISIBLE);
                    itemRowHolderShot.itemTitle.setText("Aeonplay Shots");
                    shotsListAdapter.notifyDataSetChanged();
                }

                itemRowHolderShot.itemMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ShotsFragment shotsFragment = new ShotsFragment();
                        mContext.addFragment(shotsFragment);
                    }
                });

                break;

            case ITEM_VIDEO_SONG:
                ItemRowHolder itemRowHolderVS = (ItemRowHolder) holder;
                getMusicVideo(itemRowHolderVS);
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + getItemViewType(position));
        }
    }

    private void getNewsList(ItemRowHolder itemRowHolder) {
        itemRowHolder.itemTitle.setVisibility(View.GONE);
        itemRowHolder.itemMore.setVisibility(View.GONE);

        NewsDataAdapter newsDataAdapter = new NewsDataAdapter(mContext, mContext.mNewsList);
        itemRowHolder.mRecyclerChildItem.setLayoutManager(
                new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
        itemRowHolder.mRecyclerChildItem.setAdapter(newsDataAdapter);

        itemRowHolder.itemMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.addFragment(new NewsFragment());
            }
        });

        if (mContext.mNewsList.size() > 0) {
            itemRowHolder.itemTitle.setText(mContext.getString(R.string.menu_item_upcoming0));
            itemRowHolder.itemTitle.setVisibility(View.VISIBLE);
            itemRowHolder.itemMore.setVisibility(View.VISIBLE);
            newsDataAdapter.notifyDataSetChanged();
        }
    }

    private void getMusicVideo(ItemRowHolder itemRowHolder) {
        itemRowHolder.itemTitle.setVisibility(View.GONE);
        itemRowHolder.itemMore.setVisibility(View.GONE);
        MusicVideoAdapter musicVideoAdapter = new MusicVideoAdapter(mContext);
        musicVideoAdapter.addAll(mContext.mMusicVideo);
        itemRowHolder.mRecyclerChildItem.setLayoutManager(
                new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
        itemRowHolder.mRecyclerChildItem.setAdapter(musicVideoAdapter);

        itemRowHolder.itemMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.LANGUAGE, "hindi");

                MusicVideoFragment musicVideoFragment = new MusicVideoFragment();
                musicVideoFragment.setArguments(bundle);
                mContext.addFragment(musicVideoFragment);
            }
        });

        if (mContext.mMusicVideo.size() > 0) {
            itemRowHolder.itemTitle.setText("Sangitik Ram");
            itemRowHolder.itemMore.setVisibility(View.VISIBLE);
            itemRowHolder.itemTitle.setVisibility(View.VISIBLE);
            musicVideoAdapter.notifyDataSetChanged();
        }
    }

    public void getWatchList(ItemRowHolder itemRowHolder) {

        itemRowHolder.itemMore.setVisibility(View.GONE);
        itemRowHolder.itemTitle.setVisibility(View.GONE);
        itemRowHolder.itemTitle.setText("Watchlist for " + SharePreferenceManager.getUserData().getFirstName() + " " +
                SharePreferenceManager.getUserData().getLastName());

        watchlistAdapter = new WatchlistAdapter(mContext, mContext.mWatchDataList);
        itemRowHolder.mRecyclerChildItem.setLayoutManager(
                new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
        itemRowHolder.mRecyclerChildItem.setAdapter(watchlistAdapter);

        if (!mContext.mWatchDataList.isEmpty()) {
            itemRowHolder.itemTitle.setVisibility(View.VISIBLE);
            watchlistAdapter.notifyDataSetChanged();
        }
    }

    public void getCampaignList(CampaignRowHolder campaignRowHolder) {

        if (mContext.isDarkTheme()) {
            campaignRowHolder.mVPContainer.setBackgroundResource(R.color.colorBlack10);

        } else {
            campaignRowHolder.mVPContainer.setBackgroundResource(R.color.colorWhite10);
        }

        if (!mContext.mCampaignList.isEmpty()) {
            PagerAdapter adapter = new CampaignAdapter(mContext, mContext.mCampaignList);
            campaignRowHolder.mViewPager.setAdapter(adapter);

            int limit = (adapter.getCount() > 1 ? adapter.getCount() - 1 : 1);
            campaignRowHolder.mViewPager.setOffscreenPageLimit(limit);

            campaignRowHolder.mViewPager.setPageMargin((int) mContext.getResources().getDimension(com.intuit.sdp.R.dimen._3sdp));
            campaignRowHolder.mViewPager.setPageTransformer(true, new ZoomOutTransformation(true));
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public int getItemViewType(int position) {
        CommanHeader recyclerViewItem = getMovies().get(position);

        if (recyclerViewItem instanceof CommanHeader) {
            if (((CommanHeader) recyclerViewItem).isAdvertisement())
                return ITEM_AD;
            if (((CommanHeader) recyclerViewItem).isHeader())
                return ITEM_HEADER;
            if (((CommanHeader) recyclerViewItem).isNews())
                return ITEM_NEWS;
            if (((CommanHeader) recyclerViewItem).isWatchlist())
                return ITEM_WATCHLIST;
            if (((CommanHeader) recyclerViewItem).isTop10())
                return ITEM_TOP10;
            if (((CommanHeader) recyclerViewItem).isComingSoon())
                return ITEM_COMING_SOON;
            if (((CommanHeader) recyclerViewItem).isCampaign())
                return ITEM_CAMPAIGN;
            if (((CommanHeader) recyclerViewItem).isShot())
                return ITEM_SHOT;
            if (((CommanHeader) recyclerViewItem).isVideoSong())
                return ITEM_VIDEO_SONG;
            else
                return (position == getMovies().size() - 1 && isLoadingAdded) ? ITEM_LOADING : ITEM_VIEW;

        } else
            return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return getMovies() == null ? 0 : getMovies().size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView itemTitle, itemMore;
        protected RecyclerView mRecyclerChildItem;
        protected LinearLayout linearSectionItem;

        public ItemRowHolder(View view) {
            super(view);
            this.linearSectionItem = view.findViewById(R.id.linearSectionItem);
            this.itemTitle = view.findViewById(R.id.itemTitle);
            this.itemMore = view.findViewById(R.id.itemMore);
            this.mRecyclerChildItem = view.findViewById(R.id.recycler_childItem);
            mRecyclerChildItem.addItemDecoration(new SpaceItemDecorationHome());
        }
    }

    public static class AdRowHolder extends RecyclerView.ViewHolder {

        protected FrameLayout adsContainer;

        public AdRowHolder(View view) {
            super(view);
            this.adsContainer = view.findViewById(R.id.adsContainer);
        }
    }

    public static class HeaderRowHolder extends RecyclerView.ViewHolder {

        private ViewPager mViewPager;
        private TextView mAdvertisementTitle, mAdvertisementGenre;
        private Button btnWatchNow;
        private LinearLayout linearMyList, linearInfo;
        private VPIndicator mIndicator;

        public HeaderRowHolder(View view) {
            super(view);
            mViewPager = view.findViewById(R.id.viewPager);
            mIndicator = view.findViewById(R.id.indicator);
            linearMyList = view.findViewById(R.id.linearMyList);
            linearInfo = view.findViewById(R.id.linearInfo);
            mAdvertisementTitle = view.findViewById(R.id.txtAdvertisementTitle);
            mAdvertisementGenre = view.findViewById(R.id.txtAdvertisementGenre);
            btnWatchNow = view.findViewById(R.id.btnWatchNow);
        }
    }

    private void getDashboardVPList(HeaderRowHolder headerRowHolder) {
        if (mContext.mLatestList.size() > 0) {
            PagerAdapter adapter = new AdvertisementAdapter(mContext, mContext.mLatestList);
            headerRowHolder.mViewPager.setAdapter(adapter);

            int limit = (adapter.getCount() > 1 ? adapter.getCount() - 1 : 1);
            headerRowHolder.mViewPager.setOffscreenPageLimit(limit);

            headerRowHolder.mViewPager.setPageMargin((int) mContext.getResources().getDimension(com.intuit.sdp.R.dimen._3sdp));
            headerRowHolder.mIndicator.setupViewPager(headerRowHolder.mViewPager);

            headerRowHolder.mViewPager.setCurrentItem(advertisementIndex, true);
            headerRowHolder.mViewPager.setPageTransformer(true, new ZoomOutTransformation(true));
            autoScroll(headerRowHolder, mContext.mLatestList);
        }
    }

    protected class LoadingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingHolder(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.loadMORE_progress);
            mRetryBtn = itemView.findViewById(R.id.loadMORE_retry);
            mErrorTxt = itemView.findViewById(R.id.loadMORE_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadMORE_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadMORE_retry:
                case R.id.loadMORE_errorlayout:

                    showRetry(false, null, getAdapterPosition());
//                    mCallback.retryPageLoad();
                    break;
            }
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg, int position) {
        retryPageLoad = show;
        notifyItemChanged(commanHeaders.get(position).getDatum().size() - 1);

//        if (errorMsg != null) this.errorMsg = errorMsg;
    }

    private void autoScroll(HeaderRowHolder headerRowHolder, List<CommanDataList> promotions) {
        if (promotions.size() > 0) {
            countDownTimer = new CountDownTimer(30000, 1000) {
                public void onTick(long millisUntilFinished) {

                }

                public void onFinish() {
                    // For Random Position ...
//                    Random rand = new Random();
//                    advertisementIndex = rand.nextInt(promotions.size());

                    // For Normal Position ...
                    if (advertisementIndex >= (promotions.size() - 1)) {
                        advertisementIndex = 0;
                    } else {
                        advertisementIndex = advertisementIndex + 1;
                    }

                    headerRowHolder.mViewPager.setCurrentItem(advertisementIndex, true);
                    countDownTimer.start();
                }
            };

            countDownTimer.start();
        }
    }

    public static class CampaignRowHolder extends RecyclerView.ViewHolder {

        private ViewPager mViewPager;
        private RelativeLayout mVPContainer;

        public CampaignRowHolder(View view) {
            super(view);
            mViewPager = view.findViewById(R.id.viewPager);
            mVPContainer = view.findViewById(R.id.vpContainer);
        }
    }

}