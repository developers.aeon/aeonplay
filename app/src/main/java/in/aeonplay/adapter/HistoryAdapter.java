package in.aeonplay.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import in.aeonplay.R;
import in.aeonplay.model.SubscriptionModel.SubscriptionHistory;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.CustomViewHolder> {

    private Activity context;
    private List<SubscriptionHistory> mItems;
    private static int currentPosition = 0;
    private RecyclerView recyclerView;

    public HistoryAdapter(Activity context, List<SubscriptionHistory> item, RecyclerView recyclerView) {
        this.context = context;
        this.mItems = item;
        this.recyclerView = recyclerView;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, final int selectedItem) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_transaction_item, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, @SuppressLint("RecyclerView") final int position) {
        SubscriptionHistory subscriptionHistory = mItems.get(position);

        final boolean isExpanded = position == currentPosition;
        customViewHolder.moreDetails.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        customViewHolder.itemView.setActivated(isExpanded);

        if (isExpanded) {
            customViewHolder.ivExpand.setImageResource(R.drawable.ic_action_up);
        } else {
            customViewHolder.ivExpand.setImageResource(R.drawable.ic_action_down);
        }

        viewExpand(isExpanded, customViewHolder.ivExpand);
        customViewHolder.ivExpand.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                currentPosition = isExpanded ? -1 : position;
                TransitionManager.endTransitions(recyclerView);
                notifyDataSetChanged();
            }
        });

        String pkgGroupTitle = subscriptionHistory.getSubscriptionPlan().getPackagegroup() != null ?
                subscriptionHistory.getSubscriptionPlan().getPackagegroup().getTitle() + " " +
                        subscriptionHistory.getSubscriptionPlan().getTitle() :
                subscriptionHistory.getSubscriptionPlan().getTitle();

        customViewHolder.txtPackageGroup.setText(pkgGroupTitle);
        customViewHolder.txtPackageName.setText(subscriptionHistory.getSubscriptionPlan().getType());

        String amount = subscriptionHistory.getSubscriptionPlan().getAmount().toString()
                .equalsIgnoreCase("0") ? "FREE" : "INR " + subscriptionHistory.getSubscriptionPlan().getAmount().toString();
        customViewHolder.txtTransactionAmount.setText(amount);

        customViewHolder.txtSubscriptionDt.setText(changeDateFormat(subscriptionHistory.getSubscribedAt()));
        customViewHolder.txtExpiryDt.setText(changeDateFormat(subscriptionHistory.getExpiryAt()));
        customViewHolder.txtTransactionID.setText(subscriptionHistory.getPayment().getTxnid());
    }

    public String changeDateFormat(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void viewExpand(boolean isExpanded, ImageView ivExpand) {
        if (isExpanded) {
            ivExpand.setImageResource(R.drawable.ic_action_up);
        } else {
            ivExpand.setImageResource(R.drawable.ic_action_down);
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout moreDetails;
        private TextView txtPackageGroup, txtPackageName, txtTransactionAmount,
                txtSubscriptionDt, txtExpiryDt, txtTransactionID;
        private ImageView ivExpand;

        public CustomViewHolder(View view) {
            super(view);
            this.moreDetails = view.findViewById(R.id.moreDetails);
            this.txtPackageGroup = view.findViewById(R.id.txtPackageGroup);
            this.txtPackageName = view.findViewById(R.id.txtPackageName);
            this.txtTransactionAmount = view.findViewById(R.id.txtTransactionAmount);
            this.txtSubscriptionDt = view.findViewById(R.id.txtSubscriptionDt);
            this.txtExpiryDt = view.findViewById(R.id.txtExpiryDt);
            this.txtTransactionID = view.findViewById(R.id.txtTransactionID);
            this.ivExpand = view.findViewById(R.id.ivExpand);
        }
    }
}