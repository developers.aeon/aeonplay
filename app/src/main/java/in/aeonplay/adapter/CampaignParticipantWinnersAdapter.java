package in.aeonplay.adapter;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.fragment.ShotsDetailsFragment;
import in.aeonplay.model.Shots.Campaign.CampaignResultDataModel;

public class CampaignParticipantWinnersAdapter extends RecyclerView.Adapter implements Filterable {

    private static List<CampaignResultDataModel> mItem;
    private MainActivity context;
    private CampaignWinnersFilter mFilter;
    private String mSearchText = "";

    public CampaignParticipantWinnersAdapter(MainActivity context, List<CampaignResultDataModel> items) {
        this.context = context;
        this.mItem = items;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new CampaignWinnersFilter(mItem, this);
        }
        return mFilter;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.card_item_followers, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (!mItem.isEmpty() && mItem != null) {
            final ViewHolder viewHolders = (ViewHolder) viewHolder;

            CampaignResultDataModel campaignResultDataModel = mItem.get(position);
            viewHolders.txtComman.setText(campaignResultDataModel.getUser().getMobileNo());
            viewHolders.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.USER_ID, campaignResultDataModel.getUserId());

                    ShotsDetailsFragment shotsDetailsFragment = new ShotsDetailsFragment();
                    shotsDetailsFragment.setArguments(bundle);
                    context.addFragment(shotsDetailsFragment);
                }
            });

            String name = campaignResultDataModel.getUser().getFirstName().toUpperCase() + " " +
                    campaignResultDataModel.getUser().getLastName().toUpperCase();
            if (mSearchText != null && !mSearchText.isEmpty()) {
                int startName = name.toUpperCase().indexOf(mSearchText.toUpperCase());
                int endName = startName + mSearchText.length();

                if (startName != -1) {
                    Spannable spannable = new SpannableString(name);
                    spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#FFBD59")), startName, endName, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    viewHolders.txtName.setText(spannable);
                } else {
                    viewHolders.txtName.setText(name);
                }

            } else {
                viewHolders.txtName.setText(name);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mItem.size();
    }

    public class CampaignWinnersFilter extends Filter {
        private CampaignParticipantWinnersAdapter adapter;
        private List<CampaignResultDataModel> filterList;

        public CampaignWinnersFilter(List<CampaignResultDataModel> filterList, CampaignParticipantWinnersAdapter adapter) {
            this.adapter = adapter;
            this.filterList = filterList;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint.toString().isEmpty()) {
                mSearchText = "";
            } else {
                mSearchText = (String) constraint;
            }

            if (constraint != null && constraint.length() > 0) {
                constraint = constraint.toString().toUpperCase();
                ArrayList<CampaignResultDataModel> filteredPlayers = new ArrayList<>();

                for (int i = 0; i < filterList.size(); i++) {
                    String name = filterList.get(i).getUser().getFirstName().toUpperCase() + " " +
                            filterList.get(i).getUser().getLastName().toUpperCase();
                    if (name.contains(constraint)) {
                        filteredPlayers.add(filterList.get(i));
                    }
                }
                results.count = filteredPlayers.size();
                results.values = filteredPlayers;
            } else {
                results.count = filterList.size();
                results.values = filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mItem = (ArrayList<CampaignResultDataModel>) results.values;
            adapter.notifyDataSetChanged();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView txtName, txtComman;

        public ViewHolder(View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtNames);
            txtComman = itemView.findViewById(R.id.txtComman);
        }
    }
}
