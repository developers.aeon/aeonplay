package in.aeonplay.adapter;

import android.animation.LayoutTransition;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ShareCompat;
import androidx.media3.ui.PlayerView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Reader;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import in.aeonplay.BuildConfig;
import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.fragment.ShotsDetailsFragment;
import in.aeonplay.model.Shots.ShotDataModel;
import in.aeonplay.model.Shots.ShotDetailModel;
import in.aeonplay.model.Shots.ShotSubDataModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class ShotsAdapter extends RecyclerView.Adapter<ShotsAdapter.VideoViewHolder> {

    private MainActivity mContext;
    boolean isVote = false;
    private ShotDataModel mShotDataModel;
    private final static int MAX_LINES_COLLAPSED = 3;
    private boolean isCollapsed = true;
    private List<ShotSubDataModel> mVieoUrls = new ArrayList<>();
    private List<ShotSubDataModel> mTempList = new ArrayList<>();

    public ShotsAdapter(MainActivity context) {
        super();
        this.mContext = context;
    }

    public void setDataList(List<ShotSubDataModel> videoUrls) {
//        mVieoUrls.clear();
//        mVieoUrls.addAll(videoUrls);
//        notifyDataSetChanged();
        int startPosition = mVieoUrls.size();
        mVieoUrls.addAll(videoUrls);
        mTempList.addAll(videoUrls);
        // Notify the adapter that data has been inserted
        notifyItemRangeInserted(startPosition, videoUrls.size());
    }

    public void addDataList(List<ShotSubDataModel> videoUrls) {
        mVieoUrls.addAll(videoUrls);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VideoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.card_shots_item, parent, false);
        return new VideoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoViewHolder holder, @SuppressLint("RecyclerView") int position) {
        ShotSubDataModel shotSubDataModel = mVieoUrls.get(position);

        holder.itemView.setTag(position);
        holder.mTitle.setText(shotSubDataModel.getTitle());
        holder.mDescription.setText(shotSubDataModel.getDescription());

        LayoutTransition transition = new LayoutTransition();
        transition.setDuration(500);
        transition.enableTransitionType(LayoutTransition.CHANGING);
        holder.mDescContainer.setLayoutTransition(transition);

        holder.mTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.USER_ID, shotSubDataModel.getUserId());

                ShotsDetailsFragment shotsDetailsFragment = new ShotsDetailsFragment();
                shotsDetailsFragment.setArguments(bundle);
                mContext.addFragment(shotsDetailsFragment);
            }
        });

        holder.mDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCollapsed) {
                    holder.mDescription.setMaxLines(Integer.MAX_VALUE);
                } else {
                    holder.mDescription.setMaxLines(MAX_LINES_COLLAPSED);
                }

                isCollapsed = !isCollapsed;
            }
        });

        holder.mVideoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.mVideoView.getPlayer() != null) {
                    if (holder.mVideoView.getPlayer().isPlaying()) {
                        holder.mVideoView.getPlayer().pause();
                    } else {
                        holder.mVideoView.getPlayer().play();
                    }
                }
            }
        });

        holder.mImgLike.setImageResource(!shotSubDataModel.isIs_liked() ? R.drawable.ic_action_like : R.drawable.ic_action_like_selected);
        holder.mImgDislike.setImageResource(!shotSubDataModel.isIs_disliked() ? R.drawable.ic_action_dislike : R.drawable.ic_action_dislike_selected);

//        holder.mLikeCount.setText(!shotSubDataModel.isIs_liked() ? "Like" : shotSubDataModel.getLikes_count() + "");
//        holder.mDislikeCount.setText(!shotSubDataModel.isIs_disliked() ? "Dislike" : shotSubDataModel.getDislikes_count() + "");

        // Check vote is enable or disable
        if (shotSubDataModel.getCampaignId() == null) {
            holder.mLinearVote.setVisibility(View.INVISIBLE);
//            holder.mLinearVote.setVisibility(View.VISIBLE);
        }

        if (shotSubDataModel.getCampaign() != null) {
            if (shotSubDataModel.getCampaign().getLastVoted() == null) {
                holder.mLinearVote.setVisibility(View.VISIBLE);
            }

            if (shotSubDataModel.getCampaign().getLastVoted() != null) {
                String voteAt = shotSubDataModel.getCampaign().getLastVoted().getNextVote();
                SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    Date lastVoteDate = inputFormat.parse(voteAt);
                    Date currentDate = new Date();

                    holder.mLinearVote.setVisibility(View.VISIBLE);
                    if (lastVoteDate.getTime() >= currentDate.getTime()) {
                        holder.mLinearVote.setVisibility(View.INVISIBLE);
//                        holder.mLinearVote.setVisibility(View.VISIBLE);
                    }

                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        holder.mLinearVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addToVote(shotSubDataModel.getId() + "", holder);
            }
        });

        holder.mLinearLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addToFeeling(shotSubDataModel.getId() + "", "like", holder, position);
            }
        });

        holder.mLinearDislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addToFeeling(shotSubDataModel.getId() + "", "dislike", holder, position);
            }
        });

        holder.mLinearShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.ic_action_share_shots);

                    Uri bitmapUri = mContext.saveImageExternal(bitmap);
                    Intent shareIntent = ShareCompat.IntentBuilder.from(mContext)
                            .setType("image/*")
                            .setStream(bitmapUri)
                            .setText("Check out " + shotSubDataModel.getTitle() + " on Aeonplay! " + BuildConfig.AUTHORIZE_URL + "shorts/" + shotSubDataModel.getId())
                            .getIntent();

                    if (shareIntent.resolveActivity(mContext.getPackageManager()) != null) {
                        mContext.startActivity(shareIntent);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

//        int followedUserID = shotSubDataModel.getUserId();
//
//        for(ShotSubDataModel shotSubDataModel1 : mVieoUrls){
//            if (shotSubDataModel1.getUserId() == followedUserID) {
//                shotSubDataModel1.setIs_following(false);
//            }
//        }

        holder.mFollow.setText(shotSubDataModel.isIs_following() ? "FOLLOWING" : "FOLLOW");
        holder.mFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isFollowed = false;
                if (shotSubDataModel.isIs_following()) {
                    postUnfollow(shotSubDataModel.getUserId() + "", holder);
                    isFollowed = false;

                } else {
                    postFollow(shotSubDataModel.getUserId() + "", holder);
                    isFollowed = true;
                }

                int followedUserID = shotSubDataModel.getUserId();
                for(int i = 0 ; i < mVieoUrls.size(); ++i) {
                    ShotSubDataModel shotSubDataModel1 = mVieoUrls.get(i);
                    if (shotSubDataModel1.getUserId() == followedUserID) {
                        shotSubDataModel1.setIs_following(isFollowed);
                        mVieoUrls.set(i, shotSubDataModel1);
                    }
                }
                notifyDataSetChanged();
            }
        });

//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
//            Date apiDate = sdf.parse(shotSubDataModel.getAwardAnnouncement());
//            Date currentDate = new Date();
//
//            if (currentDate.compareTo(apiDate) >= 0) {
//                holder.mLinearVote.setVisibility(View.VISIBLE);
//            } else {
//                holder.mLinearVote.setVisibility(View.GONE);
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    private void addToVote(String id, VideoViewHolder videoViewHolder) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.postVote(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), id);

        APIClient.callAPI(mContext, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject != null) {
                        JSONObject joData = jsonObject.getJSONObject("data");
                        boolean isVoted = joData.optBoolean("voted");
                        if (isVoted) {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext, R.style.AlertTheme);
                            alertDialog.setTitle("Dear User!")
                                    .setMessage("You votes is successfully submitted.")
                                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            videoViewHolder.mImgVote.setImageResource(R.drawable.ic_action_vote_selected);
                                            dialog.dismiss();
                                        }
                                    });
                            Dialog dialog = alertDialog.show();
                            dialog.show();
                        }

                    } else {
                        String error = jsonObject.optString("error");
                        mContext.showMessageToUser(error);
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    mContext.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                try {
                    JSONObject jsonObject = new JSONObject(error);
                    mContext.showMessageToUser(jsonObject.optString("error"));
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public void onError(String error) {
                mContext.showMessageToUser(error);
            }
        });
    }

    private void addToFeeling(String id, String type, VideoViewHolder videoViewHolder, int position) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.postFeeling(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), id, type);

        APIClient.callAPI(mContext, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final ShotDetailModel shotsDetailModel = gson.fromJson(reader, ShotDetailModel.class);

                    if (shotsDetailModel.getData() != null) {
                        videoViewHolder.mImgLike.setImageResource(!shotsDetailModel.getData().isIs_liked() ? R.drawable.ic_action_like : R.drawable.ic_action_like_selected);
                        videoViewHolder.mImgDislike.setImageResource(!shotsDetailModel.getData().isIs_disliked() ? R.drawable.ic_action_dislike : R.drawable.ic_action_dislike_selected);

                        mVieoUrls.set(position, shotsDetailModel.getData());
                        notifyItemChanged(position);
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    mContext.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                mContext.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                mContext.showMessageToUser(error);
            }
        });
    }

    private void postFollow(String userID, VideoViewHolder videoViewHolder) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.postFollow(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), userID);

        APIClient.callAPI(mContext, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    videoViewHolder.mFollow.setText("FOLLOWING");

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    mContext.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                mContext.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                mContext.showMessageToUser(error);
            }
        });
    }

    private void postUnfollow(String userID, VideoViewHolder videoViewHolder) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.postUnfollow(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), userID);

        APIClient.callAPI(mContext, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    videoViewHolder.mFollow.setText("FOLLOW");

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    mContext.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                mContext.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                mContext.showMessageToUser(error);
            }
        });
    }

    public String getUrlByPos(int pos) {
        return mVieoUrls.get(pos).getDownloadUrl();
    }

    @Override
    public int getItemCount() {
        return mVieoUrls.size();
    }

    public ShotDataModel getData() {
        return mShotDataModel;
    }

    public void setData(ShotDataModel datum) {
        this.mShotDataModel = datum;
    }

    public class VideoViewHolder extends RecyclerView.ViewHolder {
        private PlayerView mVideoView;
        private TextView mTitle, mDescription, mLikeCount, mDislikeCount, mFollow;
        private ImageView mImgVote, mImgLike, mImgDislike;
        private ConstraintLayout mDescContainer;
        private LinearLayout mLinearVote, mLinearLike, mLinearDislike, mLinearShare;
        private SeekBar mSeekbarProgress;

        VideoViewHolder(View itemView) {
            super(itemView);
            mVideoView = itemView.findViewById(R.id.player_view_story);
            mDescContainer = itemView.findViewById(R.id.desc_container);
            mTitle = itemView.findViewById(R.id.txtTitle);
            mDescription = itemView.findViewById(R.id.txtDescription);

            mFollow = itemView.findViewById(R.id.txtFollow);
            mImgVote = itemView.findViewById(R.id.imgVote);
            mImgLike = itemView.findViewById(R.id.imgLike);
            mImgDislike = itemView.findViewById(R.id.imgDislike);
            mLikeCount = itemView.findViewById(R.id.txtLikeCount);
            mDislikeCount = itemView.findViewById(R.id.txtDislikeCount);
            mLinearVote = itemView.findViewById(R.id.linearVote);
            mLinearLike = itemView.findViewById(R.id.linearLike);
            mLinearDislike = itemView.findViewById(R.id.linearDislike);
            mLinearShare = itemView.findViewById(R.id.linearShare);
            mSeekbarProgress = itemView.findViewById(R.id.seekbarProgress);

            mLikeCount.setText("Like");
            mDislikeCount.setText("Dislike");
            mSeekbarProgress.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }
    }

    public boolean isPositionMultipleOfFive(int position) {
        return (position + 1) % 5 == 0; // Adjust position by 1 since it's 0-based
    }
}