package in.aeonplay.adapter;

import android.annotation.SuppressLint;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.model.Comman.CommanDataList;

public class EpisodeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private static final String TAG = EpisodeAdapter.class.getSimpleName();
    private final MainActivity mContext;
    private List<CommanDataList> mItems;
    private CustomFilter mFilter;

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;

    public List<CommanDataList> getEpisode() {
        return mItems;
    }

    public void setEpisode(List<CommanDataList> episode) {
        this.mItems = episode;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new CustomFilter(mItems, this);
        }
        return mFilter;
    }

    public class CustomFilter extends Filter {
        EpisodeAdapter adapter;
        List<CommanDataList> mFilterList;

        public CustomFilter(List<CommanDataList> filterList, EpisodeAdapter adapter) {
            this.adapter = adapter;
            this.mFilterList = filterList;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                constraint = constraint.toString().toUpperCase();
                List<CommanDataList> filteredPlayers = new ArrayList<>();
                for (int i = 0; i < mFilterList.size(); i++) {
                    if (mFilterList.get(i).getTitle().toUpperCase().contains(constraint)) {
                        filteredPlayers.add(mFilterList.get(i));
                    }
                }
                results.count = filteredPlayers.size();
                results.values = filteredPlayers;
            } else {
                results.count = mFilterList.size();
                results.values = mFilterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mItems = (List<CommanDataList>) results.values;
            adapter.notifyDataSetChanged();
        }
    }

    public EpisodeAdapter(MainActivity context) {
        this.mContext = context;
        this.mItems = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.card_episode_item, parent, false);
                viewHolder = new CustomViewHolder(viewItem);
                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.card_progress_item, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        CommanDataList commanDataModel = mItems.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                CustomViewHolder customViewHolder = (CustomViewHolder) holder;
                customViewHolder.episode_title.setText(commanDataModel.getTitle().toString());

                String time = "";
                if (!commanDataModel.getDuration().equalsIgnoreCase("")) {
                    int timeline = Integer.parseInt(commanDataModel.getDuration());
                    int hours = timeline / 3600;
                    int minutes = (timeline % 3600) / 60;
                    time = minutes + "m";
                }

                String airDate = "";
                if (!commanDataModel.getOriginalAirDate().equalsIgnoreCase("")) {

                    try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        Date rDate = dateFormat.parse(commanDataModel.getOriginalAirDate());
                        DateFormat relDateFormat = new SimpleDateFormat("dd MMM, yyyy");
                        airDate = relDateFormat.format(rDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                customViewHolder.season_number.setText("S" + commanDataModel.getSeasonNumber());
                customViewHolder.episode_number.setText("E" + commanDataModel.getEpisodeNumber());
                customViewHolder.episode_duration.setText(airDate + " | " + time);

                customViewHolder.episode_description.setText(commanDataModel.getDiscription().toString());
                if (commanDataModel.getLongDescription() != null)
                    customViewHolder.episode_description.setText(commanDataModel.getLongDescription().toString());

                mContext.loadImage(commanDataModel, Constants.LANDSCAPE, customViewHolder.appImage);
                customViewHolder.providerImage.setVisibility(View.GONE);
                if (commanDataModel.getProvider().equalsIgnoreCase("sonyliv")) {
                    customViewHolder.providerImage.setImageResource(R.drawable.ic_action_crown);
                    customViewHolder.providerImage.setVisibility(View.VISIBLE);
                }

                customViewHolder.appImage.setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onClick(View v) {
                        mContext.requestDeepLink(commanDataModel);
                    }
                });

                break;

            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

//                    loadingVH.mErrorTxt.setText(
//                            errorMsg != null ?
//                                    errorMsg :
//                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mItems == null ? 0 : mItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == mItems.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public void add(CommanDataList r) {
        mItems.add(r);
        //notifyItemInserted(movieResults.size() - 1);
    }

    public void addAll(List<CommanDataList> moveResults) {
        for (CommanDataList result : moveResults) {
            add(result);
        }
    }

    public void remove(CommanDataList r) {
        int position = mItems.indexOf(r);
        if (position > -1) {
            mItems.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new CommanDataList());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mItems.size() - 1;
        CommanDataList result = getItem(position);

        if (result != null) {
            mItems.remove(position);
            notifyItemRemoved(position);
        }
    }

    public CommanDataList getItem(int position) {
        return mItems.get(position);
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(mItems.size() - 1);

//        if (errorMsg != null) this.errorMsg = errorMsg;
    }

    public static class CustomViewHolder extends RecyclerView.ViewHolder {
        protected ImageView appImage, providerImage;
        protected TextView episode_title, episode_duration, episode_description, season_number, episode_number;

        public CustomViewHolder(final View view) {
            super(view);
            this.appImage = view.findViewById(R.id.itemImage);
            this.providerImage = view.findViewById(R.id.providerImage);

            this.episode_title = view.findViewById(R.id.episode_title);
            this.episode_description = view.findViewById(R.id.episode_description);
            this.season_number = view.findViewById(R.id.season_no);
            this.episode_number = view.findViewById(R.id.episode_no);
            this.episode_duration = view.findViewById(R.id.episode_duration);
        }
    }

    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.loadMORE_progress);
            mRetryBtn = itemView.findViewById(R.id.loadMORE_retry);
            mErrorTxt = itemView.findViewById(R.id.loadMORE_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadMORE_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadMORE_retry:
                case R.id.loadMORE_errorlayout:

                    showRetry(false, null);
//                    mCallback.retryPageLoad();
                    break;
            }
        }
    }

}