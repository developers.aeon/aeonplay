package in.aeonplay.adapter;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.fragment.ShotsDetailsFragment;
import in.aeonplay.model.Shots.Follow.FollowerDataModel;

public class UserFollowerAdapter extends RecyclerView.Adapter implements Filterable {

    private MainActivity context;
    private static List<FollowerDataModel> mItem;
    private FollowerFilter mFilter;
    private String mSearchText = "";
    private boolean responseFailed = false;

    public UserFollowerAdapter(MainActivity context, List<FollowerDataModel> items) {
        this.context = context;
        this.mItem = items;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new FollowerFilter(mItem, this);
        }
        return mFilter;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (!mItem.isEmpty() && mItem != null) {
            final ViewHolder viewHolders = (ViewHolder) viewHolder;

            FollowerDataModel followerDataModel = mItem.get(position);
            viewHolders.txtComman.setText("@" + followerDataModel.getFirstName().toLowerCase() + followerDataModel.getLastName().toLowerCase());

            viewHolders.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.USER_ID, followerDataModel.getId());

                    ShotsDetailsFragment shotsDetailsFragment = new ShotsDetailsFragment();
                    shotsDetailsFragment.setArguments(bundle);
                    context.addFragment(shotsDetailsFragment);
                }
            });

            String name = followerDataModel.getFirstName().toUpperCase() + " " + followerDataModel.getLastName().toUpperCase();
            if (mSearchText != null && !mSearchText.isEmpty()) {
                int startName = name.toUpperCase().indexOf(mSearchText.toUpperCase());
                int endName = startName + mSearchText.length();

                if (startName != -1) {
                    Spannable spannable = new SpannableString(name);
                    spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#FFBD59")), startName, endName, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    viewHolders.txtName.setText(spannable);
                } else {
                    viewHolders.txtName.setText(name);
                }

            } else {
                viewHolders.txtName.setText(name);
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.card_item_followers, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public class FollowerFilter extends Filter {
        private UserFollowerAdapter adapter;
        private List<FollowerDataModel> filterList;

        public FollowerFilter(List<FollowerDataModel> filterList, UserFollowerAdapter adapter) {
            this.adapter = adapter;
            this.filterList = filterList;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint.toString().isEmpty()) {
                mSearchText = "";
            } else {
                mSearchText = (String) constraint;
            }

            if (constraint != null && constraint.length() > 0) {
                constraint = constraint.toString().toUpperCase();
                ArrayList<FollowerDataModel> filteredPlayers = new ArrayList<>();

                for (int i = 0; i < filterList.size(); i++) {
                    String name = filterList.get(i).getFirstName().toUpperCase() + " " +
                            filterList.get(i).getLastName().toUpperCase();
                    if (name.contains(constraint)) {
                        filteredPlayers.add(filterList.get(i));
                    }
                }
                results.count = filteredPlayers.size();
                results.values = filteredPlayers;
            } else {
                results.count = filterList.size();
                results.values = filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mItem = (ArrayList<FollowerDataModel>) results.values;
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return mItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView txtName, txtComman;

        public ViewHolder(View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtNames);
            txtComman = itemView.findViewById(R.id.txtComman);
        }
    }
}
