package in.aeonplay.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.StrokedTextView;
import in.aeonplay.fragment.DetailFragment;
import in.aeonplay.model.Comman.CommanDataList;

public class HomeChildTop10Adapter extends RecyclerView.Adapter<HomeChildTop10Adapter.CustomViewHolder> implements Filterable {

    private static final String TAG = HomeChildTop10Adapter.class.getSimpleName();
    private final MainActivity mContext;
    private List<CommanDataList> mItems;
    private CustomFilter mFilter;

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new CustomFilter(mItems, this);
        }
        return mFilter;
    }

    public class CustomFilter extends Filter {
        HomeChildTop10Adapter adapter;
        List<CommanDataList> mFilterList;

        public CustomFilter(List<CommanDataList> filterList, HomeChildTop10Adapter adapter) {
            this.adapter = adapter;
            this.mFilterList = filterList;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                constraint = constraint.toString().toUpperCase();
                List<CommanDataList> filteredPlayers = new ArrayList<>();
                for (int i = 0; i < mFilterList.size(); i++) {
                    if (mFilterList.get(i).getTitle().toUpperCase().contains(constraint)) {
                        filteredPlayers.add(mFilterList.get(i));
                    }
                }
                results.count = filteredPlayers.size();
                results.values = filteredPlayers;
            } else {
                results.count = mFilterList.size();
                results.values = mFilterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mItems = (List<CommanDataList>) results.values;
            adapter.notifyDataSetChanged();
        }
    }

    public HomeChildTop10Adapter(MainActivity context,
                                 List<CommanDataList> item) {
        this.mContext = context;
        this.mItems = item;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, final int selectedItem) {
        @SuppressLint("InflateParams") final View view =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_top10_item, null);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, @SuppressLint("RecyclerView") final int position) {
        CommanDataList commanDataModel = mItems.get(position);
        customViewHolder.txtCounter.setText(position + 1 + "");

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) customViewHolder.txtCounter.getLayoutParams();
        layoutParams.topMargin = (int) (mContext.getResources().getBoolean(R.bool.isTablet) ?
                        mContext.getResources().getDimension(com.intuit.sdp.R.dimen._10sdp):
                        mContext.getResources().getDimension(com.intuit.sdp.R.dimen._35sdp));
        customViewHolder.txtCounter.setLayoutParams(layoutParams);

        if (commanDataModel.getProvider() != null) {

            if (commanDataModel.getProvider().equalsIgnoreCase("youtube")) {
                customViewHolder.appImage.setVisibility(View.GONE);
                customViewHolder.appImageland.setVisibility(View.VISIBLE);
                mContext.loadImage(commanDataModel, Constants.LANDSCAPE, customViewHolder.appImageland);

            } else {
                customViewHolder.appImage.setVisibility(View.VISIBLE);
                customViewHolder.appImageland.setVisibility(View.GONE);
                mContext.loadImage(commanDataModel, Constants.PORTRAIT, customViewHolder.appImage);

//                Glide.with(mContext)
//                        .load(commanDataModel.getThumbnail().getPortrait())
//                        .transition(GenericTransitionOptions.with(R.anim.fade_in))
//                        .placeholder(R.drawable.ic_action_noimage)
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .listener(new RequestListener<Drawable>() {
//                            @Override
//                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                                return false;
//                            }
//
//                            @Override
//                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                                if (commanDataModel.getPosterPalette() != null) {
//
//                                    PaletteColors colors = PaletteUtils.getPaletteColors(commanDataModel.getPosterPalette());
//                                    customViewHolder.txtCounter.setOutlineColor(colors.getToolbarBackgroundColor());
//
//                                } else {
//                                    Palette.from(((BitmapDrawable) resource).getBitmap())
//                                            .generate(new Palette.PaletteAsyncListener() {
//                                                @Override
//                                                public void onGenerated(@Nullable Palette palette) {
//                                                    commanDataModel.setPosterPalette(palette);
//
//                                                    PaletteColors colors = PaletteUtils.getPaletteColors(palette);
//                                                    customViewHolder.txtCounter.setOutlineColor(colors.getToolbarBackgroundColor());
//                                                }
//                                            });
//                                }
//
//                                return false;
//                            }
//                        })
//                        .into(customViewHolder.appImage);
            }
        }

        customViewHolder.providerImage.setVisibility(View.GONE);
        if (commanDataModel.getProvider().equalsIgnoreCase("sonyliv")) {
            customViewHolder.providerImage.setImageResource(R.drawable.ic_action_crown);
            customViewHolder.providerImage.setVisibility(View.VISIBLE);
        }

        customViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {

                if (commanDataModel.getProvider().equalsIgnoreCase("youtube")) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + commanDataModel.getHostUrl().split("=")[1]));
                    intent.putExtra("force_fullscreen", true);
                    intent.putExtra("finish_on_ended", true);
                    mContext.startActivity(intent);

                } else {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("DATA", commanDataModel);

                    DetailFragment detailFragment = new DetailFragment();
                    detailFragment.setArguments(bundle);
                    mContext.addFragment(detailFragment);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != mItems ? mItems.size() : 0);
    }

    public static class CustomViewHolder extends RecyclerView.ViewHolder {
        protected ImageView appImage, appImageland, providerImage;
        protected StrokedTextView txtCounter;

        public CustomViewHolder(final View view) {
            super(view);
            this.appImage = view.findViewById(R.id.itemImage);
            this.appImageland = view.findViewById(R.id.itemImageLand);
            this.providerImage = view.findViewById(R.id.providerImage);
            this.txtCounter = view.findViewById(R.id.txtCounter);
        }
    }
}