package in.aeonplay.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.aeonplay.R;

public class NavigationAdapter extends RecyclerView.Adapter<NavigationAdapter.SingleCheckViewHolder> {

    private List<String> mSingleCheckList;
    private Context mContext;
    private int checkedPosition;
    private OnItemClickListener mItemClickListener;

    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public NavigationAdapter(Context context, List<String> listItems) {
        mContext = context;
        mSingleCheckList = listItems;
    }

    @Override
    public SingleCheckViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final View view = inflater.inflate(R.layout.card_menu_item, viewGroup, false);
        return new SingleCheckViewHolder(view);
    }

    public void setSelectedPosition(int selectedPosition) {
        checkedPosition = selectedPosition;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(SingleCheckViewHolder viewHolder, @SuppressLint("RecyclerView") int position) {
        String item = mSingleCheckList.get(position);
        viewHolder.mMenuTitle.setText(item);

//        if (checkedPosition == -1) {
//            viewHolder.mMenuTitle.setSelected(false);
//            viewHolder.mMenuTitle.setTextColor(mContext.getResources().getColor(R.color.colorGray));
//
//        } else {
            if (checkedPosition == position) {
                viewHolder.mMenuTitle.setSelected(true);
                viewHolder.mMenuTitle.setTextColor(mContext.getResources().getColor(R.color.colorWhite));

            } else {
                viewHolder.mMenuTitle.setSelected(false);
                viewHolder.mMenuTitle.setTextColor(mContext.getResources().getColor(R.color.colorGray));
            }
//        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewHolder.mMenuTitle.setSelected(true);
                viewHolder.mMenuTitle.setTextColor(mContext.getResources().getColor(R.color.colorWhite));
                if (checkedPosition != position) {
                    notifyItemChanged(checkedPosition);
                    checkedPosition = position;
                }

                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(view, checkedPosition);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSingleCheckList.size();
    }

    class SingleCheckViewHolder extends RecyclerView.ViewHolder {
        private TextView mMenuTitle;

        public SingleCheckViewHolder(View itemView) {
            super(itemView);
            mMenuTitle = itemView.findViewById(R.id.mMenuTitle);
        }
    }
}
