package in.aeonplay.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.aeonplay.R;
import in.aeonplay.activity.MasterActivity;
import in.aeonplay.model.Shots.Campaign.CampaignDataModel;

public class CampaignListAdapter extends RecyclerView.Adapter<CampaignListAdapter.MyViewHolder> {
    private MasterActivity context;
    private ArrayList<CampaignDataModel> mList;
    private OnItemClickListener mItemClickListener;

    public CampaignListAdapter(MasterActivity context, ArrayList<CampaignDataModel> list) {
        this.context = context;
        this.mList = list;
    }

    public static String toTitleCase(String str) {
        if (str == null) {
            return null;
        }
        boolean space = true;
        StringBuilder builder = new StringBuilder(str);
        final int len = builder.length();

        for (int i = 0; i < len; ++i) {
            char c = builder.charAt(i);
            if (space) {
                if (!Character.isWhitespace(c)) {
                    // Convert to title case and switch out of whitespace mode.
                    builder.setCharAt(i, Character.toTitleCase(c));
                    space = false;
                }
            } else if (Character.isWhitespace(c)) {
                space = true;
            } else {
                builder.setCharAt(i, Character.toLowerCase(c));
            }
        }

        return builder.toString();
    }

    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public ArrayList<CampaignDataModel> getListModel(int position) {
        return mList;
    }

    public ArrayList<CampaignDataModel> getList() {
        return mList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_campaign_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.txtName.setText(toTitleCase(mList.get(position).getTitle()));
        holder.txtComman.setText("Closing dt: " + context.changeDateFormat(mList.get(position).getClosing()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(view, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        protected TextView txtName, txtComman;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtNames);
            txtComman = itemView.findViewById(R.id.txtComman);
        }
    }
}
