package in.aeonplay.adapter;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;

import java.util.List;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.fragment.DetailFragment;
import in.aeonplay.model.Comman.CommanDataList;

public class AdvertisementAdapter extends PagerAdapter {

    private List<CommanDataList> mItems;
    private MainActivity mContext;

    public AdvertisementAdapter(MainActivity context, List<CommanDataList> items) {
        this.mContext = context;
        this.mItems = items;
    }

    @Override
    public int getCount() {
        return mItems.size() ;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        CommanDataList commanDataList = mItems.get(position);

        CardView layout = (CardView) mContext.getLayoutInflater()
                .inflate(R.layout.card_advertisement_item, container, false);
        container.addView(layout);

        ImageView providerImage = layout.findViewById(R.id.providerImage);
        ImageView imageView = layout.findViewById(R.id.advertisementImage);
        mContext.loadImage(commanDataList, Constants.LANDSCAPE, imageView);

        providerImage.setVisibility(View.GONE);
        if (commanDataList.getProvider().equalsIgnoreCase("sonyliv")) {
            providerImage.setImageResource(R.drawable.ic_action_crown);
            providerImage.setVisibility(View.VISIBLE);
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (commanDataList.getProvider().equalsIgnoreCase("youtube")) {
                    if (commanDataList.getHostUrl() != null) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + commanDataList.getHostUrl().split("=")[1]));
                        intent.putExtra("force_fullscreen", true);
                        intent.putExtra("finish_on_ended", true);
                        mContext.startActivity(intent);
                    } else {
                        mContext.showMessageToUser("Coming soon");
                    }

                } else if (commanDataList.getContentType().equalsIgnoreCase("live_channel") ||
                        commanDataList.getContentType().equalsIgnoreCase("live_stream")) {
                    if (commanDataList.getHostUrl() != null)
                        mContext.requestDeepLink(commanDataList);
                    else
                        mContext.showMessageToUser("Coming soon");

                } else {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("DATA", commanDataList);

                    DetailFragment detailFragment = new DetailFragment();
                    detailFragment.setArguments(bundle);
                    mContext.addFragment(detailFragment);
                }
            }
        });

        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
