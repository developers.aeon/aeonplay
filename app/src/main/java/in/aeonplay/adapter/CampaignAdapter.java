package in.aeonplay.adapter;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.viewpager.widget.PagerAdapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.fragment.CampaignAwardAnnouncement;
import in.aeonplay.fragment.CampaignDetailsFragment;
import in.aeonplay.fragment.CampaignParticipantsFragment;
import in.aeonplay.model.Shots.Campaign.CampaignDataModel;

public class CampaignAdapter extends PagerAdapter {

    private List<CampaignDataModel> mItems;
    private MainActivity mContext;

    public CampaignAdapter(MainActivity context, List<CampaignDataModel> items) {
        this.mContext = context;
        this.mItems = items;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        CampaignDataModel campaignDataModel = mItems.get(position);

        LinearLayout layout = (LinearLayout) mContext.getLayoutInflater()
                .inflate(R.layout.card_campaign_item, container, false);
        container.addView(layout);

        ImageView imageView = layout.findViewById(R.id.advertisementImage);
        mContext.loadImage(campaignDataModel, Constants.LANDSCAPE, imageView);

        Button btnViewParticipantList = layout.findViewById(R.id.btnViewParticipantList);
        Button btnWinnerAnnouncement = layout.findViewById(R.id.btnWinnerAnnouncement);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Date apiDate = sdf.parse(campaignDataModel.getAwardAnnouncement());
            Date currentDate = new Date();

            if (currentDate.compareTo(apiDate) >= 0) {
                btnWinnerAnnouncement.setVisibility(View.VISIBLE);

            } else {
                btnWinnerAnnouncement.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        btnWinnerAnnouncement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("DATA", campaignDataModel);

                CampaignAwardAnnouncement campaignAwardAnnouncement = new CampaignAwardAnnouncement();
                campaignAwardAnnouncement.setArguments(bundle);
                mContext.addFragment(campaignAwardAnnouncement);
            }
        });

        btnViewParticipantList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("DATA", campaignDataModel);

                CampaignParticipantsFragment campaignParticipantsFragment = new CampaignParticipantsFragment();
                campaignParticipantsFragment.setArguments(bundle);
                mContext.addFragment(campaignParticipantsFragment);
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("DATA", campaignDataModel);

                CampaignDetailsFragment campaignDetailsFragment = new CampaignDetailsFragment();
                campaignDetailsFragment.setArguments(bundle);
                mContext.addFragment(campaignDetailsFragment);
            }
        });

//        if (activity.showOfferDialog) {
//            activity.showOfferDialog = false;
//        showOffers(campaignDataModel);
//        }

        return layout;
    }
//    private void showOffers(CampaignDataModel campaignDataModel) {
//        final Dialog dialog = new Dialog(mContext, R.style.DialogTheme);
//        dialog.setContentView(R.layout.dialog_inday);
//        dialog.setCancelable(false);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(mContext.getResources().getColor(R.color.colorTransparent)));
//        dialog.show();
//
//        ImageView imageView = dialog.findViewById(R.id.imageView);
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Bundle bundle = new Bundle();
//                bundle.putParcelable("DATA", campaignDataModel);
//
//                CampaignDetailsFragment campaignDetailsFragment = new CampaignDetailsFragment();
//                campaignDetailsFragment.setArguments(bundle);
//                mContext.addFragment(campaignDetailsFragment);
//
//                dialog.dismiss();
//            }
//        });
//
//        ImageView imgClose = dialog.findViewById(R.id.imgClose);
//        imgClose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.dismiss();
//            }
//        });
//    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
