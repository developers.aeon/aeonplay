package in.aeonplay.adapter;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.comman.SpaceItemDecoration;
import in.aeonplay.model.NewsModel.NewsHeader;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ItemRowHolder> {

    private MainActivity context;
    private List<NewsHeader> mItems;

    public NewsAdapter(MainActivity context, List<NewsHeader> item) {
        this.context = context;
        this.mItems = item;
    }

    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, final int selectedItem) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_home_item, null);
        ItemRowHolder viewHolder = new ItemRowHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemRowHolder itemRowHolder, @SuppressLint("RecyclerView") final int position) {
        NewsHeader newsHeader = mItems.get(position);

        itemRowHolder.itemTitle.setText(context.titleCase(newsHeader.getTitle()));
        NewsDataAdapter itemListDataAdapter = new NewsDataAdapter(context, newsHeader.getDatum());

        int spanCount = context.getResources().getBoolean(R.bool.isTablet) ? 3 : 2;
        itemRowHolder.mRecyclerChildItem.setLayoutManager(
                new GridLayoutManager(context, spanCount, GridLayoutManager.VERTICAL, false));
        itemRowHolder.mRecyclerChildItem.setItemAnimator(new DefaultItemAnimator());
        itemRowHolder.mRecyclerChildItem.setAdapter(itemListDataAdapter);
    }

    @Override
    public int getItemCount() {
        return mItems.size() > 0 ? mItems.size() : 0;
    }

    public static class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView itemTitle, itemMore;
        protected RecyclerView mRecyclerChildItem;
        protected LinearLayout linearSectionItem;

        public ItemRowHolder(View view) {
            super(view);
            this.linearSectionItem = view.findViewById(R.id.linearSectionItem);
            this.itemTitle = view.findViewById(R.id.itemTitle);
            this.itemMore = view.findViewById(R.id.itemMore);
            itemMore.setVisibility(View.GONE);
            this.mRecyclerChildItem = view.findViewById(R.id.recycler_childItem);
            mRecyclerChildItem.addItemDecoration(new SpaceItemDecoration());
        }
    }
}