package in.aeonplay.adapter;

import static in.aeonplay.fragment.ShotsDetailsFragment.mUserID;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.HalfFloat;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.OptIn;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ShareCompat;
import androidx.media3.common.util.UnstableApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import in.aeonplay.BuildConfig;
import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.ItemTouchHelperCallback;
import in.aeonplay.comman.NumberFormatUtils;
import in.aeonplay.comman.OneShotClickListener;
import in.aeonplay.fragment.UserFollowersFragment;
import in.aeonplay.fragment.UserFollowingsFragment;
import in.aeonplay.model.Profile.ProfileModel;
import in.aeonplay.model.Shots.ShotDetailModel;
import in.aeonplay.model.Shots.ShotSubDataModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class ShotsDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements ItemTouchHelperCallback.ItemTouchHelperAdapter {

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private static final int HEADER = 2;

    private List<ShotSubDataModel> movieResults;
    private MainActivity mContext;

    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;
    private MovieVH movieVH;
    private String mFullName = "";

    public ShotsDetailsAdapter(MainActivity context) {
        this.mContext = context;
        movieResults = new ArrayList<>();
        movieResults.add(0, new ShotSubDataModel());
    }

    public List<ShotSubDataModel> getShots() {
        return movieResults;
    }

    public void setShots(List<ShotSubDataModel> movieResults) {
        this.movieResults = movieResults;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case HEADER:
                View viewItemH = inflater.inflate(R.layout.card_shots_detail_header, parent, false);
                viewHolder = new HeaderVH(viewItemH);
                break;

            case ITEM:
                View viewItem = inflater.inflate(R.layout.card_shots_detail_item, parent, false);
                viewHolder = new MovieVH(viewItem);
                break;

            case LOADING:
                View viewLoading = inflater.inflate(R.layout.card_progress_item, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }

    @OptIn(markerClass = UnstableApi.class)
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        ShotSubDataModel shotSubDataModel = getShots().get(position);

        switch (getItemViewType(position)) {
            case HEADER:
                final HeaderVH headerVH = (HeaderVH) holder;
                getProfileInfo(mUserID + "", headerVH);

                headerVH.linearFollowers.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bundle bundle = new Bundle();
                        bundle.putInt(Constants.USER_ID, mUserID);

                        UserFollowersFragment userFollowersFragment = new UserFollowersFragment();
                        userFollowersFragment.setArguments(bundle);
                        mContext.addFragment(userFollowersFragment);
                    }
                });

                headerVH.linearFollowing.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bundle bundle = new Bundle();
                        bundle.putInt(Constants.USER_ID, mUserID);

                        UserFollowingsFragment userFollowingsFragment = new UserFollowingsFragment();
                        userFollowingsFragment.setArguments(bundle);
                        mContext.addFragment(userFollowingsFragment);
                    }
                });

                break;

            case ITEM:
                movieVH = (MovieVH) holder;

                movieVH.appMore.setVisibility(View.GONE);
                if (Integer.compare(shotSubDataModel.getUserId(), SharePreferenceManager.getUserData().getId()) == 0) {
                    movieVH.appMore.setVisibility(View.VISIBLE);
                }

                movieVH.appMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext, R.style.AlertTheme);
                        alertDialog.setTitle("Remove shot!")
                                .setMessage("Are you sure you want to remove this shot?")
                                .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        deleteShotByID(shotSubDataModel.getId(), position);
                                        dialog.dismiss();
                                    }
                                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        Dialog dialog = alertDialog.show();
                        dialog.show();
                    }
                });

                movieVH.shotsTitle.setText(shotSubDataModel.getTitle());
                movieVH.shotsDescription.setText(shotSubDataModel.getDescription());

                movieVH.mImgLike.setImageResource(!shotSubDataModel.isIs_liked() ? R.drawable.ic_action_like : R.drawable.ic_action_like_selected);
                movieVH.mImgDislike.setImageResource(!shotSubDataModel.isIs_disliked() ? R.drawable.ic_action_dislike : R.drawable.ic_action_dislike_selected);

                movieVH.linearLike.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addToFeeling(shotSubDataModel.getId() + "", "like", movieVH, position);
                    }
                });

                movieVH.linearDislike.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addToFeeling(shotSubDataModel.getId() + "", "dislike", movieVH, position);
                    }
                });

                movieVH.linearShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(),
                                    R.drawable.ic_action_share_shots);

                            Uri bitmapUri = mContext.saveImageExternal(bitmap);
                            Intent shareIntent = ShareCompat.IntentBuilder.from(mContext)
                                    .setType("image/*")
                                    .setStream(bitmapUri)
                                    .setText("Check out " + shotSubDataModel.getTitle() + " on Aeonplay! " + BuildConfig.AUTHORIZE_URL + "shorts/" + shotSubDataModel.getId())
                                    .getIntent();

                            if (shareIntent.resolveActivity(mContext.getPackageManager()) != null) {
                                mContext.startActivity(shareIntent);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                if (shotSubDataModel.getDownloadUrl() != null) {
//                    RequestOptions options = new RequestOptions()
//                            .frame(10000000);
//                    Glide.with(mContext)
//                            .asBitmap()
//                            .load(shotSubDataModel.getDownloadUrl())
//                            .apply(options)
//                            .transition(GenericTransitionOptions.with(R.anim.fade_in))
//                            .placeholder(R.drawable.ic_action_noimage)
//                            .diskCacheStrategy(DiskCacheStrategy.ALL)
//                            .into(movieVH.appImage);

                    Glide.with(mContext)
                            .asBitmap()
                            .load(shotSubDataModel.getThumbnail().getPortrait().getConverted())
                            .transition(GenericTransitionOptions.with(R.anim.fade_in))
                            .placeholder(R.drawable.ic_action_noimage)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(movieVH.appImage);

                } else {
                    Glide.with(mContext)
                            .asDrawable()
                            .load(R.drawable.ic_action_noimage)
                            .transition(GenericTransitionOptions.with(R.anim.fade_in))
                            .placeholder(R.drawable.ic_action_noimage)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(movieVH.appImage);
                }

                final Dialog dialog = new Dialog(mContext, R.style.DialogTheme);
                dialog.setContentView(R.layout.dialog_player);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);

                TextView textView = dialog.findViewById(R.id.txtPromptTitle);
                textView.setText(shotSubDataModel.getTitle());

                VideoView videoView = dialog.findViewById(R.id.videoView);
                videoView.setVideoURI(Uri.parse(shotSubDataModel.getDownloadUrl())); // Set playback URLs
                videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        videoView.start();
                    }
                });

                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        videoView.stopPlayback();
                        dialog.dismiss();
                    }
                });

                movieVH.itemView.setOnClickListener(new OneShotClickListener() {
                    @Override
                    public void onClicked(View v) {
                        videoView.start();
                        dialog.show();
                    }
                });

                break;

            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

//                    loadingVH.mErrorTxt.setText(
//                            errorMsg != null ?
//                                    errorMsg :
//                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    private void deleteShotByID(int ShotID, int position) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> loginCall = apiInterface.deleteShotsByID(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), String.valueOf(ShotID));

        APIClient.callAPI(mContext, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final ShotDetailModel shotsDetailModel = gson.fromJson(reader, ShotDetailModel.class);

                    if (shotsDetailModel.getData() != null) {
                        movieResults.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, movieResults.size());
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    mContext.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                mContext.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                mContext.showMessageToUser(error);
            }
        });
    }

    private void getProfileInfo(String userID, HeaderVH headerVH) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getUserProfileByID(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), userID);

        APIClient.callAPI(mContext, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final ProfileModel profileModel = gson.fromJson(reader, ProfileModel.class);

                    if (profileModel.getData() != null) {
                        mFullName = profileModel.getData().getFirstName() + " " + profileModel.getData().getLastName();
                        headerVH.txtFollowers.setText(NumberFormatUtils.formatNumber(profileModel.getData().getFollowers_count()));
                        headerVH.txtFollowings.setText(NumberFormatUtils.formatNumber(profileModel.getData().getFollowings_count()));
                        headerVH.txtShots.setText(NumberFormatUtils.formatNumber(profileModel.getData().getShorts_count()));
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    mContext.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                mContext.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                mContext.showMessageToUser(error);
            }
        });
    }

    private void addToFeeling(String id, String type, MovieVH movieVH, int position) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> loginCall = apiInterface.postFeeling(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), id, type);

        APIClient.callAPI(mContext, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final ShotDetailModel shotsDetailModel = gson.fromJson(reader, ShotDetailModel.class);

                    if (shotsDetailModel.getData() != null) {
                        movieVH.mImgLike.setImageResource(!shotsDetailModel.getData().isIs_liked() ? R.drawable.ic_action_like : R.drawable.ic_action_like_selected);
                        movieVH.mImgDislike.setImageResource(!shotsDetailModel.getData().isIs_disliked() ? R.drawable.ic_action_dislike : R.drawable.ic_action_dislike_selected);

                        movieResults.set(position, shotsDetailModel.getData());
                        notifyItemChanged(position);
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    mContext.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                mContext.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                mContext.showMessageToUser(error);
            }
        });
    }

    @Override
    public int getItemCount() {
        return movieResults == null ? 0 : movieResults.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return HEADER;

        else
            return (position == movieResults.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public void add(ShotSubDataModel r) {
        movieResults.add(r);
        //notifyItemInserted(movieResults.size() - 1);
    }

    public void addAll(List<ShotSubDataModel> moveResults) {
        for (ShotSubDataModel result : moveResults) {
            add(result);
        }
    }

    public void remove(ShotSubDataModel r) {
        int position = movieResults.indexOf(r);
        if (position > -1) {
            movieResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ShotSubDataModel());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = movieResults.size() - 1;
        ShotSubDataModel result = getItem(position);

        if (result != null) {
            movieResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public ShotSubDataModel getItem(int position) {
        return movieResults.get(position);
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(movieResults.size() - 1);

//        if (errorMsg != null) this.errorMsg = errorMsg;
    }

    @Override
    public void onItemDismiss(int position) {
        // do nothing ...
    }

    protected class MovieVH extends RecyclerView.ViewHolder {
        protected ImageView appImage, appMore;
        protected ImageView mImgLike, mImgDislike, mImgShare;
        protected TextView shotsTitle, shotsDescription;
        protected LinearLayout linearLike, linearShare, linearDislike;

        public MovieVH(View view) {
            super(view);

            this.appImage = view.findViewById(R.id.itemImage);
            this.appMore = view.findViewById(R.id.appMore);
            this.shotsTitle = itemView.findViewById(R.id.shots_title);
            this.shotsDescription = itemView.findViewById(R.id.shots_description);

            this.mImgLike = itemView.findViewById(R.id.imgLike);
            this.mImgDislike = itemView.findViewById(R.id.imgDislike);
            this.mImgShare = itemView.findViewById(R.id.imgShare);

            this.linearLike = itemView.findViewById(R.id.linearLike);
            this.linearShare = itemView.findViewById(R.id.linearShare);
            this.linearDislike = itemView.findViewById(R.id.linearDislike);
        }
    }

    protected class HeaderVH extends RecyclerView.ViewHolder {
        protected LinearLayout linearPosts, linearFollowers, linearFollowing;
        protected TextView txtShots, txtFollowers, txtFollowings;

        public HeaderVH(View view) {
            super(view);
            this.txtShots = view.findViewById(R.id.txtShots);
            this.txtFollowers = view.findViewById(R.id.txtFollowers);
            this.txtFollowings = view.findViewById(R.id.txtFollowings);

            this.linearPosts = view.findViewById(R.id.linearPosts);
            this.linearFollowers = view.findViewById(R.id.linearFollowers);
            this.linearFollowing = view.findViewById(R.id.linearFollowing);
        }
    }

    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.loadMORE_progress);
            mRetryBtn = itemView.findViewById(R.id.loadMORE_retry);
            mErrorTxt = itemView.findViewById(R.id.loadMORE_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadMORE_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadMORE_retry:
                case R.id.loadMORE_errorlayout:

                    showRetry(false, null);
//                    mCallback.retryPageLoad();
                    break;
            }
        }
    }
}