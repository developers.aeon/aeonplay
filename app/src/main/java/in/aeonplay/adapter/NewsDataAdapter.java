package in.aeonplay.adapter;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;

import java.util.List;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.comman.VerticalTextView;
import in.aeonplay.fragment.PDFFragment;
import in.aeonplay.model.NewsModel.NewsData;

public class NewsDataAdapter extends RecyclerView.Adapter<NewsDataAdapter.ItemRowHolder> {

    private MainActivity context;
    private List<NewsData> mItems;

    public NewsDataAdapter(MainActivity context, List<NewsData> item) {
        this.context = context;
        this.mItems = item;
    }

    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, final int selectedItem) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_news_item, null);
        ItemRowHolder viewHolder = new ItemRowHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemRowHolder itemRowHolder, @SuppressLint("RecyclerView") final int position) {
        NewsData newsData = mItems.get(position);

        String date = context.changeDateFormat(newsData.getUpdatedAt(), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "dd MMM, yy");
        itemRowHolder.itemTime.setText(date);
        itemRowHolder.itemText.setText(context.titleCase(newsData.getTitle()));

        Glide.with(context)
                .load(newsData.getImageUrl())
                .placeholder(R.color.colorPrimaryLight)
                .transition(GenericTransitionOptions.with(R.anim.fade_in))
                .into(itemRowHolder.itemImage);

        itemRowHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("DATA", newsData);

                PDFFragment pdfFragment = new PDFFragment();
                pdfFragment.setArguments(bundle);
                context.addFragment(pdfFragment);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public static class ItemRowHolder extends RecyclerView.ViewHolder {

        private TextView itemText;
        private ImageView itemImage;
        private VerticalTextView itemTime;

        public ItemRowHolder(View view) {
            super(view);
            this.itemText = view.findViewById(R.id.itemText);
            this.itemImage = view.findViewById(R.id.itemImage);
            this.itemTime = view.findViewById(R.id.itemTime);
        }
    }
}