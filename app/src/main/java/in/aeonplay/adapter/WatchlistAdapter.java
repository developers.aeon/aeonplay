package in.aeonplay.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.fragment.DetailFragment;
import in.aeonplay.model.WatchlistModel.WatchlistModelListDatumModel;

public class WatchlistAdapter extends RecyclerView.Adapter<WatchlistAdapter.CustomViewHolder> implements Filterable {

    private static final String TAG = WatchlistAdapter.class.getSimpleName();
    private final MainActivity mContext;
    private List<WatchlistModelListDatumModel> mItems;
    private CustomFilter mFilter;

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new CustomFilter(mItems, this);
        }
        return mFilter;
    }

    public class CustomFilter extends Filter {
        WatchlistAdapter adapter;
        List<WatchlistModelListDatumModel> mFilterList;

        public CustomFilter(List<WatchlistModelListDatumModel> filterList, WatchlistAdapter adapter) {
            this.adapter = adapter;
            this.mFilterList = filterList;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                constraint = constraint.toString().toUpperCase();
                List<WatchlistModelListDatumModel> filteredPlayers = new ArrayList<>();
                for (int i = 0; i < mFilterList.size(); i++) {
                    if (mFilterList.get(i).getWatchable().getTitle().toUpperCase().contains(constraint)) {
                        filteredPlayers.add(mFilterList.get(i));
                    }
                }
                results.count = filteredPlayers.size();
                results.values = filteredPlayers;
            } else {
                results.count = mFilterList.size();
                results.values = mFilterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mItems = (List<WatchlistModelListDatumModel>) results.values;
            adapter.notifyDataSetChanged();
        }
    }

    public WatchlistAdapter(MainActivity context,
                            List<WatchlistModelListDatumModel> item) {
        this.mContext = context;
        this.mItems = item;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, final int selectedItem) {
        @SuppressLint("InflateParams") final View view =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_movie_item, null);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, @SuppressLint("RecyclerView") final int position) {
        WatchlistModelListDatumModel watchlistModelListDatumModel = mItems.get(position);

        if (watchlistModelListDatumModel.getWatchable() != null) {
            if (watchlistModelListDatumModel.getWatchable().getProvider() != null) {

                if (watchlistModelListDatumModel.getWatchable().getProvider().equalsIgnoreCase("youtube")) {
                    customViewHolder.appImage.setVisibility(View.GONE);
                    customViewHolder.appImageland.setVisibility(View.VISIBLE);
                    mContext.loadImage(watchlistModelListDatumModel.getWatchable(), Constants.LANDSCAPE, customViewHolder.appImageland);

                } else {
                    customViewHolder.appImage.setVisibility(View.VISIBLE);
                    customViewHolder.appImageland.setVisibility(View.GONE);
                    mContext.loadImage(watchlistModelListDatumModel.getWatchable(), Constants.PORTRAIT, customViewHolder.appImage);
                }

                customViewHolder.providerImage.setVisibility(View.GONE);
                if (watchlistModelListDatumModel.getWatchable().getProvider().equalsIgnoreCase("sonyliv")) {
                    customViewHolder.providerImage.setImageResource(R.drawable.ic_action_crown);
                    customViewHolder.providerImage.setVisibility(View.VISIBLE);
                }
            }
        }

        customViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                if (watchlistModelListDatumModel.getWatchable().getProvider().equalsIgnoreCase("youtube")) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + watchlistModelListDatumModel.getWatchable().getHostUrl().split("=")[1]));
                    intent.putExtra("force_fullscreen", true);
                    intent.putExtra("finish_on_ended", true);
                    mContext.startActivity(intent);

                } else {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("DATA", watchlistModelListDatumModel.getWatchable());

                    DetailFragment detailFragment = new DetailFragment();
                    detailFragment.setArguments(bundle);
                    mContext.addFragment(detailFragment);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != mItems ? mItems.size() : 0);
    }

    public static class CustomViewHolder extends RecyclerView.ViewHolder {
        protected ImageView appImage, appImageland, providerImage;

        public CustomViewHolder(final View view) {
            super(view);
            this.appImage = view.findViewById(R.id.itemImage);
            this.appImageland = view.findViewById(R.id.itemImageLand);
            this.providerImage = view.findViewById(R.id.providerImage);
        }
    }

    public void updateData(ArrayList<WatchlistModelListDatumModel> viewModels) {
        mItems.clear();
        mItems.addAll(viewModels);
        notifyDataSetChanged();
    }

    public void addItem(WatchlistModelListDatumModel model) {
        mItems.add(model);
        notifyDataSetChanged();
    }
}