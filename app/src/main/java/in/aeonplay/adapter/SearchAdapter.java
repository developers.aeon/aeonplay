package in.aeonplay.adapter;

import static in.aeonplay.fragment.SearchFragment.mSearchText;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.fragment.DetailFragment;
import in.aeonplay.model.Comman.CommanDataList;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.CustomViewHolder> implements Filterable {

    private static final String TAG = SearchAdapter.class.getSimpleName();
    private final MainActivity mContext;
    private List<CommanDataList> mItems;
    private CustomFilter mFilter;

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new CustomFilter(mItems, this);
        }
        return mFilter;
    }

    public class CustomFilter extends Filter {
        SearchAdapter adapter;
        List<CommanDataList> mFilterList;

        public CustomFilter(List<CommanDataList> filterList, SearchAdapter adapter) {
            this.adapter = adapter;
            this.mFilterList = filterList;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint.toString().isEmpty()) {
                mSearchText = "";
            } else {
                mSearchText = (String) constraint;
            }

            if (constraint != null && constraint.length() > 0) {
                constraint = constraint.toString().toLowerCase(Locale.US);
                List<CommanDataList> filteredPlayers = new ArrayList<>();
                for (int i = 0; i < mFilterList.size(); i++) {
                    if (mFilterList.get(i).getTitle().toLowerCase(Locale.US).contains(constraint)) {
                        filteredPlayers.add(mFilterList.get(i));
                    }
                }
                results.count = filteredPlayers.size();
                results.values = filteredPlayers;
            } else {
                results.count = mFilterList.size();
                results.values = mFilterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mItems = (List<CommanDataList>) results.values;
            adapter.notifyDataSetChanged();
        }
    }

    public SearchAdapter(MainActivity context,
                         List<CommanDataList> item) {
        this.mContext = context;
        this.mItems = item;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, final int selectedItem) {
        @SuppressLint("InflateParams") final View view =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_search_item, null);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, @SuppressLint("RecyclerView") final int position) {
        CommanDataList commanDataList = mItems.get(position);

        String seasonEpisodeNumber = "";
        if (commanDataList.getContentType().equalsIgnoreCase("episode"))
            seasonEpisodeNumber = " | S" + commanDataList.getSeasonNumber() +
                    " - E" + commanDataList.getEpisodeNumber();

        customViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                mContext.hideKeyboardFrom(mContext, v);

                Bundle bundle = new Bundle();
                bundle.putParcelable("DATA", commanDataList);

                DetailFragment detailFragment = new DetailFragment();
                detailFragment.setArguments(bundle);
                mContext.addFragment(detailFragment);
            }
        });

        if (mSearchText != null && !mSearchText.isEmpty()) {
            int startPos = commanDataList.getTitle().toLowerCase().indexOf(mSearchText.toLowerCase());
            int endPos = startPos + mSearchText.length();
            if (startPos != -1) {
                Spannable spannable = new SpannableString(commanDataList.getTitle() + seasonEpisodeNumber);
                ColorStateList colorStateList = new ColorStateList(new int[][]{new int[]{}}, new int[]{Color.BLUE});
                TextAppearanceSpan textAppearanceSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, colorStateList, null);
                spannable.setSpan(textAppearanceSpan, startPos, endPos, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                customViewHolder.searchItemName.setText(spannable);
            } else {
                customViewHolder.searchItemName.setText(commanDataList.getTitle() + seasonEpisodeNumber);
            }
        } else {
            customViewHolder.searchItemName.setText(commanDataList.getTitle() + seasonEpisodeNumber);
        }
    }

    @Override
    public int getItemCount() {
        return (null != mItems ? mItems.size() : 0);
    }

    public static class CustomViewHolder extends RecyclerView.ViewHolder {
        protected TextView searchItemName;

        public CustomViewHolder(final View view) {
            super(view);
            this.searchItemName = view.findViewById(R.id.searchItemName);
        }
    }
}