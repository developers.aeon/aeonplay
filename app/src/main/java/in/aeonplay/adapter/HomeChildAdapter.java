package in.aeonplay.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.fragment.DetailFragment;
import in.aeonplay.model.Comman.CommanDataList;

public class HomeChildAdapter extends RecyclerView.Adapter<HomeChildAdapter.CustomViewHolder> implements Filterable {

    private static final String TAG = HomeChildAdapter.class.getSimpleName();
    private final MainActivity mContext;
    private List<CommanDataList> mItems;
    private CustomFilter mFilter;

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new CustomFilter(mItems, this);
        }
        return mFilter;
    }

    public class CustomFilter extends Filter {
        HomeChildAdapter adapter;
        List<CommanDataList> mFilterList;

        public CustomFilter(List<CommanDataList> filterList, HomeChildAdapter adapter) {
            this.adapter = adapter;
            this.mFilterList = filterList;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                constraint = constraint.toString().toUpperCase();
                List<CommanDataList> filteredPlayers = new ArrayList<>();
                for (int i = 0; i < mFilterList.size(); i++) {
                    if (mFilterList.get(i).getTitle().toUpperCase().contains(constraint)) {
                        filteredPlayers.add(mFilterList.get(i));
                    }
                }
                results.count = filteredPlayers.size();
                results.values = filteredPlayers;
            } else {
                results.count = mFilterList.size();
                results.values = mFilterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mItems = (List<CommanDataList>) results.values;
            adapter.notifyDataSetChanged();
        }
    }

    public HomeChildAdapter(MainActivity context,
                            List<CommanDataList> item) {
        this.mContext = context;
        this.mItems = item;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, final int selectedItem) {
        @SuppressLint("InflateParams") final View view =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_movie_item, null);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, @SuppressLint("RecyclerView") final int position) {
        CommanDataList commanDataModel = mItems.get(position);
        if (commanDataModel.getProvider() != null) {

            if (commanDataModel.getProvider().equalsIgnoreCase("youtube")) {
                customViewHolder.appImage.setVisibility(View.GONE);
                customViewHolder.appImageland.setVisibility(View.VISIBLE);
                mContext.loadImage(commanDataModel, Constants.LANDSCAPE, customViewHolder.appImageland);

            } else {

                if (commanDataModel.getContentType().equalsIgnoreCase("live_channel")) {
                    customViewHolder.itemView.setBackgroundColor(Color.WHITE);
                    customViewHolder.appImage.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    customViewHolder.appImage.getLayoutParams().width = (int) mContext.getResources().getDimension(R.dimen.portrait_poster_width);
                    customViewHolder.appImage.getLayoutParams().height = (int) mContext.getResources().getDimension(R.dimen.portrait_poster_width);
                }

//                if (commanDataModel.getComingSoon() == 1) {
//                    customViewHolder.appImage.getLayoutParams().width = (int) mContext.getResources().getDimension(R.dimen.portrait_poster_width_big);
//                    customViewHolder.appImage.getLayoutParams().height = (int) mContext.getResources().getDimension(R.dimen.portrait_poster_height_big);
//                }

                customViewHolder.appImage.setVisibility(View.VISIBLE);
                customViewHolder.appImageland.setVisibility(View.GONE);
                mContext.loadImage(commanDataModel, Constants.PORTRAIT, customViewHolder.appImage);
            }
        }

        customViewHolder.providerImage.setVisibility(View.GONE);
        if (commanDataModel.getProvider().equalsIgnoreCase("sonyliv")) {
            customViewHolder.providerImage.setImageResource(R.drawable.ic_action_crown);
            customViewHolder.providerImage.setVisibility(View.VISIBLE);
        }

        customViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {

                if (commanDataModel.getProvider().equalsIgnoreCase("youtube")) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + commanDataModel.getHostUrl().split("=")[1]));
                    intent.putExtra("force_fullscreen", true);
                    intent.putExtra("finish_on_ended", true);
                    mContext.startActivity(intent);

                } else if (commanDataModel.getContentType().equalsIgnoreCase("live_channel") ||
                        commanDataModel.getContentType().equalsIgnoreCase("live_stream")) {
                    mContext.requestDeepLink(commanDataModel);

                } else {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("DATA", commanDataModel);

                    DetailFragment detailFragment = new DetailFragment();
                    detailFragment.setArguments(bundle);
                    mContext.addFragment(detailFragment);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != mItems ? mItems.size() : 0);
    }

    public static class CustomViewHolder extends RecyclerView.ViewHolder {
        protected ImageView appImage, appImageland, providerImage;

        public CustomViewHolder(final View view) {
            super(view);
            this.appImage = view.findViewById(R.id.itemImage);
            this.appImageland = view.findViewById(R.id.itemImageLand);
            this.providerImage = view.findViewById(R.id.providerImage);
        }
    }
}