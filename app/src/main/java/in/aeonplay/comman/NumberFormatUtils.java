package in.aeonplay.comman;

import java.text.DecimalFormat;

public class NumberFormatUtils {

    public static String formatNumber(long value) {
        if (value >= 1_000_000_000) {
            // Billion and above: 1B, 2B, ...
            return formatDecimal(value, 1_000_000_000, "B");
        } else if (value >= 1_000_000) {
            // Million and above: 1M, 2M, ...
            return formatDecimal(value, 1_000_000, "M");
        } else if (value >= 1_000) {
            // Thousand and above: 1k, 2k, ...
            return formatDecimal(value, 1_000, "k");
        } else {
            // Less than 1000: 999, 888, ...
            return String.valueOf(value);
        }
    }

    private static String formatDecimal(long value, long divisor, String suffix) {
        double result = (double) value / divisor;
        // Use a DecimalFormat to control the number of decimal places
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        return decimalFormat.format(result) + suffix;
    }
}