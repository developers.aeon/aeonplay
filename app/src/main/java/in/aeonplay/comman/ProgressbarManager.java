package in.aeonplay.comman;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import in.aeonplay.R;

public class ProgressbarManager extends Dialog {

    public ProgressbarManager(Context context) {
        super(context, R.style.TransparentProgressDialog);
        WindowManager.LayoutParams wlmp = getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER;
        getWindow().setAttributes(wlmp);
        setTitle(null);
        setCancelable(false);
        setOnCancelListener(null);
        RelativeLayout layout = new RelativeLayout(context);
        layout.setGravity(Gravity.CENTER);
        layout.setBackgroundColor(Color.TRANSPARENT);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        ImageView imageViewPlay = new ImageView(context);
        imageViewPlay.setImageResource(R.drawable.ic_action_loader_play);

        ImageView imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.ic_action_loader);
        RotateAnimation anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(1000);
        imageView.setAnimation(anim);
        imageView.startAnimation(anim);

        layout.addView(imageViewPlay, params);
        layout.addView(imageView, params);
        addContentView(layout, params);
    }
}