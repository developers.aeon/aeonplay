package in.aeonplay.comman;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import in.aeonplay.R;

public class VideoThumbnailTask extends AsyncTask<String, Void, Bitmap> {

    private ImageView imageView; // The ImageView where you want to set the thumbnail
    private Context context;

    public VideoThumbnailTask(Context context, ImageView imageView) {
        this.context = context;
        this.imageView = imageView;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        String videoUrl = params[0];
        Bitmap bitmap = null;
        MediaMetadataRetriever retriever = null;

        try {
            retriever = new MediaMetadataRetriever();
            retriever.setDataSource(videoUrl);

            // Get the thumbnail at the first frame
            bitmap = retriever.getFrameAtTime(0, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (retriever != null) {
                try {
                    retriever.release();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        if (result != null) {
            Glide.with(context)
                    .asBitmap()
                    .load(result)
                    .transition(GenericTransitionOptions.with(R.anim.fade_in))
                    .placeholder(R.drawable.ic_action_noimage)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);

            Uri tempUri = getImageUri(context, result);
            String imagePath = getRealPathFromURI(context, tempUri);
            Log.e("VideoThumbUrl: ", imagePath);

        } else {
            // Handle case where thumbnail retrieval failed
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
        String filename = dateFormat.format(new Date());
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, filename, null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Context context, Uri uri) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
}