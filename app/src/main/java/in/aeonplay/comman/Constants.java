package in.aeonplay.comman;

public interface Constants {

    int INTERVAL = 1000;
    String INDEX = "INDEX";
    String NAME = "NAME";
    String EMAIL = "EMAIL";
    String MODE = "MODE";
    String USERNAME = "USERNAME";
    String LOGIN = "LOGIN";
    String TOKEN_TYPE = "TOKEN_TYPE";
    String TOKEN_ACCESS = "TOKEN_ACCESS";
    String TOKEN_REFRESH = "TOKEN_REFRESH";
    String TOKEN_EXPIRES_IN = "TOKEN_EXPIRES_IN";
    String DATA = "DATA";
    String TRAILER = "TRAILER";
    String TITLE = "TITLE";
    String LANGUAGE = "LANGUAGE";
    String SHOW_NAME = "SHOW_NAME";
    String SHOW_ID = "SHOW_ID";
    String USER_ID = "USER_ID";
    String PROVIDER = "PROVIDER";
    String PAGE = "PAGE";
    String CATEGORY_ID = "CATEGORY_ID";
    String PAYMENT = "PAYMENT";
    String WATCHLIST = "WATCHLIST";
    String RPAYMENT_ORDER_ID = "RPAYMENT_ORDER_ID";
    String RPAYMENT_AMOUNT = "RPAYMENT_AMOUNT";
    String RPAYMENT_STATUS = "RPAYMENT_STATUS";
    String ACTIVATE = "activate";
    String DEACTIVATE = "deactivate";

    String PDF_ROOT_URL = "https://docs.google.com/gview?embedded=true&url=";
    String EROS_PARTNER_EPISODE = "https://staging.mzaalo.com/partner-api/getDataSeries?asset_id=";
    String EROS_PARTNER_PLAYBACK = "https://staging.mzaalo.com/partner-api/player/details?";
    String VIDEO_SONG = "video_song";
    String KEY_AUDIO = "AUDIO_TRACK";
    String KEY_VIDEO = "VIDEO_TRACK";
    String KEY_SUBTITLE = "SUBTITLE_TRACK";
    String BACKGROUND = "BACKGROUND";
    String LANDSCAPE = "LANDSCAPE";
    String PORTRAIT = "PORTRAIT";

    // For movie listing ...
    String CONTENT_TYPE_MOVIE = "movie";
    String INCLUDE_DEFAULT_PROVIDER_DEFAULT = "api";
    String INCLUDE_DEFAULT_PROVIDER_AEONPLAY = "aeonplay";
    String CONTENT_PROVIDER_MOVIE_AEONPLAY = "2";
    String CONTENT_PROVIDER_MOVIE_SONYLIV = "1";

    // For live channel listing ...
    String CONTENT_TYPE_LIVE_CHANNEL = "live_channel";
    String CONTENT_PROVIDER_YOUTUBE_ID = "4";

    // For tv show listing ...
    String CONTENT_TYPE_TV_SHOW = "show";
    String CONTENT_PROVIDER_SONY_ID = "1";

    // For episode listing ...
    String CONTENT_TYPE_EPISODE = "episode";
    String ORDER_BY_EPISODE_NUMBER = "episode_number";
}
