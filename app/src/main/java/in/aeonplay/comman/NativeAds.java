package in.aeonplay.comman;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.MediaView;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;

import in.aeonplay.R;
import in.aeonplay.activity.MasterActivity;

public class NativeAds {

    private static String TAG = NativeAds.class.getSimpleName();
    /*Full Native Advertisement*/
    public static void ShowFullNativeAds(MasterActivity context, FrameLayout mAdViewLayout) {
        AdLoader.Builder builder = new AdLoader.Builder(context, context.getString(R.string.native_ads));
        builder.forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
            @Override
            public void onNativeAdLoaded(@NonNull NativeAd nativeAd) {
                NativeAdView adView =
                        (NativeAdView) context.getLayoutInflater()
                                .inflate(R.layout.card_native_ads, null);
                populateFullNativeAdView(nativeAd, adView);
                mAdViewLayout.removeAllViews();
                mAdViewLayout.addView(adView);
            }
        });

        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        }).build();
        adLoader.loadAds(new AdRequest.Builder().build(), 1);
    }

    /*Full with Index Native Advertisement*/
    public static void ShowNativeAdsIndex(MasterActivity context, FrameLayout mAdViewLayout, int position) {
        AdLoader.Builder builder = new AdLoader.Builder(context, context.getString(R.string.native_ads));
        builder.forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
            @Override
            public void onNativeAdLoaded(@NonNull NativeAd nativeAd) {
                NativeAdView adView =
                        (NativeAdView) context.getLayoutInflater()
                                .inflate(R.layout.card_native_ads, null);
                populateFullNativeAdView(nativeAd, adView);
                mAdViewLayout.removeAllViews();
                mAdViewLayout.addView(adView);
            }
        });

        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        }).build();
        adLoader.loadAds(new AdRequest.Builder().build(), position);
    }

    private static void populateFullNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        MediaView mediaView = (MediaView) adView.findViewById(R.id.ad_media);
        adView.setMediaView(mediaView);

        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_icon));
        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        adView.getMediaView().setMediaContent(nativeAd.getMediaContent());
        adView.getMediaView().setImageScaleType(ImageView.ScaleType.CENTER_CROP);
        if (nativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }

        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }

        if (nativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        adView.setNativeAd(nativeAd);
    }

    /*Text Native Advertisement*/
    public static void ShowTextNativeAds(MasterActivity context, FrameLayout mAdViewLayout) {
        AdLoader.Builder builder = new AdLoader.Builder(context, context.getString(R.string.native_ads));
        builder.forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
            @Override
            public void onNativeAdLoaded(@NonNull NativeAd nativeAd) {
                NativeAdView adView =
                        (NativeAdView) context.getLayoutInflater()
                                .inflate(R.layout.card_native_ads_textonly, null);
                populateTextNativeAdView(nativeAd, adView);
                mAdViewLayout.removeAllViews();
                mAdViewLayout.addView(adView);
            }
        });

        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        }).build();
        adLoader.loadAds(new AdRequest.Builder().build(), 1);
    }

    private static void populateTextNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_icon));
        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }

        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }

        if (nativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        adView.setNativeAd(nativeAd);
    }

    /*Image Native Advertisement*/
    public static void ShowImageNativeAds(MasterActivity context, FrameLayout mAdViewLayout) {
        AdLoader.Builder builder = new AdLoader.Builder(context, context.getString(R.string.native_ads));
        builder.forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
            @Override
            public void onNativeAdLoaded(@NonNull NativeAd nativeAd) {
                NativeAdView adView =
                        (NativeAdView) context.getLayoutInflater()
                                .inflate(R.layout.card_native_ads_imageonly, null);
                populateImageNativeAdView(nativeAd, adView);
                mAdViewLayout.removeAllViews();
                mAdViewLayout.addView(adView);
            }
        });

        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        }).build();
        adLoader.loadAds(new AdRequest.Builder().build(), 1);
    }

    private static void populateImageNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        MediaView mediaView = (MediaView) adView.findViewById(R.id.ad_media);
        mediaView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);
        adView.setMediaView(mediaView);

        adView.setNativeAd(nativeAd);
    }

    public static void ShowIskconAds(MasterActivity context, FrameLayout mAdViewLayout) {
        RelativeLayout adView =
                (RelativeLayout) context.getLayoutInflater()
                        .inflate(R.layout.card_iskcon_ads, null);

        ImageView imageView = adView.findViewById(R.id.ad_media);
        imageView.setImageResource(R.drawable.ic_action_iskcon);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent launchIntent = context
                            .getPackageManager()
                            .getLaunchIntentForPackage("in.aeongroup.iskconkharghar");
                    context.startActivity(launchIntent);

                } catch (ActivityNotFoundException | NullPointerException |
                         SecurityException e) {
                    context.openStore("in.aeongroup.iskconkharghar");
                }
            }
        });

        mAdViewLayout.removeAllViews();
        mAdViewLayout.addView(adView);
    }
}
