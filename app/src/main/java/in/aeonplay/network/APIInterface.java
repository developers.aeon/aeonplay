package in.aeonplay.network;

import in.aeonplay.BuildConfig;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface APIInterface {

    // For OAuth Token ...
    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "oauth/token")
    @FormUrlEncoded
    Call<ResponseBody> postOAuthToken(
            @Field("client_id") String client_id,
            @Field("client_secret") String client_secret,
            @Field("grant_type") String grant_type,
            @Field("scope") String scope);

    // For Check user is found or not
    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/can-login")
    @FormUrlEncoded
    Call<ResponseBody> postUserCanLogin(
            @Header("Authorization") String accessToken,
            @Field("mobile_no") String mobile_no);

    // For User login
    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/login/mobile")
    @FormUrlEncoded
    Call<ResponseBody> postUserLogin(
            @Header("Authorization") String accessToken,
            @Field("client_id") String client_id,
            @Field("client_secret") String client_secret,
            @Field("grant_type") String grant_type,
            @Field("otp") String otp,
            @Field("mobile_no") String mobile_no,
            @Field("username") String username,
            @Field("fcmId") String fcmId,
            @Field("deviceName") String deviceName,
            @Field("deviceModel") String deviceModel);

    // For User registration
    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/register/mobile")
    @FormUrlEncoded
    public Call<ResponseBody> postUserRegister(
            @Header("Authorization") String accessToken,
            @Field("client_id") String client_id,
            @Field("client_secret") String client_secret,
            @Field("grant_type") String grant_type,
            @Field("otp") String otp,
            @Field("first_name") String first_name,
            @Field("last_name") String last_name,
            @Field("mobile_no") String mobile_no,
            @Field("username") String username,
            @Field("fcmId") String fcmId,
            @Field("deviceName") String deviceName,
            @Field("deviceModel") String deviceModel,
            @Field("email") String email);

    // For Send/Resend SMS
    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "api/version1.0/send-otp")
    @FormUrlEncoded
    Call<ResponseBody> postUserOTP(
            @Header("Authorization") String accessToken,
            @Field("mobile_no") String mobile_no,
            @Field("action") String action);

    // For getting refresh token
    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "oauth/token")
    @FormUrlEncoded
    Call<ResponseBody> getRefreshToken(
            @Field("client_id") String client_id,
            @Field("client_secret") String client_secret,
            @Field("grant_type") String grant_type,
            @Field("refresh_token") String refresh_token);

    // For self logout
    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/logout?")
    Call<ResponseBody> getSelfLogout(
            @Header("Authorization") String accessToken);

    // For getting user info
    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/profile?")
    Call<ResponseBody> getUserProfile(
            @Header("Authorization") String accessToken);

    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/profile/{user_id}")
    Call<ResponseBody> getUserProfileByID(
            @Header("Authorization") String accessToken,
            @Path("user_id") String user_id);

    // For account deactivate and activate
    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/profile")
    @FormUrlEncoded
    Call<ResponseBody> getUserProfileDeactivate(
            @Header("Authorization") String accessToken,
            @Field("status") String status);

    // For update user operator
    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/mobile?")
    Call<ResponseBody> getUpdtOperator(
            @Header("Authorization") String accessToken);

    // For getting dashboard data
    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/dashboard?")
    Call<ResponseBody> getDashboardList(
            @Header("Authorization") String accessToken,
            @Query("with_coming_soon") String with_coming_soon,
            @Query("with_latest") String with_latest,
            @Query("with_top_ten") String with_top_ten);

    // For getting list
    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/list?")
    Call<ResponseBody> getCommanListIncludeDefault(
            @Header("Authorization") String accessToken,
            @Query("content_type") String content_type,
            @Query("include_default_provider_filter") String include_default_provider_filter,
            @Query("page") String page);

    // For getting list
    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/list?")
    Call<ResponseBody> getMusicVideByLanguage(
            @Header("Authorization") String accessToken,
            @Query("content_type") String content_type,
            @Query("language") String language,
            @Query("page") String page);

    // For getting list
    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/list?")
    Call<ResponseBody> getCommanListProviderID(
            @Header("Authorization") String accessToken,
            @Query("content_type") String content_type,
            @Query("provider_id") String provider_id,
            @Query("page") String page);

    // For getting list
    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/list?")
    Call<ResponseBody> getCommanList(
            @Header("Authorization") String accessToken,
            @Query("content_type") String content_type,
            @Query("page") String page);

    // For getting episode list
    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/list?")
    Call<ResponseBody> getEpisode(
            @Header("Authorization") String accessToken,
            @Query("content_type") String content_type,
            @Query("show_id") String show_id,
            @Query("page") String page,
            @Query("order_by") String order_by);

    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/list?")
    Call<ResponseBody> getEpisodeSeasonList(
            @Header("Authorization") String accessToken,
            @Query("content_type") String content_type,
            @Query("show_id") String show_id,
            @Query("season_number") String season_number,
            @Query("page") String page,
            @Query("order_by") String order_by);

    // For getting episode list
    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/eros/episode?")
    Call<ResponseBody> getEROSEpisode(
            @Header("Authorization") String accessToken,
            @Query("show_id") String show_id);

    // For getting package list
    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/packageBygroup?")
    Call<ResponseBody> getPackageList(
            @Header("Authorization") String accessToken);

    // For getting search list
    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/search?")
    @FormUrlEncoded
    Call<ResponseBody> getSearch(
            @Header("Authorization") String accessToken,
            @Field("keyword") String keyword);

    // For getting subscription history list
    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/subscription?")
    Call<ResponseBody> getSubscriptionHistory(
            @Header("Authorization") String accessToken);

    // For getting playback info
    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/eros/playable?")
    Call<ResponseBody> getPlaybackInfo(
            @Header("Authorization") String accessToken,
            @Query("asset_type") String asset_type,
            @Query("asset_id") String asset_id);

    // For comman pagination
    @Headers("Accept: application/json")
    @GET
    Call<ResponseBody> getCommanPagination(
            @Header("Authorization") String accessToken,
            @Url String pagination);

    // For create a new order - razorpay
    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "api/version1.0/orders/create/{packageId}")
    Call<ResponseBody> createOrder(
            @Header("Authorization") String accessToken,
            @Path("packageId") String packageId);

    // For get order - razorpay
    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/orders/status/{packageId}")
    Call<ResponseBody> getRazorpayOrder(
            @Header("Authorization") String accessToken,
            @Path("packageId") String packageId);

    // For checkout - razorpay
    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "payment/razorpay/{paymentId}")
    Call<ResponseBody> confirmOrder(
            @Header("Authorization") String accessToken,
            @Path("paymentId") String paymentId);

    // For getting news
    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/news/v2")
    Call<ResponseBody> getNews(
            @Header("Authorization") String accessToken);

    @Headers("Accept: application/json")
    @GET("https://pub.gamezop.com/v3/games?id=6466&lang=en")
    public Call<ResponseBody> getAllGames();

    @Headers("Accept: application/json")
    @GET("https://pub.gamezop.com/v3/games?id=6467&lang=en")
    public Call<ResponseBody> getAllQuiz();

    // Add to wishlist
    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/{id}/watchlist?")
    Call<ResponseBody> addToWishlist(
            @Header("Authorization") String accessToken,
            @Path("id") String id);

    // Get wishlist
    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/watchlist")
    public Call<ResponseBody> getWishList(@Header("Authorization") String accessToken);

    // Remove from wishlist
    @Headers("Accept: application/json")
    @DELETE(BuildConfig.AUTHORIZE_URL + "api/version1.0/watchlist/{id}")
    public Call<ResponseBody> deleteWatchList(
            @Header("Authorization") String accessToken,
            @Path("id") String id);

    // Content Details
    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/{id}")
    Call<ResponseBody> getContentsFromID(
            @Header("Authorization") String accessToken,
            @Path("id") String id);

    @Headers("Accept: application/json")
    @GET("https://rss.app/feeds/v1.1/thyeanLqQlx9N5FG.json")
    public Call<ResponseBody> getNewsFeedList();

    @Multipart
    @Headers("Accept: application/json")
    @POST("http://devsupload.aeongroup.in/api/version1.0/upload/shorts")
    Call<String> postShotsVideoCampaign(
            @Header("Authorization") String accessToken,
            @Part("title") RequestBody title,
            @Part("description") RequestBody description,
            @Part MultipartBody.Part shorts_file,
            @Part MultipartBody.Part portrait_image,
            @Part("campaign_id") RequestBody campaign_id
    );

    @Multipart
    @Headers("Accept: application/json")
    @POST("http://devsupload.aeongroup.in/api/version1.0/upload/shorts")
    Call<String> postShotsVideo(
            @Header("Authorization") String accessToken,
            @Part("title") RequestBody title,
            @Part("description") RequestBody description,
            @Part MultipartBody.Part shorts_file,
            @Part MultipartBody.Part portrait_image
    );

    @Multipart
    @Headers("Accept: application/json")
    @POST("http://prodsupload.aeongroup.in/api/version1.0/upload/shorts")
    Call<String> postShotsVideoProdCampaign(
            @Header("Authorization") String accessToken,
            @Part("title") RequestBody title,
            @Part("description") RequestBody description,
            @Part MultipartBody.Part shorts_file,
            @Part MultipartBody.Part portrait_image,
            @Part("campaign_id") RequestBody campaign_id
    );

    @Multipart
    @Headers("Accept: application/json")
    @POST("http://prodsupload.aeongroup.in/api/version1.0/upload/shorts")
    Call<String> postShotsVideoProd(
            @Header("Authorization") String accessToken,
            @Part("title") RequestBody title,
            @Part("description") RequestBody description,
            @Part MultipartBody.Part shorts_file,
            @Part MultipartBody.Part portrait_image
    );

    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/shorts")
    Call<ResponseBody> getShots(
            @Header("Authorization") String accessToken,
            @Query("page") String page);

    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/shorts?")
    Call<ResponseBody> getShotsDetail(
            @Header("Authorization") String accessToken,
            @Query("userId") String userId,
            @Query("page") String page);

    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/shorts/{id}/feeling")
    @FormUrlEncoded
    Call<ResponseBody> postFeeling(
            @Header("Authorization") String accessToken,
            @Path("id") String id,
            @Field("feeling") String feeling);

    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/follow")
    @FormUrlEncoded
    Call<ResponseBody> postFollow(
            @Header("Authorization") String accessToken,
            @Field("user_id") String user_id);

    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/unfollow")
    @FormUrlEncoded
    Call<ResponseBody> postUnfollow(
            @Header("Authorization") String accessToken,
            @Field("user_id") String user_id);

    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/followers/{user_id}")
    Call<ResponseBody> getFollowers(
            @Header("Authorization") String accessToken,
            @Path("user_id") String user_id);

    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/followings/{user_id}")
    Call<ResponseBody> getFollowings(
            @Header("Authorization") String accessToken,
            @Path("user_id") String user_id);

    // Get campaignList
    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/campaign")
    public Call<ResponseBody> getCampaignList(@Header("Authorization") String accessToken);

    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "api/version1.0/campaign/{campaignId}/participant-register")
    Call<ResponseBody> postParticipantRegistration(
            @Header("Authorization") String accessToken,
            @Path("campaignId") String campaignId);

    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/campaign/{campaignId}/participant")
    Call<ResponseBody> getParticipantList(
            @Header("Authorization") String accessToken,
            @Path("campaignId") String campaignId);

    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/shorts/{id}/vote")
    Call<ResponseBody> postVote(
            @Header("Authorization") String accessToken,
            @Path("id") String id);

    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/campaign/{campaignId}/winners")
    Call<ResponseBody> getWinnerList(
            @Header("Authorization") String accessToken,
            @Path("campaignId") String campaignId);

    @Headers("Accept: application/json")
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/shorts/{id}")
    Call<ResponseBody> getShotsFromID(
            @Header("Authorization") String accessToken,
            @Path("id") String id);

    @Headers("Accept: application/json")
    @DELETE(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/shorts/{id}/delete")
    Call<ResponseBody> deleteShotsByID(
            @Header("Authorization") String accessToken,
            @Path("id") String id);
}
