package in.aeonplay.network;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.ConnectException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLHandshakeException;

import in.aeonplay.BuildConfig;
import in.aeonplay.R;
import in.aeonplay.activity.MasterActivity;
import in.aeonplay.comman.ConnectivityReceiver;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.ExceptionHandler;
import in.aeonplay.model.Login.JWTModel;
import in.aeonplay.model.Login.TokenModel;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class APIClient {
    private static Retrofit retrofit = null;
    private static String TAG = APIClient.class.getSimpleName();
    private static ArrayList<PendingAPICall> apiCallArrayList = new ArrayList<>();

    public static Retrofit getClient() {

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(new MyOkHttpInterceptor());

        client.connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .callTimeout(60, TimeUnit.SECONDS);

        retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.AUTHORIZE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build();

        return retrofit;
    }

    public static class MyOkHttpInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request originalRequest = chain.request();
            String token_type = SharePreferenceManager.getString(Constants.TOKEN_TYPE);
            String token_access = SharePreferenceManager.getString(Constants.TOKEN_ACCESS);

            if (!token_type.equalsIgnoreCase("") && !token_access.equalsIgnoreCase("")) {
                Request newRequest = originalRequest.newBuilder().header("Authorization", token_type + " " + token_access).build();
                return chain.proceed(newRequest);
            }

            return chain.proceed(originalRequest);
        }
    }

    public static String isTokenValid(MasterActivity context) {
        if (!SharePreferenceManager.getString(Constants.TOKEN_ACCESS).equalsIgnoreCase("") &&
                !SharePreferenceManager.getString(Constants.TOKEN_REFRESH).equalsIgnoreCase("")) {

            // Check Token is Valid or Expire. In Login AccessToken you will get
            // 'headers/payload/signature' take 'payload' from accessToken.

            // Get payload ...
            String[] loadAccessToken = SharePreferenceManager.getString(Constants.TOKEN_ACCESS).split("[.]");
            String loadPayload = loadAccessToken[1];

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                // Base64 Decode payload
                String decodedPart = new String(Base64.getUrlDecoder().decode(loadPayload), StandardCharsets.UTF_8);

                Gson gson = new Gson();
                Reader reader = new StringReader(decodedPart);
                JWTModel jwtModel = gson.fromJson(reader, JWTModel.class);

                long millis = System.currentTimeMillis();
                long currentSeconds = millis / 1000;

                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(millis);

                // Check token is expired or not ...
                if (currentSeconds >= jwtModel.getExp()) {
                    getRefreshToken(context);
                    return "Failure";

                } else {
                    return "Success";

                }
            }

        }
        return "";
    }

    private static void getRefreshToken(MasterActivity masterActivity) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> apiCall = apiInterface.getRefreshToken(
                BuildConfig.CLIENT_ID,
                BuildConfig.CLIENT_SECRET,
                masterActivity.getString(R.string.refresh_token),
                SharePreferenceManager.getString(Constants.TOKEN_REFRESH));
        apiCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                try {
                    if (response.isSuccessful()) {
                        try {
                            Gson gson = new Gson();
                            Reader reader = new StringReader(response.body().string());
                            TokenModel loginModel = gson.fromJson(reader, TokenModel.class);

                            SharePreferenceManager.save(Constants.TOKEN_TYPE, loginModel.getTokenType());
                            SharePreferenceManager.save(Constants.TOKEN_ACCESS, loginModel.getAccessToken());
                            SharePreferenceManager.save(Constants.TOKEN_REFRESH, loginModel.getRefreshToken());
                            SharePreferenceManager.save(Constants.TOKEN_EXPIRES_IN, loginModel.getExpiresIn());

                            for (PendingAPICall pendingAPICall : apiCallArrayList) {
                                callAPI(masterActivity, pendingAPICall.call, pendingAPICall.callBack);
                            }

                        } catch (Exception e) {
                            ExceptionHandler.recordException(e);
                            e.printStackTrace();
                        }
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    ExceptionHandler.recordResponseException(apiCall.request().url().toString(), exception);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                ExceptionHandler.recordResponseException(apiCall.request().url().toString(), t);
            }
        });
    }

    public static void callAPI(final MasterActivity activity, Call<ResponseBody> call, final APICallback apiCallback) {
        final long start = System.currentTimeMillis();
        if (ConnectivityReceiver.isConnected() == false) {
            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        activity.showNoInternetDialog();
                    }
                });

            }
            return;
        }
        String result = isTokenValid(activity);
        if (result.equals("Success") || result.equals("")) {
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    long end = System.currentTimeMillis();
                    String url = call.request().url().toString();

                    if (response.code() == 200) {
                        try {
                            String res = response.body().string();
                            if (apiCallback != null) {
                                apiCallback.onSuccess(res);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            String error = response.errorBody().string();
                            apiCallback.onFailure(error, response.code());
                            ExceptionHandler.recordException(new Exception("Url: " + url + "\nResponse:" + error));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    String url = call.request().url().toString();
                    long end = System.currentTimeMillis();
                    if (t.getCause() != null && t.getCause() instanceof SSLHandshakeException) {
                        if (apiCallback != null) {
                            apiCallback.onError("SSL Handshake Exception");
                        }
                        return;
                    }
                    if (t instanceof ConnectException) {
                        if (activity != null) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    activity.showNoInternetDialog();
                                }
                            });

                        }
                        return;
                    }

                    ExceptionHandler.recordException(new Exception("Url: " + url + "\nException:" + t + " Duration:" + (end - start) + " in milliseconds"));
                    t.printStackTrace();
                }
            });

        } else {
            final PendingAPICall pendingAPICall = new PendingAPICall();
            pendingAPICall.call = call;
            pendingAPICall.callBack = apiCallback;
            apiCallArrayList.add(pendingAPICall);
        }
    }

    public interface APICallback {
        void onSuccess(String response);

        void onFailure(String error, int responseCode);

        void onError(String error);
    }

    public static class PendingAPICall {
        public Call<ResponseBody> call;
        public APICallback callBack;
    }
}
