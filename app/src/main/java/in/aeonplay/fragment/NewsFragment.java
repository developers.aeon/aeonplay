package in.aeonplay.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import androidx.annotation.RequiresApi;
import androidx.core.app.ShareCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.aeonplay.BuildConfig;
import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.activity.MasterActivity;
import in.aeonplay.adapter.NewsAdapter;
import in.aeonplay.comman.InterstitialAds;
import in.aeonplay.comman.OneShotClickListener;
import in.aeonplay.comman.ProgressbarManager;
import in.aeonplay.comman.SpaceItemDecoration;
import in.aeonplay.model.NewsModel.NewsData;
import in.aeonplay.model.NewsModel.NewsHeader;

public class NewsFragment extends BaseFragment {

    private RecyclerView mNewsRecyclerView;
    private RelativeLayout rootContainer;
    private FrameLayout adContainer;
    private List<NewsHeader> headerAdapter;
    private ImageButton ibShare;
    private ProgressbarManager progressbarManager;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_news, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {

        Init(v);
        setData();
    }

    private void setData() {

        progressbarManager.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getNewsList();
            }
        }, 2000);

        ibShare.setOnClickListener(new OneShotClickListener() {
            @Override
            public void onClicked(View v) {
                try {
                    Bitmap bitmap = BitmapFactory.decodeResource(getActivity().getResources(),
                            R.drawable.ic_action_newspaper);

                    Uri bitmapUri = activity.saveImageExternal(bitmap);
                    Intent shareIntent = ShareCompat.IntentBuilder.from(getActivity())
                            .setType("image/*")
                            .setStream(bitmapUri)
                            .setText("Check out, Today's breaking news on Aeonplay! " + BuildConfig.AUTHORIZE_URL + "newspaper")
                            .getIntent();

                    if (shareIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivity(shareIntent);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void getNewsList() {
        progressbarManager.dismiss();
        if (activity.mNewsList.size() > 0) {
            rootContainer.setVisibility(View.VISIBLE);

            ArrayList<String> languageList = new ArrayList<>();
            for (NewsData newsData : activity.mNewsList) {
                languageList.add(newsData.getLanguage());
            }

            List<String> newLanguageList = removeDuplicates(languageList);

            headerAdapter = new ArrayList<>();
            for (String language : newLanguageList) {

                List<NewsData> mList = new ArrayList<>();
                for (NewsData newsData : activity.mNewsList) {
                    if (language.equalsIgnoreCase(newsData.getLanguage())) {
                        mList.add(newsData);
                    }
                }

                headerAdapter.add(new NewsHeader(language, mList));
            }

            NewsAdapter newsAdapter = new NewsAdapter(activity, headerAdapter);
            mNewsRecyclerView.setAdapter(newsAdapter);
            newsAdapter.notifyDataSetChanged();

        } else {
            rootContainer.setVisibility(View.GONE);
            activity.showMessageToUser("Data not found!");
        }
    }

    public static <T> ArrayList<T> removeDuplicates(ArrayList<T> list) {
        ArrayList<T> newList = new ArrayList<T>();
        for (T element : list) {
            if (!newList.contains(element)) {
                newList.add(element);
            }
        }

        return newList;
    }

    private void Init(View view) {
        progressbarManager = new ProgressbarManager(getActivity());
        adContainer = view.findViewById(R.id.adContainer);
        rootContainer = view.findViewById(R.id.rootContainer);
        ibShare = view.findViewById(R.id.ibShare);

        mNewsRecyclerView = view.findViewById(R.id.commanList);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false);
        mNewsRecyclerView.setLayoutManager(gridLayoutManager);
        mNewsRecyclerView.addItemDecoration(new SpaceItemDecoration());
    }

    @Override
    public void onBack() {
        if (getActivity().getSupportFragmentManager().getBackStackEntryCount() == 1) {
            activity.addFragment(new HomeFragment());

        } else {
            Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            activity.removeFragment(fragment);
        }
    }

}