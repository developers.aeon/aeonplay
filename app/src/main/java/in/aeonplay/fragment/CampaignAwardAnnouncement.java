package in.aeonplay.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import in.aeonplay.R;
import in.aeonplay.activity.MasterActivity;
import in.aeonplay.adapter.CampaignParticipantWinnersAdapter;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.InterstitialAds;
import in.aeonplay.comman.ProgressbarManager;
import in.aeonplay.model.Shots.Campaign.CampaignDataModel;
import in.aeonplay.model.Shots.Campaign.CampaignResultDataModel;
import in.aeonplay.model.Shots.Campaign.CampaignResultModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class CampaignAwardAnnouncement extends BaseFragment {

    private String TAG = CampaignParticipantsFragment.class.getSimpleName();
    private Bundle bundle;
    private CampaignDataModel mCampaignDataModel;
    private RecyclerView rcvParticipantList;
    private CampaignParticipantWinnersAdapter campaignParticipantWinnersAdapter;
    private RelativeLayout rootContainer;
    private ProgressbarManager progressbarManager;
    private TextView txtName1, txtName2, txtName3;
    private TextView txtVoteCount1, txtVoteCount2, txtVoteCount3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_campaign_result, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Init(view);
        setData();
    }

    private void Init(View view) {
        progressbarManager = new ProgressbarManager(getActivity());
        rootContainer = (RelativeLayout) view.findViewById(R.id.rootContainer);
        rcvParticipantList = (RecyclerView) view.findViewById(R.id.rcvAwardList);

        rcvParticipantList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.recyclerview_item_divider));
        rcvParticipantList.addItemDecoration(dividerItemDecoration);
        rcvParticipantList.setItemAnimator(new DefaultItemAnimator());

        txtName1 = view.findViewById(R.id.txtName1);
        txtName2 = view.findViewById(R.id.txtName2);
        txtName3 = view.findViewById(R.id.txtName3);
        txtVoteCount1 = view.findViewById(R.id.txtVoteCount1);
        txtVoteCount2 = view.findViewById(R.id.txtVoteCount2);
        txtVoteCount3 = view.findViewById(R.id.txtVoteCount3);
    }

    private void setData() {
        //InterstitialAds.ShowInterstitialAds((MasterActivity) getActivity());

        bundle = getArguments();
        if (bundle != null) {
            mCampaignDataModel = bundle.getParcelable("DATA");

            progressbarManager.show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getAllParticipantList(String.valueOf(mCampaignDataModel.getId()));
                }
            }, Constants.INTERVAL);
        }
    }

    private void getAllParticipantList(String campaignID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getWinnerList(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), campaignID);
        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                progressbarManager.dismiss();

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response.toString());
                    final CampaignResultModel campaignResultModel = gson.fromJson(reader, CampaignResultModel.class);

                    if (campaignResultModel.getData().size() > 0) {
                        rootContainer.setVisibility(View.VISIBLE);
                        rcvParticipantList.setVisibility(View.VISIBLE);

                        if (campaignResultModel.getData().size() > 0) {
                            CampaignResultDataModel campaignResultDataModel = campaignResultModel.getData().get(0);
                            txtName1.setText(campaignResultDataModel.getUser().getFirstName() + " " + campaignResultDataModel.getUser().getLastName());
                            txtVoteCount1.setText(campaignResultDataModel.getUser().getShorts_count() + "");
                        }

                        if (campaignResultModel.getData().size() > 1) {
                            CampaignResultDataModel campaignResultDataModel = campaignResultModel.getData().get(1);
                            txtName2.setText(campaignResultDataModel.getUser().getFirstName() + " " + campaignResultDataModel.getUser().getLastName());
                            txtVoteCount2.setText(campaignResultDataModel.getUser().getShorts_count() + "");
                        }

                        if (campaignResultModel.getData().size() > 2) {
                            CampaignResultDataModel campaignResultDataModel = campaignResultModel.getData().get(2);
                            txtName3.setText(campaignResultDataModel.getUser().getFirstName() + " " + campaignResultDataModel.getUser().getLastName());
                            txtVoteCount3.setText(campaignResultDataModel.getUser().getShorts_count() + "");
                        }

                        List<CampaignResultDataModel> mFilterList = new ArrayList<>();
                        for (int i = 3; i < campaignResultModel.getData().size(); i++) {
                            mFilterList.add(campaignResultModel.getData().get(i));
                        }

                        campaignParticipantWinnersAdapter = new CampaignParticipantWinnersAdapter(activity, mFilterList);
                        rcvParticipantList.setAdapter(campaignParticipantWinnersAdapter);
                        campaignParticipantWinnersAdapter.notifyDataSetChanged();

                    } else {
                        rootContainer.setVisibility(View.GONE);
                        rcvParticipantList.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                progressbarManager.dismiss();
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                progressbarManager.dismiss();
                activity.showMessageToUser(error);
            }
        });
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        activity.removeFragment(fragment);
    }
}
