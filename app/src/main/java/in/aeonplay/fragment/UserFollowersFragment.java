package in.aeonplay.fragment;

import android.app.Activity;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.util.Locale;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.adapter.UserFollowerAdapter;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.ProgressbarManager;
import in.aeonplay.model.Shots.Follow.FollowModel;
import in.aeonplay.model.Shots.ShotSubDataModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class UserFollowersFragment extends BaseFragment {

    private RecyclerView rcvAdminList;
    private SearchView mSearchView;
    private int REQUEST_VOICE = 13;
    private UserFollowerAdapter userFollowerAdapter;
    private LinearLayout errorLayout;
    private Bundle bundle;
    private int mUserID;
    private ProgressbarManager progressbarManager;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_follower, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);
        setData();
    }

    private void setData() {
        bundle = getArguments();
        if (bundle != null) {
            mUserID = bundle.getInt(Constants.USER_ID);

            progressbarManager.show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getFollowers(mUserID + "");
                }
            }, Constants.INTERVAL);
        }

        mSearchView.setQueryRefinementEnabled(true);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (userFollowerAdapter != null) {
                    userFollowerAdapter.getFilter().filter(query);
                    hideKeyboard();
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (userFollowerAdapter != null) {
                    userFollowerAdapter.getFilter().filter(query);
                }
                return false;
            }
        });

        ImageView clearButton = (ImageView) mSearchView.findViewById(androidx.appcompat.R.id.search_close_btn);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchView.setQuery("", false);
                userFollowerAdapter.getFilter().filter("");
                mSearchView.clearFocus();
            }
        });

        ImageView voiceButton = (ImageView) mSearchView.findViewById(androidx.appcompat.R.id.search_voice_btn);
        voiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askSpeechInput();
            }
        });
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    private void getFollowers(String id) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getFollowers(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), id);

        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                progressbarManager.dismiss();

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final FollowModel followModel = gson.fromJson(reader, FollowModel.class);

                    if (followModel.getData().getFollowers().size() > 0) {
                        errorLayout.setVisibility(View.GONE);
                        rcvAdminList.setVisibility(View.VISIBLE);

                        userFollowerAdapter = new UserFollowerAdapter(activity, followModel.getData().getFollowers());
                        rcvAdminList.setAdapter(userFollowerAdapter);
                        userFollowerAdapter.notifyDataSetChanged();

                    } else {
                        errorLayout.setVisibility(View.VISIBLE);
                        rcvAdminList.setVisibility(View.GONE);
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                progressbarManager.dismiss();
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                progressbarManager.dismiss();
                activity.showMessageToUser(error);
            }
        });
    }

    private void askSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Try saying something");
        try {
            startActivityForResult(intent, REQUEST_VOICE);
        } catch (ActivityNotFoundException a) {
            a.printStackTrace();
        }
    }

    private void Init(View v) {
        progressbarManager = new ProgressbarManager(getActivity());
        errorLayout = (LinearLayout) v.findViewById(R.id.errorLayout);
        SearchManager mSearchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mSearchView = (SearchView) v.findViewById(R.id.searchView);

        mSearchView.setSearchableInfo(mSearchManager.getSearchableInfo(getActivity().getComponentName()));
        mSearchView.setMaxWidth(Integer.MAX_VALUE);
        mSearchView.setIconified(false);
        mSearchView.clearFocus();

        rcvAdminList = (RecyclerView) v.findViewById(R.id.rcvAdminList);
        rcvAdminList.setLayoutManager(new LinearLayoutManager(getActivity(), GridLayoutManager.VERTICAL, false));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.recyclerview_item_divider));
        rcvAdminList.addItemDecoration(dividerItemDecoration);

        rcvAdminList.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        activity.removeFragment(fragment);
    }
}
