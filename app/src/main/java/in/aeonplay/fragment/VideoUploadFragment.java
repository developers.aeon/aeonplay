package in.aeonplay.fragment;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import in.aeonplay.BuildConfig;
import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.activity.MasterActivity;
import in.aeonplay.adapter.CampaignListAdapter;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.OneShotClickListener;
import in.aeonplay.comman.ProgressbarManager;
import in.aeonplay.model.Shots.Campaign.CampaignDataModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoUploadFragment extends BaseFragment {

    private static final int REQUEST_PICK_CAPTURE_VIDEO = 01;
    private String videoPath = null, imagePath = null;
    private int mCampaignID = 1;
    private Button btnSubmit;
    private ImageView uploadVideo;
    private EditText edtVideoDescription, edtCampaign;
    private ProgressbarManager progressbarManager;
    private LinearLayout linearCampaign;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_video_upload, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE |
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Init(v);
        setData();
    }

    private void setData() {
        uploadVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertTheme);
                builder.setTitle("Choose your video");
                String[] images = {"Take From Gallery", "Take From Camera"};
                builder.setItems(images, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if (checkStoragePermission()) {
                                    pickVideo();

                                } else {
                                    askStoragePermission();
                                }
                                break;

                            case 1:
                                if (checkStoragePermission()) {
                                    captureVideo();

                                } else {
                                    askStoragePermission();
                                }

                                break;
                        }
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        btnSubmit.setOnClickListener(new OneShotClickListener() {
            @Override
            public void onClicked(View v) {
                if (videoPath == null && imagePath == null) {
                    activity.showMessageToUser("Please select video");

                }
                if (edtVideoDescription.getText().toString().equalsIgnoreCase("")) {
                    activity.showMessageToUser("Please enter the description");

                } else {
                    progressbarManager.show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            postAddVideoRequest();
                        }
                    }, Constants.INTERVAL);

                }
            }
        });

        linearCampaign.setVisibility(View.GONE);
        if (!activity.mCampaignList.isEmpty()) {
            linearCampaign.setVisibility(View.VISIBLE);
        }

        edtCampaign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View parentView = getLayoutInflater().inflate(R.layout.dialog_list, null);
                final BottomSheetDialog dialog = new BottomSheetDialog(getActivity(), R.style.AppBottomSheetDialogTheme);
                dialog.setContentView(parentView);
                dialog.show();

                RecyclerView mCampaignList = (RecyclerView) dialog.findViewById(R.id.genreList);
                LinearLayoutManager lm = new LinearLayoutManager(getActivity());
                mCampaignList.setLayoutManager(lm);

                if (activity.mCampaignList.size() > 0) {
                    CampaignListAdapter adapter = new CampaignListAdapter((MasterActivity) getActivity(), activity.mCampaignList);
                    mCampaignList.setAdapter(adapter);
                    adapter.setOnItemClickListener(new CampaignListAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            CampaignDataModel campaignDataModel = activity.mCampaignList.get(position);
                            mCampaignID = campaignDataModel.getId();
                            edtCampaign.setText(campaignDataModel.getTitle());
                            dialog.dismiss();
                        }
                    });

                }
            }
        });
    }

    private void captureVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 90);
        intent.putExtra("EXTRA_VIDEO_QUALITY", 1);
        startActivityForResult(intent, REQUEST_PICK_CAPTURE_VIDEO);
    }

    private void pickVideo() {
        Intent pickVideoIntent = new Intent(Intent.ACTION_PICK);
        pickVideoIntent.setType("video/*");
        startActivityForResult(pickVideoIntent, REQUEST_PICK_CAPTURE_VIDEO);
    }

    private void postAddVideoRequest() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        String username = SharePreferenceManager.getUserData().getFirstName() + " " +
                SharePreferenceManager.getUserData().getLastName();
        Log.e("postAddVideoRequest: ", imagePath +"\n" + videoPath + "\n" + mCampaignID + "\n" + username + "\n" + edtVideoDescription.getText().toString());

        File file = new File(imagePath);
        RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part bannerPath = MultipartBody.Part.createFormData("portrait_image", file.getName(), mFile);

        File fileVideo = new File(videoPath);
        RequestBody mFileVideo = RequestBody.create(MediaType.parse("multipart/form-data"), fileVideo);
        MultipartBody.Part videoPath = MultipartBody.Part.createFormData("shorts_file", fileVideo.getName(), mFileVideo);

        RequestBody campaignID = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(mCampaignID));
        RequestBody videoTitle = RequestBody.create(MediaType.parse("multipart/form-data"), username);
        RequestBody videoDesc = RequestBody.create(MediaType.parse("multipart/form-data"), edtVideoDescription.getText().toString());

        Call<String> loginCall;
        if (BuildConfig.DEBUG) {

            if (!activity.mCampaignList.isEmpty()) {
                loginCall = apiInterface.postShotsVideoCampaign(
                        SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                                SharePreferenceManager.getString(Constants.TOKEN_ACCESS), videoTitle, videoDesc, videoPath, bannerPath, campaignID);
            } else {
                loginCall = apiInterface.postShotsVideo(
                        SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                                SharePreferenceManager.getString(Constants.TOKEN_ACCESS), videoTitle, videoDesc, videoPath, bannerPath);
            }

        } else {

            if (!activity.mCampaignList.isEmpty()) {
                loginCall = apiInterface.postShotsVideoProdCampaign(
                        SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                                SharePreferenceManager.getString(Constants.TOKEN_ACCESS), videoTitle, videoDesc, videoPath, bannerPath, campaignID);
            } else {
                loginCall = apiInterface.postShotsVideoProd(
                        SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                                SharePreferenceManager.getString(Constants.TOKEN_ACCESS), videoTitle, videoDesc, videoPath, bannerPath);
            }
        }

        loginCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                activity.showMessageToUser("Shorts uploaded successfully!");
                progressbarManager.dismiss();
                onBack();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage().toString());
                activity.showMessageToUser(t.getMessage());
                progressbarManager.dismiss();
            }
        });
    }

    private void Init(View v) {
        progressbarManager = new ProgressbarManager(getActivity());
        edtVideoDescription = (EditText) v.findViewById(R.id.edtVideoDescription);
        edtCampaign = (EditText) v.findViewById(R.id.edtCampaign);

        linearCampaign = (LinearLayout) v.findViewById(R.id.linearCampaign);
        btnSubmit = (Button) v.findViewById(R.id.btnSubmit);
        uploadVideo = (ImageView) v.findViewById(R.id.uploadVideo);
    }

    @SuppressLint("Range")
    public long checkVideoDurationValidation(Context context, Uri uri) {
        Cursor cursor = MediaStore.Video.query(context.getContentResolver(), uri, new
                String[]{MediaStore.Video.VideoColumns.DURATION});
        long duration = 0;
        if (cursor != null && cursor.moveToFirst()) {
            duration = cursor.getLong(cursor.getColumnIndex(MediaStore.Video.VideoColumns.DURATION));
            cursor.close();
        }
        return duration;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        System.gc();
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_PICK_CAPTURE_VIDEO) {
                if (data != null) {
                    Uri selectedVideo = data.getData();
                    videoFile(selectedVideo);
                }
            }

        } else if (resultCode != RESULT_CANCELED) {
            activity.showMessageToUser("Sorry, there was an error!");
        }
    }

    private void videoFile(Uri selectedVideo) {
//        String fileExtension = MimeTypeMap.getFileExtensionFromUrl(getPath(getActivity(), selectedVideo));
//                    if (fileExtension.equalsIgnoreCase("mp4")) {

        long duration = checkVideoDurationValidation(getActivity(), selectedVideo);
        if (duration < 91000) {

//            long minutes = TimeUnit.MILLISECONDS.toMinutes(duration);
//            long seconds = (TimeUnit.MILLISECONDS.toSeconds(duration) % 60);
//            uploadLength.setText(minutes + ":" + seconds);

            videoPath = getPath(getActivity(), selectedVideo);
            if (videoPath != null) {

                Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(
                        videoPath, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
                uploadVideo.setImageBitmap(bitmap);

                Uri tempUri = getImageUri(getActivity(), bitmap);
                imagePath = getRealPathFromURI(tempUri);
            }

        } else {
            activity.showMessageToUser("Dear User, The maximum video duration should be 90 seconds.");
        }
//                    } else {
//                        activity.showMessageToUser("Dear User, The Video file extension should be .mp4");
//                    }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
        String filename = dateFormat.format(new Date());
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, filename, null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        activity.removeFragment(fragment);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public void askStoragePermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) +
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) +
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (getActivity(), Manifest.permission.CAMERA) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                requestPermissions(
                        new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PICK_CAPTURE_VIDEO);

            } else {
                requestPermissions(
                        new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_PICK_CAPTURE_VIDEO);
            }

        }
    }

    public boolean checkStoragePermission() {
        boolean flag;
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED) {
            flag = false;
        } else {
            flag = true;
        }
        return flag;
    }
}
