package in.aeonplay.fragment;

import static in.aeonplay.activity.MainActivity.mToolbar;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;

import in.aeonplay.BuildConfig;
import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.activity.MasterActivity;
import in.aeonplay.adapter.PaginationCommanChildAdapter;
import in.aeonplay.adapter.PaginationScrollListener;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.InterstitialAds;
import in.aeonplay.comman.ProgressbarManager;
import in.aeonplay.comman.SpaceItemDecoration;
import in.aeonplay.model.Comman.CommanData;
import in.aeonplay.model.Comman.CommanDataList;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class MoreFragment extends BaseFragment {

    private RecyclerView mMoreRecyclerView;
    private RelativeLayout rootContainer;
    private Bundle bundle;
    private ArrayList<CommanDataList> commanDataModel;
    private String headerTitle;
    private GridLayoutManager gridLayoutManager;
    private PaginationCommanChildAdapter adapter;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private int categoryID;
    private ProgressbarManager progressbarManager;
    private String PAGE_URL = BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/category/v2/";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_comman, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new PaginationCommanChildAdapter(activity);

        Init(view);
        setData();
    }

    private void setData() {
//        InterstitialAds.ShowInterstitialAds((MasterActivity) getActivity());

        bundle = getArguments();
        if (bundle != null) {
            commanDataModel = bundle.getParcelableArrayList(Constants.DATA);
            headerTitle = bundle.getString(Constants.TITLE);
            categoryID = bundle.getInt(Constants.CATEGORY_ID);
            mToolbar.setSubtitle(headerTitle);
        }

        if (commanDataModel != null) {
            progressbarManager.show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressbarManager.dismiss();
                    rootContainer.setVisibility(View.VISIBLE);

                    String contentType = commanDataModel.get(0).getProvider();
                    if (!contentType.equalsIgnoreCase("youtube")) {

                        int spanCount = getResources().getBoolean(R.bool.isTablet) ? 4 : 3;
                        gridLayoutManager = new GridLayoutManager(activity, spanCount, GridLayoutManager.VERTICAL, false);
                        mMoreRecyclerView.setLayoutManager(gridLayoutManager);

                        mMoreRecyclerView.addOnScrollListener(new PaginationScrollListener(1,
                                gridLayoutManager) {
                            @Override
                            protected void loadMoreItems() {
                                if (currentPage != 0) {
                                    isLoading = true;
                                    currentPage += 1;

                                    loadNextPage(PAGE_URL + categoryID + "?page=" + currentPage);
                                }
                            }

                            @Override
                            public int getTotalPageCount() {
                                return TOTAL_PAGES;
                            }

                            @Override
                            public boolean isLastPage() {
                                return isLastPage;
                            }

                            @Override
                            public boolean isLoading() {
                                return isLoading;
                            }
                        });

                        adapter.addAll(commanDataModel);
                        TOTAL_PAGES = currentPage;

                        if (currentPage <= TOTAL_PAGES)
                            adapter.addLoadingFooter();
                        else isLastPage = true;

                    } else {
                        int spanCount = getResources().getBoolean(R.bool.isTablet) ? 3 : 2;
                        gridLayoutManager = new GridLayoutManager(activity, spanCount, GridLayoutManager.VERTICAL, false);
                        mMoreRecyclerView.setLayoutManager(gridLayoutManager);

                        mMoreRecyclerView.addOnScrollListener(new PaginationScrollListener(1,
                                gridLayoutManager) {
                            @Override
                            protected void loadMoreItems() {
                                if (currentPage != 0) {
                                    isLoading = true;
                                    currentPage += 1;

                                    loadNextPage(PAGE_URL + categoryID + "?page=" + currentPage);
                                }
                            }

                            @Override
                            public int getTotalPageCount() {
                                return TOTAL_PAGES;
                            }

                            @Override
                            public boolean isLastPage() {
                                return isLastPage;
                            }

                            @Override
                            public boolean isLoading() {
                                return isLoading;
                            }
                        });

                        adapter.addAll(commanDataModel);
                        TOTAL_PAGES = currentPage;

                        if (currentPage <= TOTAL_PAGES)
                            adapter.addLoadingFooter();
                        else isLastPage = true;
                    }
                }
            }, Constants.INTERVAL);

        } else {
            activity.showMessageToUser("No Data Found!");
        }
    }

    private void Init(View view) {
        progressbarManager = new ProgressbarManager(getActivity());
        rootContainer = view.findViewById(R.id.rootContainer);

        mMoreRecyclerView = view.findViewById(R.id.commanList);
        mMoreRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mMoreRecyclerView.addItemDecoration(new SpaceItemDecoration());
        mMoreRecyclerView.setAdapter(adapter);
    }

    public void loadNextPage(String nextPageUrl) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getCommanPagination(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), nextPageUrl);

        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                rootContainer.setVisibility(View.VISIBLE);
                progressbarManager.dismiss();

                if (!response.equals("{\"data\":[]}")) {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    CommanData commanModel = gson.fromJson(reader, CommanData.class);

                    if (commanModel.getData() != null) {
                        TOTAL_PAGES = currentPage + 1;

                        adapter.removeLoadingFooter();
                        isLoading = false;

                        adapter.addAll(commanModel.getData());
                        if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
                        else isLastPage = true;

                    } else {
                        currentPage = 0;
                        adapter.removeLoadingFooter();
                        isLoading = false;
                    }

                } else {
                    currentPage = 0;
                    adapter.removeLoadingFooter();
                    isLoading = false;
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                activity.showMessageToUser(error);
            }
        });
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        activity.removeFragment(fragment);
    }
}
