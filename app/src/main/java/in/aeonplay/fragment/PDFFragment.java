package in.aeonplay.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.danjdt.pdfviewer.PdfViewer;
import com.danjdt.pdfviewer.interfaces.OnErrorListener;
import com.danjdt.pdfviewer.interfaces.OnPageChangedListener;
import com.danjdt.pdfviewer.utils.PdfPageQuality;
import com.danjdt.pdfviewer.view.PdfViewerRecyclerView;

import java.io.IOException;

import in.aeonplay.R;
import in.aeonplay.activity.MasterActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.InterstitialAds;
import in.aeonplay.comman.ProgressbarManager;
import in.aeonplay.model.NewsModel.NewsData;


public class PDFFragment extends BaseFragment implements OnPageChangedListener, OnErrorListener {

    private TextView tvCounter;
    private FrameLayout rootView;
    private RelativeLayout rootContainer;
    private Bundle bundle;
    private NewsData newsData;
    private CardView counterContainer;
    private ProgressbarManager progressbarManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pdf, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InterstitialAds.ShowInterstitialAds((MasterActivity) getActivity());

        progressbarManager = new ProgressbarManager(getActivity());
        progressbarManager.show();
        bundle = getArguments();
        if (bundle != null)
            newsData = bundle.getParcelable(Constants.DATA);

        counterContainer = view.findViewById(R.id.counterContainer);
        rootContainer = view.findViewById(R.id.rootContainer);

        rootView = view.findViewById(R.id.rootView);
        tvCounter = view.findViewById(R.id.tvCounter);

        new PdfViewer.Builder(rootView)
                .view(new PdfViewerRecyclerView(getActivity()))
                .setMaxZoom(5f)
                .setZoomEnabled(true)
                .quality(PdfPageQuality.QUALITY_1080)
                .setOnErrorListener(this)
                .setOnPageChangedListener(this)
                .build()
                .load(newsData.getUrl());
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        activity.removeFragment(fragment);
    }

    @Override
    public void onAttachViewError(@NonNull Exception e) {
        e.printStackTrace();
    }

    @Override
    public void onFileLoadError(@NonNull Exception e) {
        e.printStackTrace();
    }

    @Override
    public void onPdfRendererError(@NonNull IOException e) {
        e.printStackTrace();
    }

    @Override
    public void onPageChanged(int page, int total) {
        progressbarManager.dismiss();
        counterContainer.setVisibility(View.VISIBLE);
        tvCounter.setText(getString(R.string.pdf_page_counter, page-1, total));
    }

}