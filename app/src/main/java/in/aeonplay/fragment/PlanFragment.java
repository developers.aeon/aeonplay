package in.aeonplay.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import in.aeonplay.R;
import in.aeonplay.adapter.PlanAdapter;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.ProgressbarManager;
import in.aeonplay.comman.SpaceItemDecoration;
import in.aeonplay.model.Package.PackageChildModel;
import in.aeonplay.model.Package.PackageDataModel;
import in.aeonplay.model.Package.PackageGroupModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class PlanFragment extends BaseFragment {

    private RecyclerView mRecyclerView;
    private RelativeLayout rootContainer;
    private ProgressbarManager progressbarManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_comman, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Init(view);

        progressbarManager.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getSubscriptionPlans();
            }
        }, Constants.INTERVAL);
    }

    private void Init(View view) {
        progressbarManager = new ProgressbarManager(getActivity());
        mRecyclerView = view.findViewById(R.id.commanList);
        rootContainer = view.findViewById(R.id.rootContainer);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        mRecyclerView.addItemDecoration(new SpaceItemDecoration());
        mRecyclerView.setNestedScrollingEnabled(false);
    }

    private void getSubscriptionPlans() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getPackageList(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS));

        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                rootContainer.setVisibility(View.VISIBLE);
                progressbarManager.dismiss();

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final PackageGroupModel packageGroupModel = gson.fromJson(reader, PackageGroupModel.class);

                    if (packageGroupModel.getData() != null) {

                        List<PackageChildModel> packageChildModels = new ArrayList<>();
                        for (PackageDataModel packageDataModel : packageGroupModel.getData()) {
                            for (PackageChildModel packageChildModel : packageDataModel.getPackages()) {
                                packageChildModel.setHeaderTitle(packageDataModel.getTitle());
                                packageChildModels.add(packageChildModel);
                            }
                        }

                        PlanAdapter adapter = new PlanAdapter(activity, packageChildModels);
                        mRecyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                    } else {
                        activity.showMessageToUser("Data not found!");
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                progressbarManager.dismiss();
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                progressbarManager.dismiss();
                activity.showMessageToUser(error);
            }
        });
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        activity.removeFragment(fragment);
    }
}
