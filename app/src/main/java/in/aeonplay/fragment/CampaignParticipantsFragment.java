package in.aeonplay.fragment;

import static android.app.Activity.RESULT_OK;

import android.app.Activity;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.io.Reader;
import java.io.StringReader;
import java.util.Locale;

import in.aeonplay.R;
import in.aeonplay.activity.MasterActivity;
import in.aeonplay.adapter.CampaignParticipantAdapter;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.InterstitialAds;
import in.aeonplay.comman.ProgressbarManager;
import in.aeonplay.model.Shots.Campaign.CampaignDataModel;
import in.aeonplay.model.Shots.Campaign.CampaignModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class CampaignParticipantsFragment extends BaseFragment {

    private String TAG = CampaignParticipantsFragment.class.getSimpleName();
    private Bundle bundle;
    private CampaignDataModel mCampaignDataModel;
    private RecyclerView rcvParticipantList;
    private SearchView mSearchView;
    private int REQUEST_VOICE = 13;
    private CampaignParticipantAdapter campaignParticipantAdapter;
    private LinearLayout errorLayout;
    private TextView txtTotalParticipants;
    private RelativeLayout rootContainer;
    private ProgressbarManager progressbarManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_campaign_participants, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Init(view);
        setData();
    }

    private void Init(View view) {
        progressbarManager = new ProgressbarManager(getActivity());
        rootContainer = (RelativeLayout) view.findViewById(R.id.rootContainer);
        errorLayout = (LinearLayout) view.findViewById(R.id.errorLayout);
        SearchManager mSearchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mSearchView = (SearchView) view.findViewById(R.id.searchView);

        mSearchView.setSearchableInfo(mSearchManager.getSearchableInfo(getActivity().getComponentName()));
        mSearchView.setMaxWidth(Integer.MAX_VALUE);
        mSearchView.setIconified(false);
        mSearchView.clearFocus();

        txtTotalParticipants = (TextView) view.findViewById(R.id.txtTotalParticipants);
        rcvParticipantList = (RecyclerView) view.findViewById(R.id.rcvParticipantList);

        rcvParticipantList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.recyclerview_item_divider));
        rcvParticipantList.addItemDecoration(dividerItemDecoration);
        rcvParticipantList.setItemAnimator(new DefaultItemAnimator());
    }

    private void setData() {
        //InterstitialAds.ShowInterstitialAds((MasterActivity) getActivity());

        bundle = getArguments();
        if (bundle != null) {
            mCampaignDataModel = bundle.getParcelable("DATA");

            progressbarManager.show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getAllParticipantList(String.valueOf(mCampaignDataModel.getId()));
                }
            }, Constants.INTERVAL);
        }

        mSearchView.setQueryRefinementEnabled(true);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (campaignParticipantAdapter != null) {
                    campaignParticipantAdapter.getFilter().filter(query);
                    hideKeyboard();
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (campaignParticipantAdapter != null) {
                    campaignParticipantAdapter.getFilter().filter(query);
                }
                return false;
            }
        });

        ImageView clearButton = (ImageView) mSearchView.findViewById(androidx.appcompat.R.id.search_close_btn);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchView.setQuery("", false);
                campaignParticipantAdapter.getFilter().filter("");
                mSearchView.clearFocus();
            }
        });

        ImageView voiceButton = (ImageView) mSearchView.findViewById(androidx.appcompat.R.id.search_voice_btn);
        voiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askSpeechInput();
            }
        });
    }

    private void getAllParticipantList(String campaignID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getParticipantList(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), campaignID);
        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                progressbarManager.dismiss();
                rootContainer.setVisibility(View.VISIBLE);

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response.toString());
                    final CampaignModel campaignModel = gson.fromJson(reader, CampaignModel.class);

                    txtTotalParticipants.setText("Total Participants: " + campaignModel.getData().getParticipants().size());
                    if (campaignModel.getData().getParticipants().size() > 0) {
                        errorLayout.setVisibility(View.GONE);
                        rcvParticipantList.setVisibility(View.VISIBLE);

                        campaignParticipantAdapter = new CampaignParticipantAdapter(activity, campaignModel.getData().getParticipants());
                        rcvParticipantList.setAdapter(campaignParticipantAdapter);
                        campaignParticipantAdapter.notifyDataSetChanged();

                    } else {
                        errorLayout.setVisibility(View.VISIBLE);
                        rcvParticipantList.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                progressbarManager.dismiss();
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                progressbarManager.dismiss();
                activity.showMessageToUser(error);
            }
        });
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        activity.removeFragment(fragment);
    }

    private void askSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Try saying something");
        try {
            startActivityForResult(intent, REQUEST_VOICE);
        } catch (ActivityNotFoundException a) {
            a.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_VOICE && resultCode == RESULT_OK) {
            String query = data.getStringExtra(SearchManager.QUERY);
            mSearchView.setQuery(query, true);
        }
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }
}
