package in.aeonplay.fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.Comparator;

import in.aeonplay.activity.MainActivity;


public abstract class BaseFragment extends Fragment {
    public MainActivity activity;

    protected void setActivity(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActivity((MainActivity) getActivity());
    }

    public abstract void onBack();

    public static <T> ArrayList<T> removeDuplicates(ArrayList<T> list) {
        ArrayList<T> newList = new ArrayList<T>();
        for (T element : list) {
            if (!newList.contains(element)) {
                newList.add(element);
            }
        }

        return newList;
    }

    public class BY_ORDER implements Comparator<String> {
        private int mod = 1;

        public BY_ORDER(boolean desc) {
            if (desc) mod = -1;
        }

        @Override
        public int compare(String arg0, String arg1) {
            return mod * arg0.compareTo(arg1);
        }
    }
}


