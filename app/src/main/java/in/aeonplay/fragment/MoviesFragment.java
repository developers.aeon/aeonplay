package in.aeonplay.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.activity.MasterActivity;
import in.aeonplay.adapter.PaginationCommanChildAdapter;
import in.aeonplay.adapter.PaginationScrollListener;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.InterstitialAds;
import in.aeonplay.comman.ProgressbarManager;
import in.aeonplay.comman.SpaceItemDecoration;
import in.aeonplay.model.Comman.CommanModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class MoviesFragment extends BaseFragment {

    private RecyclerView mMovieRecyclerView;
    private RelativeLayout rootContainer;
    private PaginationCommanChildAdapter commanChildAdapter;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private GridLayoutManager gridLayoutManager;
    private ProgressbarManager progressbarManager;
    private int currentPageNumber;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_comman, container, false);
    }

    public static MoviesFragment newInstance(int page) {
        MoviesFragment fragment = new MoviesFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.PAGE, page);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentPageNumber = getArguments().getInt(Constants.PAGE);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        commanChildAdapter = new PaginationCommanChildAdapter((MainActivity) getActivity());
        Init(view);
        setData();
    }

    private void setData() {
//        InterstitialAds.ShowInterstitialAds((MasterActivity) getActivity());

        int spanCount = getResources().getBoolean(R.bool.isTablet) ? 4 : 3;
        gridLayoutManager = new GridLayoutManager(getActivity(), spanCount, GridLayoutManager.VERTICAL, false);
        mMovieRecyclerView.setLayoutManager(gridLayoutManager);

        if (currentPageNumber == 0) {
            progressbarManager.show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getMovieListingProvider(currentPageNumber);
                }
            }, Constants.INTERVAL);

        } else if (currentPageNumber == 1) {
            progressbarManager.show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getMovieListing();
                }
            }, Constants.INTERVAL);

        } else {
            progressbarManager.show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getMovieListingProvider(currentPageNumber);
                }
            }, Constants.INTERVAL);
        }

    }

    private void Init(View view) {
        progressbarManager = new ProgressbarManager(getActivity());
        rootContainer = view.findViewById(R.id.rootContainer);

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) rootContainer.getLayoutParams();
        layoutParams.topMargin = activity.getToolbarHeight();
        rootContainer.setLayoutParams(layoutParams);

        mMovieRecyclerView = view.findViewById(R.id.commanList);
        mMovieRecyclerView.addItemDecoration(new SpaceItemDecoration());
        mMovieRecyclerView.setAdapter(commanChildAdapter);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        activity.removeFragment(fragment);
    }

    public void getMovieListing() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> loginCall = apiInterface.getCommanListIncludeDefault(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), Constants.CONTENT_TYPE_MOVIE, Constants.INCLUDE_DEFAULT_PROVIDER_DEFAULT, "1");

        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                progressbarManager.dismiss();
                rootContainer.setVisibility(View.VISIBLE);

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final CommanModel commanModel = gson.fromJson(reader, CommanModel.class);

                    if (commanModel.getData() != null) {
                        mMovieRecyclerView.addOnScrollListener(new PaginationScrollListener(1,
                                gridLayoutManager) {
                            @Override
                            protected void loadMoreItems() {
                                if (commanModel.getData().getNextPageUrl() != null) {
                                    isLoading = true;
                                    currentPage += 1;
                                    loadNextPage(currentPage + "");
                                }
                            }

                            @Override
                            public int getTotalPageCount() {
                                return TOTAL_PAGES;
                            }

                            @Override
                            public boolean isLastPage() {
                                return isLastPage;
                            }

                            @Override
                            public boolean isLoading() {
                                return isLoading;
                            }
                        });

                        commanChildAdapter.addAll(commanModel.getData().getData());
                        TOTAL_PAGES = getNextPageNumber(commanModel.getData().getNextPageUrl());

                        if (currentPage <= TOTAL_PAGES)
                            commanChildAdapter.addLoadingFooter();
                        else isLastPage = true;

                    } else {
                        activity.showMessageToUser("Data not found!");
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                progressbarManager.dismiss();
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                progressbarManager.dismiss();
                activity.showMessageToUser(error);
            }
        });
    }

    public void getMovieListingProvider(int position) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> loginCall = null;

        if (position == 0){
            loginCall = apiInterface.getCommanListProviderID(
                    SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                            SharePreferenceManager.getString(Constants.TOKEN_ACCESS), Constants.CONTENT_TYPE_MOVIE, Constants.CONTENT_PROVIDER_MOVIE_AEONPLAY, "1");

        } else {
            loginCall = apiInterface.getCommanListProviderID(
                    SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                            SharePreferenceManager.getString(Constants.TOKEN_ACCESS), Constants.CONTENT_TYPE_MOVIE, Constants.CONTENT_PROVIDER_MOVIE_SONYLIV, "1");
        }

        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                progressbarManager.dismiss();
                rootContainer.setVisibility(View.VISIBLE);

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final CommanModel commanModel = gson.fromJson(reader, CommanModel.class);

                    if (commanModel.getData() != null) {
                        mMovieRecyclerView.addOnScrollListener(new PaginationScrollListener(1,
                                gridLayoutManager) {
                            @Override
                            protected void loadMoreItems() {
                                if (commanModel.getData().getNextPageUrl() != null) {
                                    isLoading = true;
                                    currentPage += 1;
                                    loadNextPageProvider(currentPage + "", position);
                                }
                            }

                            @Override
                            public int getTotalPageCount() {
                                return TOTAL_PAGES;
                            }

                            @Override
                            public boolean isLastPage() {
                                return isLastPage;
                            }

                            @Override
                            public boolean isLoading() {
                                return isLoading;
                            }
                        });

                        commanChildAdapter.addAll(commanModel.getData().getData());
                        TOTAL_PAGES = getNextPageNumber(commanModel.getData().getNextPageUrl());

                        if (currentPage <= TOTAL_PAGES)
                            commanChildAdapter.addLoadingFooter();
                        else isLastPage = true;

                    } else {
                        activity.showMessageToUser("Data not found!");
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                progressbarManager.dismiss();
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                progressbarManager.dismiss();
                activity.showMessageToUser(error);
            }
        });
    }

    public int getNextPageNumber(String nextPage) {
        return nextPage == null ? 0 : Integer.parseInt(nextPage.split("=")[1]);
    }

    public void loadNextPage(String nextPage) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getCommanListIncludeDefault(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), Constants.CONTENT_TYPE_MOVIE, Constants.INCLUDE_DEFAULT_PROVIDER_DEFAULT, nextPage);
        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                Reader reader = new StringReader(response);
                CommanModel commanModel = gson.fromJson(reader, CommanModel.class);

                if (commanModel != null) {

                    if (commanModel.getData() != null) {

                        if (commanModel.getData().getData() != null) {
                            TOTAL_PAGES = getNextPageNumber(commanModel.getData().getNextPageUrl());

                            commanChildAdapter.removeLoadingFooter();
                            isLoading = false;

                            commanChildAdapter.addAll(commanModel.getData().getData());
                            if (currentPage <= TOTAL_PAGES) commanChildAdapter.addLoadingFooter();
                            else isLastPage = true;

                        }
                    }
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                activity.showMessageToUser(error);
            }
        });
    }

    public void loadNextPageProvider(String nextPage, int position) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<ResponseBody> loginCall = null;
        if (position == 0){
            loginCall = apiInterface.getCommanListProviderID(
                    SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                            SharePreferenceManager.getString(Constants.TOKEN_ACCESS), Constants.CONTENT_TYPE_MOVIE, Constants.CONTENT_PROVIDER_MOVIE_AEONPLAY, nextPage);

        } else {
            loginCall = apiInterface.getCommanListProviderID(
                    SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                            SharePreferenceManager.getString(Constants.TOKEN_ACCESS), Constants.CONTENT_TYPE_MOVIE, Constants.CONTENT_PROVIDER_MOVIE_SONYLIV, nextPage);
        }

        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                Reader reader = new StringReader(response);
                CommanModel commanModel = gson.fromJson(reader, CommanModel.class);

                if (commanModel != null) {

                    if (commanModel.getData() != null) {

                        if (commanModel.getData().getData() != null) {
                            TOTAL_PAGES = getNextPageNumber(commanModel.getData().getNextPageUrl());

                            commanChildAdapter.removeLoadingFooter();
                            isLoading = false;

                            commanChildAdapter.addAll(commanModel.getData().getData());
                            if (currentPage <= TOTAL_PAGES) commanChildAdapter.addLoadingFooter();
                            else isLastPage = true;

                        }
                    }
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                activity.showMessageToUser(error);
            }
        });
    }
}
