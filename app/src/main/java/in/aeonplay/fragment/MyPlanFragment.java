package in.aeonplay.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;

import in.aeonplay.R;
import in.aeonplay.activity.MasterActivity;
import in.aeonplay.adapter.HistoryAdapter;
import in.aeonplay.adapter.MyPlanAdapter;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.ProgressbarManager;
import in.aeonplay.comman.SpaceItemDecoration;
import in.aeonplay.model.SubscriptionModel.SubscriptionModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class MyPlanFragment extends BaseFragment {

    private ProgressbarManager progressbarManager;
    private RecyclerView mySubscriptionHistoryList, mMyPlanList;
    private RelativeLayout dataFound, dataNotFound;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_myplan, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Init(view);
        setData();
    }

    private void getSubscriptionList() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getSubscriptionHistory(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS));

        APIClient.callAPI((MasterActivity) getActivity(), loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                progressbarManager.dismiss();

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final SubscriptionModel subscriptionModel = gson.fromJson(reader, SubscriptionModel.class);

                    if (subscriptionModel.getData() != null) {

                        if (subscriptionModel.getData().getActiveSubscription().size() == 0 &&
                                subscriptionModel.getData().getSubscriptionHistory().size() == 0) {
                            dataFound.setVisibility(View.GONE);
                            dataNotFound.setVisibility(View.VISIBLE);

                        } else {
                            dataFound.setVisibility(View.VISIBLE);
                            dataNotFound.setVisibility(View.GONE);

                            // Check for active subscription ....
                            if (subscriptionModel.getData().getActiveSubscription() != null) {
                                MyPlanAdapter myPlanAdapter = new MyPlanAdapter(activity,
                                        subscriptionModel.getData().getActiveSubscription(), mMyPlanList);
                                mMyPlanList.setAdapter(myPlanAdapter);
                                myPlanAdapter.notifyDataSetChanged();
                            }

                            // Check for subscription history....
                            if (subscriptionModel.getData().getSubscriptionHistory() != null) {
                                HistoryAdapter historyAdapter = new HistoryAdapter(activity,
                                        subscriptionModel.getData().getSubscriptionHistory(), mySubscriptionHistoryList);
                                mySubscriptionHistoryList.setAdapter(historyAdapter);
                                historyAdapter.notifyDataSetChanged();
                            }
                        }

                    } else {
                        dataFound.setVisibility(View.GONE);
                        dataNotFound.setVisibility(View.VISIBLE);
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                dataFound.setVisibility(View.GONE);
                dataNotFound.setVisibility(View.VISIBLE);
                progressbarManager.dismiss();
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                dataFound.setVisibility(View.GONE);
                dataNotFound.setVisibility(View.VISIBLE);
                progressbarManager.dismiss();
                activity.showMessageToUser(error);
            }
        });
    }


    private void setData() {
        progressbarManager.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getSubscriptionList();
            }
        }, Constants.INTERVAL);
    }

    private void Init(View view) {
        progressbarManager = new ProgressbarManager(getActivity());

        dataFound = view.findViewById(R.id.dataFound);
        dataNotFound = view.findViewById(R.id.dataNotFound);

        mMyPlanList = view.findViewById(R.id.myPlanList);
        mMyPlanList.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        mMyPlanList.addItemDecoration(new SpaceItemDecoration());
        mMyPlanList.setNestedScrollingEnabled(false);

        mySubscriptionHistoryList = view.findViewById(R.id.mySubscriptionHistoryList);
        mySubscriptionHistoryList.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        mySubscriptionHistoryList.addItemDecoration(new SpaceItemDecoration());
        mySubscriptionHistoryList.setNestedScrollingEnabled(false);

//        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) rootContainer.getLayoutParams();
//        layoutParams.topMargin = activity.getStatusBarHeight() + activity.getToolbarHeight();
//        rootContainer.setLayoutParams(layoutParams);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        activity.removeFragment(fragment);
    }
}
