package in.aeonplay.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceScreen;
import androidx.preference.PreferenceViewHolder;
import androidx.preference.SwitchPreference;

import in.aeonplay.BuildConfig;
import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.OneShotClickListener;
import in.aeonplay.preferences.SharePreferenceManager;

public class SettingsFragment extends PreferenceFragmentCompat implements
        PreferenceFragmentCompat.OnPreferenceStartScreenCallback {

    private SwitchPreference keyTheme;
    private PreferenceScreen keyProfile, keyShot, keyTerms,
            keyPrivacy, keyAboutUs, keyCancellation, keyContactUs, keyDeactivate;
    private PreferenceCategory keySignout;
    private MainActivity mMainActivity = null;
    private SharePreferenceManager sharePreferenceManager = new SharePreferenceManager();

    public SettingsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        boolean selectedTheme = sharePreferenceManager.getBoolean("SelectedTheme");
        view.setBackgroundColor(selectedTheme == false ?
                getResources().getColor(R.color.colorPrimaryDark) :
                getResources().getColor(R.color.colorOffWhite));
        return view;
    }

    @Override
    public void onCreatePreferences(@Nullable Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.settings, rootKey);
        mMainActivity = (MainActivity) getActivity();
        InitView();
        setData();
    }

    private void setData() {
        showTheme();
        showProfile();
        showSignout();

        if (keyTerms != null &&
                keyPrivacy != null && keyAboutUs != null &&
                keyCancellation != null && keyContactUs != null && keyDeactivate != null) {

            keyTerms.setTitle("Terms of Use");
            keyPrivacy.setTitle("Privacy Policy");
            keyAboutUs.setTitle("About Us");
            keyCancellation.setTitle("Cancellation and Refund Policy");
            keyContactUs.setTitle("Contact Us");
            keyDeactivate.setTitle("Deactivate your account?");
        }

        mMainActivity.setOnBackListener(new MainActivity.OnBackListener() {
            @Override
            public void onBackPress() {

                Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                mMainActivity.removeFragment(fragment);
            }
        });
    }

    private void InitView() {
        keyProfile = findPreference(getString(R.string.keyProfiles));
        keyShot = findPreference(getString(R.string.keyShot));
        keyTheme = findPreference(getString(R.string.keyTheme));
        keyTerms = findPreference(getString(R.string.keyTerms));
        keyPrivacy = findPreference(getString(R.string.keyPrivacy));
        keyAboutUs = findPreference(getString(R.string.keyAboutUs));
        keyCancellation = findPreference(getString(R.string.keyCancellation));
        keyContactUs = findPreference(getString(R.string.keyContactUs));
        keyDeactivate = findPreference(getString(R.string.keyDeactivate));
        keySignout = findPreference(getString(R.string.keySignout));
        keyDeactivate = findPreference(getString(R.string.keyDeactivate));
    }

    @Override
    public boolean onPreferenceTreeClick(@NonNull Preference preference) {
        if (preference.getKey() != null) {
            if (preference.getKey().equalsIgnoreCase(getString(R.string.keyTerms))) {
                mMainActivity.addFragment(WebFragment.newInstance("https://aeongroup.in/terms"));

            } else if (preference.getKey().equalsIgnoreCase(getString(R.string.keyPrivacy))) {
                mMainActivity.addFragment(WebFragment.newInstance("https://aeongroup.in/privacy"));

            } else if (preference.getKey().equalsIgnoreCase(getString(R.string.keyAboutUs))) {
                mMainActivity.addFragment(WebFragment.newInstance("https://aeongroup.in/about"));

            } else if (preference.getKey().equalsIgnoreCase(getString(R.string.keyCancellation))) {
                mMainActivity.addFragment(WebFragment.newInstance("https://aeongroup.in/refund"));

            } else if (preference.getKey().equalsIgnoreCase(getString(R.string.keyContactUs))) {
                mMainActivity.addFragment(WebFragment.newInstance("https://aeongroup.in/contact"));

            } else if (preference.getKey().equalsIgnoreCase(getString(R.string.keyShot))) {
                Bundle bundle = new Bundle();
                bundle.putInt("USER_ID", SharePreferenceManager.getUserData().getId());

                ShotsDetailsFragment shotsDetailsFragment = new ShotsDetailsFragment();
                shotsDetailsFragment.setArguments(bundle);
                mMainActivity.addFragment(shotsDetailsFragment);

            } else if (preference.getKey().equalsIgnoreCase(getString(R.string.keyDeactivate))) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mMainActivity.showAccountStatusDialog(Constants.DEACTIVATE);
                    }
                }, 700);
            }
        }
        return super.onPreferenceTreeClick(preference);

    }

    @Override
    public boolean onPreferenceStartScreen(@NonNull PreferenceFragmentCompat caller, @NonNull PreferenceScreen pref) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        args.putString(PreferenceFragmentCompat.ARG_PREFERENCE_ROOT, pref.getKey());
        fragment.setArguments(args);
        ft.add(R.id.fragment_container, fragment, pref.getKey());
        ft.addToBackStack(pref.getKey());
        ft.commit();

        return true;
    }

    private void showTheme() {
        if (keyTheme != null) {
            keyTheme.setTitle("App Theme");
            keyTheme.setSummary(mMainActivity.isDarkTheme() ? "Dark" : "Default");
            keyTheme.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(@NonNull Preference preference, Object newValue) {
                    if (Boolean.valueOf(newValue.toString())) {
                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);

                    } else {
                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    }

                    SharePreferenceManager.save("SelectedTheme", Boolean.valueOf(newValue.toString()));
                    Intent intent = getActivity().getIntent();
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    getActivity().finish();
                    startActivity(intent);
                    return true;
                }
            });
        }
    }

    private void showProfile() {
        if (keyProfile != null) {
            keyProfile.setTitle("My Information");
            keyProfile.setSummary(Html.fromHtml("<font color='" + Color.GRAY + "'>" + SharePreferenceManager.getUserData().getFirstName() + " " +
                    SharePreferenceManager.getUserData().getLastName() + "</font>"));

            Preference preference0 = new Preference(keyProfile.getContext());
            preference0.setTitle(Html.fromHtml("<font color='" + Color.WHITE + "'>" + "User ID" + "</font>"));
            preference0.setSummary(Html.fromHtml("<font color='" + Color.GRAY + "'>" + SharePreferenceManager.getUserData().getId() + "</font>"));
            preference0.setIcon(R.drawable.ic_action_user);

            Preference preference1 = new Preference(keyProfile.getContext());
            preference1.setTitle(Html.fromHtml("<font color='" + Color.WHITE + "'>" + "Name" + "</font>"));
            preference1.setSummary(Html.fromHtml("<font color='" + Color.GRAY + "'>" + SharePreferenceManager.getUserData().getFirstName() + " " +
                    SharePreferenceManager.getUserData().getLastName() + "</font>"));
            preference1.setIcon(R.drawable.ic_action_user);

            Preference preference2 = new Preference(keyProfile.getContext());
            String email = SharePreferenceManager.getUserData().getEmail().equalsIgnoreCase("") ? "Not Found!" :
                    SharePreferenceManager.getUserData().getEmail();
            preference2.setTitle(Html.fromHtml("<font color='" + Color.WHITE + "'>" + "Email" + "</font>"));
            preference2.setSummary(Html.fromHtml("<font color='" + Color.GRAY + "'>" + email + "</font>"));
            preference2.setIcon(R.drawable.ic_action_user);

            Preference preference3 = new Preference(keyProfile.getContext());
            preference3.setTitle(Html.fromHtml("<font color='" + Color.WHITE + "'>" + "Mobile" + "</font>"));
            preference3.setSummary(Html.fromHtml("<font color='" + Color.GRAY + "'>" + SharePreferenceManager.getUserData().getMobileNo() + "</font>"));
            preference3.setIcon(R.drawable.ic_action_user);

            Preference preference4 = new Preference(keyProfile.getContext());
            preference4.setTitle(Html.fromHtml("<font color='" + Color.WHITE + "'>" + "Mobile Operator" + "</font>"));
            preference4.setSummary(Html.fromHtml("<font color='" + Color.GRAY + "'>" + SharePreferenceManager.getUserData().getMobileNetworkOperator() + "</font>"));
            preference4.setIcon(R.drawable.ic_action_user);

            keyProfile.addPreference(preference0);
            keyProfile.addPreference(preference1);
            keyProfile.addPreference(preference2);
            keyProfile.addPreference(preference3);
            keyProfile.addPreference(preference4);
        }
    }

    private void showSignout() {
        if (keySignout != null) {

            Preference preference = new Preference(getActivity()) {
                @Override
                public void onBindViewHolder(PreferenceViewHolder holder) {
                    super.onBindViewHolder(holder);

                    Button button = (Button) holder.findViewById(R.id.btnLogout);
                    button.setOnClickListener(new OneShotClickListener() {
                        @Override
                        public void onClicked(View v) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mMainActivity.selfLogout();
                                }
                            }, 700);
                        }
                    });

                    TextView textView = (TextView) holder.findViewById(R.id.app_version);
                    textView.setText("App Version: " + BuildConfig.VERSION_NAME);
                }
            };

            preference.setLayoutResource(R.layout.nav_drawer_coming);
            keySignout.addPreference(preference);
        }
    }
}