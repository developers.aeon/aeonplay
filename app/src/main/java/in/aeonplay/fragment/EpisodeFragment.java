package in.aeonplay.fragment;

import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.adapter.EpisodeAdapter;
import in.aeonplay.adapter.NavigationAdapter;
import in.aeonplay.adapter.PaginationScrollListener;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.ProgressbarManager;
import in.aeonplay.comman.SpaceItemDecoration;
import in.aeonplay.model.Comman.CommanDataList;
import in.aeonplay.model.Comman.CommanModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class EpisodeFragment extends BaseFragment {

    private String TAG = EpisodeFragment.class.getSimpleName();
    private RelativeLayout rootContainer;
    private RecyclerView mEpisodeList;
    private Bundle bundle;
    private String showID, showNAME, provider;
    private TextView content_name, content_program_title;
    private ArrayList<String> newSeriesList = new ArrayList<>();
    private ArrayList<String> updtSeriesList = new ArrayList<>();
    private List<CommanDataList> newDataList = new ArrayList<>();
    private TextView content_season_name;
    private LinearLayout linearSeason;
    private ProgressbarManager progressbarManager;
    private EpisodeAdapter episodeAdapter;
    private LinearLayoutManager linearLayoutManager;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int PAGE_START = 1;
    private int currentPage = PAGE_START;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_episode, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        episodeAdapter = new EpisodeAdapter((MainActivity) getActivity());
        setHasOptionsMenu(false);

        Init(v);
        SetData();
    }

    private void SetData() {
        bundle = getArguments();
        if (bundle != null) {
            showID = bundle.getString(Constants.SHOW_ID);
            showNAME = bundle.getString(Constants.SHOW_NAME);
            provider = bundle.getString(Constants.PROVIDER);

//            if (provider.equalsIgnoreCase("sonyliv")) {
            progressbarManager.show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getEpisodeListing(showID);
                }
            }, Constants.INTERVAL);

//            } else if (provider.equalsIgnoreCase("erosnow")) {
//                progressbarManager.show();
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        getEROSEpisodeListing(showID);
//                    }
//                }, Constants.INTERVAL);
//            }
        }

        content_season_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int activeSeasonNo = Integer.parseInt(Collections.max(newSeriesList));
                showPopup(content_season_name, activeSeasonNo);
            }
        });
    }

    public void getEpisodeListing(String showID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getEpisode(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), Constants.CONTENT_TYPE_EPISODE, showID, "1", Constants.ORDER_BY_EPISODE_NUMBER);

        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                progressbarManager.dismiss();
                rootContainer.setVisibility(View.VISIBLE);

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final CommanModel commanModel = gson.fromJson(reader, CommanModel.class);

                    if (commanModel.getData() != null) {

                        mEpisodeList.addOnScrollListener(new PaginationScrollListener(0,
                                linearLayoutManager) {
                            @Override
                            protected void loadMoreItems() {
                                if (commanModel.getData().getNextPageUrl() != null) {
                                    isLoading = true;
                                    currentPage += 1;
                                    loadNextPage(currentPage + "");
                                }
                            }

                            @Override
                            public int getTotalPageCount() {
                                return TOTAL_PAGES;
                            }

                            @Override
                            public boolean isLastPage() {
                                return isLastPage;
                            }

                            @Override
                            public boolean isLoading() {
                                return isLoading;
                            }
                        });

                        ArrayList<String> seriesList = new ArrayList<>();
                        for (CommanDataList commanDataModel : commanModel.getData().getData()) {
                            seriesList.add(commanDataModel.getSeasonNumber() + "");
                        }

                        newDataList = commanModel.getData().getData();
                        newSeriesList = removeDuplicates(seriesList);
                        Collections.sort(newSeriesList, Collections.reverseOrder());

                        if (newDataList.size() > 0) {
                            linearSeason.setVisibility(View.VISIBLE);
                            episodeList(commanModel.getData().getNextPageUrl(), newDataList, newSeriesList.get(0), true);

                        } else {
                            linearSeason.setVisibility(View.GONE);
                            activity.showMessageToUser("Episode not found!");
//                            activity.showErrorDialog(getActivity(),
//                                    loginCall.request().url().toString(),
//                                    showID);
                        }

                    } else {
//                        linearSeason.setVisibility(View.GONE);
                        activity.showMessageToUser("Data not found!");
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    Log.e("getEpisodeListing: ", exception.getMessage().toString());
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                progressbarManager.dismiss();
                Log.e("getEpisodeListing: ", error + " " + responseCode);
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                progressbarManager.dismiss();
                activity.showMessageToUser(error);
            }
        });
    }

    public void getEpisodeSeasonListing(String showID, String seasonNumber) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getEpisodeSeasonList(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), Constants.CONTENT_TYPE_EPISODE, showID, seasonNumber, "1", Constants.ORDER_BY_EPISODE_NUMBER);

        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                progressbarManager.dismiss();
                rootContainer.setVisibility(View.VISIBLE);

                episodeAdapter.clear();
                episodeAdapter.notifyDataSetChanged();

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final CommanModel commanModel = gson.fromJson(reader, CommanModel.class);

                    if (commanModel.getData() != null) {

                        mEpisodeList.addOnScrollListener(new PaginationScrollListener(0,
                                linearLayoutManager) {
                            @Override
                            protected void loadMoreItems() {
                                if (commanModel.getData().getNextPageUrl() != null) {
                                    isLoading = true;
                                    currentPage += 1;
                                    loadSeasonNextPage(currentPage + "", seasonNumber);
                                }
                            }

                            @Override
                            public int getTotalPageCount() {
                                return TOTAL_PAGES;
                            }

                            @Override
                            public boolean isLastPage() {
                                return isLastPage;
                            }

                            @Override
                            public boolean isLoading() {
                                return isLoading;
                            }
                        });

                        ArrayList<String> seriesList = new ArrayList<>();
                        for (CommanDataList commanDataModel : commanModel.getData().getData()) {
                            seriesList.add(commanDataModel.getSeasonNumber() + "");
                        }

                        List<CommanDataList> newDataList = commanModel.getData().getData();
                        ArrayList<String> newSeriesList = removeDuplicates(seriesList);
                        Collections.sort(newSeriesList, Collections.reverseOrder());

                        if (newDataList.size() > 0) {
//                            linearSeason.setVisibility(View.VISIBLE);
                            episodeList(commanModel.getData().getNextPageUrl(), newDataList, newSeriesList.get(0), true);

                        } else {
//                            linearSeason.setVisibility(View.GONE);
                            activity.showMessageToUser("Episode not found!");
//                            activity.showErrorDialog(getActivity(),
//                                    loginCall.request().url().toString(),
//                                    showID + " " +seasonNumber);
                        }

                    } else {
//                        linearSeason.setVisibility(View.GONE);
                        activity.showMessageToUser("Data not found!");
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                progressbarManager.dismiss();
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                progressbarManager.dismiss();
                activity.showMessageToUser(error);
            }
        });
    }

    public void getEROSEpisodeListing(String showID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getEROSEpisode(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), showID);

        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                rootContainer.setVisibility(View.VISIBLE);

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final CommanModel commanModel = gson.fromJson(reader, CommanModel.class);

                    if (commanModel.getData() != null) {

                        mEpisodeList.addOnScrollListener(new PaginationScrollListener(0,
                                linearLayoutManager) {
                            @Override
                            protected void loadMoreItems() {
                                if (commanModel.getData().getNextPageUrl() != null) {
                                    isLoading = true;
                                    currentPage += 1;
                                    loadNextPage(currentPage + "");
                                }
                            }

                            @Override
                            public int getTotalPageCount() {
                                return TOTAL_PAGES;
                            }

                            @Override
                            public boolean isLastPage() {
                                return isLastPage;
                            }

                            @Override
                            public boolean isLoading() {
                                return isLoading;
                            }
                        });

                        ArrayList<String> seriesList = new ArrayList<>();
                        for (CommanDataList commanDataModel : commanModel.getData().getData()) {
                            seriesList.add(commanDataModel.getSeasonNumber() + "");
                        }

                        newDataList = commanModel.getData().getData();
                        ArrayList<String> newSeriesList = removeDuplicates(seriesList);
                        Collections.sort(newSeriesList, Collections.reverseOrder());

                        if (newDataList.size() > 0) {
//                            linearSeason.setVisibility(View.VISIBLE);
                            episodeList(commanModel.getData().getNextPageUrl(), newDataList, newSeriesList.get(0), true);

                        } else {
//                            linearSeason.setVisibility(View.GONE);
                            activity.showMessageToUser("Episode not found!");
                            activity.showErrorDialog(getActivity(),
                                    loginCall.request().url().toString(),
                                    Constants.EROS_PARTNER_EPISODE + showID);
                        }

                    } else {
//                        linearSeason.setVisibility(View.GONE);
                        activity.showMessageToUser("Data not found!");
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                activity.showMessageToUser(error);
            }
        });
    }

    public int getNextPageNumber(String nextPage) {
        return nextPage == null ? 0 : Integer.parseInt(nextPage.split("=")[1]);
    }

    public void loadNextPage(String nextPage) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getEpisode(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), Constants.CONTENT_TYPE_EPISODE, showID, nextPage, Constants.ORDER_BY_EPISODE_NUMBER);
        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                Reader reader = new StringReader(response);
                CommanModel commanModel = gson.fromJson(reader, CommanModel.class);

                if (commanModel != null) {

                    if (commanModel.getData() != null) {

                        if (commanModel.getData().getData() != null) {

                            ArrayList<String> seriesList = new ArrayList<>();
                            for (CommanDataList commanDataModel : commanModel.getData().getData()) {
                                seriesList.add(commanDataModel.getSeasonNumber() + "");
                            }

                            newDataList = commanModel.getData().getData();
                            ArrayList<String> newSeriesList = removeDuplicates(seriesList);
                            Collections.sort(newSeriesList, Collections.reverseOrder());

                            episodeList(commanModel.getData().getNextPageUrl(), newDataList, newSeriesList.get(0), false);

                        }
                    }
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                activity.showMessageToUser(error);
            }
        });
    }

    public void loadSeasonNextPage(String nextPage, String seasonNumber) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getEpisodeSeasonList(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), Constants.CONTENT_TYPE_EPISODE, showID, seasonNumber, nextPage, Constants.ORDER_BY_EPISODE_NUMBER);
        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                Reader reader = new StringReader(response);
                CommanModel commanModel = gson.fromJson(reader, CommanModel.class);

                if (commanModel != null) {

                    if (commanModel.getData() != null) {

                        if (commanModel.getData().getData() != null) {

                            ArrayList<String> seriesList = new ArrayList<>();
                            for (CommanDataList commanDataModel : commanModel.getData().getData()) {
                                seriesList.add(commanDataModel.getSeasonNumber() + "");
                            }

                            newDataList = commanModel.getData().getData();
                            ArrayList<String> newSeriesList = removeDuplicates(seriesList);
                            Collections.sort(newSeriesList, Collections.reverseOrder());

                            episodeList(commanModel.getData().getNextPageUrl(), newDataList, newSeriesList.get(0), false);

                        }
                    }
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                activity.showMessageToUser(error);
            }
        });
    }

    private void episodeList(String nextPage, List<CommanDataList> list, String seasonNumber, boolean isInitial) {
        ArrayList<CommanDataList> dataList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            CommanDataList commanDataList = list.get(i);
            if (String.valueOf(commanDataList.getSeasonNumber()).contains(seasonNumber)) {
                dataList.add(commanDataList);
            }
        }

        StringBuilder stringBuilder = new StringBuilder();
        if (dataList.get(0).getGenre() != null) {
            for (String genre : dataList.get(0).getGenre()) {
                stringBuilder.append(genre).append(", ");
            }

            if (stringBuilder.length() > 0) {
                stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(","));
            }

            stringDecode(content_program_title, stringBuilder.toString());

        } else {
            stringDecode(content_program_title, "Genre not found!");
        }

        stringDecode(content_season_name, "Season: " + seasonNumber);
        stringDecode(content_name, showNAME);

        if (isInitial) {
//            Collections.sort(list, new BY_EPISODE_DT(true));
            episodeAdapter.addAll(list);
            TOTAL_PAGES = getNextPageNumber(nextPage);

            if (currentPage <= TOTAL_PAGES)
                episodeAdapter.addLoadingFooter();
            else isLastPage = true;

        } else {
            TOTAL_PAGES = getNextPageNumber(nextPage);

            episodeAdapter.removeLoadingFooter();
            isLoading = false;

//            Collections.sort(list, new BY_EPISODE_DT(true));
            episodeAdapter.addAll(list);
            if (currentPage <= TOTAL_PAGES) episodeAdapter.addLoadingFooter();
            else isLastPage = true;
        }
//        EpisodeAdapter episodeAdapter = new EpisodeAdapter(activity, dataList);
//        mEpisodeList.setAdapter(episodeAdapter);
//        episodeAdapter.notifyDataSetChanged();
    }

//    public class BY_EPISODE_DT implements Comparator<CommanDataList> {
//        private int mod = 1;
//
//        public BY_EPISODE_DT(boolean desc) {
//            if (desc) mod = -1;
//        }
//
//        @Override
//        public int compare(CommanDataList arg0, CommanDataList arg1) {
//            return mod * arg0.getOriginalAirDate().compareTo(arg1.getOriginalAirDate());
//        }
//    }

    public void updateNavigation(NavigationAdapter adapter, int position) {
        adapter.setSelectedPosition(position);
        adapter.notifyDataSetChanged();
    }

    public void stringDecode(TextView textView, String s) {
        try {
            textView.setText(URLDecoder.decode(s.replaceAll("%(?![0-9a-fA-F]{2})", "%25"), "utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void Init(View v) {
//        InterstitialAds.ShowInterstitialAds((MasterActivity) getActivity());

        progressbarManager = new ProgressbarManager(getActivity());
        rootContainer = v.findViewById(R.id.rootContainer);
        linearSeason = v.findViewById(R.id.linearSeason);
        mEpisodeList = v.findViewById(R.id.recycler_episode);

        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mEpisodeList.setLayoutManager(linearLayoutManager);

        mEpisodeList.setItemAnimator(new DefaultItemAnimator());
        mEpisodeList.setAdapter(episodeAdapter);

        content_name = v.findViewById(R.id.content_name);
        content_program_title = v.findViewById(R.id.content_program_title);
        content_season_name = v.findViewById(R.id.content_season_name);
    }

    public void showPopup(View view, int activeSeasonNumber) {
        View popupView = getLayoutInflater().inflate(R.layout.pop_layout, null);
        PopupWindow popupWindow = new PopupWindow(popupView,
                WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);

        RecyclerView mNavigationList = (RecyclerView) popupView.findViewById(R.id.nav_list);
        mNavigationList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mNavigationList.addItemDecoration(new SpaceItemDecoration());
        mNavigationList.setItemAnimator(new DefaultItemAnimator());

        updtSeriesList = new ArrayList<>();
        for (int count = activeSeasonNumber; count >= 1; count--) {
            updtSeriesList.add("Season: " + count);
        }

        NavigationAdapter mNavigationAdapter = new NavigationAdapter(activity, updtSeriesList);
        mNavigationList.setAdapter(mNavigationAdapter);
        mNavigationAdapter.notifyDataSetChanged();

        mNavigationAdapter.setOnItemClickListener(new NavigationAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                content_season_name.setText(updtSeriesList.get(position));
                updateNavigation(mNavigationAdapter, position);
                popupWindow.dismiss();

                if (provider.equalsIgnoreCase("sonyliv")) {
                    progressbarManager.show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            getEpisodeSeasonListing(showID, updtSeriesList.get(position).replaceAll("Season: ", ""));
                        }
                    }, Constants.INTERVAL);

                }
            }
        });

        ImageView mUpImageView = (ImageView) popupView.findViewById(R.id.arrow_up);
        ImageView mDownImageView = (ImageView) popupView.findViewById(R.id.arrow_down);

        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new ColorDrawable());
        int[] location = new int[2];
        view.getLocationOnScreen(location);

        Rect anchorRect = new Rect(location[0], location[1], location[0]
                + view.getWidth(), location[1] + view.getHeight());

        popupView.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        int rootHeight = popupView.getMeasuredHeight();
        int rootWidth = popupView.getMeasuredWidth();
        final int screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
        final int screenHeight = Resources.getSystem().getDisplayMetrics().heightPixels;

        int yPos = anchorRect.top - rootHeight;

        boolean onTop = true;

        if (anchorRect.top < screenHeight / 2) {
            yPos = anchorRect.bottom;
            onTop = false;
        }

        int whichArrow, requestedX;

        whichArrow = ((onTop) ? R.id.arrow_down : R.id.arrow_up);
        requestedX = anchorRect.centerX();

        View arrow = whichArrow == R.id.arrow_up ? mUpImageView
                : mDownImageView;
        View hideArrow = whichArrow == R.id.arrow_up ? mDownImageView
                : mUpImageView;

        final int arrowWidth = arrow.getMeasuredWidth();

        arrow.setVisibility(View.VISIBLE);

        ViewGroup.MarginLayoutParams param = (ViewGroup.MarginLayoutParams) arrow
                .getLayoutParams();

        hideArrow.setVisibility(View.INVISIBLE);

        int xPos = 0;

        // ETXTREME RIGHT CLICKED
        if (anchorRect.left + rootWidth > screenWidth) {
            xPos = (screenWidth - rootWidth);
        }
        // ETXTREME LEFT CLICKED
        else if (anchorRect.left - (rootWidth / 2) < 0) {
            xPos = anchorRect.left;
        }
        // IN BETWEEN
        else {
            xPos = (anchorRect.centerX() - (rootWidth / 2));
        }

        param.leftMargin = (requestedX - xPos) - (arrowWidth / 2);
        popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, xPos, yPos);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        activity.removeFragment(fragment);
    }
}