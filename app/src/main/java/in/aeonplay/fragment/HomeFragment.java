package in.aeonplay.fragment;

import static in.aeonplay.activity.MasterActivity.mComingSoonList;
import static in.aeonplay.activity.MasterActivity.mLatestList;
import static in.aeonplay.activity.MasterActivity.mTop10List;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.reflect.TypeToken;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import in.aeonplay.BuildConfig;
import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.adapter.HomeAdapter;
import in.aeonplay.adapter.PaginationScrollListener;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.ProgressbarManager;
import in.aeonplay.model.Comman.CommanDataList;
import in.aeonplay.model.Comman.CommanHeader;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class HomeFragment extends BaseFragment {

    private HomeAdapter homeAdapter;
    public static RecyclerView mHomeListing;
    private ProgressbarManager progressbarManager;
    private RelativeLayout rootContainer;
    private boolean doubleBackToExitPressedOnce;
    private LinearLayoutManager mLinearLayoutManager;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private String PAGE_URL = BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/dashboard";
//    private ArrayList<CommanHeader> headerAdapter = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        homeAdapter = new HomeAdapter((MainActivity) getActivity());
        Init(view);
        setData();
    }

    private void setData() {
        activity.captureFirebaseEvent(FirebaseAnalytics.Event.APP_OPEN);
        mLinearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        mHomeListing.setLayoutManager(mLinearLayoutManager);

        progressbarManager.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getDashboardList();
            }
        }, Constants.INTERVAL);
    }

    private void Init(View view) {
        progressbarManager = new ProgressbarManager(getActivity());
        mHomeListing = view.findViewById(R.id.homeListing);
        mHomeListing.setItemAnimator(new DefaultItemAnimator());
        mHomeListing.setAdapter(homeAdapter);
        mHomeListing.setNestedScrollingEnabled(false);

        rootContainer = view.findViewById(R.id.rootContainer);
    }

    @Override
    public void onBack() {
        if (activity.mDrawerLayout.isDrawerOpen(GravityCompat.START))
            activity.mDrawerLayout.closeDrawers();
        else {
            if (doubleBackToExitPressedOnce) {
                activity.finish();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            activity.showMessageToUser(activity.getResources().getString(R.string.press_again));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }


    public void getDashboardList() {
        homeAdapter.getMovies().clear();
        homeAdapter.notifyDataSetChanged();

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getDashboardList(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), "1", "1", "1");

        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                progressbarManager.dismiss();
                rootContainer.setVisibility(View.VISIBLE);

                try {
                    JSONObject responseDataObj = new JSONObject(response);
                    JSONObject responseObj = responseDataObj.getJSONObject("data");

                    LinkedHashMap<String, String> yourHashMap = new Gson().fromJson(responseObj.toString(), LinkedHashMap.class);
                    ArrayList<CommanHeader> headerAdapter = new ArrayList<>();

                    int i = 0;
                    for (String currentDynamicKey : yourHashMap.keySet()) {
                        Gson gson = new Gson();
                        Reader reader = new StringReader(responseObj.getJSONArray(currentDynamicKey).toString());
                        Type listType = new TypeToken<ArrayList<CommanDataList>>() {
                        }.getType();
                        ArrayList<CommanDataList> list = gson.fromJson(reader, listType);

                        if (list.size() > 0) {
                            if (currentDynamicKey.equalsIgnoreCase("coming_soon")) {
                                mComingSoonList = new ArrayList<>(list);
                                list.remove(list.get(i++));
                                continue;
                            }

                            if (currentDynamicKey.equalsIgnoreCase("latest")) {
                                mLatestList = new ArrayList<>(list);
                                list.remove(list.get(i++));
                                continue;
                            }

                            if (currentDynamicKey.equalsIgnoreCase("top_ten")) {
                                mTop10List = new ArrayList<>(list);
                                list.remove(list.get(i++));
                                continue;
                            }

                            headerAdapter.add(new CommanHeader(currentDynamicKey, list));
                        }

                        Log.d("MAYANK: ", currentDynamicKey + " " + list.size());
                    }

                    Log.d("MAYANK1: ", headerAdapter.size() + "");

                    if (headerAdapter != null) {

                        // For Header ...
                        headerAdapter.add(0, new CommanHeader(false, true, false, false, false, false, false, false, false));

                        // For Watchlist ...
                        headerAdapter.add(1, new CommanHeader(false, false, false, true, false, false, false, false, false));

                        // For Video Songs
                        headerAdapter.add(3, new CommanHeader(false, false, false, false, false, false, false, false, true));

                        // For Shot ...
                        headerAdapter.add(4, new CommanHeader(false, false, false, false, false, false, false, true, false));

                        // For News ...
                        headerAdapter.add(5, new CommanHeader(false, false, true, false, false, false, false, false, false));

                        // For Top10
                        headerAdapter.add(7, new CommanHeader(false, false, false, false, true, false, false, false, false));

                        // For Coming ...
                        headerAdapter.add(8, new CommanHeader(false, false, false, false, false, false, true, false, false));

                        // For Advertisement ...
                        headerAdapter.add(9, new CommanHeader(true, false, false, false, false, false, false, false, false));

                        // For Campaign ...
//                        if (activity.mCampaignList.size() > 0) {
//                            headerAdapter.add(2, new CommanHeader(false, false, false, false, false, true, false));
//                        }

                        mHomeListing.addOnScrollListener(new PaginationScrollListener(0,
                                mLinearLayoutManager) {
                            @Override
                            protected void loadMoreItems() {
                                if (currentPage != 0) {
                                    isLoading = true;
                                    currentPage += 1;

                                    loadNextPage(PAGE_URL + "?page=" + currentPage);
                                }
                            }

                            @Override
                            public int getTotalPageCount() {
                                return TOTAL_PAGES;
                            }

                            @Override
                            public boolean isLastPage() {
                                return isLastPage;
                            }

                            @Override
                            public boolean isLoading() {
                                return isLoading;
                            }
                        });

                        homeAdapter.addAll(headerAdapter);
                        TOTAL_PAGES = currentPage;

                        if (currentPage <= TOTAL_PAGES)
                            homeAdapter.addLoadingFooter();
                        else isLastPage = true;

                    } else {
                        progressbarManager.dismiss();
                        activity.showMessageToUser("Data not found!");
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    progressbarManager.dismiss();
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                activity.showMessageToUser(error);
            }
        });
    }

    public void loadNextPage(String nextPage) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getCommanPagination(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), nextPage);
        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    if (!response.equalsIgnoreCase("{\"data\":[]}")) {
                        JSONObject responseDataObj = new JSONObject(response);
                        JSONObject responseObj = responseDataObj.getJSONObject("data");

                        LinkedHashMap<String, String> yourHashMap = new Gson().fromJson(responseObj.toString(), LinkedHashMap.class);
                        List<CommanHeader> headerAdapter = new ArrayList<>();
                        for (String currentDynamicKey : yourHashMap.keySet()) {
                            Gson gson = new Gson();
                            Reader reader = new StringReader(responseObj.getJSONArray(currentDynamicKey).toString());
                            Type listType = new TypeToken<ArrayList<CommanDataList>>() {
                            }.getType();
                            ArrayList<CommanDataList> list = gson.fromJson(reader, listType);

                            if (list.size() > 0) {
                                headerAdapter.add(new CommanHeader(currentDynamicKey, list));
                            }
                        }

                        TOTAL_PAGES = currentPage + 1;

                        homeAdapter.removeLoadingFooter();
                        isLoading = false;

//                        int randomPosition = new Random().nextInt(headerAdapter.size());
//
//                        // For News ...
//                        if (currentPage == 2) {
//                            headerAdapter.add(randomPosition, new CommanHeader(false, false, true, false, false, false, false));
//                        }
//
//                        // For Top10 ...
//                        if (currentPage == 3) {
//                            headerAdapter.add(randomPosition, new CommanHeader(false, false, false, false, true, false, false));
//                        }

                        homeAdapter.addAll(headerAdapter);

                        if (currentPage <= TOTAL_PAGES) homeAdapter.addLoadingFooter();
                        else isLastPage = true;

                    } else {
                        currentPage = 0;
                        homeAdapter.removeLoadingFooter();
                        isLoading = false;
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                activity.showMessageToUser(error);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.action_main, menu);
    }

    public HomeAdapter getHomeAdapter() {
        return homeAdapter;
    }
}
