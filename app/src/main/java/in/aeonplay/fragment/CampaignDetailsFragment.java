package in.aeonplay.fragment;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;

import java.io.Reader;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import in.aeonplay.R;
import in.aeonplay.activity.MasterActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.InterstitialAds;
import in.aeonplay.comman.ProgressbarManager;
import in.aeonplay.model.Shots.Campaign.CampaignDataModel;
import in.aeonplay.model.Shots.Campaign.CampaignModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class CampaignDetailsFragment extends BaseFragment {

    private String TAG = CampaignDetailsFragment.class.getSimpleName();
    private Bundle bundle;
    private CampaignDataModel mCampaignDataModel;
    private TextView txtCampaignName, txtCampaignDuration, txtCampaignRegistration, txtCampaignVote, txtCampaignDescription,
            txtAwardAnnouncement;
    private Button btnJoinParticipants;
    private ImageView imgLogo;
    private ProgressbarManager progressbarManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_campaign_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Init(view);
        setData();
    }

    private void Init(View view) {
        //InterstitialAds.ShowInterstitialAds((MasterActivity) getActivity());

        progressbarManager = new ProgressbarManager(getActivity());
        btnJoinParticipants = view.findViewById(R.id.btnJoinParticipants);

        txtCampaignName = view.findViewById(R.id.txtCampaignName);
        txtCampaignDuration = view.findViewById(R.id.txtCampaignDuration);
        txtCampaignRegistration = view.findViewById(R.id.txtCampaignRegistration);
        txtCampaignVote = view.findViewById(R.id.txtCampaignVote);
        txtCampaignDescription = view.findViewById(R.id.txtCampaignDescription);
        txtAwardAnnouncement = view.findViewById(R.id.txtAwardAnnouncement);

        imgLogo = view.findViewById(R.id.imgLogo);
        if (activity.isDarkTheme()) {
            imgLogo.setImageResource(R.drawable.ic_action_shotstar_light);

        } else {
            imgLogo.setImageResource(R.drawable.ic_action_shotstar_dark);
        }
    }

    private void setData() {
        InterstitialAds.ShowInterstitialAds((MasterActivity) getActivity());

        bundle = getArguments();
        if (bundle != null) {
            mCampaignDataModel = bundle.getParcelable("DATA");

            txtCampaignName.setText(mCampaignDataModel.getTitle());

            String textDuration = "<font color=" + getResources().getColor(R.color.colorPureWhite) + "> " + "From" + "</font> " +
                    "<b> " + changeDateFormat(mCampaignDataModel.getStarting()) + "</b> To " +
                    "<b> " + changeDateFormat(mCampaignDataModel.getClosing()) + "</b>";
            txtCampaignDuration.setText(Html.fromHtml(textDuration));

            String textReg = "<font color=" + getResources().getColor(R.color.colorPureWhite) + "> " + "From" + "</font> " +
                    "<b> " + changeDateFormat(mCampaignDataModel.getRegistrationOpening()) + "</b> To " +
                    "<b> " + changeDateFormat(mCampaignDataModel.getRegistrationClosing()) + "</b>";
            txtCampaignRegistration.setText(Html.fromHtml(textReg));

            String textVote = "<font color=" + getResources().getColor(R.color.colorPureWhite) + "> " + "From" + "</font> " +
                    "<b> " + changeDateFormat(mCampaignDataModel.getVoteOpening()) + "</b> To " +
                    "<b> " + changeDateFormat(mCampaignDataModel.getVoteClosing()) + "</b>";
            txtCampaignVote.setText(Html.fromHtml(textVote));

            txtCampaignDescription.setText(mCampaignDataModel.getDescription());
            txtAwardAnnouncement.setText(changeDateFormat(mCampaignDataModel.getAwardAnnouncement()));

            btnJoinParticipants.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    postParticipantRegistration(String.valueOf(mCampaignDataModel.getId()));
                }
            });
        }
    }

    private void postParticipantRegistration(String campaignID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.postParticipantRegistration(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), campaignID);
        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response.toString());
                    final CampaignModel campaignModel = gson.fromJson(reader, CampaignModel.class);

                    if (campaignModel.getData() != null) {
                        activity.showMessageToUser("Registration Successfully!");
                        onBack();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                activity.showMessageToUser(error);
            }
        });
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        activity.removeFragment(fragment);
    }

    public String changeDateFormat(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd MMM, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
