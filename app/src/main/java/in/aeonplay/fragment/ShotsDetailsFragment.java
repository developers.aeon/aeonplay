package in.aeonplay.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.activity.MasterActivity;
import in.aeonplay.adapter.PaginationScrollListener;
import in.aeonplay.adapter.ShotsDetailsAdapter;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.InterstitialAds;
import in.aeonplay.comman.ProgressbarManager;
import in.aeonplay.comman.SpaceItemDecoration;
import in.aeonplay.model.Shots.ShotModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class ShotsDetailsFragment extends BaseFragment {

    private RecyclerView mRecyclerView;
    private ShotsDetailsAdapter shotsDetailsAdapter;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private LinearLayoutManager linearLayoutManager;
    public static int mUserID;
    private Bundle bundle;
    private RelativeLayout rootContainer;
    private ProgressbarManager progressbarManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_shots_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        shotsDetailsAdapter = new ShotsDetailsAdapter((MainActivity) getActivity());
        Init(view);
        setData();
    }

    private void setData() {
        bundle = getArguments();
        if (bundle != null) {
            mUserID = bundle.getInt(Constants.USER_ID);

            progressbarManager.show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getShortListing(mUserID + "");
                }
            }, Constants.INTERVAL);
        }
    }

    private void Init(View view) {
        progressbarManager = new ProgressbarManager(getActivity());
        rootContainer = view.findViewById(R.id.rootContainer);
        mRecyclerView = view.findViewById(R.id.commanList);

        int spanCount = getResources().getBoolean(R.bool.isTablet) ? 4 : 3;
        linearLayoutManager = new LinearLayoutManager(getActivity(), GridLayoutManager.VERTICAL, false);
//        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
//            @Override
//            public int getSpanSize(int position) {
//                if (position == 0)
//                    return spanCount;
//
//                return 1;
//            }
//        });

        mRecyclerView.setLayoutManager(linearLayoutManager);

        mRecyclerView.addItemDecoration(new SpaceItemDecoration());
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setAdapter(shotsDetailsAdapter);
    }

    @Override
    public void onBack() {
        if (getActivity().getSupportFragmentManager().getBackStackEntryCount() == 1) {
            activity.addFragment(new HomeFragment());

        } else {
            Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            activity.removeFragment(fragment);
        }
    }

    public void getShortListing(String userID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getShotsDetail(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), userID, "1");

        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                progressbarManager.dismiss();
                rootContainer.setVisibility(View.VISIBLE);

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final ShotModel shotsModel = gson.fromJson(reader, ShotModel.class);

                    if (shotsModel.getData() != null) {

                        mRecyclerView.addOnScrollListener(new PaginationScrollListener(0,
                                linearLayoutManager) {
                            @Override
                            protected void loadMoreItems() {
                                if (shotsModel.getData().getNextPageUrl() != null) {
                                    isLoading = true;
                                    currentPage += 1;
                                    loadNextPage(currentPage + "", userID + "");
                                }
                            }

                            @Override
                            public int getTotalPageCount() {
                                return TOTAL_PAGES;
                            }

                            @Override
                            public boolean isLastPage() {
                                return isLastPage;
                            }

                            @Override
                            public boolean isLoading() {
                                return isLoading;
                            }
                        });

                        shotsDetailsAdapter.addAll(shotsModel.getData().getData());
                        TOTAL_PAGES = getNextPageNumber(shotsModel.getData().getNextPageUrl());

                        if (currentPage <= TOTAL_PAGES)
                            shotsDetailsAdapter.addLoadingFooter();
                        else isLastPage = true;

                    } else {
                        activity.showMessageToUser("Data not found!");
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                progressbarManager.dismiss();
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                progressbarManager.dismiss();
                activity.showMessageToUser(error);
            }
        });
    }

    public int getNextPageNumber(String nextPage) {
        return nextPage == null ? 0 : Integer.parseInt(nextPage.split("=")[1]);
    }

    public void loadNextPage(String nextPage, String userID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getShotsDetail(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), userID, nextPage);
        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                Reader reader = new StringReader(response);
                ShotModel shotsModel = gson.fromJson(reader, ShotModel.class);

                if (shotsModel != null) {

                    if (shotsModel.getData() != null) {

                        if (shotsModel.getData().getData() != null) {
                            TOTAL_PAGES = getNextPageNumber(shotsModel.getData().getNextPageUrl());

                            shotsDetailsAdapter.removeLoadingFooter();
                            isLoading = false;

                            shotsDetailsAdapter.addAll(shotsModel.getData().getData());
                            if (currentPage <= TOTAL_PAGES) shotsDetailsAdapter.addLoadingFooter();
                            else isLastPage = true;

                        }
                    }
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                activity.showMessageToUser(error);
            }
        });
    }


}
