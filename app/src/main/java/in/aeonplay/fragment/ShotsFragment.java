package in.aeonplay.fragment;

import static in.aeonplay.activity.MasterActivity.mShotDataModel;
import static in.aeonplay.activity.MasterActivity.mShotList;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.media3.ui.PlayerView;
import androidx.viewpager2.widget.ViewPager2;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.activity.MasterActivity;
import in.aeonplay.adapter.ShotsAdapter;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.InterstitialAds;
import in.aeonplay.comman.ProgressbarManager;
import in.aeonplay.model.Shots.ShotModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import in.aeonplay.shots.VideoPlayManager;
import in.aeonplay.shots.VideoPlayTask;
import okhttp3.ResponseBody;
import retrofit2.Call;


public class ShotsFragment extends BaseFragment {

    private ViewPager2 mViewPager2;
    private Handler handler;
    private Runnable runnable;
    private LinearLayout linearAdd;
    private ShotsAdapter mShotsAdapter;
    private int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private ProgressbarManager progressbarManager;
    private Bundle bundle;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_shots, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        Init(v);
        setData();
    }

    private void setData() {
        bundle = getArguments();
        if (bundle != null) {
            int selectedIndex = bundle.getInt(Constants.INDEX);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mShotList != null) {
                        if (mShotsAdapter != null) {
                            mShotsAdapter.setData(mShotDataModel);
                            mShotsAdapter.setDataList(mShotList);
                            mViewPager2.setCurrentItem(selectedIndex, false);
                        }
                    }
                }
            }, Constants.INTERVAL);

        } else {
            progressbarManager.show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getShortListing(String.valueOf(currentPage));
                }
            }, Constants.INTERVAL);

        }

        mShotsAdapter = new ShotsAdapter((MainActivity) getActivity());
        mViewPager2.setOrientation(ViewPager2.ORIENTATION_VERTICAL);
        mViewPager2.setAdapter(mShotsAdapter);

        mViewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);

                if (mShotsAdapter.isPositionMultipleOfFive(position)) {
                    if (mShotsAdapter.getData().getNextPageUrl() != null) {
                        currentPage += 1;
                        getShortListing(String.valueOf(currentPage));
                    }
                }

                View itemView = mViewPager2.findViewWithTag(position);
                if (itemView != null) {
                    PlayerView simpleExoPlayerView = itemView.findViewById(R.id.player_view_story);
                    ImageView visualizer = itemView.findViewById(R.id.visualizer);

                    SeekBar seekBar = itemView.findViewById(R.id.seekbarProgress);
                    VideoPlayManager.getInstance(getActivity()).setCurVideoPlayTask(new VideoPlayTask(simpleExoPlayerView,
                            mShotsAdapter.getUrlByPos(position)));

                    VideoPlayManager.getInstance(getActivity()).startPlay();

                    handler = new Handler();
                    runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (simpleExoPlayerView.getPlayer() != null) {
                                seekBar.setProgress((int) ((simpleExoPlayerView.getPlayer().getCurrentPosition() * 100) /
                                        simpleExoPlayerView.getPlayer().getDuration()));
                                handler.postDelayed(runnable, 1000);
                            } else {
                                handler.removeCallbacksAndMessages(null);
                            }
                        }
                    };
                    handler.postDelayed(runnable, 0);
                }
            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);


            }
        });

        linearAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.addFragment(new VideoUploadFragment());
            }
        });
    }

    private void Init(View rootView) {
        progressbarManager = new ProgressbarManager(getActivity());
        mViewPager2 = rootView.findViewById(R.id.viewPager2);
        linearAdd = rootView.findViewById(R.id.linearAdd);
    }

    public void getShortListing(String page) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getShots(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), page);

        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                progressbarManager.dismiss();

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final ShotModel shotsModel = gson.fromJson(reader, ShotModel.class);

                    if (shotsModel.getData() != null) {
                        mShotsAdapter.setData(shotsModel.getData());
                        mShotsAdapter.setDataList(shotsModel.getData().getData());

                    } else {
                        activity.showMessageToUser("Data not found!");
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                progressbarManager.dismiss();
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                progressbarManager.dismiss();
                activity.showMessageToUser(error);
            }
        });
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        activity.removeFragment(fragment);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            VideoPlayManager.getInstance(getActivity()).resumePlay();
        } else {
            VideoPlayManager.getInstance(getActivity()).pausePlay();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        VideoPlayManager.getInstance(getActivity()).pausePlay();
    }

    @Override
    public void onStop() {
        super.onStop();
        VideoPlayManager.getInstance(getActivity()).pausePlay();
    }
}