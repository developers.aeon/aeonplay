package in.aeonplay.fragment;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;

import in.aeonplay.R;
import in.aeonplay.activity.MasterActivity;
import in.aeonplay.adapter.HomeAdapter;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.NativeAds;
import in.aeonplay.model.Comman.CommanDataList;
import in.aeonplay.model.WatchlistModel.WatchlistModel;
import in.aeonplay.model.WatchlistModel.WatchlistModelList;
import in.aeonplay.model.WatchlistModel.WatchlistModelListDatumModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class DetailFragment extends BaseFragment {

    private Bundle bundle;
    private String TAG = DetailFragment.class.getSimpleName();
    private ImageView moviePoster, providerImage, imgExpand, imgPlay, imgMyList;
    private TextView content_name, content_other,
            content_cast, content_director, content_language;
    private TextView content_info;
    private LinearLayout linearEpisode, linearMyList, linearShare, linearTrailer;
    private boolean isExpanded = true;
    private FrameLayout adContainer;
    private CommanDataList commanDataList;
    private String youtubeURL;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_movie_detail, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        bundle = getArguments();
        if (bundle != null) {
            commanDataList = bundle.getParcelable("DATA");
        }

        Init(v);
        setData();
    }

    private void setData() {
        if (commanDataList.getProvider().equalsIgnoreCase("aeonplay")) {
            linearTrailer.setVisibility(View.GONE);

        } else if (commanDataList.getProvider().equalsIgnoreCase("erosnow")) {
            linearTrailer.setVisibility(View.GONE);
            imgPlay.setVisibility(View.VISIBLE);

            if (commanDataList.getContentType().equalsIgnoreCase("original")) {
                imgPlay.setVisibility(View.GONE);
            }

        } else {
            linearTrailer.setVisibility(View.GONE);
            imgPlay.setVisibility(View.VISIBLE);
        }

        activity.loadImage(commanDataList, Constants.LANDSCAPE, moviePoster);

        providerImage.setVisibility(View.GONE);
        if (commanDataList.getProvider().equalsIgnoreCase("sonyliv")) {
            providerImage.setImageResource(R.drawable.ic_action_crown);
            providerImage.setVisibility(View.VISIBLE);
        }

        stringDecode(content_info, commanDataList.getDiscription());
        if (commanDataList.getLongDescription() != null)
            stringDecode(content_info, commanDataList.getLongDescription());

        stringDecode(content_name, commanDataList.getTitle());
        stringDecode(content_language, "Language: " + commanDataList.getLanguage());

        if (commanDataList.getCastAndCrew() != null) {
            if (commanDataList.getCastAndCrew().contains("|")) {
                String castCrew[] = commanDataList.getCastAndCrew().split("\\|");
                stringDecode(content_cast, castCrew[0].trim().replaceAll("|", ""));
                stringDecode(content_director, castCrew[1].trim());
                content_director.setVisibility(View.VISIBLE);

            } else {
                content_director.setVisibility(View.GONE);
                stringDecode(content_cast, commanDataList.getCastAndCrew());
            }

        } else {
            content_cast.setVisibility(View.GONE);
            content_director.setVisibility(View.GONE);
        }

        linearEpisode.setVisibility(View.GONE);
        if (commanDataList.getContentType().equalsIgnoreCase("show") ||
                commanDataList.getContentType().equalsIgnoreCase("episode") ||
                commanDataList.getContentType().equalsIgnoreCase("original"))
            linearEpisode.setVisibility(View.VISIBLE);

        StringBuilder stringBuilder = new StringBuilder();
        if (commanDataList.getGenre() != null) {
            for (String genre : commanDataList.getGenre()) {
                stringBuilder.append(genre).append(", ");
            }

            if (stringBuilder.length() > 0) {
                stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(","));
            }
        }

        String releaseYear = "";
        if (!commanDataList.getReleaseYear().equalsIgnoreCase("")) {
            releaseYear = " | " + commanDataList.getReleaseYear();
        }

        content_other.setText(stringBuilder.toString() + releaseYear);

        configureExpandView();
        imgPlay.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                if (commanDataList.getContentType().equalsIgnoreCase("show") ||
                        commanDataList.getContentType().equalsIgnoreCase("episode") ||
                        commanDataList.getContentType().equalsIgnoreCase("original")) {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.SHOW_NAME, commanDataList.getTitle());
                    bundle.putString(Constants.SHOW_ID, commanDataList.getShowId());
                    bundle.putString(Constants.PROVIDER, commanDataList.getProvider());
                    EpisodeFragment episodeFragment = new EpisodeFragment();
                    episodeFragment.setArguments(bundle);
                    activity.addFragment(episodeFragment);

                } else {
                    activity.requestDeepLink(commanDataList);
                }
            }
        });

        imgMyList.setImageResource(R.drawable.ic_action_add);
        if (!activity.mWatchDataList.isEmpty()) {
            for (WatchlistModelListDatumModel watchlistModelListDatumModel : activity.mWatchDataList) {
                if (watchlistModelListDatumModel.getWatchable().getContentId().equalsIgnoreCase(commanDataList.getContentId())) {
                    imgMyList.setImageResource(R.drawable.ic_action_check);
                    break;
                } else {
                    imgMyList.setImageResource(R.drawable.ic_action_add);
                }
            }
        }

        linearMyList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (activity.checkWatchListExist(commanDataList.getContentId())) {
                    removeFromWishlist(String.valueOf(commanDataList.getWatchListID()));

                } else {
                    addToWishlist(String.valueOf(commanDataList.getId()));
                }
            }
        });

        linearShare.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                activity.shareLink(commanDataList.getTitle(), String.valueOf(commanDataList.getId()), moviePoster);
            }
        });

        linearEpisode.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.SHOW_NAME, commanDataList.getTitle());
                bundle.putString(Constants.SHOW_ID, commanDataList.getShowId());
                bundle.putString(Constants.PROVIDER, commanDataList.getProvider());
                EpisodeFragment episodeFragment = new EpisodeFragment();
                episodeFragment.setArguments(bundle);
                activity.addFragment(episodeFragment);
            }
        });

        linearTrailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.showMessageToUser("Coming soon");
            }
        });
    }

    private void configureExpandView() {
        content_info.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                content_info.getViewTreeObserver().removeOnPreDrawListener(this);

                imgExpand.setVisibility(View.GONE);
                if (content_info.getLineCount() > 5) {
                    content_info.setMaxLines(5);
                    imgExpand.setVisibility(View.VISIBLE);
                }

                return true;
            }
        });

        imgExpand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isExpanded) {
                    imgExpand.setImageResource(R.drawable.ic_action_up);
                    content_info.setMaxLines(Integer.MAX_VALUE);
                    content_info.setEllipsize(null);

                } else {
                    imgExpand.setImageResource(R.drawable.ic_action_down);
                    content_info.setMaxLines(5);
                    content_info.setEllipsize(TextUtils.TruncateAt.END);
                }

                isExpanded = !isExpanded;
            }
        });
    }

    private void Init(View v) {
        adContainer = v.findViewById(R.id.adContainer);
        NativeAds.ShowTextNativeAds((MasterActivity) getActivity(), adContainer);

        moviePoster = v.findViewById(R.id.moviePoster);
        providerImage = v.findViewById(R.id.providerImage);

        content_name = v.findViewById(R.id.content_name);
        content_other = v.findViewById(R.id.content_other);
        content_info = v.findViewById(R.id.content_info);
        content_cast = v.findViewById(R.id.content_cast);
        content_director = v.findViewById(R.id.content_director);
        content_language = v.findViewById(R.id.content_language);
        imgExpand = v.findViewById(R.id.imgExpand);
        imgPlay = v.findViewById(R.id.imgPlay);
        imgMyList = v.findViewById(R.id.imgMyList);
        linearEpisode = v.findViewById(R.id.linearEpisode);
        linearMyList = v.findViewById(R.id.linearMyList);
        linearShare = v.findViewById(R.id.linearShare);
        linearTrailer = v.findViewById(R.id.linearTrailer);
    }

    public void stringDecode(TextView textView, String s) {
        try {
            textView.setText(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBack() {
        if (getActivity().getSupportFragmentManager().getBackStackEntryCount() == 1) {
            activity.addFragment(new HomeFragment());

        } else {
            Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            activity.removeFragment(fragment);
        }
    }

    private void addToWishlist(String contentID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.addToWishlist(SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                SharePreferenceManager.getString(Constants.TOKEN_ACCESS), contentID);

        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final WatchlistModel watchlistModel = gson.fromJson(reader, WatchlistModel.class);

                    if (watchlistModel.getData() != null) {
                        activity.showMessageToUser("Added to My List");
                        imgMyList.setImageResource(R.drawable.ic_action_check);

                        WatchlistModelListDatumModel watchlistModelListDatumModel = watchlistModel.getData().toWatchlistModelListDatumModel();
                        commanDataList.setWatchListID(watchlistModelListDatumModel.getId());
                        HomeAdapter.watchlistAdapter.addItem(watchlistModelListDatumModel);
                        HomeAdapter.watchlistAdapter.notifyDataSetChanged();

                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(error);
                    final WatchlistModel watchlistModel = gson.fromJson(reader, WatchlistModel.class);

                    if (watchlistModel.getData().getError() != null) {
                        activity.showMessageToUser(watchlistModel.getData().getError());
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                activity.showMessageToUser(error);
            }
        });
    }

    private void removeFromWishlist(String contentID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.deleteWatchList(SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                SharePreferenceManager.getString(Constants.TOKEN_ACCESS), contentID);

        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final WatchlistModelList watchlistModelList = gson.fromJson(reader, WatchlistModelList.class);

                    ArrayList<WatchlistModelListDatumModel> arrayList = new ArrayList<>();
                    if (watchlistModelList.getData() != null) {
                        activity.showMessageToUser("Removed from My List");
                        imgMyList.setImageResource(R.drawable.ic_action_add);

                        for (WatchlistModelListDatumModel watchlistModelListDatumModel : watchlistModelList.getData()) {
                            arrayList.add(watchlistModelListDatumModel.toWatchlistModelListDatumModel());
                        }

                        HomeAdapter.watchlistAdapter.updateData(arrayList);
                        HomeAdapter.watchlistAdapter.notifyDataSetChanged();
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(error);
                    final WatchlistModel watchlistModel = gson.fromJson(reader, WatchlistModel.class);

                    if (watchlistModel.getData().getError() != null) {
                        activity.showMessageToUser(watchlistModel.getData().getError());
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                activity.showMessageToUser(error);
            }
        });
    }
}

