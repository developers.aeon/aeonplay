package in.aeonplay.fragment;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.activity.MasterActivity;
import in.aeonplay.comman.InterstitialAds;


public class WebFragment extends BaseFragment {

    private WebView webNews;
    private Bundle bundle;
    private String webLink;
    private ImageView web_loading;
    private RelativeLayout rootContainer;

    public static WebFragment newInstance(String url) {
        WebFragment webFragment = new WebFragment();

        Bundle args = new Bundle();
        args.putString("DATA", url);
        webFragment.setArguments(args);
        return webFragment;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_web, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        InterstitialAds.ShowInterstitialAds((MasterActivity) getActivity());

        bundle = getArguments();

        web_loading = (ImageView) v.findViewById(R.id.web_loading);
        webNews = (WebView) v.findViewById(R.id.webView);
        webNews.setVerticalScrollBarEnabled(true);
        webNews.setHorizontalScrollBarEnabled(true);

        rootContainer = v.findViewById(R.id.rootContainer);

        if (bundle != null) {
            webLink = bundle.getString("DATA");

            if (webLink.equalsIgnoreCase("https://6466.play.gamezop.com/") ||
                    webLink.equalsIgnoreCase("https://6467.play.quizzop.com/")) {
                ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) rootContainer.getLayoutParams();
                layoutParams.topMargin = activity.getToolbarHeight();
                rootContainer.setLayoutParams(layoutParams);
            }
        }

        if (webLink.length() > 0) {
            webNews.getSettings().setJavaScriptEnabled(true);
            webNews.getSettings().setAllowFileAccess(true);
            webNews.getSettings().setDisplayZoomControls(false);

            webNews.getSettings().setPluginState(WebSettings.PluginState.ON);
            WebSettings webSettings = webNews.getSettings();
            webSettings.setBuiltInZoomControls(true);

            webNews.setVerticalScrollBarEnabled(true);
            webNews.setHorizontalScrollBarEnabled(true);

            webNews.getSettings().setLoadWithOverviewMode(true);
            webNews.getSettings().setUseWideViewPort(true);
            webNews.getSettings().setDomStorageEnabled(true);
            webNews.getSettings().setLoadsImagesAutomatically(true);
            webNews.getSettings().setPluginState(WebSettings.PluginState.ON);
            webNews.getSettings().setAllowContentAccess(true);

            webNews.setWebViewClient(new Callback());  //HERE IS THE MAIN CHANGE
            webNews.loadUrl(webLink);
        }
    }

    private class Callback extends WebViewClient {  //HERE IS THE MAIN CHANGE.

        public Callback() {
            web_loading.setVisibility(View.VISIBLE);
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            web_loading.setVisibility(View.GONE);
        }

//        @Override
//        public void onReceivedHttpAuthRequest(WebView view,
//                                              HttpAuthHandler handler, String host, String realm) {
//            handler.proceed("tfsuser", "QZk3wUxSpHBrr9HNVg");
//        }
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        activity.removeFragment(fragment);

        if (webNews != null)
            webNews.canGoBack();
    }

}