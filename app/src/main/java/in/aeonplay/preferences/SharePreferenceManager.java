package in.aeonplay.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

import java.util.Map;

import in.aeonplay.MyApplication;
import in.aeonplay.comman.Constants;
import in.aeonplay.model.Profile.ProfileDataModel;

public class SharePreferenceManager {
    private static final SharePreferenceManager INSTANCE = new SharePreferenceManager();
    private static final String APPINFO = "app_info";
    private static final String OLD_PREF = "aeonPlay";
    private final SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;

    public SharePreferenceManager() {
        mSharedPreferences = MyApplication.getInstance().getSharedPreferences(APPINFO, Context.MODE_PRIVATE);
    }

    public static void copyPref(){
        SharedPreferences old = MyApplication.getInstance().getSharedPreferences("aeonPlay", Context.MODE_PRIVATE);
        for (Map.Entry<String, ?> entry : old.getAll().entrySet()) {
            Object v = entry.getValue();
            String key = entry.getKey();
            SharePreferenceManager.save(key, v);
        }
    }

    public static SharePreferenceManager getInstance() {
        return INSTANCE;
    }

    public static void save(String title, Object obj) {
        SharedPreferences.Editor editor = SharePreferenceManager.getInstance().getEditor();
        if (obj instanceof String)
            editor.putString(title, (String) obj);
        else if (obj instanceof Boolean)
            editor.putBoolean(title, (Boolean) obj);
        else if (obj instanceof Float)
            editor.putFloat(title, (Float) obj);
        else if (obj instanceof Integer)
            editor.putInt(title, (Integer) obj);
        else if (obj instanceof Long)
            editor.putLong(title, (Long) obj);

        editor.apply();
        editor.commit();
    }

    public static String getString(String title) {
        return getPreferences().getString(title, "");
    }

    public static boolean getBoolean(String title) {
        return getPreferences().getBoolean(title, false);
    }

    public static float getFloat(String title) {
        return getPreferences().getFloat(title, 0);
    }

    public static int getInt(String title) {
        return getPreferences().getInt(title, 0);
    }

    public static long getLong(String title) {
        return getPreferences().getLong(title, 0l);
    }

    private static void clear() {
        SharedPreferences.Editor editor = SharePreferenceManager.getInstance().getEditor();
        editor.clear();
        editor.apply();
        editor.commit();
    }

    private static SharedPreferences getPreferences() {
        return SharePreferenceManager.getInstance().getPreference();
    }

    public static void clearDB() {
        SharedPreferences.Editor dbEditor = SharePreferenceManager.INSTANCE.getEditor();
        dbEditor.clear();
        dbEditor.commit();

        clear();
    }

    public static ProfileDataModel getUserData() {
        SharedPreferences prefs = SharePreferenceManager.getPreferences();

        ProfileDataModel user = new ProfileDataModel();
        user.setId(prefs.getInt("id", 0));
        user.setFirstName(prefs.getString("first_name", ""));
        user.setMiddleName(prefs.getString("middle_name", ""));
        user.setLastName(prefs.getString("last_name", ""));
        user.setMobileNo(prefs.getString("mobile_no", ""));
        user.setEmail(prefs.getString("email", ""));
        user.setAvatar(prefs.getString("avatar", ""));
        user.setDob(prefs.getString("dob", ""));
        user.setGender(prefs.getString("gender", ""));
        user.setProvider(prefs.getString("provider", ""));
        user.setProviderId(prefs.getString("provider_id", ""));
        user.setProviderAccessToken(prefs.getString("provider_access_token", ""));
        user.setEmailVerifiedAt(prefs.getString("email_verified_at", ""));
        user.setMobileVerifiedAt(prefs.getString("mobile_verified_at", ""));
        user.setStatus(prefs.getInt("status", 0));
        user.setCreatedAt(prefs.getString("created_at", ""));
        user.setUpdatedAt(prefs.getString("updated_at", ""));
        user.setMobileNetworkOperator(prefs.getString("mobile_network_operator", ""));
        user.setMnc(prefs.getString("mnc", ""));
        user.setMcc(prefs.getString("mcc", ""));
        user.setMobileNumberRegisteredLocation(prefs.getString("mobile_number_registered_location", ""));
        return user;
    }

    public static void setUserData(ProfileDataModel user) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor dbEditor = SharePreferenceManager.getInstance().getEditor();

        dbEditor.putInt("id", user.getId());
        dbEditor.putString("first_name", user.getFirstName());
        dbEditor.putString("middle_name", user.getMiddleName());
        dbEditor.putString("last_name", user.getLastName());
        dbEditor.putString("mobile_no", user.getMobileNo());
        dbEditor.putString("email", user.getEmail());
        dbEditor.putString("avatar", user.getAvatar());
        dbEditor.putString("dob", user.getDob());
        dbEditor.putString("gender", user.getGender());
        dbEditor.putString("provider", user.getProvider());
        dbEditor.putString("provider_id", user.getProviderId());
        dbEditor.putString("provider_access_token", user.getProviderAccessToken());
        dbEditor.putString("email_verified_at", user.getEmailVerifiedAt());
        dbEditor.putString("mobile_verified_at", user.getMobileVerifiedAt());
        dbEditor.putInt("status", user.getStatus());
        dbEditor.putString("created_at", user.getCreatedAt());
        dbEditor.putString("updated_at", user.getUpdatedAt());
        dbEditor.putString("mobile_network_operator", user.getMobileNetworkOperator());
        dbEditor.putString("mnc", user.getMnc());
        dbEditor.putString("mcc", user.getMcc());
        dbEditor.putString("mobile_number_registered_location", user.getMobileNumberRegisteredLocation());
        dbEditor.commit();
    }

    public static void saveVideoTrack(Context mContext, String key, String value) {
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public static String getVideoTrack(Context mContext) {
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(mContext);
        return sp.getString(Constants.KEY_VIDEO, null);
    }

    public static void saveAudioTrack(Context mContext, String key, String value) {
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public static String getAudioTrack(Context mContext) {
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(mContext);
        return sp.getString(Constants.KEY_AUDIO, null);
    }

    public static void saveSubTitleTrack(Context mContext, String key, String value) {
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public static String getSubTitleTrack(Context mContext) {
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(mContext);
        return sp.getString(Constants.KEY_SUBTITLE, null);
    }

    public SharedPreferences getPreference() {
        return mSharedPreferences;
    }

    public SharedPreferences.Editor getEditor() {
        if (mEditor == null) {
            mEditor = mSharedPreferences.edit();
        }
        return mEditor;
    }
}
