package in.aeonplay.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import in.aeonplay.model.Comman.CommanDataList;

public class WatchlistPreferencesManager {

    public static final String PREFS_NAME = "watchlist_info";
    public static final String WATCHLIST = "WATCHLIST";

    public WatchlistPreferencesManager() {
        super();
    }

    public static void clearRecentALL(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor dbEditor = prefs.edit();
        dbEditor.clear();
        dbEditor.commit();
    }

    public void saveRecents(Context context, List<CommanDataList> favorites) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);
        editor.putString(WATCHLIST, jsonFavorites);

        editor.commit();
    }

    public void addRecent(Context context, CommanDataList product, int index) {
        List<CommanDataList> favorites = getWatchlist(context);
        if (favorites == null)
            favorites = new ArrayList<CommanDataList>();

        favorites.add(index, product);
        saveRecents(context, favorites);
    }

    public void removeRecent(Context context, CommanDataList product) {
        ArrayList<CommanDataList> recents = getWatchlist(context);
        if (recents != null) {
            recents.remove(product);
            saveRecents(context, recents);
        }
    }

    public ArrayList<CommanDataList> getWatchlist(Context context) {
        SharedPreferences settings;
        List<CommanDataList> recents;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(WATCHLIST)) {
            String jsonFavorites = settings.getString(WATCHLIST, null);
            Gson gson = new Gson();
            CommanDataList[] recentItems = gson.fromJson(jsonFavorites,
                    CommanDataList[].class);

            recents = Arrays.asList(recentItems);
            recents = new ArrayList<CommanDataList>(recents);
        } else
            return null;

        return (ArrayList<CommanDataList>) recents;
    }

    public boolean checkRecentItem(CommanDataList checkProduct, Context context) {
        boolean check = false;
        List<CommanDataList> favorites = getWatchlist(context);
        if (favorites != null) {
            for (CommanDataList product : favorites) {
                if (product.getWatchListID() == checkProduct.getWatchListID()) {
                    check = true;
                    break;
                }
            }
        }
        return check;
    }
}